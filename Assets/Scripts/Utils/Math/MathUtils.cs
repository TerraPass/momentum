using UnityEngine;
using System;

namespace Momentum.Utils.Math
{
	public static class MathUtils
	{
		public static int TrimToRange(int value, int min, int max = int.MaxValue)
		{
			return Mathf.Max(Mathf.Min(value, max), min);
		}
		
		public static float TrimToRange(float value, float min, float max = float.PositiveInfinity)
		{
			return Mathf.Max(Mathf.Min(value, max), min);
		}
	}
}

