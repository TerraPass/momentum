using System;

namespace Momentum.Utils.Time
{
	public interface IScalableTimer : ITimer
	{
		float Scale {get;set;}
	}
}

