using UnityEngine;
using System;

namespace Momentum.Utils.Time
{
	public interface ITimeIntervalFormatter
	{
		string Format(float seconds);
	}
}

