using System;

namespace Momentum.Utils.Time
{
	public interface IResettableTimer : ITimer
	{
		void Reset(bool startPaused = true);
	}	
}

