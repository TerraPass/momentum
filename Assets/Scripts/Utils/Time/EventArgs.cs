using UnityEngine;
using System;

namespace Momentum.Utils.Time
{
	public class AlarmFiredEventArgs : EventArgs
	{
		private readonly IAlarmClock alarmClock;

		public AlarmFiredEventArgs(IAlarmClock alarmClock)
		{
			this.alarmClock = alarmClock;
		}

		public IAlarmClock AlarmClock
		{
			get {
				return this.alarmClock;
			}
		}
	}
}

