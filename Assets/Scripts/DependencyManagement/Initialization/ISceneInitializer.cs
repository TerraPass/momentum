using UnityEngine;
using System;

using Momentum.Main;

namespace Momentum.DependencyManagement.Initialization
{
	/// <summary>
	/// This interface describes objects in charge of initializing 
	/// the scene once it has been completely loaded.
	/// Initialization involves performing dependency injection
	/// on containers this initializer is aware of.
	/// </summary>
	public interface ISceneInitializer
	{
		event EventHandler<SceneInitializedEventArgs> SceneInitialized;
		event EventHandler<SceneDeinitializedEventArgs> SceneDeinitialized;

		/// <summary>
		/// Performs injection on underlying containers and
		/// other operations needed to make the scene operational.
		/// </summary>
		/// <param name="gameApplication">Game application.</param>
		void InitScene(IGameApplication gameApplication);

		/// <summary>
		/// Performs the necessary finalization before the scene can be unloaded.
		/// </summary>
		/// <param name="gameApplication">Game application.</param>
		void DeinitScene(IGameApplication gameApplication);
	}
}

