using System;

namespace Momentum.DependencyManagement.Initialization
{
	public abstract class SceneInitializationEventArgs : EventArgs
	{
		private readonly ISceneInitializer sceneInitializer;

		public SceneInitializationEventArgs(ISceneInitializer sceneInitializer)
		{
			if(sceneInitializer == null)
			{
				throw new ArgumentNullException("sceneInitializer");
			}
			this.sceneInitializer = sceneInitializer;
		}

		public ISceneInitializer SceneInitializer
		{
			get {
				return this.sceneInitializer;
			}
		}
	}

	public class SceneInitializedEventArgs : SceneInitializationEventArgs
	{
		public SceneInitializedEventArgs(ISceneInitializer sceneInitializer)
			: base(sceneInitializer)
		{

		}
	}

	public class SceneDeinitializedEventArgs : SceneInitializationEventArgs
	{
		public SceneDeinitializedEventArgs(ISceneInitializer sceneInitializer)
			: base(sceneInitializer)
		{
			
		}
	}
}

