using UnityEngine;
using System;

using Momentum.Main;
using Momentum.UI.MainMenu;
using Momentum.Common.Commands;

namespace Momentum.DependencyManagement.Initialization
{
	public class MainMenuInitializer : AbstractSceneInitializerComponent
	{
		[SerializeField]
		private AbstractMainMenuUIDependencyContainerComponent uiDependencies;

		public override void InitSceneImpl(IGameApplication gameApplication)
		{
			this.uiDependencies.PerformInjection(
				new SimpleCommand(() => gameApplication.QuitToDesktop()),
				(levelId) => gameApplication.LoadLevel(levelId),
				gameApplication.LevelLoader.AvailableLevels,
				gameApplication.GlobalDependencies.MouseCursor
			);
		}

		public override void DeinitSceneImpl(IGameApplication gameApplication)
		{

		}
	}
}

