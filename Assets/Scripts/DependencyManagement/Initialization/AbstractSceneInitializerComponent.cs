using UnityEngine;
using System;

using Momentum.Main;

namespace Momentum.DependencyManagement.Initialization
{
	public abstract class AbstractSceneInitializerComponent : MonoBehaviour, ISceneInitializer
	{
		public event EventHandler<SceneInitializedEventArgs> SceneInitialized = delegate {};
		public event EventHandler<SceneDeinitializedEventArgs> SceneDeinitialized = delegate {};

		public void InitScene(IGameApplication gameApplication)
		{
			this.InitSceneImpl(gameApplication);
			this.SceneInitialized(
				this,
				new SceneInitializedEventArgs(this)
			);
		}

		public void DeinitScene(IGameApplication gameApplication)
		{
			this.DeinitSceneImpl(gameApplication);
			this.SceneDeinitialized(
				this,
				new SceneDeinitializedEventArgs(this)
			);
		}

		public abstract void InitSceneImpl(IGameApplication gameApplication);
		public abstract void DeinitSceneImpl(IGameApplication gameApplication);
	}
}

