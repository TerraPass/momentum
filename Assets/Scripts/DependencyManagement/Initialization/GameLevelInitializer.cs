using UnityEngine;
using System;
using System.Diagnostics;
using Terrapass.Debug;
using Terrapass.Extensions.Unity;

using Momentum.Main;
using Momentum.DependencyManagement.Facades;
using Momentum.Level.Metadata;
using Momentum.Common.Commands;
using Momentum.Gameplay;
using Momentum.Cameras.InGame;
using Momentum.UI.InGame;

namespace Momentum.DependencyManagement.Initialization
{
	using Facades;

	public class GameLevelInitializer : AbstractSceneInitializerComponent
	{
		[SerializeField]
		AbstractGameLevelMetadataComponent gameLevelMetadata;

		[SerializeField]
		AbstractGameplayDependencyContainerComponent gameplayDependencies;
		[SerializeField]
		AbstractCameraDependencyContainerComponent cameraDependencies;
		[SerializeField]
		AbstractUIDependencyContainerComponent uiDependencies;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();
		}

		public override void InitSceneImpl(IGameApplication gameApplication)
		{
			this.gameplayDependencies.PerformInjection();
			this.cameraDependencies.PerformInjection(
				this.gameplayDependencies.Player,
				this.gameplayDependencies.GameLevelEventSystem
			);
			this.uiDependencies.PerformInjection(
				this.gameplayDependencies.TimeMechanism,
				new GameplayEvents(
					this.gameplayDependencies.GameLevelEventSystem,
					this.gameplayDependencies.TimeMechanism
				),
				ImmutableEndOfLevelCommands.FromDependencies(
					gameApplication,
					this.gameplayDependencies.GameLevelReferee,
					this.gameLevelMetadata.NextLevelId
				),
				new PauseMenuCommandsDictionaryBuilder(
					gameApplication,
					this.gameplayDependencies.GameLevelReferee
				).Build(),
				gameApplication.GlobalDependencies.MouseCursor
			);

			this.gameplayDependencies.GameLevelReferee.RestartLevel();
		}

		public override void DeinitSceneImpl(IGameApplication gameApplication)
		{

		}
	}
}
