using System;

using Momentum.DependencyManagement.Initialization;
using Momentum.DependencyManagement.Facades;

namespace Momentum.DependencyManagement.Loading
{
	public interface ILoadingStatusMonitor
	{
		event EventHandler<LoadingStartingEventArgs> LoadingStarting;
		event EventHandler<LoadingStartedEventArgs> LoadingStarted;
		event EventHandler<LoadingCompletedEventArgs> LoadingCompleted;

		float LoadingProgress {get;}

		ISceneInitializer CurrentSceneInitializer {get;set;}
	}
}

