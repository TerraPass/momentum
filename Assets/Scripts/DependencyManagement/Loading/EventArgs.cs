using UnityEngine;
using System;

namespace Momentum.DependencyManagement.Loading
{
	public abstract class LoadingStatusMonitorEventArgs : EventArgs
	{
		private readonly ILoadingStatusMonitor monitor;

		public LoadingStatusMonitorEventArgs(ILoadingStatusMonitor monitor)
		{
			if(monitor == null)
			{
				throw new ArgumentNullException("monitor");
			}
			this.monitor = monitor;
		}

		public ILoadingStatusMonitor Monitor
		{
			get {
				return this.monitor;
			}
		}
	}

	public class LoadingStartingEventArgs : LoadingStatusMonitorEventArgs
	{
		public LoadingStartingEventArgs(ILoadingStatusMonitor monitor)
			: base(monitor)
		{
			
		}
	}

	public class LoadingStartedEventArgs : LoadingStatusMonitorEventArgs
	{
		public LoadingStartedEventArgs(ILoadingStatusMonitor monitor)
			: base(monitor)
		{
			
		}
	}

	public class LoadingCompletedEventArgs : LoadingStatusMonitorEventArgs
	{
		public LoadingCompletedEventArgs(ILoadingStatusMonitor monitor)
			: base(monitor)
		{
			
		}
	}
}

