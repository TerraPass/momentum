using UnityEngine;
using System;

using Momentum.Main.SceneTransition;
using Momentum.DependencyManagement.Initialization;

namespace Momentum.DependencyManagement.Loading
{
	public class LoadingStatusMonitor : ILoadingStatusMonitor
	{
		private const float SCENE_LOADING_PORTION = 0.99f;

		private readonly ISceneLoader sceneLoader;

		private ISceneInitializer sceneInitializer;

		public LoadingStatusMonitor(
			ISceneLoader sceneLoader
		)
		{
			if(sceneLoader == null)
			{
				throw new ArgumentNullException("sceneLoader");
			}
			this.sceneLoader = sceneLoader;
			this.sceneLoader.SceneLoadingStarting += this.OnSceneLoadingStarting;
			this.sceneLoader.SceneLoadingStarted += this.OnSceneLoadingStarted;
		}

		#region ILoadingStatusMonitor implementation

		public event EventHandler<LoadingStartingEventArgs> LoadingStarting = delegate{};
		public event EventHandler<LoadingStartedEventArgs> LoadingStarted = delegate{};
		public event EventHandler<LoadingCompletedEventArgs> LoadingCompleted = delegate{};

		// TODO: Make LoadingProgress cover scene initialization as well.
		public float LoadingProgress
		{
			get {
				return this.sceneLoader.LoadingProgress * SCENE_LOADING_PORTION;
			}
		}

		public ISceneInitializer CurrentSceneInitializer
		{
			get {
				return this.sceneInitializer;
			}
			set {
				if(this.sceneInitializer != null)
				{
					this.sceneInitializer.SceneInitialized -= this.OnSceneInitialized;
				}
				this.sceneInitializer = value;
				if(this.sceneInitializer != null)
				{
					this.sceneInitializer.SceneInitialized += this.OnSceneInitialized;
				}
			}
		}

		#endregion

		private void OnSceneLoadingStarting(object sender, SceneLoadingStartingEventArgs args)
		{
			this.LoadingStarting(
				this,
				new LoadingStartingEventArgs(this)
			);
		}

		private void OnSceneLoadingStarted(object sender, SceneLoadingStartedEventArgs args)
		{
			this.LoadingStarted(
				this,
				new LoadingStartedEventArgs(this)
			);
		}

		private void OnSceneInitialized(object sender, SceneInitializedEventArgs args)
		{
			this.LoadingCompleted(
				this,
				new LoadingCompletedEventArgs(this)
			);
		}
	}
}

