using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

using Momentum.Common.DependencyManagement;
using Momentum.Configuration;
using Momentum.Main.SceneTransition;
using Momentum.UI.Common;
using Momentum.UI.Common.MouseCursor;
using Momentum.UI.LoadingScreen;
using Momentum.DependencyManagement.Facades;
using Momentum.DependencyManagement.Loading;

namespace Momentum.DependencyManagement.Global
{
	public class GlobalDependencyContainer : AbstractGlobalDependencyContainerComponent
	{
		[SerializeField]
		private AbstractLoadingScreenComponent loadingScreen;

		private ISceneLoader sceneLoader;

		private GlobalDependencies globalDependencies;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			DontDestroyOnLoad(this.gameObject);
		}

		#region implemented abstract members of AbstractGlobalDependencyContainerComponent

		public override void PerformInjection()
		{
			this.EnsureRequiredDependenciesAreProvided();

			this.globalDependencies = new GlobalDependencies();

			this.globalDependencies.Configuration = HardcodedConfiguration.Instance;
			this.globalDependencies.LoadingStatusMonitor = new LoadingStatusMonitor(this.SceneLoader);
			this.globalDependencies.MouseCursor = UnityMouseCursor.Instance;

			// Setup loading screen to use loading status monitor
			this.loadingScreen.LoadingStatusMonitor = this.globalDependencies.LoadingStatusMonitor;

			// Setup mouse cursor to be automatically hidden during loading screens
			// and hide it for now.
			new AutoHidingOnLoadingStartedHideable(
				this.globalDependencies.MouseCursor,
				this.globalDependencies.LoadingStatusMonitor
			).Hide();
		}

		[Dependency]
		public override ISceneLoader SceneLoader
		{
			get {
				return this.sceneLoader;
			}
			set {
				this.sceneLoader = value;
			}
		}

		public override IGlobalDependencies GlobalDependencies
		{
			get {
				if(this.globalDependencies == null)
				{
					throw new InvalidOperationException(
						"Attemped to access GlobalDependencies before PerformInjection() has been called"
					);
				}
				return this.globalDependencies;
			}
		}

		#endregion
	}
}

