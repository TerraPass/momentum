using System;

using Momentum.Common.DependencyManagement;
using Momentum.Main.SceneTransition;
using Momentum.UI.LoadingScreen;
using Momentum.DependencyManagement.Facades;

namespace Momentum.DependencyManagement.Global
{
	public interface IGlobalDependencyContainer : IDependencyContainer
	{
		// Input
		ISceneLoader SceneLoader {get;set;}

		// Output
		IGlobalDependencies GlobalDependencies {get;}
	}

	public static class IGlobalDependencyContainerExtensions
	{
		public static void PerformInjection(
			this IGlobalDependencyContainer container,
			ISceneLoader sceneLoader
		)
		{
			container.SceneLoader = sceneLoader;
			container.PerformInjection();
		}
	}
}