using UnityEngine;
using System;

using Momentum.Main.SceneTransition;
using Momentum.UI.LoadingScreen;
using Momentum.DependencyManagement.Loading;
using Momentum.DependencyManagement.Facades;

namespace Momentum.DependencyManagement.Global
{
	public abstract class AbstractGlobalDependencyContainerComponent : MonoBehaviour, IGlobalDependencyContainer
	{
		#region IDependencyContainer implementation

		public abstract void PerformInjection();

		#endregion

		#region IGlobalDependencyContainer implementation

		public abstract ISceneLoader SceneLoader {get;set;}

		public abstract IGlobalDependencies GlobalDependencies {get;}

		#endregion


	}
}

