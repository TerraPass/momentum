using System;
using System.Collections.Generic;

using Momentum.Common.Commands;
using Momentum.Gameplay.Core;
using Momentum.Main;
using Momentum.UI.InGame.Pause;

namespace Momentum.DependencyManagement.Facades
{
	public interface IMenuCommandsDictionaryBuilder<TOptionEnum>
		where TOptionEnum : struct
	{
		IDictionary<TOptionEnum, ICommand> Build();
	}
}

