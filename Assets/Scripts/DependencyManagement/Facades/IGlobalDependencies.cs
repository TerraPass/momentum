using System;

using Momentum.Configuration;
using Momentum.DependencyManagement.Loading;
using Momentum.UI.Common.MouseCursor;

namespace Momentum.DependencyManagement.Facades
{
	public interface IGlobalDependencies
	{
		IConfiguration Configuration {get;}
		ILoadingStatusMonitor LoadingStatusMonitor {get;}
		IMouseCursor MouseCursor {get;}
	}
}

