using System;

using Momentum.Gameplay.Core;
using Momentum.Gameplay.TimeMechanic;

namespace Momentum.DependencyManagement.Facades
{
	public class GameplayEvents : IGameplayEvents
	{
		private readonly IGameLevelEventSystem gameLevelEventSystem;
		private readonly ITimeMechanism timeMechanism;

		#region IGameplayEvents implementation

		public event EventHandler<RewindStartedEventArgs> RewindStarted
		{
			add {
				this.timeMechanism.RewindStarted += value;
			}
			remove {
				this.timeMechanism.RewindStarted -= value;
			}
		}

		public event EventHandler<RewindPausedEventArgs> RewindPaused
		{
			add {
				this.timeMechanism.RewindPaused += value;
			}
			remove {
				this.timeMechanism.RewindPaused -= value;
			}
		}

		public event EventHandler<RewindEndedEventArgs> RewindEnded
		{
			add {
				this.timeMechanism.RewindEnded += value;
			}
			remove {
				this.timeMechanism.RewindEnded -= value;
			}
		}

		public event EventHandler<TimeFlowPausedEventArgs> TimeFlowPaused
		{
			add {
				this.timeMechanism.TimeFlowPaused += value;
			}
			remove {
				this.timeMechanism.TimeFlowPaused -= value;
			}
		}

		public event EventHandler<TimeFlowResumedEventArgs> TimeFlowResumed
		{
			add {
				this.timeMechanism.TimeFlowResumed += value;
			}
			remove {
				this.timeMechanism.TimeFlowResumed -= value;
			}
		}

		public event EventHandler<LevelStartingEventArgs> LevelStarting
		{
			add {
				this.gameLevelEventSystem.LevelStarting += value;
			}
			remove {
				this.gameLevelEventSystem.LevelStarting -= value;
			}
		}

		public event EventHandler<LevelStartedEventArgs> LevelStarted
		{
			add {
				this.gameLevelEventSystem.LevelStarted += value;
			}
			remove {
				this.gameLevelEventSystem.LevelStarted -= value;
			}
		}

		public event EventHandler<LevelCompletedEventArgs> LevelCompleted
		{
			add {
				this.gameLevelEventSystem.LevelCompleted += value;
			}
			remove {
				this.gameLevelEventSystem.LevelCompleted -= value;
			}
		}

		public event EventHandler<PseudoGameoverOccurredEventArgs> PseudoGameoverOccurred
		{
			add {
				this.gameLevelEventSystem.PseudoGameoverOccurred += value;
			}
			remove {
				this.gameLevelEventSystem.PseudoGameoverOccurred -= value;
			}
		}

		public event EventHandler<LevelPausedEventArgs> LevelPaused
		{
			add {
				this.gameLevelEventSystem.LevelPaused += value;
			}
			remove {
				this.gameLevelEventSystem.LevelPaused -= value;
			}
		}

		public event EventHandler<LevelResumedEventArgs> LevelResumed
		{
			add {
				this.gameLevelEventSystem.LevelResumed += value;
			}
			remove {
				this.gameLevelEventSystem.LevelResumed -= value;
			}
		}

		#endregion

		public GameplayEvents(
			IGameLevelEventSystem gameLevelEventSystem,
			ITimeMechanism timeMechanism
		)
		{
			if(gameLevelEventSystem == null)
			{
				throw new ArgumentNullException("gameLevelEventSystem");
			}
			if(timeMechanism == null)
			{
				throw new ArgumentNullException("timeMechanism");
			}
			this.gameLevelEventSystem = gameLevelEventSystem;
			this.timeMechanism = timeMechanism;
		}
	}
}

