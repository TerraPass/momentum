using System;

using Momentum.Configuration;
using Momentum.DependencyManagement.Loading;
using Momentum.UI.Common.MouseCursor;

namespace Momentum.DependencyManagement.Facades
{
	public class GlobalDependencies : IGlobalDependencies
	{
		private IConfiguration configuration;
		private ILoadingStatusMonitor loadingStatusMonitor;
		private IMouseCursor mouseCursor;

		public GlobalDependencies(
			IConfiguration configuration = null,
			ILoadingStatusMonitor loadingStatusMonitor = null,
			IMouseCursor mouseCursor = null
		)
		{
			this.configuration = configuration;
			this.loadingStatusMonitor = loadingStatusMonitor;
			this.mouseCursor = mouseCursor;
		}

		#region IGlobalDependencies implementation

		public IConfiguration Configuration
		{
			get {
				return this.configuration;
			}
			set {
				this.configuration = value;
			}
		}

		public ILoadingStatusMonitor LoadingStatusMonitor
		{
			get {
				return this.loadingStatusMonitor;
			}
			set {
				this.loadingStatusMonitor = value;
			}
		}

		public IMouseCursor MouseCursor
		{
			get {
				return this.mouseCursor;
			}
			set {
				this.mouseCursor = value;
			}
		}

		#endregion


	}
}

