using System;

using Momentum.Common.Commands;
using Momentum.Main;
using Momentum.Gameplay.Core;
using Momentum.Level.Metadata;

namespace Momentum.DependencyManagement.Facades
{
	public class ImmutableEndOfLevelCommands : IEndOfLevelCommands
	{
		private readonly ICommand quitCommand;
		private readonly ICommand restartCommand;
		private readonly ICommand nextLevelCommand;

		public static ImmutableEndOfLevelCommands FromDependencies(
			IGameApplication gameApplication,
			IGameLevelReferee gameLevelReferee,
			ILevelId nextLevelId
		)
		{
			return new ImmutableEndOfLevelCommands(
				new SimpleCommand(() => gameApplication.QuitToMainMenu()),
				new SimpleCommand(() => gameLevelReferee.RestartLevel()),
				nextLevelId != null
					? new SimpleCommand(
						() => gameApplication.LoadLevel(nextLevelId)
					)
					: null
			);
		}

		public ImmutableEndOfLevelCommands(
			ICommand quitCommand,
			ICommand restartCommand,
			ICommand nextLevelCommand
		)
		{
			if(quitCommand == null)
			{
				throw new ArgumentNullException("quitCommand");
			}
			if(restartCommand == null)
			{
				throw new ArgumentNullException("restartCommand");
			}
			this.quitCommand = quitCommand;
			this.restartCommand = restartCommand;

			this.nextLevelCommand = nextLevelCommand;	// May be null
		}

		#region IEndOfLevelCommands implementation

		public ICommand QuitCommand
		{
			get {
				return this.quitCommand;
			}
		}

		public ICommand RestartCommand
		{
			get {
				return this.restartCommand;
			}
		}

		public ICommand NextLevelCommand
		{
			get {
				return this.nextLevelCommand;
			}
		}

		#endregion
	}
}

