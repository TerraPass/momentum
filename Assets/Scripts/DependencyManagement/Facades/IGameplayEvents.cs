using System;

using Momentum.Gameplay.Core;
using Momentum.Gameplay.TimeMechanic;

namespace Momentum.DependencyManagement.Facades
{
	/// <summary>
	/// Facade to be used by UI and other clients,
	/// which provides an accessible interface to all gameplay events,
	/// regardless of whether they originate from time mechanism or game level event system.
	/// </summary>
	public interface IGameplayEvents
	{
		// ITimeMechanism events
		/// <summary>
		/// Occurs whenever ITimeMechanism starts rewinding.
		/// </summary>
		event EventHandler<RewindStartedEventArgs> RewindStarted;
		/// <summary>
		/// Occurs whenever ITimeMechanism pauses rewinding without resuming normal flow of time.
		/// (Possibly due to reaching the beginning of recorded time.)
		/// </summary>
		event EventHandler<RewindPausedEventArgs> RewindPaused;
		/// <summary>
		/// Occurs whenever ITimeMechanism ends rewinding and resumes normal flow of time.
		/// </summary>
		event EventHandler<RewindEndedEventArgs> RewindEnded;
		/// <summary>
		/// Occurs whenever ITimeMechanism pauses the flow of time, except when RewindPaused occurs.
		/// (This event is raised when an effective call to Pause() is made.)
		/// </summary>
		event EventHandler<TimeFlowPausedEventArgs> TimeFlowPaused;
		/// <summary>
		/// Occurs whenever ITimeMechanism resumes the flow of time, except when RewindEnded occurs.
		/// (This event is raised when an effective call to Resume() is made.)
		/// </summary>
		event EventHandler<TimeFlowResumedEventArgs> TimeFlowResumed;

		// IGameLevelEventSystem events
		event EventHandler<LevelStartingEventArgs> LevelStarting;
		event EventHandler<LevelStartedEventArgs> LevelStarted;
		event EventHandler<LevelCompletedEventArgs> LevelCompleted;
		event EventHandler<PseudoGameoverOccurredEventArgs> PseudoGameoverOccurred;
		event EventHandler<LevelPausedEventArgs> LevelPaused;
		event EventHandler<LevelResumedEventArgs> LevelResumed;
	}
}

