using System;

using Momentum.Common.Commands;

namespace Momentum.DependencyManagement.Facades
{
	public interface IEndOfLevelCommands
	{
		ICommand QuitCommand {get;}
		ICommand RestartCommand {get;}
		ICommand NextLevelCommand {get;}
	}
}

