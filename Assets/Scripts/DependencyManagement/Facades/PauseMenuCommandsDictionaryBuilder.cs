using System;
using System.Collections.Generic;

using Momentum.Common.Commands;
using Momentum.Gameplay.Core;
using Momentum.Main;
using Momentum.UI.InGame.Pause;

namespace Momentum.DependencyManagement.Facades
{
	public class PauseMenuCommandsDictionaryBuilder : IMenuCommandsDictionaryBuilder<PauseMenuOption>
	{
		private readonly IGameApplication application;
		private readonly IGameLevelReferee gameLevelReferee;

		public PauseMenuCommandsDictionaryBuilder(
			IGameApplication application,
			IGameLevelReferee gameLevelReferee
		)
		{
			if(application == null)
			{
				throw new ArgumentNullException("application");
			}
			if(gameLevelReferee == null)
			{
				throw new ArgumentNullException("gameLevelReferee");
			}
			this.application = application;
			this.gameLevelReferee = gameLevelReferee;
		}

		public IDictionary<PauseMenuOption, ICommand> Build ()
		{
			IDictionary<PauseMenuOption, ICommand> commands = new Dictionary<PauseMenuOption, ICommand>();
			commands[PauseMenuOption.RESUME] = new SimpleCommand(() => this.gameLevelReferee.ResumeLevel());
			commands[PauseMenuOption.RESTART] = new SimpleCommand(() => this.gameLevelReferee.RestartLevel());
			commands[PauseMenuOption.MAIN_MENU] = new SimpleCommand(() => this.application.QuitToMainMenu());
			// TODO: Add commands for other options.
			commands[PauseMenuOption.QUIT] = new SimpleCommand(() => this.application.QuitToDesktop());

			return commands;
		}
	}
}

