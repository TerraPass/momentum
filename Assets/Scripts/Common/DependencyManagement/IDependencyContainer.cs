using UnityEngine;
using System;
using System.Reflection;
using Terrapass.Debug;

namespace Momentum.Common.DependencyManagement
{
	public interface IDependencyContainer
	{
		/// <summary>
		/// Interconnect underlying objects based on current
		/// values of this container's properties.
		/// </summary>
		void PerformInjection();
	}

	public static class IDependencyContainerExtensions
	{
		private const BindingFlags BINDING_FLAGS = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;

		/// <summary>
		/// This method ensures that all the fields and properties of an IDependencyContainer,
		/// marked as required dependencies by a DependencyAttribute(false)
		/// are set to a non-null value.
		/// If this is not the case, an InvalidOperationException will be thrown.
		/// </summary>
		/// <param name="monoBehaviour">a MonoBehaviour instance.</param>
		public static void EnsureRequiredDependenciesAreProvided(this IDependencyContainer dependencyContainer)
		{
			dependencyContainer.EnsureRequiredFieldsAreSet();
			dependencyContainer.EnsureRequiredPropertiesAreSet();
		}

		private static void EnsureRequiredFieldsAreSet(this IDependencyContainer dependencyContainer)
		{
			var fields = dependencyContainer.GetType().GetFields(BINDING_FLAGS);
			foreach(var field in fields)
			{
				var attr = Attribute.GetCustomAttribute(field, typeof(DependencyAttribute)) as DependencyAttribute;
				// If field has a DependencyAttribute
				if(attr != null)
				{
					// If this dependency is required but has null value
					if(!attr.IsOptional && field.GetValue(dependencyContainer) == null)
					{
						throw new InvalidOperationException(
							String.Format(
								"Required field {0} of {1} has not been initialized before the call to PerformInjection()",
								field.Name,
								dependencyContainer.GetType().ToString()
							)
						);
					}
				}
			}
		}

		private static void EnsureRequiredPropertiesAreSet(this IDependencyContainer dependencyContainer)
		{
			var props = dependencyContainer.GetType().GetProperties(BINDING_FLAGS);
			foreach(var prop in props)
			{
				var attr = Attribute.GetCustomAttribute(prop, typeof(DependencyAttribute)) as DependencyAttribute;
				// If property has a DependencyAttribute
				if(attr != null)
				{
					// If this dependency is required but has null value
					if(!attr.IsOptional && prop.GetValue(dependencyContainer, null) == null)
					{
						throw new InvalidOperationException(
							String.Format(
								"Required property {0} of {1} has not been initialized before the call to PerformInjection()",
								prop.Name,
								dependencyContainer.GetType().ToString()
							)
						);
					}
				}
			}
		}
	}
}

