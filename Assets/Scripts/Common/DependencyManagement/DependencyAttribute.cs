using System;

namespace Momentum.Common.DependencyManagement
{
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
	public class DependencyAttribute : System.Attribute
	{
		private readonly bool optional;

		public DependencyAttribute(bool optional = false)
		{
			this.optional = optional;
		}

		public bool IsOptional
		{
			get {
				return this.optional;
			}
		}
	}
}
