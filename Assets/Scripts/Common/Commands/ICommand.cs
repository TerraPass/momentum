using System;

namespace Momentum.Common.Commands
{
	public interface ICommand
	{
		void Execute();
	}
}

