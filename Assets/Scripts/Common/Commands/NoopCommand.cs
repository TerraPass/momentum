using System;

namespace Momentum.Common.Commands
{
	/// <summary>
	/// Command that does nothing.
	/// </summary>
	public sealed class NoopCommand : ICommand
	{
		#region ICommand implementation

		public void Execute()
		{

		}

		#endregion
	}
}

