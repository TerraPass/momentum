using UnityEngine;
using System;

namespace Momentum.Common.Commands
{
	public interface IMacroCommand : ICommand
	{
		void Add(ICommand command);
		void Remove(ICommand command);
	}
}
