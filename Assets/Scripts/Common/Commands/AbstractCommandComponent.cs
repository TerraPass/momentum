using UnityEngine;
using System;
using Terrapass.Debug;

namespace Momentum.Common.Commands
{
	public abstract class AbstractCommandComponent : MonoBehaviour, ICommand
	{
		public abstract void Execute();
	}

	public abstract class AbstractCommandComponent<TCommand> : AbstractCommandComponent where TCommand : ICommand
	{
		private TCommand command;

		public sealed override void Execute()
		{
			DebugUtils.Assert(
				this.Command != null,
				String.Format(
					"Command is null on call to Execute() of {0}",
					this.GetType().ToString()
				)
			);
			this.ExecuteImpl();
		}

		protected virtual void ExecuteImpl()
		{
			this.Command.Execute();
		}

		protected TCommand Command
		{
			get {
				return this.command;
			}
			set {
				this.command = value;
			}
		}
	}
}

