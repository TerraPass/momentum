using UnityEngine;
using System;
using System.Collections.Generic;

namespace Momentum.Common.Commands
{
	public class MacroCommand : IMacroCommand
	{
		protected ICollection<ICommand> commands;

		public MacroCommand()
		{
			this.commands = new List<ICommand>();
		}

		public MacroCommand(IEnumerable<ICommand> commands)
		{
			this.commands = new List<ICommand>(commands);
		}

		#region IMacroCommand implementation

		public void Add (ICommand command)
		{
			this.commands.Add(command);
		}

		public void Remove (ICommand command)
		{
			this.commands.Remove(command);
		}

		#endregion

		#region ICommand implementation

		public void Execute ()
		{
			foreach(var command in this.commands)
			{
				command.Execute();
			}
		}

		#endregion
	}
}
