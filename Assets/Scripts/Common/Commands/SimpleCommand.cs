using System;

namespace Momentum.Common.Commands
{
	public class SimpleCommand : ICommand
	{
		private Action action;

		public SimpleCommand(Action action)
		{
			this.action = action;
		}

		public void Execute()
		{
			this.action();
		}
	}
}

