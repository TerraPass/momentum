using UnityEngine;
using System;

using Momentum.Configuration;
using Momentum.Level.Loaders;
using Momentum.Level.Metadata;
using Momentum.DependencyManagement.Facades;

namespace Momentum.Main
{
	public interface IGameApplication
	{
		ILevelLoader LevelLoader {get;}	// TODO: May be better as a part of GlobalDependencies.
		IGlobalDependencies GlobalDependencies {get;}

		void QuitToDesktop();
		void QuitToMainMenu();
	}

	public static class IGameApplicationExtensions
	{
		public static void LoadLevel(this IGameApplication gameApplication, ILevelId levelId)
		{
			gameApplication.LevelLoader.LoadLevel(levelId);
		}
	}
}

