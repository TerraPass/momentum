using UnityEngine;
using System;

using Momentum.Configuration;
using Momentum.Level.Loaders;
using Momentum.DependencyManagement.Facades;

namespace Momentum.Main
{
	public abstract class AbstractGameApplicationComponent : MonoBehaviour, IGameApplication
	{
		public abstract ILevelLoader LevelLoader {get;}
		public abstract IGlobalDependencies GlobalDependencies {get;}

		public abstract void QuitToDesktop();
		public abstract void QuitToMainMenu();
	}
}

