using UnityEngine;
using System;

namespace Momentum.Main.Modes
{
	public interface IGameMode
	{
		void Enter(IGameApplicationFriend gameApplicationFriend);
		void Exit(IGameApplicationFriend gameApplicationFriend);
	}
}
