using UnityEngine;
using System;
using Terrapass.Debug;
using Terrapass.Extensions.Unity;

using Momentum.Level;
using Momentum.DependencyManagement.Global;

#if UNITY_EDITOR
using Momentum.Main.Debug;
#endif

namespace Momentum.Main.Modes
{
	/// <summary>
	/// GameApplication is supposed to enter this mode when the game is launched.
	/// On entry this mode initializes global dependencies via a call to
	/// IGlobalDependencyContainer.PerformInjection(), as well as 
	/// triggers loading of a scene by the name, specified
	/// in its corresponding field in the inspector.
	/// </summary>
	public class BootstrapGameMode : AbstractGameModeComponent
	{
		[SerializeField]
		private string nameOfTheSceneToLoad;
		[SerializeField]
		private AbstractGlobalDependencyContainerComponent globalDependencies;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();
			DebugUtils.Assert(
				!this.nameOfTheSceneToLoad.Equals(""),
				string.Format(
					"Name of the scene to load was not set in editor for the {0} component",
					this.GetType().ToString()
				)
			);
		}

		#region implemented abstract members of AbstractGameModeComponent

		public override void Enter (IGameApplicationFriend gameApplicationFriend)
		{
			this.globalDependencies.PerformInjection(gameApplicationFriend.SceneLoader);
			gameApplicationFriend.GlobalDependencies = this.globalDependencies.GlobalDependencies;

#if UNITY_EDITOR
			// If the loading of init scene has been triggered by InEditorInitSceneLoader,
			// query it for the scene to load instead of the one specified in nameOfTheSceneToLoad field.
			if(InEditorInitSceneLoader.ReturnSceneName != null)
			{
				gameApplicationFriend.LoadScene(InEditorInitSceneLoader.ReturnSceneName);
				return;
			}
#endif
			gameApplicationFriend.LoadScene(this.nameOfTheSceneToLoad);
		}

		public override void Exit (IGameApplicationFriend gameApplicationFriend)
		{

		}

		#endregion
	}
}

