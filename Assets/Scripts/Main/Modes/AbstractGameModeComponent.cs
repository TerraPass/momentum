using UnityEngine;
using System;

namespace Momentum.Main.Modes
{
	public abstract class AbstractGameModeComponent : MonoBehaviour, IGameMode
	{
		#region IGameApplicationState implementation

		public abstract void Enter(IGameApplicationFriend gameApplicationFriend);

		public abstract void Exit(IGameApplicationFriend gameApplicationFriend);

		#endregion
	}
}

