using UnityEngine;
using System;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

using Momentum.DependencyManagement.Initialization;

namespace Momentum.Main.Modes
{
	public class SceneGameMode : AbstractGameModeComponent
	{
		[SerializeField]
		private AbstractSceneInitializerComponent sceneInitializer;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();
		}

		#region implemented abstract members of AbstractGameApplicationStateComponent

		public override void Enter (IGameApplicationFriend gameApplicationFriend)
		{
			// TODO: Refactor IGameApplicationFriend and GameApplication to expose
			// OnSceneInitializerChanged(ISceneInitializer) event trigger, which would
			// invoke IGameApplication.SceneInitializerChanged, which would be listened to
			// by loading status monitor.
			// FIXME: Severe breach of Demetre's law.
			gameApplicationFriend.GlobalDependencies.LoadingStatusMonitor.CurrentSceneInitializer = this.sceneInitializer;
			this.sceneInitializer.InitScene(gameApplicationFriend.Application);
		}

		public override void Exit (IGameApplicationFriend gameApplicationFriend)
		{
			this.sceneInitializer.DeinitScene(gameApplicationFriend.Application);
			// FIXME: Severe breach of Demetre's law.
			gameApplicationFriend.GlobalDependencies.LoadingStatusMonitor.CurrentSceneInitializer = null;
		}

		#endregion
	}
}

