using System;

namespace Momentum.Main.SceneTransition
{
	public abstract class AbstractSceneLoaderDecorator : ISceneLoader
	{
		private readonly ISceneLoader decoratedSceneLoader;

		public AbstractSceneLoaderDecorator(ISceneLoader decoratedSceneLoader)
		{
			if(decoratedSceneLoader == null)
			{
				throw new ArgumentNullException("decoratedSceneLoader");
			}
			this.decoratedSceneLoader = decoratedSceneLoader;
		}

		protected ISceneLoader DecoratedSceneLoader
		{
			get {
				return this.decoratedSceneLoader;
			}
		}


		#region ISceneLoader implementation
		public virtual event EventHandler<SceneLoadingStartingEventArgs> SceneLoadingStarting
		{
			add {
				this.DecoratedSceneLoader.SceneLoadingStarting += value;
			}
			remove {
				this.DecoratedSceneLoader.SceneLoadingStarting -= value;
			}
		}

		public virtual event EventHandler<SceneLoadingStartedEventArgs> SceneLoadingStarted
		{
			add {
				this.DecoratedSceneLoader.SceneLoadingStarted += value;
			}
			remove {
				this.DecoratedSceneLoader.SceneLoadingStarted -= value;
			}
		}

		public virtual event EventHandler<SceneLoadingCompletedEventArgs> SceneLoadingCompleted
		{
			add {
				this.DecoratedSceneLoader.SceneLoadingCompleted += value;
			}
			remove {
				this.DecoratedSceneLoader.SceneLoadingCompleted -= value;
			}
		}

		public virtual event EventHandler<SceneLoadingProgressChanged> SceneLoadingProgressChanged
		{
			add {
				this.DecoratedSceneLoader.SceneLoadingProgressChanged += value;
			}
			remove {
				this.DecoratedSceneLoader.SceneLoadingProgressChanged -= value;
			}
		}

		public virtual void LoadScene (string sceneName)
		{
			this.DecoratedSceneLoader.LoadScene(sceneName);
		}

		public virtual bool IsLoadingScene
		{
			get {
				return this.DecoratedSceneLoader.IsLoadingScene;
			}
		}

		public virtual float LoadingProgress
		{
			get {
				return this.DecoratedSceneLoader.LoadingProgress;
			}
		}

		public virtual float SceneLoadingProgress
		{
			get {
				return this.DecoratedSceneLoader.LoadingProgress;
			}
		}
		#endregion
	}
}

