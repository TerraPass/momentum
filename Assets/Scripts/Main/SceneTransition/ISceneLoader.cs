using UnityEngine;
using System;

namespace Momentum.Main.SceneTransition
{
	public interface ISceneLoader
	{
		event EventHandler<SceneLoadingStartingEventArgs> SceneLoadingStarting;
		event EventHandler<SceneLoadingStartedEventArgs> SceneLoadingStarted;
		event EventHandler<SceneLoadingCompletedEventArgs> SceneLoadingCompleted;
		event EventHandler<SceneLoadingProgressChanged> SceneLoadingProgressChanged;

		void LoadScene(string sceneName);

		bool IsLoadingScene {get;}
		float LoadingProgress {get;}
	}
}

