using UnityEngine;
using System;
using Terrapass.Debug;

using Momentum.Utils.Time;

namespace Momentum.Main.SceneTransition
{
	public class AlarmClockBasedSceneLoader : ISceneLoader
	{
		private const float DEFAULT_LOADING_CHECK_INTERVAL_SECONDS = 0.25f;

		private ICallbackAlarmClock alarmClock;

		private string currentlyLoadingSceneName;
		private AsyncOperation loadingOperation;

		public AlarmClockBasedSceneLoader(float loadingCheckIntervalSeconds = DEFAULT_LOADING_CHECK_INTERVAL_SECONDS)
		{
			// Initialize alarm clock but don't start it just yet
			this.alarmClock = new CallbackAlarmClock(
				this.OnAlarmFired,
				loadingCheckIntervalSeconds,
				true
			);

			this.currentlyLoadingSceneName = null;
			this.loadingOperation = null;
		}

		#region ISceneLoader implementation

		public event EventHandler<SceneLoadingStartingEventArgs> SceneLoadingStarting = delegate{};

		public event EventHandler<SceneLoadingStartedEventArgs> SceneLoadingStarted = delegate{};

		public event EventHandler<SceneLoadingCompletedEventArgs> SceneLoadingCompleted = delegate{};

		public event EventHandler<SceneLoadingProgressChanged> SceneLoadingProgressChanged = delegate{};

		public bool IsLoadingScene
		{
			get {
				return this.loadingOperation != null;
			}
		}

		public float LoadingProgress
		{
			get {
				return this.loadingOperation == null
					? 1.0f
					: this.loadingOperation.progress;
			}
		}

		public void LoadScene (string sceneName)
		{
			if(this.loadingOperation != null)
			{
				throw new InvalidOperationException(
					"A scene is already being loaded"
				);
			}

			this.SceneLoadingStarting(
				this,
				new SceneLoadingStartingEventArgs(
					this,
					sceneName
				)
			);

			this.currentlyLoadingSceneName = sceneName;
			this.loadingOperation = Application.LoadLevelAsync(sceneName);

			// Setup the timer to regularly check whether the loading has finished and update progress
			this.alarmClock.Reset(false);

			this.SceneLoadingStarted(
				this,
				new SceneLoadingStartedEventArgs(
					this,
					sceneName
				)
			);
		}

		#endregion

		private void OnAlarmFired(AlarmFiredEventArgs args)
		{
			DebugUtils.Assert(
				this.loadingOperation != null,
				"loadingOperation is null on a call to OnAlarmFired(). Perhaps alarm clock was not reset properly?"
			);
			DebugUtils.Assert(
				this.currentlyLoadingSceneName != null,
				"currentlyLoadingSceneName is null on a call to OnAlarmFired(). Perhaps alarm clock was not reset properly?"
			);

			this.SceneLoadingProgressChanged(
				this,
				new SceneLoadingProgressChanged(
					this,
					this.currentlyLoadingSceneName,
					this.loadingOperation.progress
				)
			);

			if(this.loadingOperation.isDone)
			{
				this.SceneLoadingCompleted(
					this,
					new SceneLoadingCompletedEventArgs(
						this,
						this.currentlyLoadingSceneName
					)
				);
				this.loadingOperation = null;
				this.currentlyLoadingSceneName = null;
			}
			else
			{
				this.alarmClock.Reset(false);
			}
		}
	}
}

