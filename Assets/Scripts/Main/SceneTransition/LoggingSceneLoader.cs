using UnityEngine;
using System;

namespace Momentum.Main.SceneTransition
{
	public class LoggingSceneLoader : AbstractSceneLoaderDecorator
	{
		public LoggingSceneLoader(ISceneLoader sceneLoader)
			: base(sceneLoader)
		{
			this.DecoratedSceneLoader.SceneLoadingStarting += this.OnSceneLoadingStarting;
			this.DecoratedSceneLoader.SceneLoadingStarted += this.OnSceneLoadingStarted;
			this.DecoratedSceneLoader.SceneLoadingCompleted += this.OnSceneLoadingCompleted;
		}

		private void OnSceneLoadingStarting(object sender, SceneLoadingStartingEventArgs args)
		{
			UnityEngine.Debug.LogFormat(
				"{0} is about to start loading scene \"{1}\"",
				this.DecoratedSceneLoader.GetType().ToString(),
				args.NewSceneName
			);
		}

		private void OnSceneLoadingStarted(object sender, SceneLoadingStartedEventArgs args)
		{
			UnityEngine.Debug.LogFormat(
				"{0} is loading scene \"{1}\"",
				this.DecoratedSceneLoader.GetType().ToString(),
				args.NewSceneName
			);
		}

		private void OnSceneLoadingCompleted(object sender, SceneLoadingCompletedEventArgs args)
		{
			UnityEngine.Debug.LogFormat(
				"{0} has loaded scene \"{1}\"",
				this.DecoratedSceneLoader.GetType().ToString(),
				args.NewSceneName
			);
		}
	}
}

