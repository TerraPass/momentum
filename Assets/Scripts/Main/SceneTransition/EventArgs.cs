using System;

namespace Momentum.Main.SceneTransition
{
	public abstract class SceneTransitionEventArgs : EventArgs
	{
		private readonly ISceneLoader sceneLoader;
		private readonly string newSceneName;

		public SceneTransitionEventArgs(ISceneLoader sceneLoader, string newSceneName)
		{
			if(sceneLoader == null)
			{
				throw new ArgumentNullException("sceneLoader");
			}
			this.sceneLoader = sceneLoader;
			this.newSceneName = newSceneName;
		}

		public string NewSceneName
		{
		 	get {
				return this.newSceneName;
			}
		}

		public ISceneLoader SceneLoader
		{
			get {
				return this.sceneLoader;
			}
		}
	}

	public class SceneLoadingStartingEventArgs : SceneTransitionEventArgs
	{
		public SceneLoadingStartingEventArgs(ISceneLoader sceneLoader, string newSceneName) : base(sceneLoader, newSceneName) {}
	}

	public class SceneLoadingStartedEventArgs : SceneTransitionEventArgs
	{
		public SceneLoadingStartedEventArgs(ISceneLoader sceneLoader, string newSceneName) : base(sceneLoader, newSceneName) {}
	}

	public class SceneLoadingCompletedEventArgs : SceneTransitionEventArgs
	{
		public SceneLoadingCompletedEventArgs(ISceneLoader sceneLoader, string newSceneName) : base(sceneLoader, newSceneName) {}
	}

	public class SceneLoadingProgressChanged : SceneTransitionEventArgs
	{
		private readonly float progress;

		public SceneLoadingProgressChanged(ISceneLoader sceneLoader, string newSceneName, float progress)
			: base(sceneLoader, newSceneName)
		{
			this.progress = progress;
		}

		public float Progress
		{
			get {
				return this.progress;
			}
		}
	}
}

