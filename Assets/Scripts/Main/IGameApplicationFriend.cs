using System;

using Momentum.DependencyManagement.Facades;
using Momentum.Main.SceneTransition;

namespace Momentum.Main
{
	public interface IGameApplicationFriend
	{
		IGameApplication Application {get;}

		ISceneLoader SceneLoader {get;}

		IGlobalDependencies GlobalDependencies {get;set;}
	}

	public static class IGameApplicationFriendExtensions
	{
		public static void LoadScene(this IGameApplicationFriend friend, string sceneName)
		{
			friend.SceneLoader.LoadScene(sceneName);
		}
	}
}

