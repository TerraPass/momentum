using UnityEngine;
using System;
using Terrapass.Debug;

using Momentum.Configuration;
using Momentum.Level.Loaders;
using Momentum.DependencyManagement.Facades;

namespace Momentum.Main
{
	using Modes;
	using SceneTransition;

	/// <summary>
	/// Root class of the game.
	/// Its instance is created once and preserved throughout the entire runtime.
	/// It exists as a component on a gameobject in the special init scene,
	/// which is the first to be loaded on game launch.
	/// When running scenes, other than init, in Unity editor's playmode,
	/// an instance of this class is created on-the-fly by
	/// Momentum.Main.Debug.InEditorGameApplicationSpawner.
	/// </summary>
	public class GameApplication : AbstractGameApplicationComponent
	{
		private class Friend : IGameApplicationFriend
		{
			private readonly GameApplication gameApplication;

			public Friend(GameApplication gameApplication)
			{
				DebugUtils.Assert(
					gameApplication != null,
					"{0} got null as its constructor's gameApplication parameter value"
				);
				this.gameApplication = gameApplication;
			}

			public IGameApplication Application
			{
				get {
					return this.gameApplication;
				}
			}

			public ISceneLoader SceneLoader
			{
				get {
					return this.gameApplication.sceneLoader;
				}
			}

			public IGlobalDependencies GlobalDependencies
			{
				get {
					return this.gameApplication.globalDependencies;
				}
				set {
					this.gameApplication.globalDependencies = value;
				}
			}
		}

		// FIXME: This is clumsy and doesn't protect from instances of other implementers
		// of IGameApplication.
		// Safeguard to ensure that only one instance of this component is ever created.
		private static bool alreadyExists = false;

		private ISceneLoader sceneLoader;
		private ILevelLoader levelLoader;

		private IGlobalDependencies globalDependencies;

		private Friend friend;

		private IGameMode currentMode;

		void Start()
		{
			// If running in editor, assert absence of duplicates
			DebugUtils.Assert(
				!alreadyExists,
				string.Format(
					"Attempted to initialize a duplicate instance of {0}",
					this.GetType().ToString()
				)
			);

			// If not running in editor, log error and destroy this instance, if duplication is detected
			if(alreadyExists)
			{
				UnityEngine.Debug.LogErrorFormat(
					"An instance of {0} has already been initialized!!! Destroying the duplicate...",
					this.GetType()
				);
				Destroy(this);
				return;
			}

			// Prevent duplicates from being initialized
			alreadyExists = true;

			// Prevent application object from being destroyed on scene transition
			DontDestroyOnLoad(this);

			// Setup scene loader to invoke game mode object lookup on completion of each scene transition
			this.sceneLoader = new LoggingSceneLoader(new AlarmClockBasedSceneLoader());
			this.sceneLoader.SceneLoadingStarting += (sender, e) => this.ChangeMode(null);	// Refactored from (LoadLevel)
			this.sceneLoader.SceneLoadingCompleted += ((sender, args) => this.SetModeFromLoadedScene());

			// Create a friend instance to be used by modes
			this.friend = new Friend(this);

			// Initialize mode to null and lookup game mode object in current scene
			this.ChangeMode(null);
			this.SetModeFromLoadedScene();	// BootstrapGameMode is entered at this point

			// Setup level loader
			this.levelLoader = new ProofOfConceptLevelLoader(
				this.sceneLoader,
				(uint)Application.levelCount,
				this.globalDependencies.Configuration.InternalSettings.NonLevelScenesNumber
			);

			UnityEngine.Debug.Log(
				string.Format(
					"Initialized the instance of {0}",
					this.GetType().ToString()
				)
			);
		}

		public override ILevelLoader LevelLoader
		{
			get {
				return this.levelLoader;
			}
		}

		public override IGlobalDependencies GlobalDependencies
		{
			get {
				return this.globalDependencies;
			}
		}

		public override void QuitToDesktop()
		{
			Application.Quit();
		}

		public override void QuitToMainMenu()
		{
			this.LoadScene(this.globalDependencies.Configuration.InternalSettings.MainMenuSceneName);
		}

		private void LoadScene(string sceneName)
		{
			if(this.sceneLoader.IsLoadingScene)
			{
				throw new InvalidOperationException("A scene is already being loaded");
			}
			// To invoke Exit() on current game mode
			this.ChangeMode(null);
			this.sceneLoader.LoadScene(sceneName);
		}

		private void SetModeFromLoadedScene()
		{
			// currentMode must be null at this point.
			// If it isn't, it means someone else triggered scene transition through ISceneLoader.
			DebugUtils.Assert(
				this.currentMode == null,
				string.Format(
					"Unexpected SceneLoadingCompleted event from {0}",
					this.sceneLoader.GetType().ToString()
				)
			);

			var newMode = this.LookupModeInLoadedScene();
			this.ChangeMode(newMode);
		}

		private void ChangeMode(IGameMode newMode)
		{
			DebugUtils.Assert(
				this.friend != null,
				string.Format(
					"{0}.friend is null on call to ChangeMode()",
					this.GetType()
				)
			);

			if(this.currentMode != null)
			{
				this.currentMode.Exit(this.friend);
			}
			this.currentMode = newMode;
			if(this.currentMode != null)
			{
				this.currentMode.Enter(this.friend);
			}
		}

		// TODO: Decompose?
		private AbstractGameModeComponent LookupModeInLoadedScene()
		{
			var modes = UnityEngine.GameObject.FindObjectsOfType(typeof(AbstractGameModeComponent));
			DebugUtils.Assert(
				modes.Length == 1,
				string.Format(
					"There must be exactly 1 AbstractGameModeComponent subclass component in the scene, found {0}",
					modes.Length
				)
			);
			return (AbstractGameModeComponent) modes[0];
		}
	}
}

