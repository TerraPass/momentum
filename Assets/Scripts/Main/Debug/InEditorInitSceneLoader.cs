using UnityEngine;
using System;

namespace Momentum.Main.Debug
{
	/// <summary>
	/// This component is only used in Unity editor
	/// and is responsible for forcing the loading of
	/// init scene to allow for initialization.
	/// </summary>
	public class InEditorInitSceneLoader : MonoBehaviour
	{
#if UNITY_EDITOR
		private const string INIT_SCENE_NAME = "init";
		private static readonly Type APPLICATION_COMPONENT_TYPE = typeof(AbstractGameApplicationComponent);

		private static string returnSceneName = null;

		/// <summary>
		/// This property is exposed for BootstrapGameMode
		/// to know which scene to return to 
		/// after init scene has been loaded.
		/// </summary>
		/// <remarks>
		/// <c>null</c> means BootstrapGameMode is to proceed normally
		/// (i.e. load the main menu).
		/// </remarks>
		/// <value>The name of the scene to return to.</value>
		public static String ReturnSceneName
		{
			get {
				return returnSceneName;
			}
			private set {
				returnSceneName = value;

				UnityEngine.Debug.LogFormat(
					"{0}: Set to reload \"{1}\" scene after initialization",
					typeof(InEditorInitSceneLoader),
					returnSceneName
				);
			}
		}
#endif

		void Start()
		{
#if UNITY_EDITOR
			// Load init scene if running in editor
			// and no game application component exists so far.
			if(FindObjectOfType(APPLICATION_COMPONENT_TYPE) == null)
			{
				UnityEngine.Debug.LogFormat(
					"{0}: Running in editor, no application component found, forcing the loading of {1} scene...",
					this.GetType().Name,
					INIT_SCENE_NAME
				);

				// Remember current scene name
				ReturnSceneName = Application.loadedLevelName;

				// Trigger init scene loading
				Application.LoadLevel(INIT_SCENE_NAME);
			}
			else
			{
				UnityEngine.Debug.LogFormat(
					"{0}: Running in editor, application component already exists, no further action will be taken",
					this.GetType().Name
				);
			}
#else
			UnityEngine.Debug.LogFormat(
				"{0}: Not running in editor, will not take any action",
				this.GetType().Name
			);
#endif
			Destroy(this);
		}
	}
}

