using System;

namespace Momentum.Cameras.Common
{
	public interface IIsometricCamera : ICamera
	{
		IsometricRotation Rotation {get;set;}
	}

	public static class IIsometricCameraExtensions
	{
		public static void RotateClockwise(this IIsometricCamera camera)
		{
			camera.Rotation = camera.Rotation.GetNextClockwise();
		}

		public static void RotateCounterClockwise(this IIsometricCamera camera)
		{
			camera.Rotation = camera.Rotation.GetNextCounterClockwise();
		}
	}
}

