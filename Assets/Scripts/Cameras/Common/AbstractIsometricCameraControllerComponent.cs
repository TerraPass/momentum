using System;

namespace Momentum.Cameras.Common
{
	public abstract class AbstractIsometricCameraControllerComponent
		: AbstractCameraControllerComponent, IIsometricCameraController
	{
		#region ICameraController implementation
		public abstract IIsometricCamera Camera {get;set;}
		#endregion

		protected override ICamera AbstractCamera
		{
			get {
				return this.Camera;
			}
			set {
				this.Camera = (IIsometricCamera) value;
			}
		}
	}
}

