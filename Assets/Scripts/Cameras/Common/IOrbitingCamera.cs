using UnityEngine;
using System;

namespace Momentum.Cameras.Common
{
	public interface IOrbitingCamera : ICamera
	{
		/// <summary>
		/// Distance to target.
		/// </summary>
		/// <value>Distance.</value>
		float Distance {get;set;}

		/// <summary>
		/// Rotation of orbiting camera around vertical axis, passing through the center of its target,
		/// relative to the initial rotation (implementation defined)
		/// as an angle in degrees, where positive values correspond to counterclockwise (left-to-right) rotation.
		/// </summary>
		/// <value>Angle.</value>
		float Rotation {get;set;}

		/// <summary>
		/// Rotation of orbiting camera around horizontal axis, passing through the center of its target,
		/// relative to the initial rotation (implementation defined)
		/// as an angle in degrees, where positive values correspond to counterclockwise (upward) rotation.
		/// </summary>
		/// <value>Angle.</value>
		float Tilt {get;set;}

		//float MinRotation {get;set;}
		float MinTilt {get;set;}

		//float MaxRotation {get;set;}
		float MaxTilt {get;set;}
	}

	public static class IOrbitingCameraExtensions
	{
		/// <summary>
		/// Rotates the camera clockwise and counterclockwise around the target.
		/// </summary>
		/// <param name="angle">Angle to rotate by, positive corresponds to clockwise.</param>
		public static void RotateBy(this IOrbitingCamera camera, float angle)	// TODO: Review the documentation
		{
			camera.Rotation += angle;
		}

		/// <summary>
		/// Tilts the camera up and down, keeping the constant distance to the target.
		/// </summary>
		/// <param name="angle">Angle to tilt by, positive corresponsds to up.</param>
		public static void TiltBy(this IOrbitingCamera camera, float angle)
		{
			camera.Tilt += angle;
		}
	}
}

