using UnityEngine;
using System;

namespace Momentum.Cameras.Common
{
	public abstract class AbstractOrbitingCameraControllerComponent 
		: AbstractCameraControllerComponent, IOrbitingCameraController
	{
		public abstract IOrbitingCamera Camera {get;set;}

		#region implemented abstract members of AbstractCameraControllerComponent
		protected override ICamera AbstractCamera {
			get {
				return this.Camera;
			}
			set {
				this.Camera = (IOrbitingCamera)value;
			}
		}
		#endregion

		public abstract float RotationSensitivity {get;set;}
		public abstract float TiltSensitivity {get;set;}

		public abstract bool RotationInverted {get;set;}
		public abstract bool TiltInverted {get;set;}
	}
}

