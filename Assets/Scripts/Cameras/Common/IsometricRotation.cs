using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

namespace Momentum.Cameras.Common
{
	public enum IsometricRotation
	{
		/// <summary>
		/// Top-down, -45deg around Y axis.
		/// </summary>
		MINUS45 	= 0,
		/// <summary>
		/// Top-down, +45deg around Y axis.
		/// </summary>
		PLUS45 		= 1,
		/// <summary>
		/// Top-down, +135deg around Y axis.
		/// </summary>
		PLUS135 	= 2,
		/// <summary>
		/// Top-down, -135deg around Y axis.
		/// </summary>
		MINUS135	= 3
	}

	public static class IsometricRotationExtensions
	{
		private const float PI_BY_2 = (float) (Math.PI / 2.0);
		private const float PI_BY_4 = (float) (Math.PI / 4.0);

		/// <summary>
		/// Rotation along global X in degrees, needed for isometricity.
		/// </summary>
		private const float ROTATION_X_DEG = 30.0f;

		/// <summary>
		/// This is the ratio of offset along y axis (delta y) to the offset
		/// along axis x (or z), assuming delta x == delta z.
		/// I.e., if isometric camera is offset by 1 along both x and z axes,
		/// this is the value, by which it must be offset along y to keep its
		/// view unchanged.
		/// </summary>
		private const float HEIGHT_OFFSET_RATIO = 0.8165f;	// sqrt(2 / 3)

		private const float REV_HEIGHT_OFFSET_RATIO = 1.0f / HEIGHT_OFFSET_RATIO;

		/// <summary>
		/// Returns rotation around global Y axis in radians,
		/// corresponding to this isometric rotation.
		/// </summary>
		/// <returns>Rotation around global Y in radians.</returns>
		/// <param name="rotation">Isometric rotation.</param>
		public static float GetAngleRadians(this IsometricRotation rotation)
		{
			return (-(PI_BY_4)) + ((int)rotation) * PI_BY_2;
		}

		/// <summary>
		/// Returns rotation around global Y axis in degrees,
		/// corresponding to this isometric rotation.
		/// </summary>
		/// <returns>Rotation around global Y in degrees.</returns>
		/// <param name="rotation">Isometric rotation.</param>
		public static float GetAngleDegrees(this IsometricRotation rotation)
		{
			return (-45.0f) + ((int)rotation) * 90.0f;
		}

		/// <summary>
		/// Returns global euler angles, corresponding to this isometric camera rotation.
		/// </summary>
		/// <returns>Euler angles in degrees as a vector.</returns>
		/// <param name="rotation">Isometric rotation.</param>
		public static Vector3 GetEulerAnglesDegrees(this IsometricRotation rotation)
		{
			return new Vector3(ROTATION_X_DEG, rotation.GetAngleDegrees(), 0);
		}

		/// <summary>
		/// Returns a Vector3, containing the signs (1.0f or -1.0f) of offset components
		/// for a certain isometric rotation.
		/// These signs have to be applied to a vector by multiplying it, per-element, with the vector,
		/// returned from this method.
		/// </summary>
		/// <returns>The offset signs.</returns>
		/// <param name="rotation">Rotation.</param>
		private static Vector3 GetOffsetSigns(this IsometricRotation rotation)
		{
			return new Vector3(
				rotation == IsometricRotation.MINUS45 || rotation == IsometricRotation.MINUS135
					? 1.0f
					: -1.0f,
				1.0f,
				rotation == IsometricRotation.MINUS135 || rotation == IsometricRotation.PLUS135
					? 1.0f
					: -1.0f
			);
		}

		/// <summary>
		/// Returns a vector, by which the (desired) displacement of isometric camera 
		/// from its target along global axis X (delta x) needs to be multiplied
		/// in order to obtain the offset vector.
		/// </summary>
		/// <returns>The vector to multiply by delta x.</returns>
		/// <param name="rotation">Rotation.</param>
		public static Vector3 GetOffsetDeltaXMultipliers(this IsometricRotation rotation)
		{
			return rotation.GetOffsetSigns().TimesPerElement(new Vector3(1.0f, HEIGHT_OFFSET_RATIO, 1.0f));
		}

		/// <summary>
		/// Based on a desired displacement of isometric camera from its target along global X (delta X),
		/// calculates and returns the offset vector.
		/// </summary>
		/// <returns>The offset vector.</returns>
		/// <param name="rotation">Rotation.</param>
		/// <param name="deltaX">Delta x.</param>
		public static Vector3 GetOffsetByDeltaX(this IsometricRotation rotation, float deltaX)
		{
			return deltaX * rotation.GetOffsetDeltaXMultipliers();
		}

		/// <summary>
		/// Returns a vector, by which the (desired) displacement of isometric camera 
		/// from its target along global axis Y (height) needs to be multiplied
		/// in order to obtain the offset vector.
		/// </summary>
		/// <returns>The vector to multiply by height.</returns>
		/// <param name="rotation">Rotation.</param>
		public static Vector3 GetOffsetHeightMultipliers(this IsometricRotation rotation)
		{
			return rotation.GetOffsetSigns().TimesPerElement(new Vector3(
				REV_HEIGHT_OFFSET_RATIO,
				1.0f,
				REV_HEIGHT_OFFSET_RATIO
			));
		}

		/// <summary>
		/// Based on a desired height of isometric camera, relative to its target,
		/// calculates and returns the offset vector.
		/// </summary>
		/// <returns>The offset vector.</returns>
		/// <param name="rotation">Rotation.</param>
		/// <param name="height">Height.</param>
		public static Vector3 GetOffsetByHeight(this IsometricRotation rotation, float height)
		{
			return height * rotation.GetOffsetHeightMultipliers();
		}

		/// <summary>
		/// Returns a vector, by which the (desired) distance of isometric camera 
		/// from its target needs to be multiplied
		/// in order to obtain the offset vector.
		/// </summary>
		/// <returns>The vector to multiply by distance to target.</returns>
		/// <param name="rotation">Rotation.</param>
		public static Vector3 GetOffsetDistanceMultipliers(this IsometricRotation rotation)
		{
			// TODO: Implement
			throw new NotImplementedException();
		}

		/// <summary>
		/// Based on a desired distance of isometric camera from its target,
		/// calculates and returns the offset vector.
		/// </summary>
		/// <returns>The offset vector.</returns>
		/// <param name="rotation">Rotation.</param>
		/// <param name="distance">Distance from target.</param>
		public static Vector3 GetOffsetByDistance(this IsometricRotation rotation, float distance)
		{
			return distance * rotation.GetOffsetDistanceMultipliers();
		}

		public static IsometricRotation GetNextCounterClockwise(this IsometricRotation rotation)
		{
			return rotation == IsometricRotation.MINUS45
				? IsometricRotation.MINUS135
				: (IsometricRotation) ((int)rotation - 1);
		}

		public static IsometricRotation GetNextClockwise(this IsometricRotation rotation)
		{
			return rotation == IsometricRotation.MINUS135
				? IsometricRotation.MINUS45
				: (IsometricRotation) ((int)rotation + 1);
		}
	}
}

