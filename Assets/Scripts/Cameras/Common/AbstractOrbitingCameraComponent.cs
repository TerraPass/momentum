using UnityEngine;
using System;

namespace Momentum.Cameras.Common
{
	public abstract class AbstractOrbitingCameraComponent : AbstractCameraComponent, IOrbitingCamera
	{
		#region IGameplayCamera implementation
		public abstract float Distance {get;set;}

		public abstract float Rotation {get;set;}
		public abstract float Tilt {get;set;}

//		public abstract float MinRotation {get;set;}
		public abstract float MinTilt {get;set;}

//		public abstract float MaxRotation {get;set;}
		public abstract float MaxTilt {get;set;}
		#endregion
		
	}
}

