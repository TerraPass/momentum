using UnityEngine;
using System;

namespace Momentum.Cameras.Common
{
	public class IsometricCameraController : AbstractIsometricCameraControllerComponent
	{
		[SerializeField]
		private string rotateCounterClockwiseButton = "IsometricRotateCCW";
		[SerializeField]
		private string rotateClockwiseButton = "IsometricRotateCW";

		private bool isEnabled = true;

		private IIsometricCamera controlledCamera;

		void Update()
		{
			if(!this.isEnabled || this.controlledCamera == null)
			{
				return;
			}

			if(Input.GetButtonDown(this.rotateCounterClockwiseButton))
			{
				this.controlledCamera.RotateCounterClockwise();
			}
			if(Input.GetButtonDown(this.rotateClockwiseButton))
			{
				this.controlledCamera.RotateClockwise();
			}
		}

		#region implemented abstract members of AbstractCameraControllerComponent

		public override bool Enabled
		{
			get {
				return this.isEnabled;
			}
			set {
				this.isEnabled = value;
			}
		}

		public override IIsometricCamera Camera
		{
			get {
				return this.controlledCamera;
			}
			set {
				this.controlledCamera = value;
			}
		}

		#endregion
	}
}

