using UnityEngine;
using System;
using System.Linq;

namespace Momentum.Cameras.Common
{
	public class OrbitingCameraController : AbstractOrbitingCameraControllerComponent
	{
		[SerializeField]
		private string[] cameraRotationAxisNames;
		[SerializeField]
		private string[] cameraTiltAxisNames;

		[SerializeField]
		private float rotationSensitivity = 1.0f;
		[SerializeField]
		private float tiltSensitivity = 1.0f;

		[SerializeField]
		private bool rotationInverted = false;
		[SerializeField]
		private bool tiltInverted = false;

		private IOrbitingCamera controlledCamera;

		private bool isEnabled = false;

		void Start()
		{

		}

		void Update()
		{
			if(!this.isEnabled)
			{
				return;
			}
			if(this.controlledCamera != null)
			{
				float rotationInput = SumOfInputs(this.cameraRotationAxisNames, this.rotationInverted);
				float tiltInput = SumOfInputs(this.cameraTiltAxisNames, this.tiltInverted);

				if(rotationInput != 0)
				{
					this.controlledCamera.RotateBy(rotationInput);
				}
				if(tiltInput != 0)
				{
					this.controlledCamera.TiltBy(tiltInput);
				}
			}
		}

		public override IOrbitingCamera Camera
		{
			get {
				return this.controlledCamera;
			}
			set {
				this.controlledCamera = value;
			}
		}

		public override float RotationSensitivity
		{
			get {
				return this.rotationSensitivity;
			}
			set {
				this.rotationSensitivity = value;
			}
		}
		
		public override float TiltSensitivity {
			get {
				return this.tiltSensitivity;
			}
			set {
				this.tiltSensitivity = value;
			}
		}

		public override bool RotationInverted {
			get {
				return this.rotationInverted;
			}
			set {
				this.rotationInverted = value;
			}
		}

		public override bool TiltInverted {
			get {
				return this.tiltInverted;
			}
			set {
				this.tiltInverted = value;
			}
		}

		public override bool Enabled {
			get {
				return this.isEnabled;
			}
			set {
				this.isEnabled = value;
			}
		}

		private static float SumOfInputs(string[] axesNames, bool negate)
		{
			// This is, essentially, a left fold.
			// It calculates the sum of inputs from all the axes,
			// whose names are contained in the passed in array.
			var sum = axesNames.Aggregate(
				0.0f,
				(acc, axisName) => acc + Input.GetAxis(axisName)
			);
			return negate ? -sum : sum;
		}
	}
}

