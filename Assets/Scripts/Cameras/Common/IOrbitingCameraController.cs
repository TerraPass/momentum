using UnityEngine;
using System;

namespace Momentum.Cameras.Common
{
	public interface IOrbitingCameraController : ICameraController<IOrbitingCamera>
	{
		//IOrbitingCamera Camera {get;set;}	// Inherited from ICameraController<IOrbitingCamera>

		float RotationSensitivity {get;set;}
		float TiltSensitivity {get;set;}

		bool RotationInverted {get;set;}
		bool TiltInverted {get;set;}
	}
}

