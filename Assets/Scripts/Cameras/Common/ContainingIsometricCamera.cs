using UnityEngine;
using System;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

namespace Momentum.Cameras.Common
{
	[RequireComponent(typeof(Camera))]
	public class ContainingIsometricCamera : AbstractIsometricCameraComponent
	{
		[SerializeField]
		private IsometricRotation initialIsometricRotation;
		[SerializeField]
		private float height = 5.0f;
		[SerializeField]
		private bool useLerp;
		[SerializeField]
		private float lerpAmount = 0.1f;
		[SerializeField]
		[Tooltip("Relative size of the viewport area, the target is to be contained within, relative to entire viewport size.")]
		private float containingScope = 0.9f;

		private Transform target;

		private IsometricRotation isometricRotation;

		private Vector3 imaginaryTargetPosition;
		private Quaternion desiredRotation;

		private Vector3 isometricOffset;

		/// <summary>
		/// This field is only used in LateUpdate() if useLerp == true.
		/// Its purpose is to track intermediate offset value, obtained by lerping so far.
		/// This is done to achieve lerp effect ONLY for isometric offset, but not for
		/// camera's base position.
		/// </summary>
		//private Vector3 lerpIsometricOffset = Vector3.zero;

		private Camera CameraImpl
		{
			get {
				return this.GetComponent<Camera>();
			}
		}

		void Start()
		{
			DebugUtils.Assert(
				this.CameraImpl != null,
				"Camera component is missing and is required by ContainingIsometricCamera"
			);

			this.Rotation = this.initialIsometricRotation;
			// this.ResetTransform()?? (See FollowingOrbitingCamera)
		}

		void LateUpdate()
		{
			if(this.target == null)
			{
				return;
			}

			// Bring the target back into containing scope, 
			// if the target has left the scope.
			if(!this.IsTargetInScope())
			{
				this.PutTargetOntoScopeBoundary();
			}

			// TODO: Optimize by checking that we are not yet
			// at (or near) the desired offset and rotation.

			if(this.useLerp)
			{
				this.transform.LerpTo(
					this.imaginaryTargetPosition + this.isometricOffset,
					this.desiredRotation,
					this.transform.localScale,
					this.lerpAmount
				);

				// The following is the code for using lerp only for isometric offset and rotation
				// but not the base (imaginary target) position.
				// (Saved as a comment, along with lerpIsometricOffset
				// declaration, just in case.)
				/*
				this.lerpIsometricOffset = Vector3.Lerp(
					this.lerpIsometricOffset,
					this.isometricOffset,
					this.lerpAmount
				);

				this.transform.position = this.desiredBasePosition + this.lerpIsometricOffset;

				this.transform.rotation = Quaternion.Lerp(
					this.transform.rotation,
					this.desiredRotation,
					this.lerpAmount
				);
				*/
			}
			else
			{
				this.transform.position = this.imaginaryTargetPosition + this.isometricOffset;
				this.transform.rotation = this.desiredRotation;
			}
		}

		#region implemented abstract members of AbstractIsometricCameraComponent

		public override IsometricRotation Rotation {
			get {
				return this.isometricRotation;
			}
			set {
				this.isometricRotation = value;

				this.desiredRotation = Quaternion.Euler(this.isometricRotation.GetEulerAnglesDegrees());
				this.isometricOffset = this.isometricRotation.GetOffsetByHeight(this.height);
			}
		}

		public override Transform Target {
			get {
				return this.target;
			}
			set {
				this.target = value;
			}
		}

		public override float FieldOfView {
			get {
				return this.CameraImpl.orthographicSize;
			}
			set {
				this.CameraImpl.orthographicSize = value;
			}
		}

		#endregion

		/// <summary>
		/// This value represents the radius of a circle in XZ plane,
		/// corresponding to 
		/// </summary>
		/// <value>The size of containing scope in XZ plane.</value>
		private float XZContainingSize
		{
			get {
				// Let orthoSize be the vertical half-size of the camera.
				float orthoSize = this.CameraImpl.orthographicSize;
				// Then the half-length of the part of the XZ plane,
				// visible through the viewport of this camera is 
				// halfSize / (sin 30deg) == 2 * halfSize
				float fullXZSize = 2 * orthoSize;

				return this.containingScope * fullXZSize;
			}
		}

		private bool IsTargetInScope()
		{
			var sqrDistToTarget = Vector3.SqrMagnitude(
					this.target.position - this.imaginaryTargetPosition
			);

			return sqrDistToTarget <= this.XZContainingSize * this.XZContainingSize;
		}

		private void PutTargetOntoScopeBoundary()
		{
			var realToImaginaryTargetDirection = Vector3.Normalize(
					this.imaginaryTargetPosition - this.target.position
			);

			this.imaginaryTargetPosition = this.target.position + realToImaginaryTargetDirection * this.XZContainingSize;
		}
	}
}

