using UnityEngine;
using System;
using Terrapass.Debug;
using Terrapass.Extensions.Unity;

using Momentum.Utils.Math;

namespace Momentum.Cameras.Common
{
	[RequireComponent(typeof(Camera))]
	public class FollowingOrbitingCamera : AbstractOrbitingCameraComponent
	{
		[SerializeField]
		private Transform target;	// Transform is a reference type
		[SerializeField]
		private float lerpAmount = 0.75f;
		[SerializeField]
		private float distance = 5.0f;
		[SerializeField]
		private float minTilt = 10.0f;
		[SerializeField]
		private float maxTilt = 80.0f;

		private float rotation = 0.0f;
		private float tilt = 30.0f;

		private Camera myCamera;

		// Used for lerping in LateUpdate()
		private Vector3 desiredOffset;
		private Quaternion desiredRotation;

		void Start()
		{
			this.myCamera = this.GetComponent<Camera>();
			DebugUtils.Assert(
				this.myCamera != null,
				"Camera component is missing and is required by FollowingOrbitingCamera"
			);
			if(this.target == null)
			{
				Debug.LogWarning("FollowingOrbitingCamera has no target set on Start()");
			}
			else
			{
				this.ResetTransform(true);
			}
		}

		// Executed after all calls to Update() have been made
		void LateUpdate()
		{
			if(this.Target != null)
			{
				// "Chase" the desired transform by lerping
				this.transform.position = Vector3.Lerp(
					this.transform.position,
					this.target.transform.position + this.desiredOffset,
					this.lerpAmount
				);
				this.transform.rotation = Quaternion.Lerp(
					this.transform.rotation,
					this.desiredRotation,
					this.lerpAmount
				);
			}
		}

		protected virtual void RotateByImpl (float angle)
		{
			if(this.target != null)
			{
				this.desiredOffset = this.desiredOffset.RotateAround(
					Vector3.zero,
					Vector3.up,
					angle
				);
				// Look at the target
				this.desiredRotation.SetLookRotation(-this.desiredOffset);
			}
			this.rotation += angle;
		}

		protected virtual void TiltByImpl (float angle)
		{
			if(this.target != null)
			{
				// Compute vector, orthogonal to the plane, 
				// containing distance-to-target vector and Vector3.up.
				// See http://docs.unity3d.com/Manual/ComputingNormalPerpendicularVector.html
				Vector3 side1 = Vector3.up;
				Vector3 side2 = this.desiredOffset;

				Vector3 rotationAxis = Vector3.Cross(side2, side1);

				this.desiredOffset = this.desiredOffset.RotateAround(
					Vector3.zero,
					rotationAxis,
					angle
				);
				// Look at the target
				this.desiredRotation.SetLookRotation(-this.desiredOffset);
			}
			this.tilt += angle;
		}

		/// <summary>
		/// Recalculates the desired offset and rotation.
		/// </summary>
		/// <param name="hard">
		/// If <c>true</c>, immediately moves this camera to the desired transform.
		/// Otherwise, just sets up desired offset and rotation as values to lerp to in LateUpdate().
		/// </param>
		protected void ResetTransform(bool hard = false)
		{
			// Put the camera behind (global -z) its target at the specified distance
			this.desiredOffset = this.Distance * Vector3.back;
			// Look at the target (forward)
			this.desiredRotation.SetLookRotation(Vector3.forward, Vector3.up);
			// Remember current rotation and tilt to be able to restore them
			float oldRotation = this.rotation;
			float oldTilt = this.tilt;
			// By putting the desired transform behind the target and 
			// looking at it we've effectively reset both tilt and rotation to 0.
			// Reset both values to reflect this fact.
			this.rotation = 0;
			this.tilt = 0;
			// Restore previous tilt and rotation in a new place
			this.RotateBy(oldRotation);
			this.TiltBy(oldTilt);

			// Immediately move to desired transform
			if(hard)
			{
				this.transform.position = this.Target.transform.position + this.desiredOffset;
				this.transform.rotation = this.desiredRotation;
			}
		}

		#region implemented abstract members of AbstractOrbitingCameraComponent

		public override Transform Target {
			get {
				return this.target;
			}
			set {
				this.target = value;
				this.ResetTransform();
			}
		}

		public override float Distance {
			get {
				return this.distance;
			}
			set {
				this.distance = value;
				this.ResetTransform();
			}
		}

		public override float FieldOfView {
			get {
				return this.myCamera.fieldOfView;
			}
			set {
				this.myCamera.fieldOfView = value;
			}
		}

		public override float Rotation {
			get {
				return this.rotation;
			}
			set {
				if(value != this.rotation)
				{
					this.RotateByImpl(value - this.rotation);
				}
			}
		}

		public override float Tilt {
			get {
				return this.tilt;
			}
			set {
				float sanitizedValue = MathUtils.TrimToRange(
					value,
					this.minTilt,
					this.maxTilt
				);
				if(sanitizedValue != this.tilt)
				{
					this.TiltByImpl(sanitizedValue - this.tilt);
				}
			}
		}

//		public override float MinRotation {
//			get {
//				throw new NotImplementedException ();
//			}
//			set {
//				throw new NotImplementedException ();
//			}
//		}

		public override float MinTilt {
			get {
				return this.minTilt;
			}
			set {
				this.minTilt = value;
			}
		}

//		public override float MaxRotation {
//			get {
//				throw new NotImplementedException ();
//			}
//			set {
//				throw new NotImplementedException ();
//			}
//		}

		public override float MaxTilt {
			get {
				return this.minTilt;
			}
			set {
				this.minTilt = value;
			}
		}

		#endregion
	}
}
	