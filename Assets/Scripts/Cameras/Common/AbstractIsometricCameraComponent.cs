using UnityEngine;
using System;

namespace Momentum.Cameras.Common
{
	public abstract class AbstractIsometricCameraComponent : AbstractCameraComponent, IIsometricCamera
	{
		#region IIsometricCamera implementation

		public abstract IsometricRotation Rotation {get;set;}

		#endregion
	}
}

