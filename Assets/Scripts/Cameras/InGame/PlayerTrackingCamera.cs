using UnityEngine;
using System;
using Terrapass.Debug;

using Momentum.Gameplay.Player;

namespace Momentum.Cameras.InGame
{
	public class PlayerTrackingCamera : AbstractCameraDecorator
	{
		private IPlayer player;

		public PlayerTrackingCamera (ICamera camera, IPlayer player)
			: base(camera)
		{
			if(player == null)
			{
				throw new ArgumentNullException("player");
			}
			this.ConnectToPlayer(player);
		}

		private void OnPlayerActiveBallChanged(object sender, ActiveBallChangedEventArgs args)
		{
			DebugUtils.Assert(
				this.player != null,
				"Player is null on invocation of OnPlayerActiveBallChanged() event handler"
			);
			this.DecoratedCamera.Target = args.NewActiveBall.Transform;
		}

		protected void DisconnectFromPlayer()
		{
			if(this.player != null)
			{
				this.player.ActiveBallChanged -= this.OnPlayerActiveBallChanged;
				this.player = null;
			}
		}

		protected void ConnectToPlayer(IPlayer player)
		{
			if(player != null)
			{
				this.player = player;
				this.player.ActiveBallChanged += this.OnPlayerActiveBallChanged;
			}
		}
	}
}

