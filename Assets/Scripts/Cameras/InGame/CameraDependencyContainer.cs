using UnityEngine;
using System;
using Terrapass.Debug;
using Terrapass.Extensions.Unity;

using Momentum.Common.DependencyManagement;
using Momentum.Cameras.Common;
using Momentum.Gameplay.Player;
using Momentum.Gameplay.Core;
using Momentum.Gameplay.Common;

namespace Momentum.Cameras.InGame
{
	public class CameraDependencyContainer : AbstractCameraDependencyContainerComponent
	{
		[SerializeField]
		private AbstractCameraComponent gameplayCameraComponent;
		[SerializeField]
		private AbstractCameraControllerComponent cameraController;

		private IPlayer player;
		private IGameLevelEventSystem gameLevelEventSystem;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();
		}

		#region implemented abstract members of AbstractCameraDependencyContainerComponent

		[Dependency]
		public override IPlayer Player 
		{
			get {
				return this.player;
			}
			set {
				this.player = value;
			}
		}

		[Dependency]
		public override IGameLevelEventSystem GameLevelEventSystem
		{
			get {
				return this.gameLevelEventSystem;
			}
			set {
				this.gameLevelEventSystem = value;
			}
		}

		public override void PerformInjection ()
		{
			this.EnsureRequiredDependenciesAreProvided();

			// Introduce camera to controller
			((ICameraController)this.cameraController).Camera = this.gameplayCameraComponent;
			// Decorate gameplay camera for it to track Player
			// (PlayerTrackingCamera subscribes for an event in Player,
			// which prevents it from being garbage collected prematurely.)
			new PlayerTrackingCamera(this.gameplayCameraComponent, this.Player);
			// Setup controllers to be automatically enabled on level (re)start
			// and disabled on level completion.
			new AutoTogglingLevelEventSystemTrackingDisableable(
				this.cameraController, 
				this.gameLevelEventSystem
			);
		}
		#endregion
	}
}

