using UnityEngine;
using System;

using Momentum.Common.DependencyManagement;
using Momentum.Gameplay.Player;
using Momentum.Cameras.Common;
using Momentum.Gameplay.Core;

namespace Momentum.Cameras.InGame
{
	public interface ICameraDependencyContainer : IDependencyContainer
	{
		IPlayer Player {get;set;}
		IGameLevelEventSystem GameLevelEventSystem {get;set;}
	}

	public static class ICameraDependencyContainerExtensions
	{
		public static void PerformInjection(
			this ICameraDependencyContainer dependencyContainer,
			IPlayer player,
			IGameLevelEventSystem gameLevelEventSystem
		)
		{
			dependencyContainer.Player = player;
			dependencyContainer.GameLevelEventSystem = gameLevelEventSystem;
			dependencyContainer.PerformInjection();
		}
	}
}

