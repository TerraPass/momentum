using UnityEngine;
using System;

namespace Momentum.Cameras.InGame
{
	public abstract class AbstractCameraDependencyContainerComponent : MonoBehaviour, ICameraDependencyContainer
	{
		#region ICameraDependencyContainer implementation

		public abstract void PerformInjection ();

		public abstract Momentum.Gameplay.Player.IPlayer Player {get;set;}

		public abstract Momentum.Gameplay.Core.IGameLevelEventSystem GameLevelEventSystem {get;set;}

		#endregion


	}
}

