using System;

using Momentum.Gameplay.Common;

namespace Momentum.Cameras
{
	public interface ICameraController : IDisableable
	{
		ICamera Camera {get;set;}
	}

	public interface ICameraController<TCamera> : ICameraController where TCamera : ICamera
	{
		new TCamera Camera {get;set;}
	}
}

