using UnityEngine;
using System;

namespace Momentum.Cameras
{
	public interface ICamera
	{
		Transform Target {get;set;}
		/// <summary>
		/// Describes field of view for perspective cameras
		/// and orthographic size for orthographic ones.
		/// </summary>
		/// <value>FOV or orthographic size.</value>
		float FieldOfView {get;set;}
	}
}

