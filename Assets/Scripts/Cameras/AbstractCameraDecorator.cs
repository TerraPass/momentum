using UnityEngine;
using System;
using Terrapass.Debug;

namespace Momentum.Cameras
{
	public abstract class AbstractCameraDecorator : ICamera
	{
		private ICamera decoratedCamera;

		public AbstractCameraDecorator (ICamera decoratedCamera)
		{
			if(decoratedCamera == null)
			{
				throw new ArgumentNullException(
					"decoratedCamera"
				);
			}
			this.decoratedCamera = decoratedCamera;
		}

		#region ICamera implementation

		public virtual Transform Target {
			get {
				return this.decoratedCamera.Target;
			}
			set {
				this.decoratedCamera.Target = value;
			}
		}

		public virtual float FieldOfView {
			get {
				return this.decoratedCamera.FieldOfView;
			}
			set {
				this.decoratedCamera.FieldOfView = value;
			}
		}

		#endregion

		protected ICamera DecoratedCamera
		{
			get {
				DebugUtils.Assert(
					this.decoratedCamera != null,
					String.Format(
						"{0} decorator has null as its decorated camera",
						this.GetType().ToString()
					)
				);
				return this.decoratedCamera;
			}
		}
	}
}

