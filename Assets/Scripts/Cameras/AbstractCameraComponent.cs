using UnityEngine;
using System;

namespace Momentum.Cameras
{
	public abstract class AbstractCameraComponent : MonoBehaviour, ICamera
	{
		#region ICamera implementation

		public abstract Transform Target {get;set;}

		public abstract float FieldOfView {get;set;}

		#endregion


	}
}

