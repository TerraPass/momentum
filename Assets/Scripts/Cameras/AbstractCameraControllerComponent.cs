using UnityEngine;
using System;

namespace Momentum.Cameras
{
	public abstract class AbstractCameraControllerComponent : MonoBehaviour, ICameraController
	{
		ICamera ICameraController.Camera
		{
			get {
				return this.AbstractCamera;
			}
			set {
				this.AbstractCamera = value;
			}
		}

		protected abstract ICamera AbstractCamera {get;set;}

		#region IDisableable implementation

		public abstract bool Enabled {get;set;}

		#endregion
	}
}

