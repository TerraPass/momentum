using System;

using Momentum.Gameplay.Common;

namespace Momentum.Gameplay.TimeMechanic
{
	public interface ITimeController : IDisableable
	{
		ITimeMechanism TimeMechanism {get;set;}
	}
}

