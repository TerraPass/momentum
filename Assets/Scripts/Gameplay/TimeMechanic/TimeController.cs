using UnityEngine;
using System;

namespace Momentum.Gameplay.TimeMechanic
{
	public class TimeController : AbstractTimeControllerComponent
	{
		protected const string REWIND_TIME_BUTTON = "RewindTime";

		private bool isEnabled = false;

		void Start()
		{

		}

		void Update()
		{
			if(!this.isEnabled)
			{
				return;
			}
			bool rewindKeyPressed = Input.GetButton(REWIND_TIME_BUTTON);
			if(rewindKeyPressed && !this.TimeMechanism.IsInRewind)
			{
				this.TimeMechanism.StartRewind();
			}
			else if(!rewindKeyPressed && this.TimeMechanism.IsInRewind)
			{
				this.TimeMechanism.EndRewind();
			}
		}

		#region implemented abstract members of AbstractTimeControllerComponent

		public override bool Enabled {
			get {
				return this.isEnabled;
			}
			set {
				this.isEnabled = value;
			}
		}

		#endregion
	}
}

