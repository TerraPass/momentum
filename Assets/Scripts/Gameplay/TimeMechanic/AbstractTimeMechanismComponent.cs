using UnityEngine;
using System;

namespace Momentum.Gameplay.TimeMechanic
{
	using Recording;
	using Measurement;

	public abstract class AbstractTimeMechanismComponent : MonoBehaviour, ITimeMechanism
	{
		[SerializeField]
		private ILevelTimer timer = LevelTimer.Instance;
		[SerializeField]
		private AbstractRecorderComponent recorder;

		protected ILevelTimer Timer
		{
			get {
				return this.timer;
			}
		}

		protected IRecorder Recorder
		{
			get {
				return this.recorder;
			}
		}

		#region ITimeMechanism implementation

		public abstract void StartRewind ();

		public abstract void EndRewind ();

		public abstract bool IsInRewind {get;}

		public abstract event EventHandler<RewindStartedEventArgs> RewindStarted;

		public abstract event EventHandler<RewindPausedEventArgs> RewindPaused;

		public abstract event EventHandler<RewindEndedEventArgs> RewindEnded;

		public abstract event EventHandler<TimeFlowPausedEventArgs> TimeFlowPaused;

		public abstract event EventHandler<TimeFlowResumedEventArgs> TimeFlowResumed;

		public abstract bool Pause ();

		public abstract bool Resume ();

		public abstract bool IsPaused {get;}

		public abstract ILevelTimer ReadOnlyTimer {get;}

		#endregion

		#region IResettable implementation

		public abstract void Reset ();

		#endregion
	}
}

