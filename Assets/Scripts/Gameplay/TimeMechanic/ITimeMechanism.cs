using UnityEngine;
using System;

using Momentum.Gameplay.Common;

namespace Momentum.Gameplay.TimeMechanic
{
	public interface ITimeMechanism : IResettable
	{
		bool IsInRewind {get;}
		bool IsPaused {get;}

		/// <summary>
		/// Returns a read-only view of the underlying timer.
		/// </summary>
		/// <value>The read only timer.</value>
		Measurement.ILevelTimer ReadOnlyTimer {get;}

		/// <summary>
		/// Occurs whenever ITimeMechanism starts rewinding.
		/// </summary>
		event EventHandler<RewindStartedEventArgs> RewindStarted;
		/// <summary>
		/// Occurs whenever ITimeMechanism pauses rewinding without resuming normal flow of time.
		/// (Possibly due to reaching the beginning of recorded time.)
		/// </summary>
		event EventHandler<RewindPausedEventArgs> RewindPaused;
		/// <summary>
		/// Occurs whenever ITimeMechanism ends rewinding and resumes normal flow of time.
		/// </summary>
		event EventHandler<RewindEndedEventArgs> RewindEnded;
		/// <summary>
		/// Occurs whenever ITimeMechanism pauses the flow of time, except when RewindPaused occurs.
		/// (This event is raised when time mechanism is NOT in rewind and an effective call to Pause() was made.)
		/// </summary>
		event EventHandler<TimeFlowPausedEventArgs> TimeFlowPaused;
		/// <summary>
		/// Occurs whenever ITimeMechanism resumes the flow of time, except when RewindEnded occurs.
		/// (This event is raised when time mechanism is NOT in rewind and an effective call to Resume() is made.)
		/// </summary>
		event EventHandler<TimeFlowResumedEventArgs> TimeFlowResumed;

		void StartRewind();
		void EndRewind();

		/// <summary>
		/// Pause the flow of time, unless already paused.
		/// If the flow of time is already paused, the call has no effect.
		/// </summary>
		/// <returns><code>true</code> if the call had effect</returns>
		bool Pause();
		/// <summary>
		/// Resume the flow of time, unless already resumed.
		/// If the flow of time is already resumed, the call has no effect.
		/// </summary>
		/// <returns><code>true</code> if the call had effect</returns>
		bool Resume();
	}
}

