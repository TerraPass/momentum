using System;

using Momentum.Gameplay.TimeMechanic.Recording;

namespace Momentum.Gameplay.TimeMechanic
{
	public abstract class TimeMechanismEventArgs : EventArgs
	{
		private readonly ITimeMechanism timeMechanism;

		public TimeMechanismEventArgs(ITimeMechanism timeMechanism)
		{
			this.timeMechanism = timeMechanism;
		}

		public ITimeMechanism TimeMechanism
		{
			get {
				return this.timeMechanism;
			}
		}
	}

	public class RewindStartedEventArgs : TimeMechanismEventArgs
	{
		public RewindStartedEventArgs(ITimeMechanism timeMechanism)
			: base(timeMechanism)
		{

		}
	}

	public class RewindEndedEventArgs : TimeMechanismEventArgs
	{
		public RewindEndedEventArgs(ITimeMechanism timeMechanism)
			: base(timeMechanism)
		{

		}
	}

	public class RewindPausedEventArgs : TimeMechanismEventArgs
	{
		public RewindPausedEventArgs(ITimeMechanism timeMechanism)
			: base(timeMechanism)
		{

		}
	}

	public class TimeFlowPausedEventArgs : TimeMechanismEventArgs
	{
		public TimeFlowPausedEventArgs(ITimeMechanism timeMechanism)
			: base(timeMechanism)
		{

		}
	}

	public class TimeFlowResumedEventArgs : TimeMechanismEventArgs
	{
		public TimeFlowResumedEventArgs(ITimeMechanism timeMechanism)
			: base(timeMechanism)
		{

		}
	}

//	public class RewindInterruptedEventArgs : TimeMechanismEventArgs
//	{
//		private readonly HistoryLookupFailedException exception;
//
//		public RewindInterruptedEventArgs(
//			ITimeMechanism timeMechanism,
//			HistoryLookupFailedException exception
//		) : base(timeMechanism)
//		{
//			this.exception = exception;
//		}
//
//		public RewindInterruptedEventArgs(ITimeMechanism timeMechanism)
//			: this(timeMechanism, null)
//		{
//
//		}
//
//		public HistoryLookupFailedException Exception
//		{
//			get {
//				return this.exception;
//			}
//		}
//
//		public HistoryLookupError Reason
//		{
//			get {
//				return this.exception != null 
//					? this.exception.Reason
//					: HistoryLookupError.NONE;
//			}
//		}
//	}
}
