using UnityEngine;
using System;
using System.Linq;

namespace Momentum.Gameplay.TimeMechanic.Recording
{
	/// <summary>
	/// This implementation of history deletes previously recorded entries,
	/// located between new entry and the last recorded entry.
	/// I.e. already recorded bits are completely rewritten.
	/// </summary>
	internal class RewritableInterpolatedHistory : InterpolatedHistory
	{
		private float? lastRecordedKey = null;
		
		public RewritableInterpolatedHistory(StateInterpolationAlgorithm interpolationAlgorithm = null)
			: base(interpolationAlgorithm)
		{

		}

		protected override InterpolatedHistory NewEmptyCopy()
		{
			return new RewritableInterpolatedHistory(this.interpolationAlgorithm);
		}

		public override IHistoryRecord this[float key] {
			get {
				return base[key];
			}
			set {
				base[key] = value;
				if(this.lastRecordedKey != null)
				{
					this.EraseHistoryBetween(
						Mathf.Min((float)this.lastRecordedKey, key),
						Mathf.Max((float)this.lastRecordedKey, key)
					);
				}
				this.lastRecordedKey = key;
			}
		}

		/// <summary>
		/// Erases the history between start and end.
		/// </summary>
		/// <param name="start">Starting key.</param>
		/// <param name="end">Ending key.</param>
		/// <param name="inclusive">If set to <c>true</c>, erase start and end as well.</param>
		private void EraseHistoryBetween(float start, float end, bool inclusive = false)
		{
			if(start > end)
			{
				throw new ArgumentException(
					String.Format(
						"start parameter must not be bigger than end parameter; got {0} and {1}",
						start,
						end
					),
					"start"
				);
			}
			if(inclusive && start == end)
			{
				return;
			}
			// Converting to float[] to allow for later removal from this.historyPoints,
			// without this conversion an InvalidOperationException occurs in foreach.
			float[] keys = (from key in this.historyPoints.Keys
				where (
					inclusive 
		       			? (key >= start && key <= end)
		       			: (key > start && key < end)
				)
                select key).ToArray();

			foreach(float key in keys)
			{
				this.historyPoints.Remove(key);
			}
		}
	}
}

