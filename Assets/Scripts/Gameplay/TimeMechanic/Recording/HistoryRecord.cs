using UnityEngine;
using System;

namespace Momentum.Gameplay.TimeMechanic.Recording
{
	public class HistoryRecord : IHistoryRecord
	{
		private ITransformSnapshot transformSnapshot;
		private object stateObject;

		public HistoryRecord(ITransformSnapshot transformSnapshot, object stateObject = null)
		{
			this.transformSnapshot = transformSnapshot;
			this.stateObject = stateObject;
		}

		public HistoryRecord(Vector3 position, Quaternion rotation, Vector3 scale, object stateObject = null)
			: this(new TransformSnapshot(position, rotation, scale), stateObject)
		{

		}

		public HistoryRecord(Transform transform, object stateObject = null)
			: this(new TransformSnapshot(transform), stateObject)
		{

		}

		public ITransformSnapshot TransformSnapshot
		{
			get {
				return this.transformSnapshot;
			}
			set {
				this.transformSnapshot = value;
			}
		}

		public object StateObject
		{
			get {
				return this.stateObject;
			}
			set {
				this.stateObject = value;
			}
		}
	}
}

