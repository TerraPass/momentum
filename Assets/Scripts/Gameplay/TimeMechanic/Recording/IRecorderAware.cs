using System;

namespace Momentum.Gameplay.TimeMechanic.Recording
{
	public interface IRecorderAware
	{
		IRecorder Recorder {get;set;}
	}
}

