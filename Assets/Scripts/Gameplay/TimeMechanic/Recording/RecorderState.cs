using System;

namespace Momentum.Gameplay.TimeMechanic.Recording
{
	[Flags]
	public enum RecorderState
	{
		NONE = 0,
		PLAYBACK = 1,
		RECORDING = 2
		//REWIND = 4
	}	
}

