using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Terrapass.Debug;

using Momentum.Utils.Time;
using Momentum.Gameplay.TimeMechanic.Measurement;

namespace Momentum.Gameplay.TimeMechanic.Recording {	

	public class Recorder : AbstractRecorderComponent {
		/**
		 * Interval in seconds between taking snapshots of registered recordables.
		 */
		private const float SNAPSHOT_INTERVAL = 0.25f;

		private IDictionary<IRecordable, IHistory> history = new Dictionary<IRecordable, IHistory>();
		private ICollection<IRecordable> suspendedRecordables = new HashSet<IRecordable>();
		private RecorderState state;
		private IResettableTimer recordingTimer;

		/// <summary>
		/// Used to track scale changes and raise ScaleChanged events accordingly.
		/// </summary>
		private float lastKnownTimerScale = 0;

		// Use this for initialization
		void Start()
		{
			this.state = RecorderState.RECORDING;
			this.recordingTimer = new ResettableExecutionTimer(false);
		}

		void FixedUpdate()
		{
			if(this.Timer == null)
			{
				Debug.LogWarning("Recorder has no Timer set on a call to FixedUpdate(); skipping FixedUpdate()");
				return;
			}

			this.HandleRecording();
		}

		void Update()
		{
			if(this.Timer == null)
			{
				Debug.LogWarning("Recorder has no Timer set on a call to Update(); skipping Update()");
				return;
			}

			// Check if underlying timer's scale has changed and raise ScaleChanged event accordingly.
			if(this.Timer.Scale != this.lastKnownTimerScale)
			{
				this.ScaleChanged(
					this,
					new ScaleChangedEventArgs(
						this,
						this.lastKnownTimerScale,
						this.Timer.Scale
					)
				);
				this.lastKnownTimerScale = this.Timer.Scale;
			}

			this.HandlePlayback();
		}

		private void HandleRecording()
		{
			if((this.State & RecorderState.RECORDING) != 0)
			{
				if(this.recordingTimer.ElapsedSeconds > SNAPSHOT_INTERVAL)
				{
					this.TakeSnapshots();
					this.recordingTimer.Reset(false);
				}
			}
		}

		private void HandlePlayback()
		{
			if((this.State & RecorderState.PLAYBACK) != 0)
			{
				this.PlaybackSnapshots();
			}
		}

		protected void TakeSnapshots()
		{
			DebugUtils.Assert(
				this.Timer != null,
				"Recorder's Timer is null on call to TakeSnapshots()"
			);

			// Get current time from internal level timer
			float currentTime = this.Timer.ElapsedSeconds;
			// Create a new history record for every observed recordable
			foreach(var kvp in this.history)
			{
				// Skip suspended recordables
				if(this.suspendedRecordables.Contains(kvp.Key))
				{
					continue;
				}

				IRecordable recordable = kvp.Key;
				MonoBehaviour ramb = recordable as MonoBehaviour;
				IHistory recordedHistory = kvp.Value;

				// If recordable is a MonoBehaviour, record its transform as well
				IHistoryRecord snapshot = (ramb == null
					? new HistoryRecord(new TransformSnapshot())
					: new HistoryRecord(ramb.transform));

				// Ask recordable to provide its snapshot
				recordable.RecordFrame(this, ref snapshot);

				if(snapshot != null)
				{
					recordedHistory[currentTime] = snapshot;
				}
			}
		}

		protected void PlaybackSnapshots()
		{
			DebugUtils.Assert(
				this.Timer != null,
				"Recorder's Timer is null on call to PlaybackSnapshots()"
			);

			// Get current time from internal level timer
			float currentTime = this.Timer.ElapsedSeconds;
			// Invoke PlayFrame() of each observed recordable with current frame
			foreach(var kvp in this.history)
			{
				IRecordable recordable = kvp.Key;
				IHistory recordedHistory = kvp.Value;

				try
				{
					recordable.PlayFrame(this, recordedHistory[currentTime]);
				}
				catch(TooEarlyInHistoryException e)
				{
					var eventArgs = new StartOfHistoryReachedEventArgs(
						this,
						recordable,
						e
					);
					this.HistoryLookupFailed(this, eventArgs);
					this.StartOfHistoryReached(this, eventArgs);
				}
				catch(TooLateInHistoryException e)
				{
					var eventArgs = new EndOfHistoryReachedEventArgs(
						this,
						recordable,
						e
					);
					this.HistoryLookupFailed(this, eventArgs);
					this.EndOfHistoryReached(this, eventArgs);
				}
				catch(HistoryLookupFailedException e)
				{
					if(e.Reason == HistoryLookupError.NOT_ENOUGH_POINTS)
					{
						Debug.LogWarning(e.Message);
					}
					else
					{
						Debug.LogException(e);
					}
					this.HistoryLookupFailed(
						this,
						new HistoryLookupFailedEventArgs(
							this,
							recordable,
							e
						)
					);
				}
			}
		}

		#region IRecorder implementation

		public override event EventHandler<RecordingResumedEventArgs> RecordingResumed 	= delegate {};
		
		public override event EventHandler<RecordingPausedEventArgs> RecordingPaused 	= delegate {};
		
		public override event EventHandler<PlaybackResumedEventArgs> PlaybackResumed 	= delegate {};
		
		public override event EventHandler<PlaybackPausedEventArgs> PlaybackPaused 		= delegate {};

		public override event EventHandler<ScaleChangedEventArgs> ScaleChanged			= delegate {};

		public override event EventHandler<HistoryLookupFailedEventArgs> HistoryLookupFailed = delegate {};
		
		public override event EventHandler<StartOfHistoryReachedEventArgs> StartOfHistoryReached = delegate {};
		
		public override event EventHandler<EndOfHistoryReachedEventArgs> EndOfHistoryReached = delegate {};

		public override void UnregisterRecordable(IRecordable recordable)
		{
			if(!this.history.ContainsKey(recordable))
			{
				Debug.LogWarning(
					"Attempted to unregister a non-registered recordable; this call to UnregisterRecordable() will be ignored"
				);
				return;
			}

			this.history.Remove(recordable);
		}

		public override void SuspendRecordable (IRecordable recordable)
		{
			this.suspendedRecordables.Add(recordable);
		}

		public override void UnsuspendRecordable (IRecordable recordable)
		{
			this.suspendedRecordables.Remove(recordable);
		}

		public override void RegisterRecordable (IRecordable recordable, IHistory history)
		{
			// Check for duplicate recordable
			if(this.history.ContainsKey(recordable))
			{
				Debug.LogWarning(
					"Attempted to register an already registered recordable; this call to RegisterRecordable() will be ignored"
				);
				return;
			}

			this.history.Add(recordable, history);
		}

		public override RecordableStatus GetRecordableStatus (IRecordable recordable)
		{
			if(this.history.ContainsKey(recordable))
			{
				if(this.suspendedRecordables.Contains(recordable))
				{
					return RecordableStatus.SUSPENDED;
				}
				else
				{
					return RecordableStatus.ACTIVE;
				}
			}
			else
			{
				return RecordableStatus.NOT_REGISTERED;
			}
		}

		public override RecorderState State {
			get {
				return this.state;
			}
			set {
				RecorderState oldValue = this.state;
				this.state = value;
				// Check if flags were changed and invoke events accordingly
				if(((oldValue ^ value) & RecorderState.RECORDING) != 0)
				{
					if((value & RecorderState.RECORDING) != 0)
					{
						this.RecordingResumed(this, new RecordingResumedEventArgs(this));
					}
					else
					{
						this.RecordingPaused(this, new RecordingPausedEventArgs(this));
					}
				}
				if(((oldValue ^ value) & RecorderState.PLAYBACK) != 0)
				{
					if((value & RecorderState.PLAYBACK) != 0)
					{
						this.PlaybackResumed(this, new PlaybackResumedEventArgs(this, this.GetTimerScale()));
					}
					else
					{
						this.PlaybackPaused(this, new PlaybackPausedEventArgs(this));
					}
				}
			}
		}

		#endregion
	}
}
