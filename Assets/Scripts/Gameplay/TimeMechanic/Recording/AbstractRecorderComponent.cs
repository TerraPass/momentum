using UnityEngine;
using System;
using Momentum.Gameplay.TimeMechanic.Measurement;

namespace Momentum.Gameplay.TimeMechanic.Recording
{
	public abstract class AbstractRecorderComponent : MonoBehaviour, IRecorder
	{
		private ILevelTimer levelTimer;

		public ILevelTimer Timer
		{
			get {
				return this.levelTimer;
			}
			set {
				this.levelTimer = value;
			}
		}

		#region IRecorder implementation

		public abstract event EventHandler<RecordingResumedEventArgs> RecordingResumed;

		public abstract event EventHandler<RecordingPausedEventArgs> RecordingPaused;

		public abstract event EventHandler<PlaybackResumedEventArgs> PlaybackResumed;

		public abstract event EventHandler<PlaybackPausedEventArgs> PlaybackPaused;

		public abstract event EventHandler<ScaleChangedEventArgs> ScaleChanged;

		public abstract event EventHandler<HistoryLookupFailedEventArgs> HistoryLookupFailed;

		public abstract event EventHandler<StartOfHistoryReachedEventArgs> StartOfHistoryReached;

		public abstract event EventHandler<EndOfHistoryReachedEventArgs> EndOfHistoryReached;

		public abstract void RegisterRecordable (IRecordable recordable, IHistory history);

		public abstract void SuspendRecordable (IRecordable recordable);

		public abstract void UnsuspendRecordable (IRecordable recordable);

		public abstract void UnregisterRecordable (IRecordable recordable);

		public abstract RecordableStatus GetRecordableStatus (IRecordable recordable);

		public abstract RecorderState State
		{
			get;
			set;
		}
		#endregion


	}
}
