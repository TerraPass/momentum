using UnityEngine;
using System;

namespace Momentum.Gameplay.TimeMechanic.Recording
{
	/// <summary>
	/// This enumeration describes the status of an IRecordable with respect to a certain IRecorder.
	/// </summary>
	public enum RecordableStatus
	{
		/// <summary>
		/// This IRecordable is not registered with this IRecorder.
		/// </summary>
		NOT_REGISTERED 	= 0,
		/// <summary>
		/// This IRecordable is registered with this IRecorder but is currently suspended from recording.
		/// </summary>
		SUSPENDED 		= 1,
		/// <summary>
		/// This IRecordable is registered with this IRecorder and is not currently suspended from recording.
		/// </summary>
		ACTIVE 			= 2
	}
}

