using UnityEngine;
using System;

namespace Momentum.Gameplay.TimeMechanic.Recording
{
	public struct TransformSnapshot : ITransformSnapshot
	{
		private Vector3 position;
		private Quaternion rotation;
		private Vector3 scale;

		#region IHistoryRecord implementation
		public Vector3 Position {
			get {
				return this.position;
			}
			set {
				this.position = value;
			}
		}
		public Quaternion Rotation {
			get {
				return this.rotation;
			}
			set {
				this.rotation = value;
			}
		}
		public Vector3 Scale {
			get {
				return this.scale;
			}
			set {
				this.scale = value;
			}
		}
		#endregion

		public TransformSnapshot(Vector3 position, Quaternion rotation, Vector3 scale)
		{
			this.position = position;
			this.rotation = rotation;
			this.scale = scale;
		}

		public TransformSnapshot(Transform transform)
			: this(transform.position, transform.rotation, transform.localScale)
		{

		}
	}
}

