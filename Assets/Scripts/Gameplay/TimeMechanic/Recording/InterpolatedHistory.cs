using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Terrapass.Debug;

namespace Momentum.Gameplay.TimeMechanic.Recording
{
	public delegate object StateInterpolationAlgorithm(
		object earlierState, 
		object laterState, 
		float interpolationAmount
	);

	internal abstract class InterpolatedHistory : IHistory
	{
		/// <summary>
		/// Takes earlier state regardless of interpolation amount.
		/// </summary>
		/// <returns>Earlier state.</returns>
		/// <param name="earlierState">Earlier state.</param>
		/// <param name="laterState">Later state (ignored).</param>
		/// <param name="interpolationAmount">Interpolation amount (ignored).</param>
		protected static object DefaultInterpolationAlgorithm(
			object earlierState, 
			object laterState, 
			float interpolationAmount
		)
		{
			return earlierState;
		}

		protected IDictionary<float, IHistoryRecord> historyPoints;
		protected StateInterpolationAlgorithm interpolationAlgorithm;

		/// <summary>
		/// This is used in lieu of a null check for KVPs.
		/// </summary>
		private static readonly KeyValuePair<float, IHistoryRecord> NOT_AN_ENTRY = default(KeyValuePair<float, IHistoryRecord>);
		
		/// <summary>
		/// Initializes a new instance of the <see cref="Momentum.Gameplay.TimeMechanic.Recording.InterpolatedHistory"/> class.
		/// </summary>
		/// <param name="interpolationAlgorithm">
		/// Interpolation algorithm to use or null.
		/// If null is passed as this parameter's value, default "take the earlier state" algorithm will be used.
		/// </param>
		public InterpolatedHistory(StateInterpolationAlgorithm interpolationAlgorithm = null)
		{
			this.historyPoints = new SortedDictionary<float, IHistoryRecord>();
			this.interpolationAlgorithm = interpolationAlgorithm != null
				? interpolationAlgorithm
				: DefaultInterpolationAlgorithm;
		}

		#region IHistory implementation

		public IHistory Clone ()
		{
			return this.ClonePartially(float.MinValue, float.MaxValue);
		}

		public IHistory ClonePartially (float from, float till)
		{
			if(till < from)
			{
				throw new ArgumentException(
					string.Format(
						"{0}.ClonePartially(): till must not be less than from, got from={1}, till={2}",
						this.GetType().ToString(),
						from,
						till
					),
					"till"
				);
			}

			InterpolatedHistory clone = this.NewEmptyCopy();
			this.CopyHistoryPointsTo(clone, from, till);
			clone.interpolationAlgorithm = this.interpolationAlgorithm;
			return clone;
		}

		/// <summary>
		/// Virtual quasi-copy constructor.
		/// Creates a copy of the current instance without any history points.
		/// Subclasses should implement this method to return an instance of a corresponding concrete type.
		/// </summary>
		/// <returns>Copy of the current instance without any history points.</returns>
		protected abstract InterpolatedHistory NewEmptyCopy();

		/// <summary>
		/// Copies history points from one InterpolatedHistory instance to another.
		/// </summary>
		/// <param name="other">Other instance.</param>
		/// <param name="startTime">Entries before this point in time will not be copied.</param>
		/// <param name="endTime">Entries after and including this point in time will not be copied.</param>
		/// <param name="addInferredPoints">
		/// If true, inferred history points, obtained by interpolation, will be added to both the
		/// beginning and the end of copied fragment.
		/// </param>
		protected void CopyHistoryPointsTo(
			InterpolatedHistory other, 
			float startTime = float.MinValue,
			float endTime = float.MaxValue,
			bool addInferredPoints = true
		)
		{
			// This routine is protected and so input to it must be sanitized by public routines
			DebugUtils.Assert(
				startTime <= endTime,
				string.Format(
					"{0}.CopyHistoryPointsTo(): endTime must not be less than startTime, got startTime={1}, endTime={2}",
					this.GetType().ToString(),
					startTime.ToString(),
					endTime.ToString()
				)
			);

			// The last entry before startTime
			KeyValuePair<float, IHistoryRecord> precedingEntry = NOT_AN_ENTRY;
			// The first entry after endTime
			KeyValuePair<float, IHistoryRecord> followingEntry = NOT_AN_ENTRY;
			foreach(var entry in this.historyPoints)
			{
				// Assuming entries are sorted by time in ascending order
				if(entry.Key < startTime)
				{
					precedingEntry = entry;
					continue;
				}
				if(entry.Key >= endTime)
				{
					followingEntry = entry;
					break;
				}
				other.historyPoints[entry.Key] = new HistoryRecord(
					entry.Value.TransformSnapshot,
					entry.Value.StateObject
				);
			}
			// If needed, add inferred starting and ending points
			if(addInferredPoints)
			{
				// Infer starting point
				if(!precedingEntry.Equals(NOT_AN_ENTRY) 
                	&& !other.historyPoints.ContainsKey(startTime))
				{
					float interpolationAmount = 
						(startTime - precedingEntry.Key)
							/(other.historyPoints.Keys.First() - precedingEntry.Key);
					other.historyPoints[startTime] = this.InterpolateBetween(
						precedingEntry.Value,
						other.historyPoints.First().Value,
						interpolationAmount
					);
				}
				// Infer ending point
				// A check for other.historyPoints non-emptiness is needed
				// to prevent InvalidOperationException on a call to Last()
				if(!followingEntry.Equals(NOT_AN_ENTRY) && other.historyPoints.Count() > 0)
				{
					float interpolationAmount = 
						(endTime - other.historyPoints.Keys.Last())
							/(followingEntry.Key - other.historyPoints.Keys.Last());
					other.historyPoints[endTime] = this.InterpolateBetween(
						other.historyPoints.Last().Value,
						followingEntry.Value,
						interpolationAmount
					);
				}
			}
		}

		#endregion

		public ICollection<float> Keys
		{
			get {
				return this.historyPoints.Keys;
			}
		}
		
		public ICollection<IHistoryRecord> Values
		{
			get {
				return this.historyPoints.Values;
			}
		}
		
		public virtual IHistoryRecord this [float key]
		{
			get
			{
				try
				{
					return this.historyPoints[key];
				}
				catch(KeyNotFoundException /*e*/)
				{
					if(this.historyPoints.Count < 2)
					{
						throw new NotEnoughHistoryPointsException(key);
					}
					else if(key < this.historyPoints.Keys.First())
					{
						throw new TooEarlyInHistoryException(
							key,
							this.historyPoints.Keys.First(),
							this.historyPoints.Keys.Last()
						);
					}
					else if(key > this.historyPoints.Keys.Last())
					{
						throw new TooLateInHistoryException(
							key,
							this.historyPoints.Keys.First(),
							this.historyPoints.Keys.Last()
						);
					}
					else
					{
						// Select KVPs by 2 keys closest to the one passed
						var kvps = (from kvp in this.historyPoints
						              orderby Mathf.Abs(kvp.Key - key) ascending
						              select kvp).Take(2).OrderBy(kvp => kvp.Key);
						var keys = from kvp in kvps select kvp.Key; 
						var points = from kvp in kvps select kvp.Value;

						float interpolationAmount = (key - keys.First())/(keys.Last() - keys.First());

						return this.InterpolateBetween(points.First(), points.Last(), interpolationAmount);
					}
				}
			}
			
			set
			{
				this.historyPoints[key] = value;
			}
		}

		private IHistoryRecord InterpolateBetween(
			IHistoryRecord recordA, 
			IHistoryRecord recordB, 
			float interpolationAmount
		)
		{
			return new HistoryRecord(
				new TransformSnapshot(
					Vector3.Lerp(
						recordA.TransformSnapshot.Position, 
						recordB.TransformSnapshot.Position,
						interpolationAmount
					),
					Quaternion.Lerp(
						recordA.TransformSnapshot.Rotation,
						recordB.TransformSnapshot.Rotation, 
						interpolationAmount
					),
					Vector3.Lerp(
						recordA.TransformSnapshot.Scale, 
						recordB.TransformSnapshot.Scale, 
						interpolationAmount
					)
				),
				this.interpolationAlgorithm(
					recordA.StateObject,
					recordB.StateObject,
					interpolationAmount
				)
			);
		}

		public void Add (float key, IHistoryRecord value)
		{
			this.historyPoints.Add(key, value);
		}
		
		public bool ContainsKey (float key)
		{
			return key >= this.Keys.First() && key <= this.Keys.Last();
		}
		
		public bool Remove (float key)
		{
			return this.historyPoints.Remove(key);
		}
		
		public bool TryGetValue (float key, out IHistoryRecord value)
		{
			try
			{
				value = this[key];
				return true;
			}
			catch(KeyNotFoundException)
			{
				value = default(HistoryRecord);
				return false;
			}
		}
		
		#region ICollection implementation
		
		public void Add (KeyValuePair<float, IHistoryRecord> item)
		{
			this.historyPoints.Add(item);
		}
		
		public void Clear ()
		{
			this.historyPoints.Clear();
		}
		
		public bool Contains (KeyValuePair<float, IHistoryRecord> item)
		{
			return this.historyPoints.Contains(item);
		}
		
		public void CopyTo (KeyValuePair<float, IHistoryRecord>[] array, int arrayIndex)
		{
			this.historyPoints.CopyTo(array, arrayIndex);
		}
		
		public bool Remove (KeyValuePair<float, IHistoryRecord> item)
		{
			return this.historyPoints.Remove(item);
		}
		
		public int Count {
			get {
				return this.historyPoints.Count;
			}
		}
		
		public bool IsReadOnly {
			get {
				return this.historyPoints.IsReadOnly;
			}
		}
		
		#endregion
		
		#region IEnumerable implementation
		
		public IEnumerator<KeyValuePair<float, IHistoryRecord>> GetEnumerator ()
		{
			return this.historyPoints.GetEnumerator();
		}
		
		#endregion
		
		#region IEnumerable implementation
		
		IEnumerator IEnumerable.GetEnumerator ()
		{
			return (this.historyPoints as IEnumerable).GetEnumerator();
		}
		
		#endregion
	}
}
