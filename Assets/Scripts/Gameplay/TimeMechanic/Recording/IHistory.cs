using System;
using System.Collections.Generic;

namespace Momentum.Gameplay.TimeMechanic.Recording
{
	public interface IHistory : IDictionary<float, IHistoryRecord>
	{
		/// <summary>
		/// Takes and returns a deep copy of this IHistory (for example for it to be used for a different recordable).
		/// </summary>
		/// <returns>A deep copy of this IHistory instance.</returns>
		IHistory Clone();

		/// <summary>
		/// Return an IHistory instance, which represents a subset of this history,
		/// contained between two timestamps.
		/// </summary>
		/// <returns>A deep copy of a part of this IHistory instance.</returns>
		/// <param name="from">Entries before this point in time will not be copied.</param>
		/// <param name="till">Entries after and including this point in time will not be copied.</param>
		IHistory ClonePartially(float from, float till);
	}
}
