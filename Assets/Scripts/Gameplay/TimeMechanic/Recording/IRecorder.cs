using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Momentum.Gameplay.TimeMechanic.Measurement;

namespace Momentum.Gameplay.TimeMechanic.Recording
{
	public interface IRecorder
	{
		/// <summary>
		/// Occurs whenever recording gets resumed by State being set 
		/// or a call to StartRecording() extension method.
		/// 
		/// Does NOT get raised if underlying timer's scale is changed from 0 to another value
		/// (ScaleChanged event is raised in that case).
		/// </summary>
		event EventHandler<RecordingResumedEventArgs> RecordingResumed;
		/// <summary>
		/// Occurs whenever recording gets paused by State being set 
		/// or a call to StopRecording() extension method.
		/// 
		/// Does NOT get raised if underlying timer's scale is set to 0
		/// (ScaleChanged event is raised in that case).
		/// </summary>
		event EventHandler<RecordingPausedEventArgs> RecordingPaused;
		/// <summary>
		/// Occurs whenever playback gets resumed by State being set 
		/// or a call to StartPlayback() extension method.
		/// 
		/// Does NOT get raised if underlying timer's scale is changed from 0 to another value
		/// (ScaleChanged event is raised in that case).
		/// </summary>
		event EventHandler<PlaybackResumedEventArgs> PlaybackResumed;
		/// <summary>
		/// Occurs whenever playback gets paused by State being set 
		/// or a call to StopPlayback() extension method.
		/// 
		/// Does NOT get raised if underlying timer's scale is set to 0
		/// (ScaleChanged event is raised in that case).
		/// </summary>
		event EventHandler<PlaybackPausedEventArgs> PlaybackPaused;
		/// <summary>
		/// Occurs whenever underlyiing timer's scale (i.e. playback speed) gets changed.
		/// </summary>
		event EventHandler<ScaleChangedEventArgs> ScaleChanged;
		/// <summary>
		/// Occurs whenever IRecorder fails to lookup a certain point in time 
		/// in the history of a certain recordable.
		/// </summary>
		/// <remarks>
		/// This event may be accompanied by StartOfHistoryReached or EndOfHistoryReached events.
		/// </remarks>
		event EventHandler<HistoryLookupFailedEventArgs> HistoryLookupFailed;
		/// <summary>
		/// Occurs whenever IRecorder fails to lookup a point in time in the history of
		/// a certain recordable because this point in time is earlier than the beginning of recorded history.
		/// </summary>
		/// <remarks>
		/// This event is always accompanied by HistoryLookupFailed event.
		/// </remarks>
		event EventHandler<StartOfHistoryReachedEventArgs> StartOfHistoryReached;
		/// <summary>
		/// Occurs whenever IRecorder fails to lookup a point in time in the history of
		/// a certain recordable because this point in time is later than the end of recorded history.
		/// </summary>
		/// <remarks>
		/// This event is always accompanied by HistoryLookupFailed event.
		/// </remarks>
		event EventHandler<EndOfHistoryReachedEventArgs> EndOfHistoryReached;

		RecorderState State {get;set;}

		/// <summary>
		/// Registers recordable with this IRecorder with an IHistory instance,
		/// which will be used for recording.
		/// </summary>
		/// <param name="recordable">Recordable.</param>
		/// <param name="existingHistory">IHistory instance to be used for recording.</param>
		void RegisterRecordable(IRecordable recordable, IHistory history);
		void SuspendRecordable(IRecordable recordable);
		void UnsuspendRecordable(IRecordable recordable);
		void UnregisterRecordable(IRecordable recordable);

		/// <summary>
		/// Use this method to check whether an IRecordable has been registered
		/// with this IRecorder ant whether it is currently suspended from recording.
		/// </summary>
		/// <remarks>
		/// Returned <see cref="RecordableStatus"/> value is NOT a bitfield, but one of three distinct values.
		/// </remarks>
		/// <returns>Corresponding <see cref="RecordableStatus"/> value.</returns>
		/// <param name="recordable">Recordable.</param>
		RecordableStatus GetRecordableStatus(IRecordable recordable);

		ILevelTimer Timer {get;set;}
	}

	public static class IRecorderExtensions
	{
		/// <summary>
		/// Registers recordable with this IRecorder 
		/// with an empty <see cref="T:Momentum.Gameplay.TimeMechanic.Recording.SilentNonRewritableInterpolatedHistory"/>.
		/// </summary>
		/// <param name="recordable">Recordable.</param>
		/// <param name="interpolationAlgorithm">
		/// Interpolation algorithm to be used for recordable's state playback. 
		/// If omitted, the default algorithm will be used.
		/// </param>
		public static void RegisterRecordable(
			this IRecorder recorder, 
			IRecordable recordable, 
			StateInterpolationAlgorithm interpolationAlgorithm = null
		) {
			IHistory history = interpolationAlgorithm == null
				? new SilentNonRewritableInterpolatedHistory()
				: new SilentNonRewritableInterpolatedHistory(interpolationAlgorithm);

			recorder.RegisterRecordable(recordable, history);
		}

		public static void StartRecording(this IRecorder recorder)
		{
			recorder.State |= RecorderState.RECORDING;
		}

		public static void StopRecording(this IRecorder recorder)
		{
			recorder.State &= ~RecorderState.RECORDING;
		}

		public static void StartPlayback(this IRecorder recorder)
		{
			recorder.State |= RecorderState.PLAYBACK;
		}

		public static void StopPlayback(this IRecorder recorder)
		{
			recorder.State &= ~RecorderState.PLAYBACK;
		}

		public static bool IsPlaying(this IRecorder recorder)
		{
			return (recorder.State & RecorderState.PLAYBACK) != 0;
		}

		public static bool IsPlayingForward(this IRecorder recorder)
		{
			return recorder.IsPlaying() && (recorder.GetTimerScale() > 0);
		}

		public static bool IsPlayingBackward(this IRecorder recorder)
		{
			return recorder.IsPlaying() && (recorder.GetTimerScale() < 0);
		}

		public static float GetCurrentTime(this IRecorder recorder)
		{
			return recorder.Timer.ElapsedSeconds;
		}

		public static float GetTimerScale(this IRecorder recorder)
		{
			return recorder.Timer.Scale;
		}
	}
}

