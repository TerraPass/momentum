using System;

namespace Momentum.Gameplay.TimeMechanic.Recording
{
	public abstract class RecordingEventArgs : EventArgs
	{
		private readonly IRecorder recorder;

		public RecordingEventArgs(IRecorder recorder)
		{
			this.recorder = recorder;
		}

		public IRecorder Recorder
		{
			get {
				return this.recorder;
			}
		}
	}

	public class RecordingResumedEventArgs : RecordingEventArgs
	{
		public RecordingResumedEventArgs(IRecorder recorder) : base(recorder) {}
	}

	public class RecordingPausedEventArgs : RecordingEventArgs
	{
		public RecordingPausedEventArgs(IRecorder recorder) : base(recorder) {}
	}

	public class PlaybackResumedEventArgs : RecordingEventArgs
	{
		private readonly float scale;

		public PlaybackResumedEventArgs(IRecorder recorder, float scale) 
			: base(recorder)
		{
			this.scale = scale;
		}

		public float Scale
		{
			get {
				return this.scale;
			}
		}
	}

	public class PlaybackPausedEventArgs : RecordingEventArgs
	{
		public PlaybackPausedEventArgs(IRecorder recorder) : base(recorder) {}
	}

	public class ScaleChangedEventArgs : RecordingEventArgs
	{
		private readonly float newScale;
		private readonly float oldScale;
		
		public ScaleChangedEventArgs(IRecorder recorder, float oldScale, float newScale) 
			: base(recorder)
		{
			this.newScale = newScale;
			this.oldScale = oldScale;
		}

		public float NewScale
		{
			get {
				return this.newScale;
			}
		}

		public float OldScale
		{
			get {
				return this.oldScale;
			}
		}
	}

	public class HistoryLookupFailedEventArgs : RecordingEventArgs
	{
		private readonly IRecordable recordable;
		private readonly HistoryLookupFailedException exception;

		public HistoryLookupFailedEventArgs(
			IRecorder recorder, 
			IRecordable recordable, 
			HistoryLookupFailedException exception
		) : base(recorder)
		{
			this.recordable = recordable;
			this.exception = exception;
		}

		public IRecordable Recordable
		{
			get {
				return this.recordable;
			}
		}

		public HistoryLookupFailedException Exception
		{
			get {
				return this.exception;
			}
		}

		public HistoryLookupError Reason
		{
			get {
				return this.exception.Reason;
			}
		}
	}

	public class StartOfHistoryReachedEventArgs : HistoryLookupFailedEventArgs
	{
		public StartOfHistoryReachedEventArgs(
			IRecorder recorder,
			IRecordable recordable,
			TooEarlyInHistoryException exception
		) : base(recorder, recordable, exception)
		{

		}
	}

	public class EndOfHistoryReachedEventArgs : HistoryLookupFailedEventArgs
	{
		public EndOfHistoryReachedEventArgs(
			IRecorder recorder,
			IRecordable recordable,
			TooLateInHistoryException exception
		) : base(recorder, recordable, exception)
		{
			
		}
	}
}

