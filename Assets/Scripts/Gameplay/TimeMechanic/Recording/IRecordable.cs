using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Momentum.Gameplay.TimeMechanic.Recording
{
	public interface IRecordable
	{
		void RecordFrame(IRecorder recorder, ref IHistoryRecord snapshot);
		void PlayFrame(IRecorder recorder, IHistoryRecord snapshot);
	}
}

