using UnityEngine;
using System;

namespace Momentum.Gameplay.TimeMechanic.Recording
{
	public enum HistoryLookupError
	{
		/// <summary>
		/// There was no error.
		/// </summary>
		NONE = 0,
		/// <summary>
		/// Unknown reason.
		/// </summary>
		UNKNOWN 			= 1,
		/// <summary>
		/// Not enough records in history to interpolate between.
		/// </summary>
		NOT_ENOUGH_POINTS 	= 2,
		/// <summary>
		/// Requested point in time is earlier then the first entry in history.
		/// </summary>
		TOO_EARLY 			= 3,
		/// <summary>
		/// Requested point in time is later then the last entry in history.
		/// </summary>
		TOO_LATE 			= 4,
		/// <summary>
		/// Requested point in time is between the first and the last entry in history
		/// but it is impossible to provide information for it (e.g. due to a gap in history).
		/// </summary>
		NO_SUCH_ENTRY 		= 5
	}
	
	[Serializable]
	public abstract class HistoryLookupFailedException : System.Collections.Generic.KeyNotFoundException
	{
		private readonly HistoryLookupError reason;
		private readonly float requestedKey;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="T:HistoryLookupFailedException"/> class
		/// </summary>
		/// <param name="message">A <see cref="T:System.String"/> that describes the exception. </param>
		public HistoryLookupFailedException (
			float requestedKey, 
			HistoryLookupError reason, 
			string message
		) : base (message)
		{
			this.requestedKey = requestedKey;
			this.reason = reason;
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="T:HistoryLookupFailedException"/> class
		/// </summary>
		/// <param name="message">A <see cref="T:System.String"/> that describes the exception. </param>
		/// <param name="inner">The exception that is the cause of the current exception. </param>
		public HistoryLookupFailedException (
			float requestedKey,
			HistoryLookupError reason, 
			string message, 
			Exception inner
		) : base (message, inner)
		{
			this.requestedKey = requestedKey;
			this.reason = reason;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:HistoryLookupFailedException"/> class
		/// </summary>
		/// <param name="context">The contextual information about the source or destination.</param>
		/// <param name="info">The object that holds the serialized object data.</param>
		protected HistoryLookupFailedException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
			: base(info, context)
		{
			this.reason = (HistoryLookupError)info.GetValue("reason", typeof(HistoryLookupError));
			this.requestedKey = (float)info.GetValue("requestedKey", typeof(float));
		}

		public override void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			
			info.AddValue("reason", this.reason);
			info.AddValue("requestedKey", this.requestedKey);

			// MUST call through to the base class to let it save its own state
			base.GetObjectData(info, context);
		}
		
		public HistoryLookupError Reason
		{
			get {
				return this.reason;
			}
		}

		public float RequestedKey
		{
			get {
				return this.requestedKey;
			}
		}
	}

	
	[Serializable]
	public class NotEnoughHistoryPointsException : HistoryLookupFailedException
	{
		private const string DEFAULT_MESSAGE = "Not enough records in history to interpolate between";

		public NotEnoughHistoryPointsException(float requestedKey)
			: base(requestedKey, HistoryLookupError.NOT_ENOUGH_POINTS, DEFAULT_MESSAGE)
		{

		}

		protected NotEnoughHistoryPointsException (System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base (info, context)
		{

		}
	}
	
	[Serializable]
	public abstract class OutsideOfRecordedHistoryException : HistoryLookupFailedException
	{
		private const string DEFAULT_MESSAGE_TEMPLATE = "{0} is outside of recorded history [{1};{2}]";

		private readonly float lowerBoundary;
		private readonly float higherBoundary;

		public OutsideOfRecordedHistoryException(
			float requestedKey, 
			float lowerBoundary, 
			float higherBoundary,
			HistoryLookupError reason
		) : base(
			requestedKey, 
			reason,
			String.Format(
				DEFAULT_MESSAGE_TEMPLATE,
				requestedKey,
				lowerBoundary,
				higherBoundary
			)
		)
		{
			this.lowerBoundary = lowerBoundary;
			this.higherBoundary = higherBoundary;
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="T:OutsideOfRecordedHistoryException"/> class
		/// </summary>
		/// <param name="context">The contextual information about the source or destination.</param>
		/// <param name="info">The object that holds the serialized object data.</param>
		protected OutsideOfRecordedHistoryException (System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base (info, context)
		{
			this.lowerBoundary = (float)info.GetValue("lowerBoundary", typeof(float));
			this.higherBoundary = (float)info.GetValue("higherBoundary", typeof(float));
		}

		public override void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			
			info.AddValue("lowerBoundary", this.lowerBoundary);
			info.AddValue("higherBoundary", this.higherBoundary);
			
			// MUST call through to the base class to let it save its own state
			base.GetObjectData(info, context);
		}

		public float LowerBoundary
		{
			get {
				return this.lowerBoundary;
			}
		}

		public float HigherBoundary
		{
			get {
				return this.higherBoundary;
			}
		}
	}

	
	[Serializable]
	public class TooEarlyInHistoryException : OutsideOfRecordedHistoryException
	{
		public TooEarlyInHistoryException(float requestedKey, float lowerBoundary, float higherBoundary)
			: base(requestedKey, lowerBoundary, higherBoundary, HistoryLookupError.TOO_EARLY)
		{

		}

		protected TooEarlyInHistoryException (System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base (info, context)
		{

		}
	}

	[Serializable]
	public class TooLateInHistoryException : OutsideOfRecordedHistoryException
	{
		public TooLateInHistoryException(float requestedKey, float lowerBoundary, float higherBoundary)
			: base(requestedKey, lowerBoundary, higherBoundary, HistoryLookupError.TOO_LATE)
		{
			
		}

		protected TooLateInHistoryException (System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base (info, context)
		{
			
		}
	}

	[Serializable]
	public class NoSuchEntryInHistoryException : OutsideOfRecordedHistoryException
	{
		public NoSuchEntryInHistoryException(float requestedKey, float lowerBoundary, float higherBoundary)
			: base(requestedKey, lowerBoundary, higherBoundary, HistoryLookupError.NO_SUCH_ENTRY)
		{
			
		}

		protected NoSuchEntryInHistoryException (System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base (info, context)
		{
			
		}
	}
}

