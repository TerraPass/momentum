using UnityEngine;
using System;

namespace Momentum.Gameplay.TimeMechanic.Recording
{
	public interface ITransformSnapshot
	{
		Vector3 Position {get;set;}
		Quaternion Rotation {get;set;}
		Vector3 Scale {get;set;}
	}

	public static class ITransformSnapshotExtensions
	{
		public static void ReinitFrom(this ITransformSnapshot snapshot, Transform transform)
		{
			snapshot.ReinitFrom(transform.position, transform.rotation, transform.localScale);
		}

		public static void ReinitFrom(
			this ITransformSnapshot snapshot, 
			Vector3 position, 
			Quaternion rotation,
			Vector3 scale
		) {
			snapshot.Position = position;
			snapshot.Rotation = rotation;
			snapshot.Scale = scale;
		}

		public static void ApplyTo(this ITransformSnapshot snapshot, Transform transform)
		{
			transform.position = snapshot.Position;
			transform.rotation = snapshot.Rotation;
			transform.localScale = snapshot.Scale;
		}

		public static void ApplyTo(this ITransformSnapshot snapshot, GameObject gameObject)
		{
			snapshot.ApplyTo(gameObject.transform);
		}
	}
}
