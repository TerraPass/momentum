using UnityEngine;
using System;
using System.Linq;

namespace Momentum.Gameplay.TimeMechanic.Recording
{
	/// <summary>
	/// This implementation of history silently discards entries,
	/// attempting to be recorded between any two already existing entries.
	/// I.e. already recorded bits are never recorded over.
	/// </summary>
	internal class SilentNonRewritableInterpolatedHistory : InterpolatedHistory
	{
		public SilentNonRewritableInterpolatedHistory(StateInterpolationAlgorithm interpolationAlgorithm = null)
			: base(interpolationAlgorithm)
		{

		}

		protected override InterpolatedHistory NewEmptyCopy()
		{
			return new SilentNonRewritableInterpolatedHistory(this.interpolationAlgorithm);
		}

		public override IHistoryRecord this[float key] {
			get {
				return base[key];
			}
			set {
				// Only record entry if it is outside of currently recorded history
				if(this.historyPoints.Count == 0 
				   || key < this.historyPoints.Keys.First() 
				   || key > this.historyPoints.Keys.Last())
				{
					base[key] = value;
				}
//				else
//				{
//					Debug.LogFormat(
//						"Ignoring attempted record insertion at key {0} to history [{1},{2}]",
//						key,
//						this.historyPoints.Keys.First(),
//						this.historyPoints.Keys.Last()
//					);
//				}
			}
		}
	}
}

