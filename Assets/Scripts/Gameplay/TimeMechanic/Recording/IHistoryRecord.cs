using UnityEngine;
using System;

namespace Momentum.Gameplay.TimeMechanic.Recording
{
	public interface IHistoryRecord
	{
		ITransformSnapshot TransformSnapshot {get;set;}
		object StateObject {get;set;}
	}

	public static class IHistoryRecordExtensions
	{
		public static void SetTransform(this IHistoryRecord record, Transform transform)
		{
			record.TransformSnapshot.ReinitFrom(transform);
		}

		public static void SetTransform(
			this IHistoryRecord record, 
			Vector3 position, 
			Quaternion rotation, 
			Vector3 scale
		) {
			record.TransformSnapshot.ReinitFrom(position, rotation, scale);
		}
	}
}

