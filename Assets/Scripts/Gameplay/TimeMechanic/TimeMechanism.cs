using UnityEngine;
using System;

namespace Momentum.Gameplay.TimeMechanic
{
	using Recording;
	using Measurement;

	public class TimeMechanism : ITimeMechanism
	{
		public event EventHandler<RewindStartedEventArgs> RewindStarted	= delegate {};
		public event EventHandler<RewindPausedEventArgs> RewindPaused	= delegate {};
		public event EventHandler<RewindEndedEventArgs> RewindEnded 	= delegate {};
		public event EventHandler<TimeFlowPausedEventArgs> TimeFlowPaused = delegate {};
		public event EventHandler<TimeFlowResumedEventArgs> TimeFlowResumed = delegate {};

		private IRecorder recorder;

		private bool isInRewind;

		public TimeMechanism(IRecorder recorder)
		{
			this.recorder = recorder;
			this.recorder.StartOfHistoryReached += this.OnStartOfHistoryReached;

			this.isInRewind = false;
		}

		protected ILevelTimer Timer
		{
			get {
				return this.recorder.Timer;
			}
		}
		
		protected IRecorder Recorder
		{
			get {
				return this.recorder;
			}
		}

		public void StartRewind ()
		{
			if(!this.IsInRewind)
			{
				this.Timer.Resume(-1.0f);
				UnityTimeScale = 0.0f;
				this.Recorder.StopRecording();
				this.Recorder.StartPlayback();

				this.isInRewind = true;

				this.RewindStarted(
					this,
					new RewindStartedEventArgs(this)
				);
			}
		}

		public void EndRewind ()
		{
			if(this.IsInRewind)
			{
				this.Timer.Resume(1.0f);
				UnityTimeScale = 1.0f;
				this.Recorder.StartRecording();
				this.Recorder.StartPlayback();

				this.isInRewind = false;

				this.RewindEnded(
					this,
					new RewindEndedEventArgs(this)
				);
			}
		}

		public bool IsInRewind {
			get {
				return this.isInRewind;
			}
		}

		public bool Pause()
		{
			if(!this.IsPaused)
			{
				this.Timer.Pause();
				UnityTimeScale = 0.0f;
				if(!this.IsInRewind)
				{
					this.TimeFlowPaused(
						this,
						new TimeFlowPausedEventArgs(this)
					);
				}
				return true;
			}
			else
			{
				return false;
			}
		}

		public bool Resume()
		{
			if(this.IsPaused)
			{
				this.Timer.Resume();
				if(!this.IsInRewind)
				{
					// Unity time scale only needs to be reset
					// if we are NOT currently rewinding.
					UnityTimeScale = 1.0f;
					this.TimeFlowResumed(
						this,
						new TimeFlowResumedEventArgs(this)
					);
				}
				return true;
			}
			else
			{
				return false;
			}
		}

		public bool IsPaused {
			get {
				return this.Timer.IsPaused;
			}
		}

		public ILevelTimer ReadOnlyTimer {
			get {
				return new ReadOnlyLevelTimer(this.Timer);
			}
		}

		#region IResettable implementation

		public void Reset ()
		{
			this.Recorder.StopPlayback();
			this.Timer.Reset();
		}

		#endregion

		protected static float UnityTimeScale
		{
			get {
				return UnityEngine.Time.timeScale;
			}
			set {
				UnityEngine.Time.timeScale = value;
			}
		}

		private void OnStartOfHistoryReached(object sender, StartOfHistoryReachedEventArgs args)
		{
			if(this.IsInRewind && !this.Timer.IsPaused)
			{
				this.Timer.Pause();
				// Resetting timer scale to prevent further rewind
				// in case the game gets paused-unpaused.
				this.Timer.Scale = 0.0f;
				this.Recorder.StopPlayback();

				this.RewindPaused(
					this,
					new RewindPausedEventArgs(this)
				);
			}
		}
	}
}

