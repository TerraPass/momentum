using UnityEngine;
using System;

namespace Momentum.Gameplay.TimeMechanic
{
	public abstract class AbstractTimeControllerComponent : MonoBehaviour, ITimeController
	{
		private ITimeMechanism timeMechanism;

		public ITimeMechanism TimeMechanism
		{
			get {
				return this.timeMechanism;
			}
			set {
				this.timeMechanism = value;
			}
		}

		#region IDisableable implementation

		public abstract bool Enabled {get;set;}

		#endregion
	}
}

