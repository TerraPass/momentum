using UnityEngine;
using System;

using Momentum.Utils.Time;

namespace Momentum.Gameplay.TimeMechanic.Measurement
{
	public sealed class LevelTimer : ILevelTimer
	{
		private static LevelTimer instance = null;

		private float timerScale;

		private float startTime;
		private float accumulatedTime;
		private bool isPaused;

		private LevelTimer ()
		{
			this.timerScale = 1.0f;
			this.Reset(true);
		}

		public static LevelTimer Instance
		{
			get {
				if(instance == null)
				{
					instance = new LevelTimer();
				}
				return instance;
			}
		}

		#region IScalableTimer implementation

		public float Scale {
			get {
				return this.timerScale;
			}
			set {
				if(!this.IsPaused)
				{
					this.accumulatedTime += this.timerScale * (CurrentTime - this.startTime);
					this.startTime = CurrentTime;
				}
				this.timerScale = value;
			}
		}

		#endregion

		#region ITimer implementation

		public bool Resume ()
		{
			if(this.IsPaused)
			{
				this.startTime = CurrentTime;
				this.isPaused = false;
				return true;
			} 
			else 
			{
				return false;
			}
		}

		public bool Pause ()
		{
			if(!this.IsPaused) 
			{
				this.accumulatedTime += this.timerScale * (CurrentTime - this.startTime);
				this.isPaused = true;
				return true;
			} 
			else 
			{
				return false;
			}
		}

		public float ElapsedSeconds {
			get {
				return this.IsPaused 
					? this.accumulatedTime 
					: this.accumulatedTime + (this.timerScale * (CurrentTime - this.startTime));
			}
		}

		public bool IsPaused {
			get {
				return this.isPaused;
			}
		}

		#endregion

		#region IResettableTimer implementation

		public void Reset (bool startPaused = true)
		{
			this.startTime = CurrentTime;
			this.accumulatedTime = 0;
			this.isPaused = startPaused;
		}

		#endregion

		private float CurrentTime
		{
			get {
				return UnityEngine.Time.unscaledTime;
			}
		}
	}
}
