using System;
using Momentum.Utils.Time;

namespace Momentum.Gameplay.TimeMechanic.Measurement
{
	public interface ILevelTimer : IScalableTimer, IResettableTimer
	{

	}

	public static class ILevelTimerExtensions
	{
		public static void Resume(this ILevelTimer timer, float scale)
		{
			timer.Scale = scale;
			timer.Resume();
		}
	}
}

