using UnityEngine;
using System;

namespace Momentum.Gameplay.TimeMechanic.Measurement
{
	public class ReadOnlyLevelTimer : AbstractLevelTimerDecorator
	{
		public class WriteAttemptException : InvalidOperationException
		{
			private const string WRITE_ATTEMPT_MESSAGE_TEMPLATE =
				"Attempted to invoke {0}() on a ReadOnlyLevelTimer wrapped instance of ILevelTimer";

			public WriteAttemptException(string methodName)
				: base(
					string.Format(
						WRITE_ATTEMPT_MESSAGE_TEMPLATE,
						methodName
					)
				)
			{

			}
		}

		public ReadOnlyLevelTimer(ILevelTimer levelTimer)
			: base(levelTimer)
		{

		}

		public override void Reset (bool startPaused)
		{
			throw new WriteAttemptException("Reset");
		}

		public override bool Resume ()
		{
			throw new WriteAttemptException("Resume");
		}

		public override bool Pause ()
		{
			throw new WriteAttemptException("Pause");
		}

		public override float Scale {
			get {
				return base.Scale;
			}
			set {
				throw new WriteAttemptException("Scale.set");
			}
		}
	}
}

