using UnityEngine;
using System;

namespace Momentum.Gameplay.TimeMechanic.Measurement
{
	public abstract class AbstractLevelTimerDecorator : ILevelTimer
	{
		private readonly ILevelTimer decoratedLevelTimer;

		public AbstractLevelTimerDecorator(ILevelTimer decoratedLevelTimer)
		{
			if(decoratedLevelTimer == null)
			{
				throw new ArgumentNullException("decoratedLevelTimer");
			}
			this.decoratedLevelTimer = decoratedLevelTimer;
		}

		protected ILevelTimer DecoratedLevelTimer
		{
			get {
				return this.decoratedLevelTimer;
			}
		}

		#region IResettableTimer implementation

		public virtual void Reset (bool startPaused = true)
		{
			this.DecoratedLevelTimer.Reset(startPaused);
		}

		#endregion

		#region ITimer implementation

		public virtual bool Resume ()
		{
			return this.DecoratedLevelTimer.Resume();
		}

		public virtual bool Pause ()
		{
			return this.DecoratedLevelTimer.Pause();
		}

		public virtual float ElapsedSeconds {
			get {
				return this.DecoratedLevelTimer.ElapsedSeconds;
			}
		}

		public virtual bool IsPaused {
			get {
				return this.DecoratedLevelTimer.IsPaused;
			}
		}

		#endregion

		#region IScalableTimer implementation

		public virtual float Scale {
			get {
				return this.DecoratedLevelTimer.Scale;
			}
			set {
				this.DecoratedLevelTimer.Scale = value;
			}
		}

		#endregion
	}
}

