using UnityEngine;
using System;

using Momentum.Gameplay.TimeMechanic.Recording;

namespace Momentum.Gameplay.Level
{
	/// <summary>
	/// 	<para>
	/// 		Describes objects capable of broadcasting calls to the following methods,
	///			optionally implemented by game level entities,
	/// 		to all the compatible entities in the level:
	/// 	</para>
	/// 	<para>- IResettable.Reset()</para>
	/// 	<para>- IDisableable.Enabled (setter)</para>
	/// 	<para>- IRecorderAware.Recorder (setter)</para>
	/// </summary>
	public interface IGameLevelRoot
	{
		/// <summary>
		/// Invoke Reset() on every child, implementing IResettable.
		/// </summary>
		void ResetAll();

		/// <summary>
		/// Invoke Enabled getter/setter on every child implementing IDisableable.
		/// </summary>
		bool EnabledAll {set;}

		/// <summary>
		/// Sets the recorder for every child, implementing IRecorderAware.
		/// </summary>
		/// <value>The recorder.</value>
		IRecorder Recorder {set;}
	}

	public static class IGameLevelRootExtensions
	{
		public static void DisableAll(this IGameLevelRoot gameLevelRoot)
		{
			gameLevelRoot.EnabledAll = false;
		}

		public static void EnableAll(this IGameLevelRoot gameLevelRoot)
		{
			gameLevelRoot.EnabledAll = true;
		}
	}
}

