using UnityEngine;
using System;

using Momentum.Gameplay.TimeMechanic.Recording;

namespace Momentum.Gameplay.Level
{
	public abstract class AbstractGameLevelRootComponent : MonoBehaviour, IGameLevelRoot
	{
		public abstract void ResetAll();

		public abstract bool EnabledAll {set;}

		public abstract IRecorder Recorder {set;}
	}
}

