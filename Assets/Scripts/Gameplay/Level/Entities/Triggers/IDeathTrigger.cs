using System;

namespace Momentum.Gameplay.Level.Entities.Triggers
{
	/// <summary>
	/// Describes triggers, which function as level borders
	/// and invoke IBall.FakeKill() on balls, which enter them.
	/// </summary>
	public interface IDeathTrigger : ILevelEntity
	{

	}
}

