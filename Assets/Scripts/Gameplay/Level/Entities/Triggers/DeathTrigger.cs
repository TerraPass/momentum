using UnityEngine;
using System;
using Terrapass.Debug;

using Momentum.Gameplay.Player;

namespace Momentum.Gameplay.Level.Entities.Triggers
{
	[RequireComponent(typeof(Collider))]
	public class DeathTrigger : AbstractDeathTriggerComponent
	{
		private Collider MyCollider
		{
			get {
				return this.GetComponent<Collider>();
			}
		}

		void Start()
		{
			DebugUtils.Assert(
				this.MyCollider != null && this.MyCollider.isTrigger,
				string.Format(
					"{0} component requires a Collider with isTrigger property set to true",
					this.GetType().Name
				)
			);
		}
	
		void OnTriggerEnter(Collider collider)
		{
			// If a ball entered this trigger, FakeKill() it
			IBall ball = collider.gameObject.GetComponent<IBall>();
			if(ball != null && ball.IsAlive())
			{
				ball.FakeKill();
			}
		}
		
		void OnTriggerExit(Collider collider)
		{

		}
	}
}

