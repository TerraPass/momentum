using UnityEngine;
using System;

namespace Momentum.Gameplay.Level.Entities.Mobile
{
	public abstract class AbstractMobileGeometryComponent : MonoBehaviour, IMobileGeometry
	{
		#region IMobileGeometry implementation

		public abstract bool IsInMotion {get;set;}

		#endregion


	}
}

