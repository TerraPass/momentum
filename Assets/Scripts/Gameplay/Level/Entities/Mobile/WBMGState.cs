using UnityEngine;
using System;
using Terrapass.Debug;

using Momentum.Gameplay.Common;
using Momentum.Gameplay.TimeMechanic.Recording;

namespace Momentum.Gameplay.Level.Entities.Mobile
{
	public partial class WaypointBasedMobileGeometry
		: IResettable, IRecorderAware, IRecordable
	{
		private struct WBMGState
		{
			private readonly TransformSnapshot transformSnapshot;

			private readonly int currentWaypointIndex;

			private MovementType movementType;
			private bool pingPongDirection;

			public WBMGState(
				Transform transform,
				int currentWaypointIndex,
				MovementType movementType,
				bool pingPongDirection
			)
			{
				this.transformSnapshot = new TransformSnapshot(transform);
				this.currentWaypointIndex = currentWaypointIndex;
				this.movementType = movementType;
				this.pingPongDirection = pingPongDirection;
			}

			public TransformSnapshot TransformSnapshot
			{
				get {
					return this.transformSnapshot;
				}
			}

			public int CurrentWaypointIndex
			{
				get {
					return this.currentWaypointIndex;
				}
			}

			public MovementType MovementType
			{
				get {
					return this.movementType;
				}
			}

			public bool PingPongDirection
			{
				get {
					return this.pingPongDirection;
				}
			}
		}

		private WBMGState TakeSnapshot()
		{
			return new WBMGState(
				this.transform,
				this.currentWaypointIndex,
				this.movementType,
				this.pingPongDirection
			);
		}

		private void ApplySnapshot(WBMGState snapshot, bool applyTransform)
		{
			if(applyTransform)
			{
				snapshot.TransformSnapshot.ApplyTo(this.transform);
			}
			this.currentWaypointIndex = snapshot.CurrentWaypointIndex;
			this.movementType = snapshot.MovementType;
			this.pingPongDirection = snapshot.PingPongDirection;
		}

		public void Reset()
		{
			this.ApplySnapshot(this.startingState, true);
		}

		private IRecorder recorder;

		public IRecorder Recorder 
		{
			get {
				return this.recorder;
			}
			set {
				if(this.recorder != null)
				{
					this.recorder.UnregisterRecordable(this);
				}
				this.recorder = value;
				if(this.recorder != null)
				{
					this.recorder.RegisterRecordable(
						this,
						new RewritableInterpolatedHistory()
					);
				}
			}
		}

		public void RecordFrame(IRecorder recorder, ref IHistoryRecord snapshot)
		{
			snapshot.StateObject = this.TakeSnapshot();
		}

		public void PlayFrame(IRecorder recorder, IHistoryRecord snapshot)
		{
			if(recorder.IsPlayingBackward())
			{
				snapshot.TransformSnapshot.ApplyTo(this.transform);
				this.ApplySnapshot(
					(WBMGState) snapshot.StateObject,
					false
				);
			}
		}
	}
}

