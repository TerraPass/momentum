using UnityEngine;
using System;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

namespace Momentum.Gameplay.Level.Entities.Mobile
{
	// TODO: Refactor by moving interaction with rigidbody
	// into a strategy (IPhysicsMovementStrategy > RigidbodyBasedMovementStrategy).
	[RequireComponent(typeof(Rigidbody))]
	public partial class WaypointBasedMobileGeometry : AbstractMobileGeometryComponent
	{
		private const float APPROXIMATION_TOLERANCE = 0.001f;
		private const float APPROXIMATION_TOLERANCE_SQUARED = APPROXIMATION_TOLERANCE * APPROXIMATION_TOLERANCE;

		private enum MovementType
		{
			ONE_WAY 	= 0,
			PING_PONG 	= 1,
			LOOP 		= 2
		}

		[SerializeField]
		private Transform[] waypoints;
		[SerializeField]
		private int startingWaypointIndex = 0;

		[SerializeField]
		private float displacementSpeed = 1.0f;
		[SerializeField]
		[Tooltip("Angular speed of rotation in degrees")]
		private float rotationSpeed = 45.0f;
		[SerializeField]
		private float scalingSpeed = 1.0f;

		[SerializeField]
		private MovementType movementType = MovementType.ONE_WAY;

		[SerializeField]
		private bool useDisplacement = true;
		[SerializeField]
		private bool useRotation = true;
		[SerializeField]
		private bool useScaling = false;

		[SerializeField]
		private bool isInMotion = true;

		private int currentWaypointIndex;

		/// <summary>
		/// This value only makes sense for PING_PONG movement type.
		/// True means the movement is directed to the next waypoint,
		/// otherwise - to the previous one.
		/// </summary>
		private bool pingPongDirection = true;

		/// <summary>
		/// Initial state, to which this mobile geometry piece
		/// will return if Reset() is invoked.
		/// </summary>
		private WBMGState startingState;

		private Rigidbody MyRigidbody
		{
			get {
				return this.GetComponent<Rigidbody>();
			}
		}

		void Start()
		{
			DebugUtils.Assert(
				this.MyRigidbody != null && this.MyRigidbody.isKinematic,
				"{0} component requires Rigidbody and expects it to be kinematic",
				this.GetType()
			);

			this.EnsureRequiredFieldsAreSetInEditor();

			DebugUtils.Assert(
				waypoints.Length > 0,
				"{0} component has no waypoints specified in editor",
				this.GetType()
			);

			DebugUtils.Assert(
				startingWaypointIndex < waypoints.Length,
				"{0} component has {1} specified as its starting waypoint index but only {2} waypoints",
				this.GetType(),
				startingWaypointIndex,
				waypoints.Length
			);

			this.currentWaypointIndex = this.startingWaypointIndex;

			// Setup initial state to accommodate for further calls to Reset()
			this.startingState = this.TakeSnapshot();
		}

		void FixedUpdate()
		{
			if(!this.IsInMotion)
			{
				return;
			}

			var nextWaypoint = this.NextWaypoint;
			if(nextWaypoint != null)
			{
				// If reached target position, rotation and scale
				if(
					this.MoveTowards(nextWaypoint) 
				   	&& this.RotateTowards(nextWaypoint) 
				   	&& this.ScaleTowards(nextWaypoint)
				)
				{
					// This sets next waypoint
					this.ShiftWaypoints();
				}
			}
		}

		/// <summary>
		/// Moves us towards the next waypoint.
		/// </summary>
		/// <returns><c>true</c>, if the waypoint position was reached, <c>false</c> otherwise.</returns>
		/// <param name="nextWaypoint">Next waypoint.</param>
		private bool MoveTowards(Transform nextWaypoint)
		{
			//if(!this.useDisplacement || this.ReachedNextWaypointPosition)
			if(
				!this.useDisplacement 
		   		|| ApproximatelyEquals(this.transform.position, this.NextWaypoint.position)
		   	)
			{
				return true;
			}

			var movementAmount = Time.fixedDeltaTime * this.displacementSpeed;
			var source = this.transform.position;
			bool result = ApproachBy(ref source, this.NextWaypoint.position, movementAmount);
			//this.transform.position = source;
			this.MyRigidbody.MovePosition(source);	// TODO: Strategize.
			return result;
		}

		/// <summary>
		/// Rotates us towards the next waypoint.
		/// </summary>
		/// <returns><c>true</c>, if the waypoint rotation was reached, <c>false</c> otherwise.</returns>
		/// <param name="nextWaypoint">Next waypoint.</param>
		private bool RotateTowards(Transform nextWaypoint)
		{
			if(
				!this.useRotation 
				|| ApproximatelyEquals(
					this.transform.rotation.eulerAngles,
					this.NextWaypoint.rotation.eulerAngles
				)
			)
			{
				return true;
			}
			
			var rotationAmount = Time.fixedDeltaTime * this.rotationSpeed;
			var source = this.transform.rotation.eulerAngles;
			bool result = ApproachBy(
				ref source,
				this.NextWaypoint.rotation.eulerAngles, 
				rotationAmount
			);
			//this.transform.rotation = Quaternion.Euler(source);
			this.MyRigidbody.MoveRotation(Quaternion.Euler(source));	// TODO: Strategize.
			return result;
		}

		/// <summary>
		/// Scales us towards the next waypoint.
		/// </summary>
		/// <returns><c>true</c>, if the waypoint scale was reached, <c>false</c> otherwise.</returns>
		/// <param name="nextWaypoint">Next waypoint.</param>
		private bool ScaleTowards(Transform nextWaypoint)
		{
			if(!this.useScaling || ApproximatelyEquals(this.transform.localScale, this.NextWaypoint.localScale))
			{
				return true;
			}

			var scalingAmount = Time.fixedDeltaTime * this.scalingSpeed;
			var source = this.transform.localScale;
			bool result = ApproachBy(ref source, this.NextWaypoint.localScale, scalingAmount);
			this.transform.localScale = source;
			return result;
		}

		private static bool ApproximatelyEquals(Vector3 a, Vector3 b)
		{
			return (a - b).sqrMagnitude <= APPROXIMATION_TOLERANCE_SQUARED;
		}

		// TODO: Maybe ApproachBy() should be made an extension of Unity's Vector3.
		/// <summary>
		/// Modifies the source vector by moving it closer to target by absoluteValue
		/// without overshooting. If absoluteValue is more than the distance between two vectors,
		/// source is made to match the target vector, instead of overshooting it.
		/// </summary>
		/// <returns><c>true</c>, if the target vector was reached, <c>false</c> otherwise.</returns>
		/// <param name="source">Source vector (gets modified).</param>
		/// <param name="target">Target vector, which is approached.</param>
		/// <param name="absoluteValue">Absolute value, by which to approach.</param>
		private static bool ApproachBy(ref Vector3 source, Vector3 target, float absoluteValue)
		{
			bool result = false;
			
			var difference = target - source;
			// A square root operation cannot be avoided in this case,
			// since the difference would have to be normalized anyway.
			var distance = difference.magnitude;

			// To avoid overshooting
			if(distance < absoluteValue)
			{
				absoluteValue = distance;
				result = true; // By matching the difference, we are going to reach the target.
			}
			
			source += (absoluteValue / distance) * difference;
			
			return result;
		}

		#region implemented abstract members of AbstractMobileGeometryComponent

		public override bool IsInMotion 
		{
			get {
				return this.isInMotion;
			}
			set {
				this.isInMotion = value;
			}
		}

		#endregion

		private Transform CurrentWaypoint
		{
			get {
				return this.waypoints[this.currentWaypointIndex];
			}
		}

		private Transform NextWaypoint
		{
			get {
				try
				{
					return this.waypoints[this.NextWaypointIndex];
				}
				catch(InvalidOperationException)
				{
					return null;
				}
			}
		}

		private int NextWaypointIndex
		{
			get {
				switch(this.movementType)
				{
				default:
					var errorMessage = string.Format(
						"{0}.NextWaypoint.get(): Unknown movement type {1}",
						this.GetType(),
						this.movementType
					);
					DebugUtils.Assert(
						false,
						errorMessage
					);
					throw new InvalidOperationException(errorMessage);
					
				case MovementType.ONE_WAY:
					if(this.currentWaypointIndex + 1 >= this.waypoints.Length)
					{
						throw new InvalidOperationException("Already at the last waypoint");
					}
					return this.currentWaypointIndex + 1;
					
				case MovementType.PING_PONG:
					return this.currentWaypointIndex + (this.pingPongDirection ? 1 : -1);
					
				case MovementType.LOOP:
					var nextLoopIndex = this.currentWaypointIndex + 1;
					return (nextLoopIndex >= this.waypoints.Length) ? 0 : nextLoopIndex;
				}
			}
		}

		/// <summary>
		/// Shift CurrentWaypoint and NextWaypoint forward,
		/// according to movement type.
		/// </summary>
		/// <returns><c>true</c> if the call had any effect.</returns>
		private bool ShiftWaypoints()
		{
			try
			{
				this.currentWaypointIndex = this.NextWaypointIndex;
				// Handle reaching the end of array under PING_PONG movement type
				if(this.movementType == MovementType.PING_PONG 
				   && this.currentWaypointIndex == this.waypoints.Length - 1 || this.currentWaypointIndex == 0)
				{
					// Flip the direction in order for the next shift not to get out of bounds.
					this.pingPongDirection = !this.pingPongDirection;
				}

				DebugUtils.Assert(
					this.currentWaypointIndex >= 0 && this.currentWaypointIndex < this.waypoints.Length,
					"{0}.ShiftWaypoints(): Waypoint index went out of bounds (index is {1}, length is {2})",
					this.GetType(),
					this.currentWaypointIndex,
					this.waypoints.Length
				);
				return true;
			}
			catch(InvalidOperationException)
			{
				return false;
			}
		}
	}
}

