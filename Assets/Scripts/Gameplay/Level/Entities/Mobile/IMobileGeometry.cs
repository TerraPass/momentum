using UnityEngine;
using System;

namespace Momentum.Gameplay.Level.Entities.Mobile
{
	public interface IMobileGeometry : ILevelEntity
	{
		bool IsInMotion {get;set;}
	}

	public static class IMobileGeometryExtensions
	{
		public static void Activate(this IMobileGeometry mobileGeometry)
		{
			mobileGeometry.IsInMotion = true;
		}

		public static void Deactivate(this IMobileGeometry mobileGeometry)
		{
			mobileGeometry.IsInMotion = false;
		}
	}
}

