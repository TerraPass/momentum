using System;

using Momentum.Gameplay.Level.Entities;

namespace Momentum.Gameplay.Level.Entities.Ethereal
{
	public interface IEtherealGeometry : ILevelEntity
	{
		bool IsMaterial {get;set;}
	}

	public static class IEtherealGeometryExtensions
	{
		public static void Materialize(this IEtherealGeometry etherealGeometry)
		{
			etherealGeometry.IsMaterial = true;
		}

		public static void Immaterialize(this IEtherealGeometry etherealGeometry)
		{
			etherealGeometry.IsMaterial = false;
		}
	}
}

