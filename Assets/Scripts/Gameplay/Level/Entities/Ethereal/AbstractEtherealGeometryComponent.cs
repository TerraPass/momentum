using UnityEngine;
using System;

namespace Momentum.Gameplay.Level.Entities.Ethereal
{
	public abstract class AbstractEtherealGeometryComponent : MonoBehaviour, IEtherealGeometry
	{
		public abstract bool IsMaterial {get;set;}
	}
}

