using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

namespace Momentum.Gameplay.Level.Entities.Ethereal
{
	public class CompositeEtherealGeometry : AbstractEtherealGeometryComponent
	{
		[SerializeField]
		private bool startEthereal = true;

		[SerializeField]
		private AbstractEtherealGeometryComponent[] components;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			this.IsMaterial = !this.startEthereal;
		}

		#region implemented abstract members of AbstractEtherealGeometryComponent

		/// <summary>
		/// Sets IsMaterial property value for all of the components.
		/// Getter returns true if all of the components  have IsMaterial property set to true,
		/// otherwise returns false.
		/// </summary>
		/// <value><c>true</c> if every component has its IsMaterial property set to true; otherwise, <c>false</c>.</value>
		public override bool IsMaterial {
			get {
				foreach(var component in this.components)
				{
					if(!component.IsMaterial)
					{
						return false;
					}
				}
				return true;
			}
			set {
				foreach(var component in this.components)
				{
					component.IsMaterial = value;
				}
			}
		}

		#endregion
	}
}

