using UnityEngine;
using System;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

using Momentum.Gameplay.TimeMechanic.Recording;

namespace Momentum.Gameplay.Level.Entities.Ethereal
{
	[RequireComponent(typeof(Collider))]
	[RequireComponent(typeof(Renderer))]
	public class EtherealGeometryUnit
		: AbstractEtherealGeometryComponent, IRecorderAware, IRecordable
	{
		[SerializeField]
		private bool startEthereal;

		[SerializeField]
		private Material etherealMaterial;
		[SerializeField]
		private Material materialMaterial;

		private bool isMaterial = false;

		private IRecorder recorder;

		private Renderer Renderer
		{
			get {
				return this.GetComponent<Renderer>();
			}
		}

		private Collider Collider
		{
			get {
				return this.GetComponent<Collider>();
			}
		}

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			DebugUtils.Assert(
				this.Renderer != null,
				string.Format(
					"Renderer component is missing from {0} and is required by {1} component",
					this.name,
					this.GetType()
				)
			);

			DebugUtils.Assert(
				this.Collider != null,
				string.Format(
					"Collider component is missing from {0} and is required by {1} component",
					this.name,
					this.GetType()
				)
			);

			this.IsMaterial = !this.startEthereal;
		}

		#region implemented abstract members of AbstractEtherealGeometryComponent

		public override bool IsMaterial
		{
			get {
				return this.isMaterial;
			}
			set {
				this.isMaterial = value;
				this.Collider.isTrigger = !this.isMaterial;
				this.Renderer.material = this.isMaterial ? materialMaterial : etherealMaterial;
			}
		}

		#endregion

		#region IRecorderAware implementation

		public IRecorder Recorder
		{
			get {
				return this.recorder;
			}
			set {
				if(this.recorder != null)
				{
					this.recorder.UnregisterRecordable(this);
				}
				this.recorder = value;
				if(this.recorder != null)
				{
					this.recorder.RegisterRecordable(
						this,
						new RewritableInterpolatedHistory()
					);
				}
			}
		}

		#endregion

		#region IRecordable implementation

		public void RecordFrame(IRecorder recorder, ref IHistoryRecord snapshot)
		{
			snapshot.StateObject = this.IsMaterial;
		}

		public void PlayFrame(IRecorder recorder, IHistoryRecord snapshot)
		{
			if(recorder.IsPlayingBackward())
			{
				bool isMaterialSnapshot = (bool)snapshot.StateObject;
				if(this.IsMaterial != isMaterialSnapshot)
				{
					this.IsMaterial = isMaterialSnapshot;
				}
			}
		}

		#endregion
	}
}

