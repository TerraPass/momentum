using UnityEngine;
using System;

namespace Momentum.Gameplay.Level.Entities
{
	/// <summary>
	/// Describes all non-static elements of a level:
	/// buttons, toggles, gates etc.
	/// </summary>
	public interface ILevelEntity
	{

	}
}

