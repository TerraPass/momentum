using UnityEngine;
using System;

namespace Momentum.Gameplay.Level.Entities.Gates
{
	public class GateOpeningEventArgs : LevelEntityEventArgs<IGate>
	{
		public GateOpeningEventArgs(
			IGate gate
		) : base(gate)
		{
			
		}
	}
	
	public class GateOpenedEventArgs : LevelEntityEventArgs<IGate>
	{
		public GateOpenedEventArgs(
			IGate gate
		) : base(gate)
		{
			
		}
	}

	public class GateClosingEventArgs : LevelEntityEventArgs<IGate>
	{
		public GateClosingEventArgs(
			IGate gate
		) : base(gate)
		{
			
		}
	}
	
	public class GateClosedEventArgs : LevelEntityEventArgs<IGate>
	{
		public GateClosedEventArgs(
			IGate gate
		) : base(gate)
		{
			
		}
	}
}
