using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

using Momentum.Gameplay.Player;
using Momentum.Gameplay.TimeMechanic.Recording;

namespace Momentum.Gameplay.Level.Entities.Gates
{
	/// <summary>
	/// Describes a gate, which lerps between two transforms.
	/// I.e. opened and closed states may differ by position, rotation and scale.
	/// </summary>
	public class TransformingDoorGate : AbstractSingleDoorGate
	{
		/// <summary>
		/// Describes the margin of error for opening/closing in Unity meters.
		/// </summary>
		private const float DISPLACEMENT_TOLERANCE = 0.0001f;
		private const float DISPLACEMENT_TOLERANCE_SQUARED = DISPLACEMENT_TOLERANCE*DISPLACEMENT_TOLERANCE;

		[SerializeField]
		private Transform closedTransform;
		[SerializeField]
		private Transform openedTransform;
		[SerializeField]
		private float lerpAmount = 0.5f;

		#region implemented abstract members of AbstractSingleDoorGate

		private Transform GetRelevantTransform(bool requestedOpen)
		{
			return requestedOpen ? this.openedTransform : this.closedTransform;
		}

		protected override void UpdateTransform (bool requestedOpen)
		{
			this.transform.LerpTo(
				this.GetRelevantTransform(requestedOpen),
				this.lerpAmount
			);
		}

		protected override bool TransformIsAdequate (bool requestedOpen)
		{
			// TODO: For rotating doors checking position is not enough,
			// rotation needs to be checked as well.
			return (this.transform.position - this.GetRelevantTransform(requestedOpen).position).sqrMagnitude <= DISPLACEMENT_TOLERANCE_SQUARED;
		}

		protected override void SwitchToState(bool open)
		{
			Transform requestedTransform = this.GetRelevantTransform(open);

			this.transform.position = requestedTransform.position;
			this.transform.rotation = requestedTransform.rotation;
			this.transform.localScale = requestedTransform.localScale;
		}

		#endregion
	}
}

