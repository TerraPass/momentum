using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

namespace Momentum.Gameplay.Level.Entities.Gates
{
	/// <summary>
	/// Describes a gate, which lerps between two positions.
	/// I.e. opened and closed states may differ by position only.
	/// </summary>
	public class SlidingDoorGate : AbstractSingleDoorGate
	{
		/// <summary>
		/// Describes the margin of error for opening/closing in Unity meters.
		/// </summary>
		private const float DISPLACEMENT_TOLERANCE = 0.0001f;
		private const float DISPLACEMENT_TOLERANCE_SQUARED = DISPLACEMENT_TOLERANCE*DISPLACEMENT_TOLERANCE;

		[SerializeField]
		private Transform openPosition;
		[SerializeField]
		private Transform closedPosition;

		[SerializeField]
		private float lerpAmount = 0.5f;

		private Vector3 GetRequestedPosition(bool requestedOpen)
		{
			return requestedOpen ? this.openPosition.position : this.closedPosition.position;
		}

		#region implemented abstract members of AbstractSingleDoorGate
		protected override void UpdateTransform (bool requestedOpen)
		{
			this.transform.position = Vector3.Lerp(
				this.transform.position,
				this.GetRequestedPosition(requestedOpen),
				this.lerpAmount
			);
		}
		protected override bool TransformIsAdequate (bool requestedOpen)
		{
			return (this.transform.position - this.GetRequestedPosition(requestedOpen)).sqrMagnitude <= DISPLACEMENT_TOLERANCE_SQUARED;
		}

		protected override void SwitchToState(bool open)
		{
			this.transform.position = this.GetRequestedPosition(open);
		}

		#endregion
	}
}

