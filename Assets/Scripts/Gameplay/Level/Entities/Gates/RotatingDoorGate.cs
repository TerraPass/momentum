using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

using Momentum.Utils.Math;
using Momentum.Gameplay.TimeMechanic.Recording;

namespace Momentum.Gameplay.Level.Entities.Gates
{
	/// <summary>
	/// Describes a gate, rotating by some angle around an axis.
	/// </summary>
	public class RotatingDoorGate : AbstractSingleDoorGate
	{
		/// <summary>
		/// Rotation tolerance in degrees for comparing rotation angles.
		/// </summary>
		private const float ROTATION_TOLERANCE = 0.001f;

		[SerializeField]
		private Transform closedTransform;

		[SerializeField]
		[Tooltip("Transform, which serves as an axis for the door to rotate around")]
		private Transform axisTransform;

		[SerializeField]
		[Tooltip("Angle between closed and opened position of the door in degrees around the axis, specified in axis transform field")]
		private float openAngle = 90.0f;

		[SerializeField]
		[Tooltip("Speed of rotation around the axis in degrees per second")]
		private float angularSpeed = 180.0f;

		/// <summary>
		/// 0 corresponds to closed state, openAngle - to open state.
		/// </summary>
		private float currentAngle = 0.0f;

		#region implemented abstract members of AbstractSingleDoorGate

		protected override void UpdateTransform(bool requestedOpen)
		{
			float angleIncrement = MathUtils.TrimToRange(
				Time.fixedDeltaTime * (requestedOpen ? this.angularSpeed : -this.angularSpeed),
				-this.currentAngle,
				this.openAngle - this.currentAngle
			);

			this.transform.RotateAround(
				this.axisTransform.position,
				this.axisTransform.up,
				angleIncrement
			);
			this.currentAngle += angleIncrement;

			// To prevent transform from visibly changing its position
			// due to accumulated floating point error
			if(this.TransformIsAdequate(false))
			{
				this.transform.position = this.closedTransform.position;
				this.transform.rotation = this.closedTransform.rotation;
			}
		}

		protected override bool TransformIsAdequate(bool requestedOpen)
		{
			return Mathf.Abs(this.currentAngle - (requestedOpen ? this.openAngle : 0)) <= ROTATION_TOLERANCE;
		}

		protected override void SwitchToState(bool open)
		{
			// Revert transform to closed state
			this.transform.position = this.closedTransform.position;
			this.transform.rotation = this.closedTransform.rotation;
			this.currentAngle = 0;

			// If the door is requested open, rotate it by openAngle around the axis
			if(open)
			{
				this.transform.RotateAround(
					this.axisTransform.position,
					this.axisTransform.up,
					this.openAngle
				);
				this.currentAngle = this.openAngle;
			}
		}

		#endregion

		protected override StateInterpolationAlgorithm RecordableStateInterpolationAlgorithm {
			get {
				// Linear interpolation
				return (earlier, later, amount) => Mathf.Lerp((float)earlier, (float)later, amount);
			}
		}

		protected override void RecordFrameImpl (IRecorder recorder, ref IHistoryRecord snapshot)
		{
			snapshot.StateObject = this.currentAngle;
		}

		protected override void PlayBackwardsFrameImpl (IRecorder recorder, IHistoryRecord snapshot)
		{
			snapshot.TransformSnapshot.ApplyTo(this.transform);
			this.currentAngle = (float)snapshot.StateObject;
		}
	}
}

