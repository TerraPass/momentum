using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

using Momentum.Gameplay.TimeMechanic.Recording;

namespace Momentum.Gameplay.Level.Entities.Gates
{
	public abstract class AbstractSingleDoorGate
		: AbstractGateComponent, IRecorderAware, IRecordable
	{
		[SerializeField]
		private bool startOpen = false;

		private IRecorder recorder;

		private bool isOpen;
		private bool requestedOpen;

		/// <summary>
		/// This flag is set on recording playback, as well as on calls to Open() and Close()
		/// to force FixedUpdate() to update the door transform, even if its state appears to be correct.
		/// It needs to be done on recording playback, since without verification 
		/// (and lerping) in FixedUpdate() the door would keep its transform from the last played frame.
		/// </summary>
		private bool transformNeedsUpdate = false;

		private void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			this.requestedOpen = startOpen;
			this.SwitchToState(this.startOpen);
		}

		void FixedUpdate()
		{
			// If the door needs to be moved
			if(this.transformNeedsUpdate)
			{
				this.UpdateTransform(this.requestedOpen);
				if(this.TransformIsAdequate(this.requestedOpen))
				{
					this.IsOpen = this.requestedOpen;
					this.transformNeedsUpdate = false;
				}
			}
		}

		/// <summary>
		/// Signals the implementation to update transform, according to requested state of the gate.
		/// </summary>
		/// <param name="requestedOpen">If <c>true</c>, the gate is requested to be opened.</param>
		protected abstract void UpdateTransform(bool requestedOpen);

		/// <summary>
		/// Checks whether current transform is adequate (i.e. does not need further updating) with respect to
		/// the requested state of the door.
		/// </summary>
		/// <returns>
		/// <c>true</c>
		/// , if current transform satisfies the requested state of the gate, <c>false</c> otherwise.
		/// </returns>
		/// <param name="requestedOpen">If <c>true</c>, the gate is requested to be opened.</param>
		protected abstract bool TransformIsAdequate(bool requestedOpen);

		/// <summary>
		/// Switches the door to opened or closed state immediately, without animation.
		/// </summary>
		/// <param name="open">If set to <c>true</c>, switches to open state, otherwise - to closed.</param>
		protected abstract void SwitchToState(bool open);

		#region implemented abstract members of AbstractGateComponent
		
		public override event EventHandler<GateOpeningEventArgs> GateOpening 	= delegate{};
		public override event EventHandler<GateOpenedEventArgs> GateOpened 		= delegate{};
		public override event EventHandler<GateClosingEventArgs> GateClosing 	= delegate{};
		public override event EventHandler<GateClosedEventArgs> GateClosed 		= delegate{};
		
		public override void Open()
		{
			if(!this.requestedOpen)
			{
				// Actual opening will be handled by FixedUpdate()
				this.requestedOpen = true;
				this.transformNeedsUpdate = true;
				this.GateOpening(
					this,
					new GateOpeningEventArgs(
					this
					)
				);
			}
		}
		
		public override void Close()
		{
			if(this.requestedOpen)
			{
				// Actual closing will be handled by FixedUpdate()
				this.requestedOpen = false;
				this.transformNeedsUpdate = true;
				this.GateClosing(
					this,
					new GateClosingEventArgs(
					this
					)
				);
			}
		}
		
		public override bool IsOpen {
			get {
				return this.isOpen;
			}
			protected set {
				if(this.isOpen != value)
				{
					this.isOpen = value;
					if(this.isOpen)
					{
						this.GateOpened(
							this,
							new GateOpenedEventArgs(
							this
							)
						);
					}
					else
					{
						this.GateClosed(
							this,
							new GateClosedEventArgs(
							this
							)
						);
					}
				}
			}
		}
		
		#endregion
		
		#region IRecorderAware implementation
		
		public IRecorder Recorder
		{
			get {
				return this.recorder;
			}
			set {
				if(this.recorder != null)
				{
					this.recorder.UnregisterRecordable(this);
				}
				this.recorder = value;
				var interpolationAlgorithm = this.RecordableStateInterpolationAlgorithm;
				this.recorder.RegisterRecordable(
					this,
					new RewritableInterpolatedHistory(interpolationAlgorithm)
				);
			}
		}

		#endregion

		protected virtual StateInterpolationAlgorithm RecordableStateInterpolationAlgorithm
		{
			get {
				return null;
			}
		}

		#region IRecordable implementation
		
		public void RecordFrame (IRecorder recorder, ref IHistoryRecord snapshot)
		{
			this.RecordFrameImpl(recorder, ref snapshot);
		}
		
		public void PlayFrame (IRecorder recorder, IHistoryRecord snapshot)
		{
			if(recorder.IsPlayingBackward())
			{
				this.PlayBackwardsFrameImpl(recorder, snapshot);
				this.transformNeedsUpdate = true;
			}
		}
		
		#endregion

		protected virtual void RecordFrameImpl(IRecorder recorder, ref IHistoryRecord snapshot)
		{

		}

		protected virtual void PlayBackwardsFrameImpl(IRecorder recorder, IHistoryRecord snapshot)
		{
			snapshot.TransformSnapshot.ApplyTo(this.transform);
		}
	}
}

