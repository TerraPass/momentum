using UnityEngine;
using System;

namespace Momentum.Gameplay.Level.Entities.Gates
{
	public interface IGate : ILevelEntity
	{
		/// <summary>
		/// Occurs when gate begins opening.
		/// </summary>
		event EventHandler<GateOpeningEventArgs> GateOpening;
		/// <summary>
		/// Occurs when gate completely opens.
		/// </summary>
		event EventHandler<GateOpenedEventArgs> GateOpened;
		/// <summary>
		/// Occurs when gate begins closing.
		/// </summary>
		event EventHandler<GateClosingEventArgs> GateClosing;
		/// <summary>
		/// Occurs when gate completely closes.
		/// </summary>
		event EventHandler<GateClosedEventArgs> GateClosed;

		void Open();
		void Close();

		bool IsOpen {get;}
//		/// <summary>
//		/// <c>true</c> if the gate is in the middle of being opened or closed,
//		/// i.e. Open()/Close() has been called, but the door is not yet completely opened/closed.
//		/// </summary>
//		/// <value><c>true</c> if this instance is changing state; otherwise, <c>false</c>.</value>
//		bool IsChangingState {get;}
	}

	public static class IGateExtensions
	{
//		public static void IsOpening(this IGate gate)
//		{
//			return !gate.IsOpen && gate.IsChangingState;
//		}
//
//		public static void IsClosing(this IGate gate)
//		{
//			return gate.IsOpen && gate.IsChangingState;
//		}
	}
}
