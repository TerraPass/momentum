using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

namespace Momentum.Gameplay.Level.Entities.Gates
{
	public sealed class CompositeGate : AbstractGateComponent
	{
		[SerializeField]
		private AbstractGateComponent[] gates;

		/// <summary>
		/// This counter tracks how many of the underlying gates
		/// have completed pending opening/closing operation
		/// and is used to raise GateClosed and GateOpened events
		/// on handling said events from the underlying gates.
		/// </summary>
		private uint gatesCompletedOperation = 0;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();
		}

		#region implemented abstract members of AbstractGateComponent

		public override event EventHandler<GateOpeningEventArgs> GateOpening 	= delegate{};
		public override event EventHandler<GateOpenedEventArgs> GateOpened 		= delegate{};
		public override event EventHandler<GateClosingEventArgs> GateClosing 	= delegate{};
		public override event EventHandler<GateClosedEventArgs> GateClosed 		= delegate{};

		public override void Open ()
		{
			this.GateOpening(
				this,
				new GateOpeningEventArgs(this)
			);

			this.gatesCompletedOperation = 0;
			foreach(var gate in this.gates)
			{
				gate.GateClosed -= this.OnUnderlyingGateClosed;
				gate.GateOpened += this.OnUnderlyingGateOpened;
				gate.Open();
			}
		}

		public override void Close ()
		{
			this.GateClosing(
				this,
				new GateClosingEventArgs(this)
			);

			this.gatesCompletedOperation = 0;
			foreach(var gate in this.gates)
			{
				gate.GateOpened -= this.OnUnderlyingGateOpened;
				gate.GateClosed += this.OnUnderlyingGateClosed;
				gate.Close();
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether all of the underlying gates are open.
		/// </summary>
		/// <value>
		/// <c>true</c> if all the underlying gates are open; otherwise, <c>false</c>.
		/// </value>
		public override bool IsOpen
		{
			get {
				foreach(var gate in this.gates)
				{
					if(!gate.IsOpen)
					{
						return false;
					}
				}
				return true;
			}
			protected set {
				throw new NotImplementedException ();
			}
		}

		#endregion

		private void OnUnderlyingGateOpened(object sender, GateOpenedEventArgs args)
		{
			this.gatesCompletedOperation++;
			if(this.gatesCompletedOperation == this.gates.Length)
			{
				this.GateOpened(
					this,
					new GateOpenedEventArgs(this)
				);
			}
		}

		private void OnUnderlyingGateClosed(object sender, GateClosedEventArgs args)
		{
			this.gatesCompletedOperation++;
			if(this.gatesCompletedOperation == this.gates.Length)
			{
				this.GateClosed(
					this,
					new GateClosedEventArgs(this)
				);
			}
		}
	}
}

