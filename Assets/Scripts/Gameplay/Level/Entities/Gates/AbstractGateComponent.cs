using UnityEngine;
using System;

namespace Momentum.Gameplay.Level.Entities.Gates
{
	public abstract class AbstractGateComponent : MonoBehaviour, IGate
	{
		#region IGate implementation

		public abstract event EventHandler<GateOpeningEventArgs> GateOpening;
		public abstract event EventHandler<GateOpenedEventArgs> GateOpened;
		public abstract event EventHandler<GateClosingEventArgs> GateClosing;
		public abstract event EventHandler<GateClosedEventArgs> GateClosed;

		public abstract void Open();
		public abstract void Close();

		public abstract bool IsOpen {
			get;
			protected set;
		}

		#endregion


	}
}

