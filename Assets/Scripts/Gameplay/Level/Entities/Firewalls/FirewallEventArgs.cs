using System;

namespace Momentum.Gameplay.Level.Entities.Firewalls
{
	public abstract class FirewallEventArgs : EventArgs
	{
		private readonly IFirewall firewall;

		public FirewallEventArgs(IFirewall firewall)
		{
			if(firewall == null)
			{
				throw new ArgumentNullException("firewall");
			}
			this.firewall = firewall;
		}

		public IFirewall Firewall
		{
			get {
				return this.firewall;
			}
		}
	}

	public class FirewallColorChangedEventArgs : FirewallEventArgs
	{
		private readonly FirewallColor oldColor;
		private readonly FirewallColor newColor;

		public FirewallColorChangedEventArgs(
			IFirewall firewall,
			FirewallColor oldColor,
			FirewallColor newColor
		) : base(firewall)
		{
			this.oldColor = oldColor;
			this.newColor = newColor;
		}

		public FirewallColor OldColor
		{
			get {
				return this.oldColor;
			}
		}

		public FirewallColor NewColor
		{
			get {
				return this.newColor;
			}
		}
	}

	public class FirewallInteractionOccurredEventArgs : FirewallEventArgs
	{
		private readonly IFirewallVulnerability interactedVulnerability;

		public FirewallInteractionOccurredEventArgs(
			IFirewall firewall,
			IFirewallVulnerability interactedVulnerability
		) : base(firewall)
		{
			if(interactedVulnerability == null)
			{
				throw new ArgumentNullException("firewallVulnerability");
			}
			this.interactedVulnerability = interactedVulnerability;
		}

		public IFirewallVulnerability InteractedVulnerability
		{
			get {
				return this.interactedVulnerability;
			}
		}
	}

	public class FirewallInteractionIgnoredEventArgs : FirewallEventArgs
	{
		private readonly IFirewallVulnerability ignoredVulnerability;
		
		public FirewallInteractionIgnoredEventArgs(
			IFirewall firewall,
			IFirewallVulnerability ignoredVulnerability
		) : base(firewall)
		{
			if(ignoredVulnerability == null)
			{
				throw new ArgumentNullException("firewallVulnerability");
			}
			this.ignoredVulnerability = ignoredVulnerability;
		}
		
		public IFirewallVulnerability IgnoredVulnerability
		{
			get {
				return this.ignoredVulnerability;
			}
		}
	}
}

