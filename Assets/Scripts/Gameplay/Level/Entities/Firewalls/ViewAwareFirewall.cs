using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

namespace Momentum.Gameplay.Level.Entities.Firewalls
{
	using View;

	public class ViewAwareFirewall : AbstractFirewallComponent 
	{
		[SerializeField]
		private FirewallColor initialColor = FirewallColor.RED;
		[SerializeField]
		private AbstractFirewallViewComponent firewallView;

		private FirewallColor color;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			this.Color = this.initialColor;
		}

		#region implemented abstract members of AbstractFirewallComponent
		
		public override event EventHandler<FirewallColorChangedEventArgs> FirewallColorChanged = delegate{};
		public override event EventHandler<FirewallInteractionOccurredEventArgs> FirewallInteractionOccurred = delegate{};
		public override event EventHandler<FirewallInteractionIgnoredEventArgs> FirewallInteractionIgnored = delegate{};
		
		protected override void OnInteractionOccurred(IFirewallVulnerability vulnerability)
		{
			if(vulnerability.ExceptColor != this.Color)
			{
				vulnerability.OnFirewallHit(this);

				this.firewallView.OnRealInteraction(vulnerability);
				this.FirewallInteractionOccurred(
					this,
					new FirewallInteractionOccurredEventArgs(
					this,
					vulnerability
					)
				);
			}
			else
			{
				this.firewallView.OnIgnoredInteraction(vulnerability);
				this.FirewallInteractionIgnored(
					this,
					new FirewallInteractionIgnoredEventArgs(
					this,
					vulnerability
					)
				);
			}
		}
		
		public override FirewallColor Color {
			get {
				return this.color;
			}
			set {
				if(this.color != value)
				{
					var oldValue = this.color;
					this.color = value;

					this.firewallView.Color = this.color;
					this.FirewallColorChanged(
						this,
						new FirewallColorChangedEventArgs(
						this,
						oldValue,
						this.color
						)
					);
				}
			}
		}
		#endregion
	}
}

