using System;

namespace Momentum.Gameplay.Level.Entities.Firewalls
{
	/// <summary>
	/// Describes immunity changers - entities,
	/// responsible for changing the value of ExceptColor
	/// property of IFirewallVulnerability components, 
	/// interacting with them.
	/// </summary>
	public interface IFirewallImmunityChanger
	{
		FirewallColor NewExceptColor {get;set;}

		event EventHandler<ImmunityChangerColorChangedEventArgs> ColorChanged;
		event EventHandler<ImmunityChangerInteractionOccurredEventArgs> InteractionOccurred;
	}
}

