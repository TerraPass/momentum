using UnityEngine;
using System;

namespace Momentum.Gameplay.Level.Entities.Firewalls
{
	public enum FirewallColor
	{
		NONE 	= 0,
		RED 	= 1,
		GREEN 	= 2,
		BLUE 	= 3,
		YELLOW 	= 4
	}

	public static class FirewallColorExtensions
	{
		private static readonly Color DEFAULT_UNITY_COLOR = Color.grey;

		public static Color GetUnityColor(this FirewallColor firewallColor)
		{
			switch(firewallColor)
			{
			default:
				throw new InvalidOperationException(
					String.Format(
						"Firewall color {0} has no corresponding Unity color defined",
						firewallColor
					)
				);
			case FirewallColor.RED: return new Color(1.0f, 0.0f, 0.0f);
			case FirewallColor.GREEN: return new Color(0.0f, 1.0f, 0.0f);
			case FirewallColor.BLUE: return new Color(0.0f, 0.0f, 1.0f);
			case FirewallColor.YELLOW: return new Color(1.0f, 1.0f, 0.0f);
			}
		}

		public static bool TryGetUnityColor(this FirewallColor firewallColor, out Color result)
		{
			try {
				result = firewallColor.GetUnityColor();
				return true;
			}
			catch(InvalidOperationException)
			{
				result = default(Color);
				return false;
			}
		}

		public static Color GetUnityColorOrDefault(this FirewallColor firewallColor, Color defaultValue)
		{
			try
			{
				return firewallColor.GetUnityColor();
			}
			catch(InvalidOperationException)
			{
				return defaultValue;
			}
		}

		public static Color GetUnityColorOrDefault(this FirewallColor firewallColor)
		{
			return firewallColor.GetUnityColorOrDefault(DEFAULT_UNITY_COLOR);
		}
	}
}

