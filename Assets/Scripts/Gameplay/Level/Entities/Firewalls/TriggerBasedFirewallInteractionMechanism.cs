using UnityEngine;
using System;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

namespace Momentum.Gameplay.Level.Entities.Firewalls
{
	[RequireComponent(typeof(Collider))]
	public class TriggerBasedFirewallInteractionMechanism
		: AbstractFirewallComponent.InteractionMechanismComponent
	{
		private Collider MyCollider
		{
			get {
				return this.GetComponent<Collider>();
			}
		}

		protected override void Start()
		{
			base.Start();

			DebugUtils.Assert(
				this.MyCollider != null,
				String.Format(
					"{0} component requires Collider",
					this.GetType()
				)
			);

			DebugUtils.Assert(
				this.MyCollider.isTrigger,
				String.Format(
					"{0} component expects Collider to have its isTrigger property set to true",
					this.GetType()
				)
			);

			this.EnsureRequiredFieldsAreSetInEditor();
		}

		void OnTriggerEnter(Collider collider)
		{
			var vulnerability = collider.gameObject.GetComponent<IFirewallVulnerability>();
			if(vulnerability != null)
			{
				this.SignalInteraction(vulnerability);
			}
		}
	}
}

