using System;

namespace Momentum.Gameplay.Level.Entities.Firewalls
{
	public abstract class FirewallImmunityChangerEventArgs : EventArgs
	{
		private readonly IFirewallImmunityChanger immunityChanger;

		public FirewallImmunityChangerEventArgs(IFirewallImmunityChanger immunityChanger)
		{
			if(immunityChanger == null)
			{
				throw new ArgumentNullException("immunityChanger");
			}
			this.immunityChanger = immunityChanger;
		}

		public IFirewallImmunityChanger ImmunityChanger
		{
			get {
				return this.immunityChanger;
			}
		}
	}

	public class ImmunityChangerColorChangedEventArgs : FirewallImmunityChangerEventArgs
	{
		private readonly FirewallColor newColor;

		public ImmunityChangerColorChangedEventArgs(
			IFirewallImmunityChanger immunityChanger,
			FirewallColor newColor
		) : base(immunityChanger)
		{
			this.newColor = newColor;
		}

		public FirewallColor NewColor
		{
			get {
				return this.newColor;
			}
		}
	}

	public class ImmunityChangerInteractionOccurredEventArgs : FirewallImmunityChangerEventArgs
	{
		private readonly IFirewallVulnerability vulnerability;

		public ImmunityChangerInteractionOccurredEventArgs(
			IFirewallImmunityChanger immunityChanger,
			IFirewallVulnerability vulnerability
		) : base(immunityChanger)
		{
			if(vulnerability == null)
			{
				throw new ArgumentNullException("vulnerability");
			}
			this.vulnerability = vulnerability;
		}

		public IFirewallVulnerability Vulnerability
		{
			get {
				return this.vulnerability;
			}
		}
	}
}

