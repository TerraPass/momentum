using System;

namespace Momentum.Gameplay.Level.Entities.Firewalls
{
	public interface IFirewall : ILevelEntity
	{
		FirewallColor Color {get;set;}

		event EventHandler<FirewallColorChangedEventArgs> FirewallColorChanged;
		event EventHandler<FirewallInteractionOccurredEventArgs> FirewallInteractionOccurred;
		event EventHandler<FirewallInteractionIgnoredEventArgs> FirewallInteractionIgnored;
	}
}

