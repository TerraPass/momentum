using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

namespace Momentum.Gameplay.Level.Entities.Firewalls
{
	public abstract class AbstractFirewallComponent : MonoBehaviour, IFirewall
	{
		#region IFirewall implementation

		public abstract event EventHandler<FirewallColorChangedEventArgs> FirewallColorChanged;
		public abstract event EventHandler<FirewallInteractionOccurredEventArgs> FirewallInteractionOccurred;
		public abstract event EventHandler<FirewallInteractionIgnoredEventArgs> FirewallInteractionIgnored;

		public abstract FirewallColor Color {get;set;}

		#endregion

		protected abstract void OnInteractionOccurred(IFirewallVulnerability vulnerability);

		public abstract class InteractionMechanismComponent : MonoBehaviour
		{
			[SerializeField]
			private AbstractFirewallComponent firewall;

			protected virtual void Start()
			{
				this.EnsureRequiredFieldsAreSetInEditor();
			}

			protected AbstractFirewallComponent Firewall
			{
				get {
					return this.firewall;
				}
			}

			protected void SignalInteraction(IFirewallVulnerability vulnerability)
			{
				this.Firewall.OnInteractionOccurred(vulnerability);
			}
		}
	}
}

