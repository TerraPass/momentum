using UnityEngine;
using System;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

namespace Momentum.Gameplay.Level.Entities.Firewalls
{
	// TODO: Move collision detection to a separate
	// class, extending AbstractFirewallImmunityChangerComponent.InteractionMechanism 
	// (or equivalent generic class).
	[RequireComponent(typeof(Collider))]
	public class FirewallImmunityChangerPickup : AbstractFirewallImmunityChangerComponent
	{
		[SerializeField]
		private FirewallColor initialColor;

		private Collider MyCollider
		{
			get {
				return this.GetComponent<Collider>();
			}
		}

		private FirewallColor color;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			DebugUtils.Assert(
				this.MyCollider != null,
				"{0} component requires Collider component",
				this.GetType()
			);

			DebugUtils.Assert(
				this.MyCollider.isTrigger,
				"{0} components expects Collider component to be a trigger",
				this.GetType()
			);

			this.NewExceptColor = this.initialColor;
		}

		#region implemented abstract members of AbstractFirewallImmunityChangerComponent

		public override event EventHandler<ImmunityChangerColorChangedEventArgs> ColorChanged = delegate{};
		public override event EventHandler<ImmunityChangerInteractionOccurredEventArgs> InteractionOccurred = delegate{};

		public override FirewallColor NewExceptColor
		{
			get {
				return this.color;
			}
			set {
				if(this.color != value)
				{
					this.color = value;
					this.ColorChanged(
						this,
						new ImmunityChangerColorChangedEventArgs(
							this,
							this.color
						)
					);
				}
			}
		}

		#endregion

		protected override void OnInteractionOccurred(IFirewallVulnerability vulnerability)
		{
			if(vulnerability.ExceptColor != this.NewExceptColor)
			{
				// TODO: If later down the line IFirewallVulnerability needs to be notified
				// about the exact changer, which changed its ExceptColor,
				// a separate SetExceptColor(FirewallColor color, IFirewallImmunityChanger changer = null)
				// method, which would be invoked from here instead of ExceptColor setter,
				// might be added to its interface.
				vulnerability.ExceptColor = this.NewExceptColor;
				this.InteractionOccurred(
					this,
					new ImmunityChangerInteractionOccurredEventArgs(
						this,
						vulnerability
					)
				);
			}
		}

		// TODO: Move collision detection to a separate class (see the TODO before the class).
		void OnTriggerEnter(Collider collider)
		{
			var vulnerability = collider.gameObject.GetComponent<IFirewallVulnerability>();
			if(vulnerability != null)
			{
				this.OnInteractionOccurred(vulnerability);
			}
		}
	}
}

