using UnityEngine;
using System;

namespace Momentum.Gameplay.Level.Entities.Firewalls
{
	public class ViewUnawareFirewall : AbstractFirewallComponent 
	{
		[SerializeField]
		private FirewallColor color = FirewallColor.RED;

		#region implemented abstract members of AbstractFirewallComponent

		public override event EventHandler<FirewallColorChangedEventArgs> FirewallColorChanged = delegate{};
		public override event EventHandler<FirewallInteractionOccurredEventArgs> FirewallInteractionOccurred = delegate{};
		public override event EventHandler<FirewallInteractionIgnoredEventArgs> FirewallInteractionIgnored = delegate{};

		protected override void OnInteractionOccurred(IFirewallVulnerability vulnerability)
		{
			if(vulnerability.ExceptColor != this.Color)
			{
				vulnerability.OnFirewallHit(this);

				this.FirewallInteractionOccurred(
					this,
					new FirewallInteractionOccurredEventArgs(
						this,
						vulnerability
					)
				);
			}
			else
			{
				this.FirewallInteractionIgnored(
					this,
					new FirewallInteractionIgnoredEventArgs(
						this,
						vulnerability
					)
				);
			}
		}

		public override FirewallColor Color {
			get {
				return this.color;
			}
			set {
				if(this.color != value)
				{
					var oldValue = this.color;
					this.color = value;

					this.FirewallColorChanged(
						this,
						new FirewallColorChangedEventArgs(
							this,
							oldValue,
							this.color
						)
					);
				}
			}
		}
		#endregion
	}
}

