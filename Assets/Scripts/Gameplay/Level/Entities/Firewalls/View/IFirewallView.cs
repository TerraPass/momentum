using System;

namespace Momentum.Gameplay.Level.Entities.Firewalls.View
{
	// TODO: Change IFirewallView and its implementers to rely on
	// events, raised by IFirewall, instead of exposing callbacks.
	// (If needed.)
	public interface IFirewallView
	{
		FirewallColor Color {get;set;}

		void OnRealInteraction(IFirewallVulnerability vulnerability);
		void OnIgnoredInteraction(IFirewallVulnerability vulnerability);
	}
}

