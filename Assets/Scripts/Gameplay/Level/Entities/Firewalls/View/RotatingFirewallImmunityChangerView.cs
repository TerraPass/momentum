using UnityEngine;
using System;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

namespace Momentum.Gameplay.Level.Entities.Firewalls.View
{
	public class RotatingFirewallImmunityChangerView
		: AbstractFirewallImmunityChangerViewComponent
	{
		[SerializeField]
		private AbstractFirewallImmunityChangerComponent immunityChanger;

		[SerializeField]
		private GameObject rotatingObject;
		[SerializeField]
		[Tooltip("Angular speed of view rotation in degrees per second")]
		private float angularSpeed = 45.0f;

		private Renderer RotatingObjectRenderer
		{
			get {
				return rotatingObject.GetComponent<Renderer>();
			}
		}

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			DebugUtils.Assert(
				this.RotatingObjectRenderer != null,
				"{0} expects the gameObject, set as its rotatingObject, to have a Renderer attached",
				this.GetType()
			);

			this.immunityChanger.ColorChanged += this.OnColorChanged;
			this.immunityChanger.InteractionOccurred += this.OnInteractionOccurred;

			this.SetMaterialColor(this.immunityChanger.NewExceptColor.GetUnityColorOrDefault());
		}

		void Update()
		{
			this.rotatingObject.transform.RotateAround(
				this.transform.position,
				Vector3.up,
				this.angularSpeed * Time.deltaTime
			);
		}

		private void OnColorChanged(object sender, ImmunityChangerColorChangedEventArgs args)
		{
			 this.SetMaterialColor(args.NewColor.GetUnityColorOrDefault());
		}

		private void OnInteractionOccurred(object sender, ImmunityChangerInteractionOccurredEventArgs args)
		{
			// TODO: Trigger some interaction animation.
		}

		private void SetMaterialColor(Color color)
		{
			// Preserve alpha
			color.a = this.RotatingObjectRenderer.material.color.a;
			this.RotatingObjectRenderer.material.color = color;
		}
	}
}

