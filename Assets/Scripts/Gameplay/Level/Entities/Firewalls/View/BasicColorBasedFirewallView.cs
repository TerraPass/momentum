using UnityEngine;
using System;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

namespace Momentum.Gameplay.Level.Entities.Firewalls.View
{
	[RequireComponent(typeof(Renderer))]
	public class BasicColorBasedFirewallView : AbstractFirewallViewComponent
	{
		private Renderer MyRenderer
		{
			get {
				return this.GetComponent<Renderer>();
			}
		}

		private FirewallColor color;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			DebugUtils.Assert(
				this.MyRenderer != null,
				"{0} component requires Renderer component",
				this.GetType()
			);
		}

		#region implemented abstract members of AbstractFirewallViewComponent

		public override void OnRealInteraction (IFirewallVulnerability vulnerability)
		{
			// TODO: Trigger interaction animation.
		}

		public override void OnIgnoredInteraction (IFirewallVulnerability vulnerability)
		{
			// TODO: Trigger ignored interaction animation.
		}

		public override FirewallColor Color
		{
			get {
				return this.color;
			}
			set {
				if(this.color != value)
				{
					this.color = value;
					this.SetMaterialColor(this.color.GetUnityColorOrDefault());
				}
			}
		}

		#endregion

		private void SetMaterialColor(Color color)
		{
			// Preserve alpha
			color.a = this.MyRenderer.material.color.a;
			this.MyRenderer.material.color = color;
		}
	}
}

