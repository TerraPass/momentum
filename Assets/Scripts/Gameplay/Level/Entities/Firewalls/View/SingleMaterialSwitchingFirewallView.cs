using UnityEngine;
using System;
using System.Collections.Generic;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

namespace Momentum.Gameplay.Level.Entities.Firewalls.View
{
	[RequireComponent(typeof(Renderer))]
	public class SingleMaterialSwitchingFirewallView : AbstractFirewallViewComponent
	{
		// TODO: Generalize dictionary builder from Momentum.UI.Common.Menu.UnityWorkaround
		// and use it for cases like these.
		[Serializable]
		private struct ColorToMaterialMapping
		{
			[SerializeField]
			private FirewallColor color;
			[SerializeField]
			private Material material;

			public FirewallColor Color
			{
				get {
					return this.color;
				}
			}

			public Material Material
			{
				get {
					return this.material;
				}
			}
		}

		[SerializeField]
		private ColorToMaterialMapping[] materialMappings;

		private IDictionary<FirewallColor, Material> materials;

		private FirewallColor color;

		private Renderer MyRenderer
		{
			get {
				return this.GetComponent<Renderer>();
			}
		}

		void Start()
		{
			DebugUtils.Assert(
				this.MyRenderer != null,
				String.Format(
					"{0} component requires Renderer",
					this.GetType()
				)
			);

			this.EnsureRequiredFieldsAreSetInEditor();

			this.materials = new Dictionary<FirewallColor, Material>();
			foreach(var mapping in materialMappings)
			{
				this.materials[mapping.Color] = mapping.Material;
			}

			UpdateMaterial();
		}

		#region implemented abstract members of AbstractFirewallViewComponent

		public override void OnRealInteraction (IFirewallVulnerability vulnerability)
		{
			// TODO: Trigger interaction animation.
		}

		public override void OnIgnoredInteraction (IFirewallVulnerability vulnerability)
		{
			// TODO: Trigger ignored interaction animation.
		}

		public override FirewallColor Color
		{
			get {
				return this.color;
			}
			set {
				if(this.color != value)
				{
					this.color = value;
					UpdateMaterial();
				}
			}
		}

		#endregion

		private void UpdateMaterial()
		{
			this.MyRenderer.material = this.materials[this.color];
		}
	}
}

