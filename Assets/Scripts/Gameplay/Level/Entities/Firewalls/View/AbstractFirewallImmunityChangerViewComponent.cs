using UnityEngine;
using System;

namespace Momentum.Gameplay.Level.Entities.Firewalls.View
{
	public abstract class AbstractFirewallImmunityChangerViewComponent
		: MonoBehaviour, IFirewallImmunityChangerView
	{

	}
}

