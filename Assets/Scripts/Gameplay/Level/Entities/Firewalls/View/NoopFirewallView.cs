using System;

namespace Momentum.Gameplay.Level.Entities.Firewalls.View
{
	public sealed class NoopFirewallView : IFirewallView
	{
		private FirewallColor color;

		public NoopFirewallView()
		{

		}

		#region IFirewallView implementation
		public FirewallColor Color
		{
			get {
				return color;
			}
			set {
				this.color = value;
			}
		}

		public void OnRealInteraction (IFirewallVulnerability vulnerability)
		{

		}

		public void OnIgnoredInteraction (IFirewallVulnerability vulnerability)
		{

		}

		#endregion
	}
}

