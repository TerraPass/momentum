using UnityEngine;
using System;

namespace Momentum.Gameplay.Level.Entities.Firewalls.View
{
	public abstract class AbstractFirewallViewComponent : MonoBehaviour, IFirewallView
	{
		#region IFirewallView implementation

		public abstract void OnRealInteraction (IFirewallVulnerability vulnerability);
		public abstract void OnIgnoredInteraction (IFirewallVulnerability vulnerability);

		public abstract FirewallColor Color {get;set;}

		#endregion
	}
}

