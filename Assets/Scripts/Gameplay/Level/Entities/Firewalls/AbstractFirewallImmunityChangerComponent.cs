using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

namespace Momentum.Gameplay.Level.Entities.Firewalls
{
	public abstract class AbstractFirewallImmunityChangerComponent
		: MonoBehaviour, IFirewallImmunityChanger
	{
		#region IFirewallImmunityChanger implementation

		public abstract FirewallColor NewExceptColor {get;set;}

		public abstract event EventHandler<ImmunityChangerColorChangedEventArgs> ColorChanged;
		public abstract event EventHandler<ImmunityChangerInteractionOccurredEventArgs> InteractionOccurred;

		#endregion

		protected abstract void OnInteractionOccurred(IFirewallVulnerability vulnerability);

		// TODO: This looks too much like AbstractFirewallComponent's interaction mechanism.
		// Maybe find some way to avoid duplication, using command pattern or something?
		// Maybe use a generic class (or classes) InteractionMechanism<TInteractable>,
		// TriggerBasedInteractionMechanism<TInteractable>?
		public abstract class InteractionMechanism : MonoBehaviour
		{
			[SerializeField]
			private AbstractFirewallImmunityChangerComponent immunityChanger;

			protected virtual void Start()
			{
				this.EnsureRequiredFieldsAreSetInEditor();
			}

			protected AbstractFirewallImmunityChangerComponent ImmunityChanger
			{
				get {
					return this.immunityChanger;
				}
			}

			protected void SignalInteraction(IFirewallVulnerability vulnerability)
			{
				this.ImmunityChanger.OnInteractionOccurred(vulnerability);
			}
		}
	}
}

