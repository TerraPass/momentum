using UnityEngine;
using System;

using Momentum.Gameplay.Player;

namespace Momentum.Gameplay.Level.Entities
{
	public abstract class LevelEntityEventArgs<TLevelEntity> : EventArgs where TLevelEntity : ILevelEntity
	{
		private readonly TLevelEntity entity;

		public LevelEntityEventArgs(TLevelEntity entity)
		{
			this.entity = entity;
		}

		public TLevelEntity Entity
		{
			get {
				return this.entity;
			}
		}
	}

	public abstract class InteractedWithBallEventArgs<TLevelEntity> 
		: LevelEntityEventArgs<TLevelEntity> where TLevelEntity : ILevelEntity
	{
		private readonly IBall ball;

		public InteractedWithBallEventArgs(
			TLevelEntity entity,
			IBall ball
		) : base(entity)
		{
			this.ball = ball;
		}

		public IBall Ball
		{
			get {
				return this.ball;
			}
		}
	}
}

