using UnityEngine;
using System;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

namespace Momentum.Gameplay.Level.Entities.Switches.View
{
	[RequireComponent(typeof(Renderer))]
	public class MaterialChangingSwitchView : MonoBehaviour
	{
		[SerializeField]
		private AbstractSwitchComponent switchComponent;

		[SerializeField]
		private Material defaultMaterial;
		[SerializeField]
		private Material activatedMaterial;

		private Renderer MyRenderer
		{
			get {
				return this.GetComponent<Renderer>();
			}
		}

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			DebugUtils.Assert(
				this.MyRenderer != null,
				"{0} component requires Renderer component",
				this.GetType()
			);

			this.switchComponent.SwitchViewUpdateRequested += this.OnSwitchViewUpdateRequested;
		}

		private void OnSwitchViewUpdateRequested(object sender, SwitchViewUpdateRequestedEventArgs args)
		{
			this.MyRenderer.material = args.IsActivated ? this.activatedMaterial : this.defaultMaterial;
		}
	}
}

