using UnityEngine;
using System;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

namespace Momentum.Gameplay.Level.Entities.Switches.View
{
	public class DisplacingSwitchView : MonoBehaviour
	{
		[SerializeField]
		private AbstractSwitchComponent switchComponent;

		[SerializeField]
		private Transform defaultTransform;
		[SerializeField]
		private Transform activatedTransform;
		[SerializeField]
		private float transformLerpAmount = 0.75f;

		// Points to either activatedTransform or defaultTransform,
		// depending on which of them this DisplacingSwitch must lerp to.
		private Transform targetTransform;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			this.targetTransform = this.defaultTransform;

			this.switchComponent.SwitchViewUpdateRequested += this.OnSwitchViewUpdateRequested;
		}

		void Update()
		{
			this.transform.LerpTo(
				this.targetTransform,
				this.transformLerpAmount
			);
		}

		private void OnSwitchViewUpdateRequested(object sender, SwitchViewUpdateRequestedEventArgs args)
		{
			this.targetTransform = args.IsActivated ? this.activatedTransform : this.defaultTransform;
		}
	}
}

