using UnityEngine;
using System;
using System.Collections.Generic;
using Terrapass.Extensions.Unity;

using Momentum.Common.Commands;

namespace Momentum.Gameplay.Level.Entities.Switches
{
	public class SwitchBinder : AbstractSwitchBinderComponent
	{
		[SerializeField]
		private AbstractSwitchComponent switchComponent; 
		[SerializeField]
		private List<AbstractCommandComponent> onSwitchActivatedCommands;
		[SerializeField]
		private List<AbstractCommandComponent> onSwitchDeactivatedCommands;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();
			this.switchComponent.SwitchActivated += this.OnSwitchActivated;
			this.switchComponent.SwitchDeactivated += this.OnSwitchDeactivated;
		}

		private void OnSwitchActivated(object sender, SwitchActivatedEventArgs args)
		{
			foreach(var command in onSwitchActivatedCommands)
			{
				command.Execute();
			}
		}

		private void OnSwitchDeactivated(object sender, SwitchDeactivatedEventArgs args)
		{
			foreach(var command in onSwitchDeactivatedCommands)
			{
				command.Execute();
			}
		}
	}
}

