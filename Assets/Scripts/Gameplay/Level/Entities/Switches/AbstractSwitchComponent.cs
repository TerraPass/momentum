using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

using Momentum.Gameplay.Player;

namespace Momentum.Gameplay.Level.Entities.Switches
{
	public abstract class AbstractSwitchComponent : MonoBehaviour, ISwitch
	{
		#region ISwitch implementation

		public abstract event EventHandler<SwitchActivatedEventArgs> SwitchActivated;

		public abstract event EventHandler<SwitchDeactivatedEventArgs> SwitchDeactivated;

		public abstract event EventHandler<SwitchViewUpdateRequestedEventArgs> SwitchViewUpdateRequested;

		public abstract bool IsActivated {get;}

		#endregion

		protected abstract void SetActivated(bool value, IBall interactingBall);

		public abstract class ActivationMechanismComponent : MonoBehaviour
		{
			[SerializeField]
			private AbstractSwitchComponent switchComponent;
			
			protected virtual void Start()
			{
				this.EnsureRequiredFieldsAreSetInEditor();
			}
			
			protected AbstractSwitchComponent Switch {
				get {
					return this.switchComponent;
				}
				set {
					this.switchComponent = value;
				}
			}

			protected void SetActivated(bool value, IBall interactingBall)
			{
				this.Switch.SetActivated(value, interactingBall);
			}
		}
	}
}

