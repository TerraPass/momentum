using UnityEngine;
using System;

using Momentum.Common.Commands;

namespace Momentum.Gameplay.Level.Entities.Switches
{
	/// <summary>
	///	This interface describes objects responsible for binding
	/// events, raised by ISwitch to specific commands.
	/// </summary>
	public interface ISwitchBinder
	{

	}
}

