using UnityEngine;
using System;

namespace Momentum.Gameplay.Level.Entities.Switches
{
	public interface ISwitch : ILevelEntity
	{
		event EventHandler<SwitchActivatedEventArgs> SwitchActivated;
		event EventHandler<SwitchDeactivatedEventArgs> SwitchDeactivated;

		event EventHandler<SwitchViewUpdateRequestedEventArgs> SwitchViewUpdateRequested;

		bool IsActivated {get;}
	}
}

