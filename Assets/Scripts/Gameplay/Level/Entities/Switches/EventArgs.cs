using UnityEngine;
using System;

using Momentum.Gameplay.Player;

namespace Momentum.Gameplay.Level.Entities.Switches
{
	public class SwitchActivatedEventArgs : InteractedWithBallEventArgs<ISwitch> 
	{
		public SwitchActivatedEventArgs(
			ISwitch entity,
			IBall ball
		) : base(entity, ball)
		{
			
		}
	}
	
	public class SwitchDeactivatedEventArgs : InteractedWithBallEventArgs<ISwitch> 
	{
		public SwitchDeactivatedEventArgs(
			ISwitch entity,
			IBall ball
		) : base(entity, ball)
		{
			
		}
	}

	public class SwitchViewUpdateRequestedEventArgs : EventArgs
	{
		private readonly ISwitch switchEntity;
		private readonly bool activated;

		public SwitchViewUpdateRequestedEventArgs(
			ISwitch switchEntity,
			bool activated
		)
		{
			if(switchEntity == null)
			{
				throw new ArgumentNullException("switchEntity");
			}
			this.switchEntity = switchEntity;
			this.activated = activated;
		}

		public ISwitch Switch
		{
			get {
				return this.switchEntity;
			}
		}

		public bool IsActivated
		{
			get {
				return this.activated;
			}
		}
	}
}
