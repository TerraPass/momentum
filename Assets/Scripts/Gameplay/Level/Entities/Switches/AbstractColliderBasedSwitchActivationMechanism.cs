using UnityEngine;
using System;
using Terrapass.Debug;
using Terrapass.Extensions.Unity;

using Momentum.Gameplay.Player;
using Momentum.Utils.Time;

namespace Momentum.Gameplay.Level.Entities.Switches
{
	[RequireComponent(typeof(Collider))]
	public abstract class AbstractColliderBasedSwitchActivationMechanism : AbstractSwitchComponent.ActivationMechanismComponent
	{
		/// <summary>
		/// Delay in seconds after the last IBall has stopped colliding
		/// before the button gets deactivated.
		/// </summary>
		[SerializeField]
		private float collisionExitDelay = 0.25f;

		private Collider myCollider;

		/// <summary>
		/// Number of IBalls currently colliding with this button
		/// </summary>
		private int ongoingCollisionsCount;
		
		private IResettableTimer collisionExitTimer;
		private IBall latestCollidedBall;
		
		protected override void Start ()
		{
			base.Start();
			this.myCollider = this.gameObject.GetComponent<Collider>();
			DebugUtils.Assert(
				this.myCollider != null,
				"ColliderBasedSwitchActivationMechanism component requires Collider"
			);
			this.EnsureRequiredFieldsAreSetInEditor();

			this.ongoingCollisionsCount = 0;
			this.collisionExitTimer = new ResettableExecutionTimer(true);
		}
		
		void FixedUpdate ()
		{
			// Observe collision exit delay timer 
			// and deactivate button once enough time has passed.
			if(this.Switch.IsActivated && this.ongoingCollisionsCount <= 0)
			{
				if(this.collisionExitTimer.ElapsedSeconds >= this.collisionExitDelay)
				{
					DebugUtils.Assert(
						this.latestCollidedBall != null,
						"latestCollidedBall must be set by OnBallCollisionEnded() when starting collision exit delay timer"
					);
					this.SetActivated(false, this.latestCollidedBall);
					this.collisionExitTimer.Reset(true);
				}
			}
		}

		protected void OnBallCollisionStarted(IBall collidingBall)
		{
			// If an alive IBall has collided with us
			if(collidingBall != null && collidingBall.IsAlive())
			{
				this.ongoingCollisionsCount++;
				this.SetActivated(true, collidingBall);
				collidingBall.BallLifeStatusChanged += this.OnBallLifeStatusChanged;
			}
		}

		protected void OnBallCollisionEnded(IBall collidingBall, bool ballDied = false)
		{
			// If an alive IBall has stopped colliding with us
			// or a colliding ball has died
			if(collidingBall != null && (ballDied || collidingBall.IsAlive()))
			{
				this.ongoingCollisionsCount--;
				collidingBall.BallLifeStatusChanged -= this.OnBallLifeStatusChanged;
				if(this.ongoingCollisionsCount <= 0)
				{
					// Start collision delay countdown.
					// Deactivation will be performed by Update()
					// when it ticks out.
					this.latestCollidedBall = collidingBall;
					this.collisionExitTimer.Resume();
				}
			}
		}

		protected Collider Collider
		{
			get {
				return this.myCollider;
			}
		}

		private void OnBallLifeStatusChanged(object sender, BallLifeStatusChangedEventArgs args)
		{
			if(args.NewLifeStatus != BallLifeStatus.ALIVE)
			{
				this.OnBallCollisionEnded(args.Ball, true);
			}
		}
	}
}

