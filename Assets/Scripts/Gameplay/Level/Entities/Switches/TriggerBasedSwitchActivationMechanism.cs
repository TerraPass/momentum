using UnityEngine;
using System;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

using Momentum.Gameplay.Player;
using Momentum.Utils.Time;

namespace Momentum.Gameplay.Level.Entities.Switches
{
	public class TriggerBasedSwitchActivationMechanism : AbstractColliderBasedSwitchActivationMechanism
	{
		void OnTriggerEnter(Collider collider)
		{
			this.OnBallCollisionStarted(collider.gameObject.GetComponent<IBall>());
		}

		void OnTriggerExit(Collider collider)
		{
			this.OnBallCollisionEnded(collider.gameObject.GetComponent<IBall>());
		}
	}
}

