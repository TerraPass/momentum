using UnityEngine;
using System;

using Momentum.Gameplay.Player;

namespace Momentum.Gameplay.Level.Entities.Switches
{
	public class CollisionBasedSwitchActivationMechanism : AbstractColliderBasedSwitchActivationMechanism
	{
		void OnCollisionEnter(Collision collision)
		{
			this.OnBallCollisionStarted(collision.gameObject.GetComponent<IBall>());
		}

		void OnCollisionExit(Collision collision)
		{
			this.OnBallCollisionEnded(collision.gameObject.GetComponent<IBall>());
		}
	}
}

