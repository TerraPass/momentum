using UnityEngine;
using System;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

using Momentum.Gameplay.Player;
using Momentum.Gameplay.Common;
using Momentum.Gameplay.TimeMechanic.Recording;

namespace Momentum.Gameplay.Level.Entities.Switches
{
	public class Switch 
		: AbstractSwitchComponent, IResettable, IRecorderAware, IRecordable
	{
		[SerializeField]
		private bool isActivated = false;

		private IRecorder recorder;

		// Points to the ball, by which this switch was last activated.
		private IBall lastActivatedBy;

		public override event EventHandler<SwitchActivatedEventArgs> SwitchActivated 		= delegate {};
		public override event EventHandler<SwitchDeactivatedEventArgs> SwitchDeactivated 	= delegate {};
		public override event EventHandler<SwitchViewUpdateRequestedEventArgs> SwitchViewUpdateRequested = delegate {};

		protected virtual void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			this.lastActivatedBy = null;
		}

		public override bool IsActivated 
		{
			get {
				return this.isActivated;
			}
		}

		protected override void SetActivated(bool value, IBall interactingBall)
		{
			if(!this.IsActivated && value)
			{
				this.SwitchActivated(
					this,
					new SwitchActivatedEventArgs(
						this,
						interactingBall
					)
				);
				this.lastActivatedBy = interactingBall;
			}
			else if(this.IsActivated && !value)
			{
				this.SwitchDeactivated(
					this,
					new SwitchDeactivatedEventArgs(
						this,
						interactingBall
					)
				);
			}
			this.isActivated = value;
			this.SetActivatedView(this.isActivated);
		}

		private void SetActivatedView(bool activated)
		{
			this.SwitchViewUpdateRequested(
				this,
				new SwitchViewUpdateRequestedEventArgs(
					this,
					activated
				)
			);
		}

		#region IResettable implementation

		public void Reset ()
		{
			this.SetActivated(false, null);
		}

		#endregion

		#region IRecorderAware implementation

		public IRecorder Recorder
		{
			get {
				return this.recorder;
			}
			set {
				if(this.recorder != null)
				{
					this.recorder.UnregisterRecordable(this);
				}
				this.recorder = value;
				// Rewritable history is needed, since
				// the switch may be affected by balls
				// even after its history has already been recorded once.
				this.recorder.RegisterRecordable(this, new RewritableInterpolatedHistory());
			}
		}

		#endregion

		#region IRecordable implementation

		public void RecordFrame (IRecorder recorder, ref IHistoryRecord snapshot)
		{
			snapshot.StateObject = new SwitchState(this.IsActivated, this.lastActivatedBy);
		}

		public void PlayFrame (IRecorder recorder, IHistoryRecord snapshot)
		{
			if(recorder.IsPlayingBackward())
			{
				SwitchState state = (SwitchState)snapshot.StateObject;
				// If on this frame the switch was activated and the ball,
				// which activated it is still alive.
				if(state.isActivated 
				   && state.lastActivatedBy != null && state.lastActivatedBy.IsAlive())
				{
					this.SetActivatedView(true);
				}
				else if(!state.isActivated)
				{
					this.SetActivatedView(false);
				}
			}
		}

		#endregion

		private struct SwitchState
		{
			public readonly bool isActivated;
			public readonly IBall lastActivatedBy;

			public SwitchState(bool isActivated, IBall lastActivatedBy)
			{
				this.isActivated = isActivated;
				this.lastActivatedBy = lastActivatedBy;
			}
		}
	}
}
