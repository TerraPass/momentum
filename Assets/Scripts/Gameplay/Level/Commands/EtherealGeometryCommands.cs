using System;

using Momentum.Common.Commands;
using Momentum.Gameplay.Level.Entities.Ethereal;

namespace Momentum.Gameplay.Level.Commands
{
	public class MaterializeEtherealGeometryCommand : SimpleCommand
	{
		public MaterializeEtherealGeometryCommand(IEtherealGeometry etherealGeometry)
			: base(etherealGeometry.Materialize)
		{

		}
	}

	public class ImmaterializeEtherealGeometryCommand : SimpleCommand
	{
		public ImmaterializeEtherealGeometryCommand(IEtherealGeometry etherealGeometry)
			: base(etherealGeometry.Immaterialize)
		{

		}
	}
}

