using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

using Momentum.Common.Commands;
using Momentum.Gameplay.Level.Entities.Gates;

namespace Momentum.Gameplay.Level.Commands.Components
{
	public class CloseGateCommandComponent : AbstractCommandComponent<CloseGateCommand>
	{
		[SerializeField]
		private AbstractGateComponent gate;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();
			this.Command = new CloseGateCommand(this.gate);
		}
	}
}

