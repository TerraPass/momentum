using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

using Momentum.Common.Commands;
using Momentum.Gameplay.Level.Entities.Ethereal;

namespace Momentum.Gameplay.Level.Commands.Components
{
	public class MaterializeEtherealGeometryCommandComponent : AbstractCommandComponent<MaterializeEtherealGeometryCommand>
	{
		[SerializeField]
		private AbstractEtherealGeometryComponent etherealGeometry;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();
			this.Command = new MaterializeEtherealGeometryCommand(this.etherealGeometry);
		}
	}
}

