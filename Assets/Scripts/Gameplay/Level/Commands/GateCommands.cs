using UnityEngine;
using System;

using Momentum.Common.Commands;
using Momentum.Gameplay.Level.Entities.Gates;

namespace Momentum.Gameplay.Level.Commands
{
	public class OpenGateCommand : SimpleCommand
	{
		public OpenGateCommand(IGate receiver)
			: base(receiver.Open)
		{

		}
	}

	public class CloseGateCommand : SimpleCommand
	{
		public CloseGateCommand(IGate receiver)
			: base(receiver.Close)
		{

		}
	}
}

