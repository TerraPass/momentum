using UnityEngine;
using System;
using System.Collections.Generic;

using Momentum.Gameplay.Common;
using Momentum.Gameplay.TimeMechanic.Recording;

namespace Momentum.Gameplay.Level
{
	public class GameLevelRoot : AbstractGameLevelRootComponent
	{
		#region implemented abstract members of AbstractGameLevelRootComponent

		public override void ResetAll()
		{
			var resettables = this.GetChildrenImplementing<IResettable>();
			foreach(var resettable in resettables)
			{
				resettable.Reset();
			}
		}

		public override bool EnabledAll
		{
			set {
				var disableables = this.GetChildrenImplementing<IDisableable>();
				foreach(var disableable in disableables)
				{
					disableable.Enabled = value;
				}
			}
		}

		public override IRecorder Recorder
		{
			set {
				var recorderAwares = this.GetChildrenImplementing<IRecorderAware>();
				foreach(var recorderAware in recorderAwares)
				{
					recorderAware.Recorder = value;
				}
			}
		}

		#endregion

		// TODO: Refactor into a generic method GetImplementingChildren<TInterface>()

		/*protected IEnumerable<IResettable> GetResettableChildren()
		{
			// TODO: Add caching?
			return this.GetComponentsInChildren<IResettable>();
		}

		protected IEnumerable<IDisableable> GetDisableableChildren()
		{
			// TODO: Add caching?
			return this.GetComponentsInChildren<IDisableable>();
		}*/

		protected IEnumerable<TInterface> GetChildrenImplementing<TInterface>() where TInterface : class
		{
			// TODO: Add caching?
			return this.GetComponentsInChildren<TInterface>();
		}
	}
}

