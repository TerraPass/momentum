using System;

namespace Momentum.Gameplay.Common
{
	public interface IDisableable
	{
		bool Enabled {get;set;}
	}

	public static class IDisableableExtensions
	{
		public static void Enable(this IDisableable disableable)
		{
			disableable.Enabled = true;
		}

		public static void Disable(this IDisableable disableable)
		{
			disableable.Enabled = false;
		}
	}
}

