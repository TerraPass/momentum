using UnityEngine;
using System;

namespace Momentum.Gameplay.Common
{
	public interface IResettable
	{
		/// <summary>
		/// This method is typically invoked on level (re)start
		/// to indicate that a resettable entity should restore its initial state.
		/// </summary>
		void Reset();
	}
}

