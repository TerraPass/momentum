using System;

using Momentum.Gameplay.Core;

namespace Momentum.Gameplay.Common
{
	/// <summary>
	/// This decorator tracks game level event system and is able to
	/// invoke Enable() on LevelStarted and/or LevelResumed events
	/// and Disable() on LevelCompleted and/or LevelPaused events.
	/// The exact behaviour depends on tracking flags, set at construction time.
	/// </summary>
	public class AutoTogglingLevelEventSystemTrackingDisableable : AbstractDisableableDecorator
	{
		/// <summary>
		/// This set of flags describes situations,
		/// in which the disableable, decorated by AutoTogglingLevelEventSystemTrackingDisableable
		/// needs to be toggled.
		/// </summary>
		[Flags]
		public enum TrackingFlags
		{
			NONE 		= 0,

			/// <summary>
			/// Toggle disableable on LevelStarted/LevelCompleted events.
			/// </summary>
			COMPLETION 	= 1,
			/// <summary>
			/// Toggle disableable on LevelPaused/LevelResumed events.
			/// </summary>
			PAUSE		= 2,

			ALL			= -1
		}

		public AutoTogglingLevelEventSystemTrackingDisableable(
			IDisableable disableable,
			IGameLevelEventSystem eventSystem,
			TrackingFlags tracking = TrackingFlags.ALL
		) : base(disableable)
		{
			if(eventSystem == null)
			{
				throw new ArgumentNullException("eventSystem");
			}
			if((tracking & TrackingFlags.COMPLETION) != 0)
			{
				eventSystem.LevelStarted  += ((sender, e) => disableable.Enable());
				eventSystem.LevelCompleted += ((sender, e) => disableable.Disable());
			}
			if((tracking & TrackingFlags.PAUSE) != 0)
			{
				eventSystem.LevelResumed += ((sender, e) => disableable.Enable());
				eventSystem.LevelPaused += ((sender, e) => disableable.Disable());
			}
		}
	}
}

