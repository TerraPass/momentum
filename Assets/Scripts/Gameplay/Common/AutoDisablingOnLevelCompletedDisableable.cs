using UnityEngine;
using System;

using Momentum.Gameplay.Core;

namespace Momentum.Gameplay.Common
{
	public class AutoDisablingOnLevelCompletedDisableable : AbstractDisableableDecorator
	{
		public AutoDisablingOnLevelCompletedDisableable(
			IDisableable disableable,
			IGameLevelEventSystem eventSystem
		) : base(disableable)
		{
			if(eventSystem == null)
			{
				throw new ArgumentNullException("eventSystem");
			}
			eventSystem.LevelCompleted += this.OnLevelCompleted;
		}

		private void OnLevelCompleted(object sender, LevelCompletedEventArgs args)
		{
			this.Disable();
		}
	}
}

