using UnityEngine;
using System;

using Momentum.Gameplay.Core;

namespace Momentum.Gameplay.Common
{
	public class AutoEnablingOnLevelRestartedDisableable : AbstractDisableableDecorator
	{
		public AutoEnablingOnLevelRestartedDisableable(
			IDisableable disableable,
			IGameLevelEventSystem eventSystem
		) : base(disableable)
		{
			if(eventSystem == null)
			{
				throw new ArgumentNullException("eventSystem");
			}
			eventSystem.LevelStarted += this.OnLevelStarted;
		}

		private void OnLevelStarted(object sender, LevelStartedEventArgs args)
		{
			this.Enable();
		}
	}
}

