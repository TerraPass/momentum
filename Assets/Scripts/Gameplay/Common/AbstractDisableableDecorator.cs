using System;

namespace Momentum.Gameplay.Common
{
	public abstract class AbstractDisableableDecorator : IDisableable
	{
		private readonly IDisableable decoratedDisableable;

		public AbstractDisableableDecorator(IDisableable decoratedDisableable)
		{
			if(decoratedDisableable == null)
			{
				throw new ArgumentNullException("decoratedDisableable");
			}
			this.decoratedDisableable = decoratedDisableable;
		}

		protected IDisableable DecoratedDisableable
		{
			get {
				return this.decoratedDisableable;
			}
		}

		#region IDisableable implementation

		public virtual bool Enabled {
			get {
				return this.DecoratedDisableable.Enabled;
			}
			set {
				this.DecoratedDisableable.Enabled = value;
			}
		}

		#endregion
	}
}

