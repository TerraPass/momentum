using UnityEngine;
using System;
using System.Diagnostics;
using Terrapass.Debug;
using Terrapass.Extensions.Unity;

using Momentum.Gameplay.Player;
using Momentum.Gameplay.TimeMechanic;
using Momentum.Gameplay.TimeMechanic.Recording;
using Momentum.Gameplay.TimeMechanic.Measurement;
using Momentum.Gameplay.Core;
using Momentum.Gameplay.Core.Commands.Components;
using Momentum.Gameplay.Core.Controllers;
using Momentum.Gameplay.Level;
using Momentum.Gameplay.Common;

namespace Momentum.Gameplay
{
	public class GameplayDependencyContainer : AbstractGameplayDependencyContainerComponent
	{
		private const string BALLS_LAYER_NAME = "Balls";

		[SerializeField]
		private AbstractGameLevelRootComponent levelRoot;
		[SerializeField]
		private ReassignableCompleteGameLevelCommandComponent completeGameLevelCommand;
		[SerializeField]
		private GameObject ballPrefab;
		[SerializeField]
		private int maxClonedBalls = 3;	// TODO: Should be contained in config with 3 as a sensible default.
//		[SerializeField]
//		private String ballsLayer;
		[SerializeField]
		private AbstractRecorderComponent recorder;

		[SerializeField]
		private AbstractPlayerControllerComponent playerController;
		[SerializeField]
		private AbstractTimeControllerComponent timeController;
		[SerializeField]
		private AbstractGameFlowControllerComponent gameFlowController;

		[SerializeField]
		private Transform playerStartingPosition;
		
		private ITimeMechanism timeMechanism;
		private IPlayer player;
		private IGameLevelReferee gameLevelReferee;
		private IGameLevelEventSystem gameLevelEventSystem;
		
		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();
		}

		#region implemented abstract members of AbstractGameplayDependencyContainerComponent

		public override void PerformInjection ()
		{
			// Initialize time mechanism
			this.recorder.Timer = LevelTimer.Instance;
			this.timeMechanism = new TimeMechanism(this.recorder);
			this.timeController.TimeMechanism = this.timeMechanism;
			
			// Disable collisions between Balls
			int ballsLayer = LayerMask.NameToLayer(BALLS_LAYER_NAME);
			Physics.IgnoreLayerCollision(ballsLayer, ballsLayer);

			// Set recorder for all the entities in the level via level root
			this.levelRoot.Recorder = this.recorder;

			// Initialize game level
			var eventInvokingGameLevelReferee = new EventInvokingGameLevelReferee(
				new GameLevelReferee(
					this.levelRoot,
					this.timeMechanism
				)
			);
			this.gameLevelReferee = eventInvokingGameLevelReferee;
			this.gameLevelEventSystem = eventInvokingGameLevelReferee;

			// TODO: Change the name of GameLevel property 
			// to GameLevelReferee in command class definition.
			this.completeGameLevelCommand.GameLevel = this.gameLevelReferee;

			// Setup pause controller
			this.gameFlowController.GameLevelReferee = this.gameLevelReferee;

			// Initialize player
			this.player = new AutoResettingOnLevelRestartingPlayer(
				new SwitchingPlayer(
					this.ballPrefab, 
					this.maxClonedBalls, 
					this.recorder,
					this.timeMechanism,
					this.playerStartingPosition
				),
				this.gameLevelEventSystem
			);
			
			this.playerController.Player = this.player;

			// Setup game level referee to automatically invoke pseudo-gameover
			// on destruction of player's currently active ball
			new AutoPseudoGameoverInvokingReferee(
				this.gameLevelReferee,
				this.player
			);

			// Setup controllers to be automatically enabled on level (re)start
			// and disabled on level completion.
			new AutoTogglingLevelEventSystemTrackingDisableable(
				this.timeController,
				this.gameLevelEventSystem
			);
			new AutoTogglingLevelEventSystemTrackingDisableable(
				this.playerController, 
				this.gameLevelEventSystem
			);
			new AutoTogglingLevelEventSystemTrackingDisableable(
				this.gameFlowController,
				this.gameLevelEventSystem,
				AutoTogglingLevelEventSystemTrackingDisableable.TrackingFlags.COMPLETION
			);
		}

		public override IPlayer Player {
			get {
				return this.player;
			}
		}

		public override ITimeMechanism TimeMechanism {
			get {
				return this.timeMechanism;
			}
		}

		public override IGameLevelReferee GameLevelReferee {
			get {
				return this.gameLevelReferee;
			}
		}

		public override IGameLevelEventSystem GameLevelEventSystem {
			get {
				return this.gameLevelEventSystem;
			}
		}

		#endregion
	}
}

