using System;

namespace Momentum.Gameplay.Player
{
	public abstract class PlayerEventArgs : EventArgs
	{
		private readonly IPlayer player;

		public PlayerEventArgs(IPlayer player)
		{
			this.player = player;
		}

		public IPlayer Player
		{
			get {
				return this.player;
			}
		}
	}

	public class ActiveBallChangedEventArgs : PlayerEventArgs
	{
		private readonly IBall ball;

		public ActiveBallChangedEventArgs(IPlayer player, IBall newActiveBall)
			: base(player)
		{
			this.ball = newActiveBall;
		}

		public IBall NewActiveBall
		{
			get {
				return this.ball;
			}
		}
	}

	public class DestroyingBallEventArgs : PlayerEventArgs
	{
		private readonly IBall ball;

		public DestroyingBallEventArgs(IPlayer player, IBall destroyedBall)
			: base(player)
		{
			this.ball = destroyedBall;
		}

		public IBall DestroyedBall
		{
			get {
				return this.ball;
			}
		}
	}

	public class ActiveBallKilledEventArgs : PlayerEventArgs
	{
		private readonly IBall ball;

		public ActiveBallKilledEventArgs(IPlayer player, IBall ball)
			: base(player)
		{
			if(ball == null)
			{
				throw new ArgumentNullException("ball");
			}
			this.ball = ball;
		}

		public IBall KilledBall
		{
			get {
				return this.ball;
			}
		}
	}
}

