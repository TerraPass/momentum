using UnityEngine;
using System;

namespace Momentum.Gameplay.Player
{
	public abstract class AbstractPlayerControllerComponent : MonoBehaviour, IPlayerController
	{
		#region IPlayerController implementation

		public abstract IPlayer Player {get;set;}

		#endregion

		#region IDisableable implementation

		public abstract bool Enabled {get;set;}

		#endregion

	}
}

