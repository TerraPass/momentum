using UnityEngine;
using System;
using System.Collections;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

using Momentum.Gameplay.TimeMechanic.Recording;
using Momentum.Gameplay.Level.Entities.Firewalls;

namespace Momentum.Gameplay.Player
{
	using Strategies;
	using View;

	[RequireComponent(typeof(Rigidbody))]
	public partial class Ball : AbstractBallComponent, IRecordable
	{
		[SerializeField]
		private AbstractBallViewComponent view;
		[SerializeField]
		private AbstractFirewallVulnerabilityComponent firewallVulnerability;

		private IBallMovementStrategy movementStrategy;

		private IRecorder recorder;
		private IHistory history;

		private BallLifeStatus lifeStatus = BallLifeStatus.ALIVE;

		private bool isClone = false;
		
		/// <summary>
		/// This flag is used to delay connection to recorder until Start() is called.
		/// </summary>
		private bool isInit = false;

		private Friend friend;

		private Rigidbody MyRigidbody
		{
			get {
				return this.GetComponent<Rigidbody>();
			}
		}

		private Friend MyFriend
		{
			get {
				if(this.friend == null)
				{
					this.friend = new Friend(this);
				}
				return this.friend;
			}
		}

		// Use this for initialization
		void Start ()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			// Assert preconditions
			DebugUtils.Assert(
				this.MyRigidbody != null,
				"Ball component requires Rigidbody"
			);

			// Perform delayed connection
			this.ConnectToRecorder(this.recorder, this.history);
			this.isInit = true;

			this.LifeStatus = BallLifeStatus.ALIVE;

			// Inject friend object into view
			this.view.BallFriend = this.MyFriend;
		}
		
		// Update is called once per frame
		void Update ()
		{
			if(this.Recorder.IsPlayingBackward()) {
				this.MyRigidbody.Sleep();
			} else if(this.MyRigidbody.IsSleeping()){
				this.MyRigidbody.WakeUp();
			}
		}

		void OnDestroy()
		{
			this.LifeStatus = BallLifeStatus.DESTROYED;
			this.DisconnectFromRecorder();
		}

		#region implemented abstract members of AbstractBallComponent

		public override event EventHandler<BallLifeStatusChangedEventArgs> BallLifeStatusChanged = delegate{};
		public override event EventHandler<BallCloneStatusChangedEventArgs> BallCloneStatusChanged = delegate{};

		public override IRecorder Recorder
		{
			get {
				return this.recorder;
			}
			set {
				// To avoid repeated (dis)connection when Recorder and History
				// are injected one by one before Start() is called.
				if(this.isInit)
				{
					this.DisconnectFromRecorder();
					this.ConnectToRecorder(value, this.history);
				}
				else
				{
					// Delay connection until Start()
					this.recorder = value;
				}
			}
		}

		public override IHistory History
		{
			get {
				return this.history;
			}
			set {
				// To avoid repeated (dis)connection when Recorder and History
				// are injected one by one before Start() is called.
				if(this.isInit)
				{
					this.DisconnectFromRecorder();
					this.ConnectToRecorder(this.recorder, value);
				}
				else
				{
					// Delay connection until Start()
					this.history = value;
				}
			}
		}

		public override IFirewallVulnerability FirewallVulnerability
		{
			get {
				return this.firewallVulnerability;
			}
		}

		public override IBallMovementStrategy MovementStrategy
		{
			get {
				return this.movementStrategy != null
					? this.movementStrategy
					: UndefinedMovementStrategy.Instance;
			}
			set {
				if(value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.movementStrategy = value;
				this.movementStrategy.Init(this.MyFriend);
			}
		}

		public override bool IsClone
		{
			get {
				return this.isClone;
			}
			set {
				this.isClone = value;
				this.view.OnBallCloneStatusChanged(this.isClone);
				this.BallCloneStatusChanged(this, new BallCloneStatusChangedEventArgs(this, this.isClone));
			}
		}

		public override BallLifeStatus LifeStatus
		{
			get {
				return this.lifeStatus;
			}

			protected set {
				this.lifeStatus = value;
				this.BallLifeStatusChanged(
					this,
					new BallLifeStatusChangedEventArgs(
						this,
						this.lifeStatus
					)
				);
			}
		}

		public override void Move (Vector3 direction, float amount)
		{
			this.MovementStrategy.Move(this.MyFriend, direction, amount);
		}

		public override void Kill()
		{
			if(this.LifeStatus == BallLifeStatus.DESTROYED)
			{
				throw new InvalidOperationException(
					string.Format(
						"Attempting to invoke Kill() for the second time on the same instance of {0}",
						this.GetType().ToString()
					)
				);
			}

			// Notify view
			this.view.OnBallDestroyed();

			// OnDestroy() will be invoked at this point
			GameObject.Destroy(this.gameObject);
		}

		public override void FakeKill()
		{
			if(this.LifeStatus != BallLifeStatus.ALIVE)
			{
				throw new InvalidOperationException(
					string.Format(
						"Attempting to invoke FakeKill() on the instance of {0}, which is {1}",
						this.GetType().ToString(),
						this.LifeStatus == BallLifeStatus.FAKE_KILLED
							? "already fake killed"
							: this.LifeStatus.ToString()
					)
				);
			}

			// Notify view
			this.view.OnBallFakeKilled();

			// Suspend recording.
			// This is done to address 2 issues:
			// 1. Prevent race condition between PlayFrame() and RecordFrame().
			//    If PlayFrame() gets invoked first, the ball would be mistakenly
			//    returned to ALIVE state immediately. Suspending prevents new frames from
			//    being recorded after fake death, so that no further calls to PlayFrame() are made
			//    until the ball gets resurrected.
			// 2. Handle possible future changes in level geometry. Suspending prevents
			//    a frame with FAKE_KILLED life status from being recorded, thus allowing the ball
			//    to stay alive longer than was originally recorded in history, if future circumstances
			//    permit it.
			this.recorder.SuspendRecordable(this);

			this.LifeStatus = BallLifeStatus.FAKE_KILLED;
		}
		
		public override void Resurrect()
		{
			if(this.LifeStatus != BallLifeStatus.FAKE_KILLED)
			{
				throw new InvalidOperationException(
					string.Format(
						"Attempting to invoke Resurrect() on the instance of {0}, which is {1}",
						this.GetType().ToString(),
						this.LifeStatus == BallLifeStatus.ALIVE
							? "already alive"
							: this.LifeStatus.ToString()
					)
				);
			}

			// Notify view
			this.view.OnBallResurrected();

			// Unsuspend recording
			this.recorder.UnsuspendRecordable(this);

			this.LifeStatus = BallLifeStatus.ALIVE;
		}

		#endregion

		#region IRecordable implementation

		public void RecordFrame (IRecorder recorder, ref IHistoryRecord snapshot)
		{
			snapshot.StateObject = this.CurrentState;
		}

		public void PlayFrame (IRecorder recorder, IHistoryRecord snapshot)
		{
			snapshot.TransformSnapshot.ApplyTo(this.transform);
			this.CurrentState = (BallState)snapshot.StateObject;
		}

		#endregion

		private void OnRecorderEndOfHistoryReached(object sender, EndOfHistoryReachedEventArgs args)
		{

		}

		private void RegisterEventHandlers(IRecorder recorder)
		{
			recorder.EndOfHistoryReached += this.OnRecorderEndOfHistoryReached;
		}

		private void UnregisterEventHandlers(IRecorder recorder)
		{
			recorder.EndOfHistoryReached -= this.OnRecorderEndOfHistoryReached;
		}

		protected void DisconnectFromRecorder()
		{
			if(this.recorder != null)
			{
				this.recorder.UnregisterRecordable(this);
				this.UnregisterEventHandlers(this.recorder);
				this.recorder = null;
			}
		}

		protected void ConnectToRecorder(IRecorder recorder, IHistory history = null)
		{
			if(recorder != null)
			{
				this.recorder = recorder;
				this.history = history != null ? history : DefaultHistory;
				this.recorder.RegisterRecordable(this, this.history);
				this.RegisterEventHandlers(this.recorder);

				// Play current frame of history if available.
				// (In this way this newly spawned ball will have the momentum of its predecessor.)
				IHistoryRecord currentFrame;
				if(this.history.TryGetValue(
					this.recorder.Timer.ElapsedSeconds,
					out currentFrame
				))
				{
					this.PlayFrame(this.recorder, currentFrame);
				}
			}
		}

		protected static IHistory DefaultHistory
		{
			get {
				// NOTE: Performance impact of calculating InterpolationAlgorithm might not
				// be worth the cost, since visually behaviour of Balls after playback
				// looks reasonable as is.
				return new SilentNonRewritableInterpolatedHistory(/*InterpolationAlgorithm*/);
			}
		}
	}
}
