using UnityEngine;
using System;

using Momentum.Gameplay.Common;

namespace Momentum.Gameplay.Player
{
	public interface IPlayerController : IDisableable
	{
		IPlayer Player {get;set;}
	}
}

