using UnityEngine;
using System;

using Momentum.Gameplay.TimeMechanic.Recording;

namespace Momentum.Gameplay.Player
{
	public abstract class AbstractPlayerComponent : MonoBehaviour, IPlayer
	{
		public abstract IRecorder Recorder {get;set;}
		public abstract IBall ActiveBall {get;}

		public abstract event EventHandler<ActiveBallChangedEventArgs> ActiveBallChanged;
		public abstract event EventHandler<DestroyingBallEventArgs> DestroyingBall;
		public abstract event EventHandler<ActiveBallKilledEventArgs> ActiveBallKilled;

		#region IResettable implementation

		public abstract void Reset();

		#endregion
	}
}

