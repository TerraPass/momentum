using UnityEngine;
using System;
using Terrapass.Debug;

namespace Momentum.Gameplay.Player.Strategies
{
	public sealed class UndefinedMovementStrategy : IBallMovementStrategy
	{
		private static UndefinedMovementStrategy instance;

		private UndefinedMovementStrategy()
		{

		}

		public static UndefinedMovementStrategy Instance
		{
			get {
				if(instance == null)
				{
					instance = new UndefinedMovementStrategy();
				}
				return instance;
			}
		}

		#region IBallMovementStrategy implementation

		public void Init(IBallFriend ballFriend)
		{
			this.FailAssertOrThrow(ballFriend);
		}

		public void Move (IBallFriend ballFriend, Vector3 direction, float amount)
		{
			this.FailAssertOrThrow(ballFriend);
		}

		#endregion

		private void FailAssertOrThrow(IBallFriend ballFriend)
		{
			string errorMessage = this.GenerateErrorMessage(ballFriend.Ball);
			
			DebugUtils.Assert(
				false,
				errorMessage
				);
			
			throw new InvalidOperationException(errorMessage);
		}

		private string GenerateErrorMessage(IBall ball)
		{
			return string.Format(
				"{0} instance is set as strategy for a {1} instance",
				this.GetType(),
				ball.GetType()
			);
		}
	}
}
