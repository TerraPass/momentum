using UnityEngine;
using System;

namespace Momentum.Gameplay.Player.Strategies
{
	public sealed class TorqueBasedMovementStrategy : IBallMovementStrategy
	{
		private readonly ForceMode forceMode;
		private readonly float maxAngularVelocity;

		public TorqueBasedMovementStrategy(float maxAngularVelocity, ForceMode forceMode)
		{
			this.maxAngularVelocity = maxAngularVelocity;
			this.forceMode = forceMode;
		}

		#region IBallMovementStrategy implementation

		public void Init(IBallFriend ballFriend)
		{
			ballFriend.Rigidbody.maxAngularVelocity = this.maxAngularVelocity;
		}

		public void Move(IBallFriend ballFriend, UnityEngine.Vector3 direction, float amount)
		{
			if(amount != 0 && direction.sqrMagnitude > 0) {
				Vector3 torqueVector = new Vector3(direction.z, 0, -direction.x);
				ballFriend.Rigidbody.AddTorque(
					torqueVector * (amount/torqueVector.magnitude),
					this.forceMode
				);
			}
		}

		#endregion
	}
}

