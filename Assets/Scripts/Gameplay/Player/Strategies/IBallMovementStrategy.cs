using UnityEngine;
using System;

namespace Momentum.Gameplay.Player.Strategies
{
	public interface IBallMovementStrategy
	{
		/// <summary>
		/// This method is called for strategy to perform all the necessary initialization on a ball.
		/// </summary>
		/// <param name="ballFriend">Ball friend.</param>
		void Init(IBallFriend ballFriend);
		void Move(IBallFriend ballFriend, Vector3 direction, float amount);
	}
}
