using UnityEngine;
using System;

namespace Momentum.Gameplay.Player.Strategies
{
	public class AngularVelocityBasedMovementStrategy : IBallMovementStrategy
	{
		private readonly float maxAngularVelocity;

		public AngularVelocityBasedMovementStrategy(float maxAngularVelocity)
		{
			this.maxAngularVelocity = maxAngularVelocity;
		}

		#region IBallMovementStrategy implementation

		public void Init(IBallFriend ballFriend)
		{
			ballFriend.Rigidbody.maxAngularVelocity = this.maxAngularVelocity;
		}

		public void Move(IBallFriend ballFriend, Vector3 direction, float amount)
		{
			ballFriend.Rigidbody.angularVelocity = new Vector3(
				direction.z,
				0,
				-direction.x
			) * (amount * this.maxAngularVelocity);
		}

		#endregion
	}
}

