using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

using Momentum.Gameplay.Player.Strategies;

namespace Momentum.Gameplay.Player.Strategies.Components
{
	public abstract class AbstractBallMovementStrategySelectorComponent : MonoBehaviour
	{
		[SerializeField]
		private AbstractBallComponent ball;

		protected virtual void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();
		}

		private IBallMovementStrategy strategy;

		protected IBallMovementStrategy Strategy
		{
			get {
				return this.ball.MovementStrategy;
			}
			set {
				this.ball.MovementStrategy = value;
			}
		}
	}
}

