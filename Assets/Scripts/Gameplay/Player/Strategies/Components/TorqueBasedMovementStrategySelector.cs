using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

namespace Momentum.Gameplay.Player.Strategies.Components
{
	public class TorqueBasedMovementStrategySelector : AbstractBallMovementStrategySelectorComponent
	{
		[SerializeField]
		private ForceMode forceMode = ForceMode.Force;
		[SerializeField]
		private float maxAngularVelocity = 25;

		protected override void Start ()
		{
			base.Start();

			this.EnsureRequiredFieldsAreSetInEditor();
			this.Strategy = new TorqueBasedMovementStrategy(this.maxAngularVelocity, this.forceMode);
		}
	}
}

