using UnityEngine;
using System;

using Momentum.Gameplay.Player.Strategies;

namespace Momentum.Gameplay.Player.Strategies.Components
{
	public class AngularVelocityBasedMovementStrategySelector : AbstractBallMovementStrategySelectorComponent
	{
		[SerializeField]
		private float maxAngularVelocity = 25;

		protected override void Start()
		{
			base.Start();

			this.Strategy = new AngularVelocityBasedMovementStrategy(this.maxAngularVelocity);
		}
	}
}

