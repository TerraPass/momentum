using UnityEngine;
using System;
using Terrapass.Debug;

namespace Momentum.Gameplay.Player
{
	public partial class Ball
	{
		private class Friend : IBallFriend
		{
			private readonly Ball ball;

			public Friend(Ball ball)
			{
				DebugUtils.Assert(
					ball != null,
					string.Format(
						"{0} got a null reference to {1} in its constructor",
						this.GetType(),
						ball.GetType()
					)
				);

				this.ball = ball;
			}

			#region IBallFriend implementation
			public IBall Ball
			{
				get {
					return this.ball;
				}
			}

			public Rigidbody Rigidbody
			{
				get {
					return this.ball.MyRigidbody;
				}
			}
			#endregion
		}
	}
}

