using UnityEngine;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Terrapass.Debug;

using Momentum.Gameplay.TimeMechanic;
using Momentum.Gameplay.TimeMechanic.Recording;

namespace Momentum.Gameplay.Player
{
	/// <summary>
	/// This IPlayer implementation spawns a new active ball every time rewind is ended,
	/// while retaining previously active balls as clones, until their max number is reached,
	/// at which point the oldest of balls gets destroyed.
	/// </summary>
	public class SwitchingPlayer : IPlayer
	{
		private readonly GameObject ballPrefab;
		private IRecorder recorder;
		private readonly Queue<IBall> ownedBalls;
		private readonly int maxClones;
		private readonly Transform startingTransform;

		private IBall activeBall;

		public event EventHandler<ActiveBallChangedEventArgs> ActiveBallChanged 	= delegate {};
		public event EventHandler<DestroyingBallEventArgs> DestroyingBall			= delegate {};
		public event EventHandler<ActiveBallKilledEventArgs> ActiveBallKilled		= delegate {};

		public SwitchingPlayer(
			GameObject ballPrefab, 
			int maxClones, 
			IRecorder recorder,
			ITimeMechanism timeMechanism,
			Transform startingTransform
		)
		{
			ValidateBallPrefab(ballPrefab);
			this.ownedBalls = new Queue<IBall>(this.maxClones);
			this.ballPrefab = ballPrefab;
			this.maxClones = maxClones;
			this.Recorder = recorder;
			this.startingTransform = startingTransform;

			timeMechanism.RewindEnded += this.OnRewindEnded;
		}

		#region IResettable implementation

		public void Reset ()
		{
			// Unset the active ball
			if(this.activeBall != null)
			{
				this.activeBall.BallLifeStatusChanged -= this.OnActiveBallLifeStatusChanged;
				this.activeBall = null;
			}

			// Destroy all existing owned balls and clear the corresponding collection
			foreach(var ball in this.ownedBalls)
			{
				ball.Kill();
			}
			this.ownedBalls.Clear();

			// Create and posess initial ball
			this.SpawnAndPosessBall(startingTransform);
		}

		#endregion

		protected IRecorder Recorder {
			get {
				return this.recorder;
			}
			set {
				this.recorder = value;
				// Propagate new recorder to owned balls
				foreach(IBall ball in this.ownedBalls)
				{
					ball.Recorder = this.recorder;
				}
			}
		}

		public IBall ActiveBall
		{
			get {
				return this.activeBall;
			}
			protected set {
				IBall previousActiveBall = this.activeBall;
				this.activeBall = value;
				this.activeBall.Recorder = this.Recorder;
				// Subscribe to life status change events of the new active ball
				this.activeBall.BallLifeStatusChanged += this.OnActiveBallLifeStatusChanged;

				if(previousActiveBall != null)
				{
					// Unsubscribe from life status change events of the previously active ball
					previousActiveBall.BallLifeStatusChanged -= this.OnActiveBallLifeStatusChanged;

					// Setup new ball with its predecessors history up until current point in time
					this.activeBall.History = previousActiveBall.History.ClonePartially(
						0,
						this.Recorder.Timer.ElapsedSeconds
					);

					// Set new ball's color to its predecessor's color
					this.activeBall.SetFirewallColor(previousActiveBall.GetFirewallColor());

					// Previously active ball is now a clone
					previousActiveBall.IsClone = true;
				}

				// Notify subscribers
				this.ActiveBallChanged(
					this, 
					new ActiveBallChangedEventArgs(
						this,
						this.activeBall
					)
				);
			}
		}

		protected Queue<IBall> OwnedBalls
		{
			get {
				return this.ownedBalls;
			}
		}

		private void OnRewindEnded(object sender, RewindEndedEventArgs args)
		{
			this.SpawnAndPosessBall(this.activeBall.Transform);
		}

		private void OnActiveBallLifeStatusChanged(object sender, BallLifeStatusChangedEventArgs args)
		{
			DebugUtils.Assert(
				this.ActiveBall == args.Ball,
				"Attempted to handle BallLifeStatusChanged event from a ball other than the currently active one"
			);

			if(args.NewLifeStatus != BallLifeStatus.ALIVE)
			{
				if(args.NewLifeStatus == BallLifeStatus.DESTROYED)
				{
					// TODO: Refactor to use logging strategy
					// instead of accessing log directly from here
					UnityEngine.Debug.Log(
						string.Format(
							"Life status of the currently active ball in {0} got changed to {1}",
							this.GetType().ToString(),
							args.NewLifeStatus.ToString()
						)
					);
				}

				this.ActiveBallKilled(
					this,
					new ActiveBallKilledEventArgs(
						this,
						args.Ball
					)
				);
			}
		}

		protected void SpawnAndPosessBall(Transform transform)
		{
			this.SpawnAndPosessBall(transform.position, transform.rotation);
		}

		protected void SpawnAndPosessBall(Vector3 position, Quaternion rotation)
		{
			this.ActiveBall = ((GameObject)GameObject.Instantiate(
				this.ballPrefab,
				position,
				rotation
			)).GetComponent<IBall>();
			if(this.ownedBalls.Count >= this.maxClones)
			{
				this.DestroyOldestBall();
			}
			this.ownedBalls.Enqueue(this.activeBall);
		}

		protected void DestroyOldestBall()
		{
			IBall oldestBall = this.ownedBalls.Dequeue();
			this.DestroyingBall(
				this,
				new DestroyingBallEventArgs(
					this,
					oldestBall
				)
			);
			oldestBall.Kill();
		}

		[Conditional("UNITY_EDITOR")]
		private static void ValidateBallPrefab(GameObject ballPrefab)
		{
			IBall ballComponent = ballPrefab.GetComponent<IBall>();
			DebugUtils.Assert(
				ballComponent != null,
				"Ball prefab must have a component implementing IBall"
			);
		}
	}
}

