using UnityEngine;
using System;

namespace Momentum.Gameplay.Player.View
{
	public abstract class AbstractBallViewComponent : MonoBehaviour, IBallView
	{
		#region IBallView implementation
		public abstract void OnBallFakeKilled();
		public abstract void OnBallResurrected();
		public abstract void OnBallDestroyed();

		public abstract void OnBallCloneStatusChanged (bool isClone);

		public abstract IBallFriend BallFriend {set;}
		#endregion
	}
}

