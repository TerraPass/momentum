using UnityEngine;
using System;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

using Momentum.Gameplay.Level.Entities.Firewalls;

namespace Momentum.Gameplay.Player.View
{
	[RequireComponent(typeof(Renderer))]
	public class BallView : AbstractBallViewComponent
	{
		[SerializeField]
		private Material defaultMaterial;
		[SerializeField]
		private Material cloneMaterial;
		
		[SerializeField]
		private float colorLerpAmount = 0.1f;

		// TODO: Create a special IBallCorpse interface to be able
		// to make corpses recordable, if such a need arises.
		[SerializeField]
		[Tooltip("GameObject to be spawned on death (e.g. some particle system)")]
		private GameObject corpsePrefab;

		private Renderer MyRenderer
		{
			get {
				return this.GetComponent<Renderer>();
			}
		}

		private IBallFriend ballFriend;

		/// <summary>
		/// Reference to the currently instantiated corpse.
		/// Kept to be able to destroy it on Resurrect()
		/// </summary>
		private GameObject currentCorpse;
		
		private Color desiredColor = FirewallColor.NONE.GetUnityColorOrDefault();

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			DebugUtils.Assert(
				this.MyRenderer != null,
				String.Format(
					"{0} component requires Renderer component",
					this.GetType()
				)
			);

			if(this.ballFriend == null)
			{
				Debug.LogWarningFormat(
					"{0} component's BallFriend property is unset at the call to Start()",
	               	this.GetType()
				);
			}

			if(this.corpsePrefab == null)
			{
				Debug.LogWarning(
					string.Format(
						"{0} component has no corpsePrefab specified",
						this.GetType().ToString()
					)
				);
			}
		}

		void Update()
		{
			// TODO: If Ball representation is later changed to a velocity-aligned vehicle,
			// query observed ball's rigidbody velocity and upldate rotation accordingly.

			// If needed, lerp to a different color to indicate a change in firewall vulnerability.
			this.MyRenderer.material.color = Color.Lerp(
				this.MyRenderer.material.color,
				this.desiredColor,
				this.colorLerpAmount
			);
		}

		private FirewallColor BallFirewallColor
		{
			get {
				return this.ballFriend.Ball.FirewallVulnerability.ExceptColor;
			}
		}

		#region implemented abstract members of AbstractBallViewComponent

		public override IBallFriend BallFriend
		{
			set {
				if(this.ballFriend != null)
				{
					// Unsubscribe from firewall vulnerability events
					this.ballFriend.Ball.FirewallVulnerability.VulnerabilityExceptColorChanged 
						-= this.OnFirewallVulnerabilityExceptColorChanged;
				}
				this.ballFriend = value;
				if(this.ballFriend != null)
				{
					// Subscribe to firewall vulnerability events
					this.ballFriend.Ball.FirewallVulnerability.VulnerabilityExceptColorChanged
						+= this.OnFirewallVulnerabilityExceptColorChanged;

					// Force material to vulnerability's color or default
					this.SetMaterialColor(
						this.BallFirewallColor.GetUnityColorOrDefault(),
						true
					);
				}
			}
		}

		public override void OnBallFakeKilled()
		{
			// Hide ourselves
			this.MyRenderer.enabled = false;
			// Spawn a corpse object.
			this.SpawnCorpse();
		}

		public override void OnBallResurrected()
		{
			// Show ourselves and destroy corpse object
			this.MyRenderer.enabled = true;
			this.DestroyCorpse();
		}

		public override void OnBallDestroyed()
		{
			// Spawn a corpse object.
			this.SpawnCorpse();
		}

		public override void OnBallCloneStatusChanged(bool isClone)
		{
			this.MyRenderer.material = isClone ? this.cloneMaterial : this.defaultMaterial;
			this.SetMaterialColor(this.desiredColor, true);
		}

		#endregion
		
		private void OnFirewallVulnerabilityExceptColorChanged(
			object sender,
			VulnerabilityExceptColorChangedEventArgs args
		)
		{
			this.SetMaterialColor(args.NewExceptColor.GetUnityColorOrDefault());
		}

		private void SpawnCorpse()
		{
			if(this.corpsePrefab != null)
			{
				this.currentCorpse = (GameObject)Instantiate(
					this.corpsePrefab,
					this.transform.position,
					this.transform.rotation
				);
			}
		}
		
		private void DestroyCorpse()
		{
			if(this.currentCorpse != null)
			{
				Destroy(this.currentCorpse);
				this.currentCorpse = null;
			}
		}

		private void SetMaterialColor(Color color, bool force = false)
		{
			// Preserve current alpha
			var alpha = this.MyRenderer.material.color.a;
			color.a = alpha;

			this.desiredColor = color;
			if(force)
			{
				this.MyRenderer.material.color = this.desiredColor;
			}
		}
	}
}

