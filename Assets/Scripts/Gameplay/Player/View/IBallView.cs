using System;

namespace Momentum.Gameplay.Player.View
{
	// This view is relatively tightly coupled
	// to IBall, exposing OnBallFakeKilled(), OnBallResurected() and the like,
	// in order to allow Ball to control the point, at which view changes its state
	// during calls to FakeKill(), Kill(), Reserrect() etc. more precisely.
	// Event-driven view would deprive Ball of any direct control over the state of its view 
	// and render it unable to recieve feedback from it
	// (such as DeathAnimationEnded event, which might become needed later down the line).
	public interface IBallView
	{
		/// <summary>
		/// This setter is exposed for IBall to be able to inject its friend object.
		/// </summary>
		/// <value>Ball friend.</value>
		IBallFriend BallFriend {set;}

		void OnBallFakeKilled();
		void OnBallResurrected();
		void OnBallDestroyed();

		void OnBallCloneStatusChanged(bool isClone);
	}
}

