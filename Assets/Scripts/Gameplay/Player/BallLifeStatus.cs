using System;

namespace Momentum.Gameplay.Player
{
	public enum BallLifeStatus
	{
		/// <summary>
		/// The ball is alive:
		/// either Resurrect() was invoked on it after FakeKill()
		/// or FakeKill()/Kill() was never called in the first place.
		/// </summary>
		ALIVE 		= 0,
		/// <summary>
		/// The ball "pretends to be dead":
		/// FakeKill() was invoked on it to make it look like it's dead,
		/// but it can still be brought back to live by a call to Resurrect().
		/// </summary>
		FAKE_KILLED = 1,
		/// <summary>
		/// This ball is irreversibly killed and destroyed:
		/// a call to its Kill() was made.
		/// </summary>
		DESTROYED 	= 2
	}
}

