using UnityEngine;
using System;

using Momentum.Gameplay.Core;

namespace Momentum.Gameplay.Player
{
	public class AutoResettingOnLevelRestartingPlayer : AbstractPlayerDecorator
	{
		private readonly IGameLevelEventSystem eventSystem;

		public AutoResettingOnLevelRestartingPlayer(IPlayer player, IGameLevelEventSystem eventSystem)
			: base(player)
		{
			if(eventSystem == null)
			{
				throw new ArgumentNullException("eventSystem");
			}
			this.eventSystem = eventSystem;
			this.eventSystem.LevelStarting += this.OnLevelStarting;
		}

		private void OnLevelStarting(object sender, LevelStartingEventArgs args)
		{
			this.Reset();
		}
	}
}

