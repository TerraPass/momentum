﻿using UnityEngine;
using System.Collections;

namespace Momentum.Gameplay.Player {

	public class CameraRelativePlayerController : AbstractPlayerControllerComponent {
		private const string VERTICAL_AXIS_NAME 	= "Vertical";
		private const string HORIZONTAL_AXIS_NAME 	= "Horizontal";

		[SerializeField]
		private Camera relativeToCamera;

		private IPlayer player;

		private bool isEnabled = false;

		void Start()
		{

		}

		// FixedUpdate to avoid tying game logic to framerate.
		void FixedUpdate()
		{
			if(this.isEnabled && this.Player != null)
			{
				float verticalInput = Input.GetAxis(VERTICAL_AXIS_NAME);
				float horizontalInput = Input.GetAxis(HORIZONTAL_AXIS_NAME);

				this.Player.Move(
					this.CameraForwardUnit * verticalInput + this.CameraRightUnit * horizontalInput
				);
			}
		}

		#region implemented abstract members of AbstractPlayerControllerComponent

		public override IPlayer Player {
			get {
				return this.player;
			}
			set {
				this.player = value;
			}
		}

		public override bool Enabled {
			get {
				return this.isEnabled;
			}
			set {
				this.isEnabled = value;
			}
		}

		#endregion

		// TODO: Review performance implications of normalizing results in
		// CameraForwardUnit and CameraRightUnit.

		private Vector3 CameraForwardUnit
		{
			get {
				var cameraFwdDirection = this.relativeToCamera.transform.forward;
				// Ignore y component and normalize to make the speed of forward ball movement
				// independent from the x euler angle of the camera.
				return new Vector3(
					cameraFwdDirection.x,
					0,
					cameraFwdDirection.z
				).normalized;
			}
		}

		private Vector3 CameraRightUnit
		{
			get {
				var cameraRgtDirection = this.relativeToCamera.transform.right;
				// Ignore y component and normalize to make the speed of rightward ball movement
				// independent from the z euler angle of the camera.
				return new Vector3(
					cameraRgtDirection.x,
					0,
					cameraRgtDirection.z
				).normalized;
			}
		}
	}
}