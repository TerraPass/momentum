using UnityEngine;
using System;

using Momentum.Gameplay.TimeMechanic.Recording;
using Momentum.Gameplay.Common;

namespace Momentum.Gameplay.Player
{
	public interface IPlayer : IResettable
	{
		IBall ActiveBall {get;}

		event EventHandler<ActiveBallChangedEventArgs> ActiveBallChanged;
		event EventHandler<DestroyingBallEventArgs> DestroyingBall;
		// TODO: Maybe only raise this event on FakeKill() but not on Kill()
		// of the active ball?
		/// <summary>
		/// Occurs whenever the active ball gets killed. 
		/// (Its life status changes to anything
		/// other than BallLifeStatus.ALIVE .)
		/// </summary>
		event EventHandler<ActiveBallKilledEventArgs> ActiveBallKilled;
	}

	public static class IPlayerExtensions
	{
		public static void Move(this IPlayer player, Vector3 direction, float amount)
		{
			if(player.ActiveBall != null)
			{
				player.ActiveBall.Move(direction, amount);
			}
		}

		public static void Move(this IPlayer player, Vector3 direction)
		{
			if(player.ActiveBall != null)
			{
				player.ActiveBall.Move(direction);
			}
		}
	}
}

