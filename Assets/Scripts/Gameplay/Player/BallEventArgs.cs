using System;

namespace Momentum.Gameplay.Player
{
	public abstract class BallEventArgs : EventArgs
	{
		private readonly IBall ball;

		public BallEventArgs(IBall ball)
		{
			if(ball == null)
			{
				throw new ArgumentNullException("ball");
			}
			this.ball = ball;
		}

		public IBall Ball
		{
			get {
				return this.ball;
			}
		}
	}

	public class BallLifeStatusChangedEventArgs : BallEventArgs
	{
		private readonly BallLifeStatus newLifeStatus;

		public BallLifeStatusChangedEventArgs(IBall ball, BallLifeStatus newLifeStatus)
			: base(ball)
		{
			this.newLifeStatus = newLifeStatus;
		}

		public BallLifeStatus NewLifeStatus
		{
			get {
				return this.newLifeStatus;
			}
		}
	}

	public class BallCloneStatusChangedEventArgs : BallEventArgs
	{
		private readonly bool isClone;
	
		public BallCloneStatusChangedEventArgs(IBall ball, bool isClone)
			: base(ball)
		{
			this.isClone = isClone;
		}

		public bool IsClone
		{
			get {
				return this.isClone;
			}
		}
	}
}

