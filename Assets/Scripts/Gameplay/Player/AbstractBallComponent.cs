﻿using UnityEngine;
using System.Collections;

using Momentum.Gameplay.TimeMechanic.Recording;

namespace Momentum.Gameplay.Player {
	using Strategies;

	public abstract class AbstractBallComponent : MonoBehaviour, IBall
	{
		#region IBall implementation

		public abstract event System.EventHandler<BallLifeStatusChangedEventArgs> BallLifeStatusChanged;
		public abstract event System.EventHandler<BallCloneStatusChangedEventArgs> BallCloneStatusChanged;

		public abstract void Move(Vector3 direction, float amount);

		public abstract void Kill();

		public abstract void FakeKill();

		public abstract void Resurrect();

		public abstract IRecorder Recorder {get;set;}

		public abstract IHistory History {get;set;}

		public abstract IBallMovementStrategy MovementStrategy {get;set;}

		public abstract bool IsClone {get;set;}

		public abstract BallLifeStatus LifeStatus {get; protected set;}

		public Transform Transform {
			get {
				return this.transform;
			}
		}

		public abstract Momentum.Gameplay.Level.Entities.Firewalls.IFirewallVulnerability FirewallVulnerability {get;}

		#endregion
	}
}