using UnityEngine;
using System;

namespace Momentum.Gameplay.Player
{
	public abstract class AbstractPlayerDecorator : IPlayer
	{
		private readonly IPlayer decoratedPlayer;

		public AbstractPlayerDecorator(IPlayer decoratedPlayer)
		{
			if(decoratedPlayer == null)
			{
				throw new ArgumentNullException("decoratedPlayer");
			}
			this.decoratedPlayer = decoratedPlayer;
		}

		#region IPlayer implementation
		public virtual event EventHandler<ActiveBallChangedEventArgs> ActiveBallChanged
		{
			add {
				this.DecoratedPlayer.ActiveBallChanged += value;
			}
			remove {
				this.DecoratedPlayer.ActiveBallChanged -= value;
			}
		}
		public virtual event EventHandler<DestroyingBallEventArgs> DestroyingBall
		{
			add {
				this.DecoratedPlayer.DestroyingBall += value;
			}
			remove {
				this.DecoratedPlayer.DestroyingBall -= value;
			}
		}

		public virtual event EventHandler<ActiveBallKilledEventArgs> ActiveBallKilled
		{
			add {
				this.DecoratedPlayer.ActiveBallKilled += value;
			}
			remove {
				this.DecoratedPlayer.ActiveBallKilled -= value;
			}
		}

		public virtual IBall ActiveBall {
			get {
				return this.DecoratedPlayer.ActiveBall;
			}
		}
		#endregion

		#region IResettable implementation
		public  virtual void Reset ()
		{
			this.DecoratedPlayer.Reset();
		}
		#endregion
	
		protected IPlayer DecoratedPlayer
		{
			get {
				return this.decoratedPlayer;
			}
		}
	}
}

