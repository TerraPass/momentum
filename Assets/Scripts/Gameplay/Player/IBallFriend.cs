using UnityEngine;
using System;

namespace Momentum.Gameplay.Player
{
	public interface IBallFriend
	{
		IBall Ball {get;}

		Rigidbody Rigidbody {get;}
	}
}

