using UnityEngine;
using System;

using Momentum.Gameplay.TimeMechanic.Recording;
using Momentum.Gameplay.Level.Entities.Firewalls;

namespace Momentum.Gameplay.Player
{
	using Strategies;

	public interface IBall : IRecorderAware
	{
		event EventHandler<BallLifeStatusChangedEventArgs> BallLifeStatusChanged;
		event EventHandler<BallCloneStatusChangedEventArgs> BallCloneStatusChanged;

		IHistory History {get;set;}
		Transform Transform {get;}
		IBallMovementStrategy MovementStrategy {get;set;}

		IFirewallVulnerability FirewallVulnerability {get;}

		bool IsClone {get;set;}

		BallLifeStatus LifeStatus {get;}

		void Move(Vector3 direction, float amount);
		/// <summary>
		/// Performs necessary finalization before irreversibly destroying the underlying GameObject
		/// and spawning a death special effect in its place.
		/// </summary>
		void Kill();
		/// <summary>
		/// Temporarily disables renderer (and spawns a particle effect etc. to emulate Kill())
		/// but does not actually destroy the GameObject or has any irreversible effects
		/// (such as unregistering from the Recorder).
		/// </summary>
		void FakeKill();
		/// <summary>
		/// Undoes the effects of FakeKill()
		/// </summary>
		void Resurrect();
	}

	public static class IBallExtensions
	{
		public static void Move(this IBall ball, Vector3 direction)
		{
			ball.Move(direction.normalized, direction.magnitude);
		}

		public static bool IsAlive(this IBall ball)
		{
			return ball.LifeStatus == BallLifeStatus.ALIVE;
		}

		public static bool IsFakeKilled(this IBall ball)
		{
			return ball.LifeStatus == BallLifeStatus.FAKE_KILLED;
		}

		public static bool IsDestroyed(this IBall ball)
		{
			return ball.LifeStatus == BallLifeStatus.DESTROYED;
		}

		public static FirewallColor GetFirewallColor(this IBall ball)
		{
			return ball.FirewallVulnerability.ExceptColor;
		}

		public static void SetFirewallColor(this IBall ball, FirewallColor color)
		{
			ball.FirewallVulnerability.ExceptColor = color;
		}
	}
}

