using UnityEngine;
using System;

using Momentum.Gameplay.TimeMechanic.Recording;
using Momentum.Gameplay.Level.Entities.Firewalls;

namespace Momentum.Gameplay.Player
{
	public partial class Ball
	{
		/// <summary>
		/// This type is a container for values, 
		/// which need to be stored in history during recording
		/// along with transform.
		/// </summary>
		private struct BallState
		{
			public readonly BallLifeStatus lifeStatus;

			public readonly Vector3 rigidbodyVelocity;
			public readonly Vector3 rigidbodyAngularVelocity;

			public readonly FirewallColor firewallColor;

			public BallState(
				BallLifeStatus lifeStatus,
				Vector3 rigidbodyVelocity,
				Vector3 rigidbodyAngularVelocity,
				FirewallColor firewallColor
			)
			{
				this.lifeStatus = lifeStatus;

				this.rigidbodyVelocity = rigidbodyVelocity;
				this.rigidbodyAngularVelocity = rigidbodyAngularVelocity;

				this.firewallColor = firewallColor;
			}
		}

		/// <summary>
		/// Form a BallState instance based on current Ball state for the purposes of recording
		/// or apply previously recorded BallState to this Ball instance.
		/// </summary>
		/// <value>State object.</value>
		private BallState CurrentState
		{
			get {
				return new BallState(
					this.LifeStatus,
					this.MyRigidbody.velocity,
					this.MyRigidbody.angularVelocity,
					this.FirewallVulnerability.ExceptColor
				);
			}
			set {
				// Fake kill or resurrect the ball, if needed
				if(this.LifeStatus == BallLifeStatus.FAKE_KILLED
				   && value.lifeStatus == BallLifeStatus.ALIVE)
				{
					this.Resurrect();
				}
				if(this.LifeStatus == BallLifeStatus.ALIVE
				   && value.lifeStatus == BallLifeStatus.FAKE_KILLED)
				{
					this.FakeKill();
				}

				// Update rigidbody
				this.MyRigidbody.velocity = value.rigidbodyVelocity;
				this.MyRigidbody.angularVelocity = value.rigidbodyAngularVelocity;

				// Set firewall vulnerability's except color
				this.FirewallVulnerability.ExceptColor = value.firewallColor;
			}
		}

		/// <summary>
		/// Algorithm used to interpolate between two states of the Ball.
		/// Interpolation is used for Ball's transform.
		/// Ball's life status is taken from the earlier state.
		/// </summary>
		/// <returns>Interpolated BallState on success, earlierState object on failure.</returns>
		/// <param name="earlierState">Earlier state.</param>
		/// <param name="laterState">Later state.</param>
		/// <param name="interpolationAmount">Interpolation amount.</param>
		private static object InterpolationAlgorithm(
			object earlierState, 
			object laterState, 
			float interpolationAmount
		)
		{
			try
			{
				BallState est = (BallState)earlierState;
				BallState lst = (BallState)laterState;

				return new BallState(
					est.lifeStatus,
					Vector3.Lerp(
						est.rigidbodyVelocity,
						lst.rigidbodyVelocity,
						interpolationAmount
					),
					Vector3.Lerp(
						est.rigidbodyAngularVelocity,
						lst.rigidbodyAngularVelocity,
						interpolationAmount
					),
					est.firewallColor
				);
			}
			catch(InvalidCastException e)
			{
				Debug.LogErrorFormat(
					"Unable to interpolate: at least one of the states is null or not convertible to BallState ({0},{1})",
					earlierState,
					laterState
				);
				Debug.LogException(e);
				return earlierState;
			}
		}
	}
}
