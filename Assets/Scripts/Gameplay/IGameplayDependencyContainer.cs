using UnityEngine;
using System;

using Momentum.Common.DependencyManagement;
using Momentum.Gameplay.TimeMechanic;
using Momentum.Gameplay.TimeMechanic.Measurement;
using Momentum.Gameplay.Player;
using Momentum.Gameplay.Core;

namespace Momentum.Gameplay
{
	/// <summary>
	/// Responsible for injecting dependencies into Gameplay-related classes
	/// </summary>
	public interface IGameplayDependencyContainer : IDependencyContainer
	{
		IPlayer Player {get;}
		ITimeMechanism TimeMechanism {get;}
		IGameLevelReferee GameLevelReferee {get;}
		IGameLevelEventSystem GameLevelEventSystem {get;}
	}
}

