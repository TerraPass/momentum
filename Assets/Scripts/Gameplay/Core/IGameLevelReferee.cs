using UnityEngine;
using System;

using Momentum.Gameplay.TimeMechanic;
using Momentum.Gameplay.TimeMechanic.Measurement;

namespace Momentum.Gameplay.Core
{
	public interface IGameLevelReferee
	{
		void RestartLevel();
		void CompleteLevel();

		void PauseLevel();
		void ResumeLevel();

		void PseudoGameover();

		float CurrentSeconds {get;}
		float CurrentRealTimeSeconds {get;}

		bool IsLevelPaused {get;}
	}

	public static class IGameLevelRefereeExtensions
	{
		public static void TogglePause(this IGameLevelReferee gameLevelReferee)
		{
			if(gameLevelReferee.IsLevelPaused)
			{
				gameLevelReferee.ResumeLevel();
			}
			else
			{
				gameLevelReferee.PauseLevel();
			}
		}
	}
}

