using UnityEngine;
using System;
using Terrapass.Debug;

using Momentum.Gameplay.TimeMechanic;
using Momentum.Gameplay.TimeMechanic.Measurement;
using Momentum.DependencyManagement;
using Momentum.Gameplay.Level;
using Momentum.Utils.Time;

namespace Momentum.Gameplay.Core
{
	public class GameLevelReferee : IGameLevelReferee
	{
		private IGameLevelRoot levelRoot;
		private ITimeMechanism timeMechanism;

		private IResettableTimer realTimer;

		/// <summary>
		/// This flag is used to prevent actually resuming time flow on ResumeLevel(),
		/// if we were in a pseudo-gameover situation at the point when PauseLevel() was called.
		/// </summary>
		private bool isPseudoGameover;

		public GameLevelReferee(
			IGameLevelRoot levelRoot,
			ITimeMechanism timeMechanism
		)
		{
			if(levelRoot == null)
			{
				throw new ArgumentNullException("levelRoot");
			}
			if(timeMechanism == null)
			{
				throw new ArgumentNullException("timeMechanism");
			}

			this.levelRoot = levelRoot;
			this.timeMechanism = timeMechanism;

			this.realTimer = new ResettableExecutionTimer(true);

			// Setup isPseudoGameover flag to be set to false whenever time is rewound.
			this.isPseudoGameover = false;
			this.timeMechanism.RewindStarted += (sender, e) => this.isPseudoGameover = false;
		}

		public void RestartLevel()
		{
			// Reset all the level entities to their initial state
			this.levelRoot.ResetAll();
			this.levelRoot.EnableAll();
			// Restart the level timer from 0
			this.timeMechanism.Reset();
			this.timeMechanism.Resume();
			// Restart the real timer from 0
			this.realTimer.Reset(false);
		}

		public void CompleteLevel()
		{
			this.timeMechanism.Pause();
			this.levelRoot.DisableAll();
		}

		public void PauseLevel()
		{
			this.timeMechanism.Pause();
			this.realTimer.Pause();
		}

		public void ResumeLevel()
		{
			// Don't resume level time flow if the game is in pseudo-gameover situation.
			// (In this case, time flow will be resumed by time mechanism after player rewinds.)
			if(!this.isPseudoGameover)
			{
				this.timeMechanism.Resume();
			}
			this.realTimer.Resume();
		}

		public void PseudoGameover()
		{
			this.timeMechanism.Pause();
			this.isPseudoGameover = true;
		}

		public float CurrentSeconds 
		{
			get {
				return this.timeMechanism.ReadOnlyTimer.ElapsedSeconds;
			}
		}

		public float CurrentRealTimeSeconds 
		{
			get {
				return this.realTimer.ElapsedSeconds;
			}
		}

		public bool IsLevelPaused
		{
			get {
				return this.realTimer.IsPaused;
			}
		}
	}
}

