using UnityEngine;
using System;

using Momentum.Common.Commands;

namespace Momentum.Gameplay.Core.Commands
{
	public class CompleteGameLevelCommand : SimpleCommand
	{
		public CompleteGameLevelCommand(IGameLevelReferee gameLevelReferee)
			: base(gameLevelReferee.CompleteLevel)
		{

		}
	}
}

