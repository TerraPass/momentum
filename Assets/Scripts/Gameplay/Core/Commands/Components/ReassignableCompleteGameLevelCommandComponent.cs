using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

using Momentum.Common.Commands;

namespace Momentum.Gameplay.Core.Commands.Components
{
	public class ReassignableCompleteGameLevelCommandComponent 
		: AbstractCommandComponent<CompleteGameLevelCommand>, IReassignableGameLevelRefereeCommand
	{
		private IGameLevelReferee gameLevel;

		#region IReassignableGameLevelCommand implementation

		public IGameLevelReferee GameLevel {
			get {
				return this.gameLevel;
			}
			set {
				if(value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.gameLevel = value;
				this.Command = new CompleteGameLevelCommand(this.gameLevel);
			}
		}

		#endregion
	}
}

