using UnityEngine;
using System;

using Momentum.Common.Commands;

namespace Momentum.Gameplay.Core.Commands
{
	public interface IReassignableGameLevelRefereeCommand : ICommand
	{
		IGameLevelReferee GameLevel {get;set;}
	}
}
