using UnityEngine;
using System;

namespace Momentum.Gameplay.Core
{
	public abstract class AbstractGameLevelRefereeDecorator : IGameLevelReferee
	{
		private readonly IGameLevelReferee decoratedGameLevel;

		public AbstractGameLevelRefereeDecorator(IGameLevelReferee decoratedGameLevel)
		{
			if(decoratedGameLevel == null)
			{
				throw new ArgumentNullException("decoratedGameLevel");
			}
			this.decoratedGameLevel = decoratedGameLevel;
		}

		protected IGameLevelReferee DecoratedGameLevelReferee
		{
			get {
				return this.decoratedGameLevel;
			}
		}

		#region IGameLevel implementation

		public virtual void RestartLevel()
		{
			this.DecoratedGameLevelReferee.RestartLevel();
		}

		public virtual void CompleteLevel()
		{
			this.DecoratedGameLevelReferee.CompleteLevel();
		}

		public virtual void PauseLevel()
		{
			this.DecoratedGameLevelReferee.PauseLevel();
		}

		public virtual void ResumeLevel()
		{
			this.DecoratedGameLevelReferee.ResumeLevel();
		}

		public virtual void PseudoGameover()
		{
			this.DecoratedGameLevelReferee.PseudoGameover();
		}

		public virtual float CurrentSeconds
		{
			get {
				return this.DecoratedGameLevelReferee.CurrentSeconds;
			}
		}

		public virtual float CurrentRealTimeSeconds
		{
			get {
				return this.DecoratedGameLevelReferee.CurrentRealTimeSeconds;
			}
		}

		public virtual bool IsLevelPaused
		{
			get {
				return this.DecoratedGameLevelReferee.IsLevelPaused;
			}
		}

		#endregion
	}
}

