using UnityEngine;
using System;

using Momentum.Gameplay.Player;

namespace Momentum.Gameplay.Core
{
	public class AutoPseudoGameoverInvokingReferee : AbstractGameLevelRefereeDecorator
	{
		private readonly IPlayer player;

		public AutoPseudoGameoverInvokingReferee(
			IGameLevelReferee gameLevelReferee,
			IPlayer player
		) : base(gameLevelReferee)
		{
			if(player == null)
			{
				throw new ArgumentNullException("player");
			}
			this.player = player;
			this.player.ActiveBallKilled += this.OnPlayerActiveBallKilled;
		}

		private void OnPlayerActiveBallKilled(object sender, ActiveBallKilledEventArgs args)
		{
			this.DecoratedGameLevelReferee.PseudoGameover();
		}
	}
}

