using UnityEngine;
using System;

namespace Momentum.Gameplay.Core.Controllers
{
	public abstract class AbstractGameFlowControllerComponent : MonoBehaviour, IGameFlowController
	{
		public abstract IGameLevelReferee GameLevelReferee {get;set;}

		public abstract bool Enabled {get;set;}
	}
}

