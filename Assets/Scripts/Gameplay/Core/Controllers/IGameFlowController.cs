using System;

using Momentum.Gameplay.Common;

namespace Momentum.Gameplay.Core.Controllers
{
	// TODO: Maybe organise controllers into a single Momentum.Gameplay.Controllers namespace?
	public interface IGameFlowController : IDisableable
	{
		IGameLevelReferee GameLevelReferee {get;set;}
	}
}

