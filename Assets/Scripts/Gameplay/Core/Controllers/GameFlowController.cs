using UnityEngine;
using System;
using Terrapass.Debug;

namespace Momentum.Gameplay.Core.Controllers
{
	public class GameFlowController : AbstractGameFlowControllerComponent
	{
		[SerializeField]
		private string pauseButtonName = "Pause";
		[SerializeField]
		private string restartButtonName = "Restart";

		private IGameLevelReferee gameLevelReferee = null;

		private bool isEnabled = true;

		void Update()
		{
			if(this.isEnabled && this.gameLevelReferee != null)
			{
				// Handle pause
				if(Input.GetButtonDown(this.pauseButtonName))
				{
					this.gameLevelReferee.TogglePause();
				}

				// Handle restart
				if(Input.GetButtonDown(this.restartButtonName))
				{
					this.gameLevelReferee.RestartLevel();
				}
			}
		}

		public override IGameLevelReferee GameLevelReferee
		{
			get {
				return this.gameLevelReferee;
			}
			set {
				this.gameLevelReferee = value;
			}
		}

		public override bool Enabled
		{
			get {
				return this.isEnabled;
			}
			set {
				this.isEnabled = value;
			}
		}
	}
}

