using UnityEngine;
using System;

namespace Momentum.Gameplay.Core
{
	public abstract class GameLevelEventArgs : EventArgs
	{
		private readonly IGameLevelReferee gameLevelReferee;

		public GameLevelEventArgs(IGameLevelReferee gameLevelReferee)
		{
			if(gameLevelReferee == null)
			{
				throw new ArgumentNullException("gameLevelReferee");
			}
			this.gameLevelReferee = gameLevelReferee;
		}

		public IGameLevelReferee GameLevelReferee
		{
			get {
				return this.gameLevelReferee;
			}
		}
	}

	public class LevelStartingEventArgs : GameLevelEventArgs
	{
		public LevelStartingEventArgs(IGameLevelReferee gameLevelReferee)
			: base(gameLevelReferee)
		{
			
		}
	}

	public class LevelStartedEventArgs : GameLevelEventArgs
	{
		public LevelStartedEventArgs(IGameLevelReferee gameLevel)
			: base(gameLevel)
		{

		}
	}

	public class LevelCompletedEventArgs : GameLevelEventArgs
	{
		private readonly float timeToComplete;
		private readonly float realTimeToComplete;

		public LevelCompletedEventArgs(
			IGameLevelReferee gameLevelReferee,
			float timeToComplete,
			float realTimeToComplete
		) : base(gameLevelReferee)
		{
			this.timeToComplete = timeToComplete;
			this.realTimeToComplete = realTimeToComplete;
		}

		public float SecondsToComplete
		{
			get {
				return this.timeToComplete;
			}
		}

		public float RealTimeSecondsToComplete
		{
			get {
				return this.realTimeToComplete;
			}
		}
	}

	public class PseudoGameoverOccurredEventArgs : GameLevelEventArgs
	{
		public PseudoGameoverOccurredEventArgs(IGameLevelReferee gameLevelReferee)
			: base(gameLevelReferee)
		{

		}
	}

	public class LevelPausedEventArgs : GameLevelEventArgs
	{
		public LevelPausedEventArgs(IGameLevelReferee gameLevelReferee)
			: base(gameLevelReferee)
		{

		}
	}

	public class LevelResumedEventArgs : GameLevelEventArgs
	{
		public LevelResumedEventArgs(IGameLevelReferee gameLevelReferee)
			: base(gameLevelReferee)
		{

		}
	}
}

