using UnityEngine;
using System;

namespace Momentum.Gameplay.Core
{
	public interface IGameLevelEventSystem
	{
		event EventHandler<LevelStartingEventArgs> LevelStarting;
		event EventHandler<LevelStartedEventArgs> LevelStarted;
		event EventHandler<LevelCompletedEventArgs> LevelCompleted;

		event EventHandler<PseudoGameoverOccurredEventArgs> PseudoGameoverOccurred;
		
		event EventHandler<LevelPausedEventArgs> LevelPaused;
		event EventHandler<LevelResumedEventArgs> LevelResumed;
	}
}
