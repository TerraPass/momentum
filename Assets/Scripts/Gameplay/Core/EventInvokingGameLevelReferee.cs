using UnityEngine;
using System;

namespace Momentum.Gameplay.Core
{
	public class EventInvokingGameLevelReferee : AbstractGameLevelRefereeDecorator, IGameLevelEventSystem
	{
		public EventInvokingGameLevelReferee(IGameLevelReferee gameLevelReferee)
			: base(gameLevelReferee)
		{

		}

		#region IGameLevelEventSystem implementation

		public event EventHandler<LevelStartingEventArgs> LevelStarting 	= delegate{};
		public event EventHandler<LevelStartedEventArgs> LevelStarted 		= delegate{};
		public event EventHandler<LevelCompletedEventArgs> LevelCompleted 	= delegate{};
		public event EventHandler<PseudoGameoverOccurredEventArgs> PseudoGameoverOccurred = delegate{};
		public event EventHandler<LevelPausedEventArgs> LevelPaused			= delegate{};
		public event EventHandler<LevelResumedEventArgs> LevelResumed		= delegate{};

		#endregion

		public override void RestartLevel ()
		{
			this.LevelStarting(
				this,
				new LevelStartingEventArgs(
					this.DecoratedGameLevelReferee
				)
			);
			this.DecoratedGameLevelReferee.RestartLevel();
			this.LevelStarted(
				this,
				new LevelStartedEventArgs(
					this.DecoratedGameLevelReferee
				)
			);
		}

		public override void CompleteLevel ()
		{
			this.DecoratedGameLevelReferee.CompleteLevel();
			this.LevelCompleted(
				this,
				new LevelCompletedEventArgs(
					this.DecoratedGameLevelReferee,
					this.CurrentSeconds,
					this.CurrentRealTimeSeconds
				)
			);
		}

		public override void PauseLevel()
		{
			base.PauseLevel();
			this.LevelPaused(
				this,
				new LevelPausedEventArgs(this.DecoratedGameLevelReferee)
			);
		}

		public override void ResumeLevel()
		{
			base.ResumeLevel();
			this.LevelResumed(
				this,
				new LevelResumedEventArgs(this.DecoratedGameLevelReferee)
			);
		}

		public override void PseudoGameover()
		{
			base.PseudoGameover();
			this.PseudoGameoverOccurred(
				this,
				new PseudoGameoverOccurredEventArgs(
					this.DecoratedGameLevelReferee
				)
			);
		}
	}
}

