using UnityEngine;
using System;

namespace Momentum.Gameplay
{
	public abstract class AbstractGameplayDependencyContainerComponent : MonoBehaviour, IGameplayDependencyContainer
	{
		#region IGameplayDependencyContainer implementation

		public abstract void PerformInjection();

		public abstract Momentum.Gameplay.Player.IPlayer Player {get;}

		public abstract Momentum.Gameplay.TimeMechanic.ITimeMechanism TimeMechanism {get;}

		public abstract Momentum.Gameplay.Core.IGameLevelReferee GameLevelReferee {get;}

		public abstract Momentum.Gameplay.Core.IGameLevelEventSystem GameLevelEventSystem {get;}

		#endregion
	}
}
