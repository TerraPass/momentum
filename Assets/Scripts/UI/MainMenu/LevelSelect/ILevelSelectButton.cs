using System;

using Momentum.Level.Metadata;

namespace Momentum.UI.MainMenu.LevelSelect
{
	public interface ILevelSelectButton
	{
		event EventHandler<LevelSelectButtonClickedEventArgs> LevelSelectButtonClicked;
		event EventHandler<LevelSelectButtonMouseOverEventArgs> LevelSelectButtonMouseOver;
		event EventHandler<LevelSelectButtonMouseOutEventArgs> LevelSelectButtonMouseOut;

		ILevelMetadata LevelMetadata {get;set;}

		bool Enabled {get;set;}
	}

	public static class ILevelSelectButtonExtensions
	{
		public static void Enable(this ILevelSelectButton button)
		{
			button.Enabled = true;
		}

		public static void Disable(this ILevelSelectButton button)
		{
			button.Enabled = false;
		}
	}
}

