using System;
using System.Collections.Generic;

using Momentum.UI.Common;
using Momentum.Level.Metadata;

namespace Momentum.UI.MainMenu.LevelSelect
{
	public interface ILevelSelectScreen : IHideable
	{
		event EventHandler<LevelSelectedEventArgs> LevelSelected;
		event EventHandler<BackButtonPressedEventArgs> BackButtonPressed;

		IList<ILevelMetadata> Levels {get;}
	}

	public static class ILevelSelectScreenExtensions
	{
		public static void AddLevel(
			this ILevelSelectScreen levelSelectScreen,
			ILevelMetadata levelMetadata
		)
		{
			levelSelectScreen.Levels.Add(levelMetadata);
		}

		public static void AddLevelRange(
			this ILevelSelectScreen levelSelectScreen,
			IEnumerable<ILevelMetadata> levelMetadataRange
		)
		{
			foreach(var metadata in levelMetadataRange)
			{
				levelSelectScreen.Levels.Add(metadata);
			}
		}
	}
}

