using System;

using Momentum.Level.Metadata;

namespace Momentum.UI.MainMenu.LevelSelect
{
	// TODO: If needed, generalize to ActionInvokingLevelSelectScreen.
	public class LevelLoadingLevelSelectScreen : AbstractLevelSelectScreenDecorator
	{
		private readonly Action<ILevelId> levelLoadingAction;

		public LevelLoadingLevelSelectScreen(
			ILevelSelectScreen levelSelectScreen,
			Action<ILevelId> levelLoadingAction
		) : base(levelSelectScreen)
		{
			if(levelLoadingAction == null)
			{
				throw new ArgumentNullException("levelLoadingAction");
			}
			this.levelLoadingAction = levelLoadingAction;

			this.DecoratedLevelSelectScreen.LevelSelected += this.OnLevelSelected;
		}

		private void OnLevelSelected(object sender, LevelSelectedEventArgs args)
		{
			this.levelLoadingAction(args.LevelId);
		}
	}
}

