using System;
using System.Collections.Generic;

using Momentum.Level.Metadata;

namespace Momentum.UI.MainMenu.LevelSelect
{
	public abstract class AbstractLevelSelectScreenDecorator : ILevelSelectScreen
	{
		private readonly ILevelSelectScreen decoratedLevelSelectScreen;

		public AbstractLevelSelectScreenDecorator(ILevelSelectScreen decoratedLevelSelectScreen)
		{
			if(decoratedLevelSelectScreen == null)
			{
				throw new ArgumentNullException("decoratedLevelSelectScreen");
			}
			this.decoratedLevelSelectScreen = decoratedLevelSelectScreen;
		}

		protected ILevelSelectScreen DecoratedLevelSelectScreen
		{
			get {
				return this.decoratedLevelSelectScreen;
			}
		}

		#region ILevelSelectScreen implementation
		public virtual event EventHandler<LevelSelectedEventArgs> LevelSelected
		{
			add {
				this.DecoratedLevelSelectScreen.LevelSelected += value;
			}
			remove {
				this.DecoratedLevelSelectScreen.LevelSelected -= value;
			}
		}

		public virtual event EventHandler<BackButtonPressedEventArgs> BackButtonPressed
		{
			add {
				this.DecoratedLevelSelectScreen.BackButtonPressed += value;
			}
			remove {
				this.DecoratedLevelSelectScreen.BackButtonPressed -= value;
			}
		}

		public virtual IList<ILevelMetadata> Levels
		{
			get {
				return this.DecoratedLevelSelectScreen.Levels;
			}
		}
		#endregion

		#region IHideable implementation
		public virtual bool Visible
		{
			get {
				return this.DecoratedLevelSelectScreen.Visible;
			}
			set {
				this.DecoratedLevelSelectScreen.Visible = value;
			}
		}
		#endregion
	}
}

