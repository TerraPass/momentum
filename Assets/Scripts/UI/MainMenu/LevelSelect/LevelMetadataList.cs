using System;
using System.Collections.Generic;
using Terrapass.Debug;

using Momentum.Level.Metadata;

namespace Momentum.UI.MainMenu.LevelSelect
{
	public partial class LevelSelectScreen
	{
		private class LevelMetadataList : IList<ILevelMetadata>
		{
			private readonly LevelSelectScreen levelSelectScreen;
			
			private readonly IList<ILevelMetadata> impl;
			
			public LevelMetadataList(LevelSelectScreen levelSelectScreen)
			{
				DebugUtils.Assert(
					levelSelectScreen != null,
					string.Format(
						"{0} got null as its levelSelectScreen parameter in constructor",
						this.GetType()
					)
				);
				
				this.levelSelectScreen = levelSelectScreen;
				this.impl = new List<ILevelMetadata>();
			}
			
			
			#region IList implementation
			public int IndexOf (ILevelMetadata item)
			{
				return this.impl.IndexOf(item);
			}

			public void Insert (int index, ILevelMetadata item)
			{
				this.impl.Insert(index, item);
				this.levelSelectScreen.AddLevel(item);
			}

			public void RemoveAt (int index)
			{
				this.levelSelectScreen.RemoveButton(this.impl[index]);
				this.impl.RemoveAt(index);
			}

			public ILevelMetadata this [int index]
			{
				get {
					return this.impl[index];
				}
				set {
					if(value == null)
					{
						throw new ArgumentNullException("value");
					}
					var lastValue = this.impl[index];
					this.impl[index] = value;
					this.levelSelectScreen.ModifyButton(lastValue, value);
				}
			}
			#endregion

			#region ICollection implementation
			public void Add(ILevelMetadata item)
			{
				this.impl.Add(item);
				this.levelSelectScreen.AddButton(item);
			}

			public void Clear()
			{
				foreach(var item in this.impl)
				{
					this.levelSelectScreen.RemoveButton(item);
				}
				this.impl.Clear();
			}

			public bool Contains(ILevelMetadata item)
			{
				return this.impl.Contains(item);
			}

			public void CopyTo(ILevelMetadata[] array, int arrayIndex)
			{
				this.impl.CopyTo(array, arrayIndex);
			}

			public bool Remove(ILevelMetadata item)
			{
				if(this.impl.Remove(item))
				{
					this.levelSelectScreen.RemoveButton(item);
					return true;
				}
				return false;
			}

			public int Count {
				get {
					return this.impl.Count;
				}
			}
			public bool IsReadOnly {
				get {
					return this.impl.IsReadOnly;
				}
			}
			#endregion
			#region IEnumerable implementation
			public IEnumerator<ILevelMetadata> GetEnumerator ()
			{
				return this.impl.GetEnumerator();
			}
			#endregion
			#region IEnumerable implementation
			System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator ()
			{
				return ((System.Collections.IEnumerable)this.impl).GetEnumerator();
			}
			#endregion
		}
	}
}

