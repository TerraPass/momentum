using System;

using Momentum.Level.Metadata;

namespace Momentum.UI.MainMenu.LevelSelect
{
	public abstract class LevelSelectButtonEventArgs : EventArgs
	{
		private readonly ILevelSelectButton button;

		public LevelSelectButtonEventArgs(ILevelSelectButton button)
		{
			if(button == null)
			{
				throw new ArgumentNullException("button");
			}
			this.button = button;
		}

		public ILevelSelectButton Button
		{
			get {
				return this.button;
			}
		}

		public ILevelMetadata LevelMetadata
		{
			get {
				return this.Button.LevelMetadata;
			}
		}

		public ILevelId LevelId
		{
			get {
				return this.LevelMetadata.Id;
			}
		}
	}

	public class LevelSelectButtonClickedEventArgs : LevelSelectButtonEventArgs
	{
		public LevelSelectButtonClickedEventArgs(ILevelSelectButton button)
			: base(button)
		{

		}
	}

	public class LevelSelectButtonMouseOverEventArgs : LevelSelectButtonEventArgs
	{
		public LevelSelectButtonMouseOverEventArgs(ILevelSelectButton button)
			: base(button)
		{

		}
	}

	public class LevelSelectButtonMouseOutEventArgs : LevelSelectButtonEventArgs
	{
		public LevelSelectButtonMouseOutEventArgs(ILevelSelectButton button)
			: base(button)
		{

		}
	}
}

