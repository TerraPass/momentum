using System;

using Momentum.Level.Metadata;

namespace Momentum.UI.MainMenu.LevelSelect
{
	public abstract class LevelSelectScreenEventArgs : EventArgs
	{
		private readonly ILevelSelectScreen levelSelectScreen;

		public LevelSelectScreenEventArgs(ILevelSelectScreen levelSelectScreen)
		{
			if(levelSelectScreen == null)
			{
				throw new ArgumentNullException("levelSelectScreen");
			}
			this.levelSelectScreen = levelSelectScreen;
		}

		public ILevelSelectScreen LevelSelectScreen
		{
			get {
				return this.levelSelectScreen;
			}
		}
	}

	public class LevelSelectedEventArgs : LevelSelectScreenEventArgs
	{
		private readonly ILevelMetadata levelMetadata;

		public LevelSelectedEventArgs(
			ILevelSelectScreen levelSelectScreen,
			ILevelMetadata levelMetadata
		) : base(levelSelectScreen)
		{
			if(levelMetadata == null)
			{
				throw new ArgumentNullException("levelMetadata");
			}
			this.levelMetadata = levelMetadata;
		}

		public ILevelMetadata LevelMetadata
		{
			get {
				return this.levelMetadata;
			}
		}

		public ILevelId LevelId
		{
			get {
				return this.levelMetadata.Id;
			}
		}
	}

	public class BackButtonPressedEventArgs : LevelSelectScreenEventArgs
	{
		public BackButtonPressedEventArgs(ILevelSelectScreen levelSelectScreen)
			: base(levelSelectScreen)
		{

		}
	}
}

