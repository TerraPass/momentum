using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

using Momentum.Level.Metadata;

namespace Momentum.UI.MainMenu.LevelSelect
{
	public partial class LevelSelectScreen : AbstractLevelSelectScreenComponent
	{
		[SerializeField]
		private Canvas levelSelectCanvas;
		[SerializeField]
		private AbstractLevelSelectButtonComponent levelSelectButtonPrototype;
		[SerializeField]
		private RectTransform parentForButtons;
		[SerializeField]
		private Button backButton;

		private LevelMetadataList levels;
		private IDictionary<ILevelMetadata, AbstractLevelSelectButtonComponent> buttons;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			this.ConstructLevelMetadataListIfNull();

			this.backButton.onClick.AddListener(
				() => this.BackButtonPressed(
					this,
					new BackButtonPressedEventArgs(this)
				)
			);

			this.buttons = new Dictionary<ILevelMetadata, AbstractLevelSelectButtonComponent>();
		}

		#region implemented abstract members of AbstractLevelSelectScreenComponent

		public override event EventHandler<LevelSelectedEventArgs> LevelSelected = delegate {};
		public override event EventHandler<BackButtonPressedEventArgs> BackButtonPressed = delegate {};

		public override IList<ILevelMetadata> Levels
		{
			get {
				this.ConstructLevelMetadataListIfNull();
				return this.levels;
			}
		}

		public override bool Visible {
			get {
				return this.levelSelectCanvas.gameObject.activeSelf;
			}
			set {
				this.levelSelectCanvas.gameObject.SetActive(value);
			}
		}

		#endregion

		private void ConstructLevelMetadataListIfNull()
		{
			if(this.levels == null)
			{
				this.levels = new LevelMetadataList(this);
			}
		}

		private void AddButton(ILevelMetadata metadata)
		{
			DebugUtils.Assert(
				metadata != null,
				string.Format(
					"{0} got null as parameter in its private AddButton() method",
					this.GetType()
				)
			);

			AbstractLevelSelectButtonComponent newButton = ((GameObject)Instantiate(
				this.levelSelectButtonPrototype.gameObject
			)).GetComponent<AbstractLevelSelectButtonComponent>();

			newButton.LevelMetadata = metadata;
			newButton.transform.SetParent(this.parentForButtons, false);

			newButton.LevelSelectButtonClicked += (sender, e) => this.LevelSelected(
				this,
				new LevelSelectedEventArgs(
					this,
					e.LevelMetadata
				)
			);

			this.buttons.Add(metadata, newButton);
		}

		private void RemoveButton(ILevelMetadata metadata)
		{
			DebugUtils.Assert(
				metadata != null && this.buttons.ContainsKey(metadata),
				string.Format(
					"{0} unexpectedly got {1} as parameter in its private RemoveButton() method",
					this.GetType()
				)
			);

			Destroy(this.buttons[metadata]);
			this.buttons.Remove(metadata);
		}

		private void ModifyButton(ILevelMetadata oldLevelMetadata, ILevelMetadata newLevelMetadata)
		{
			DebugUtils.Assert(
				oldLevelMetadata != null && this.buttons.ContainsKey(oldLevelMetadata),
				string.Format(
					"{0} unexpectedly got {1} as oldLevelMetadata parameter in its private RemoveButton() method",
					this.GetType()
				)
			);
			DebugUtils.Assert(
				newLevelMetadata != null,
				string.Format(
					"{0} got null as newLevelMetadata parameter in its private RemoveButton() method",
					this.GetType()
				)
			);

			var button = this.buttons[oldLevelMetadata];
			this.buttons.Remove(oldLevelMetadata);
			button.LevelMetadata = newLevelMetadata;
			this.buttons.Add(newLevelMetadata, button);
		}
	}
}

