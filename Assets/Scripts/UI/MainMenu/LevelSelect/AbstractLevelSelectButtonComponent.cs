using UnityEngine;
using System;

namespace Momentum.UI.MainMenu.LevelSelect
{
	public abstract class AbstractLevelSelectButtonComponent : MonoBehaviour, ILevelSelectButton
	{
		#region ILevelSelectButton implementation

		public abstract event EventHandler<LevelSelectButtonClickedEventArgs> LevelSelectButtonClicked;

		public abstract event EventHandler<LevelSelectButtonMouseOverEventArgs> LevelSelectButtonMouseOver;

		public abstract event EventHandler<LevelSelectButtonMouseOutEventArgs> LevelSelectButtonMouseOut;

		public abstract Momentum.Level.Metadata.ILevelMetadata LevelMetadata {get;set;}

		public abstract bool Enabled {get;set;}

		#endregion
	}
}

