using UnityEngine;
using System;

namespace Momentum.UI.MainMenu.LevelSelect
{
	public abstract class AbstractLevelSelectScreenComponent : MonoBehaviour, ILevelSelectScreen
	{
		#region ILevelSelectScreen implementation

		public abstract event EventHandler<LevelSelectedEventArgs> LevelSelected;
		public abstract event EventHandler<BackButtonPressedEventArgs> BackButtonPressed;

		public abstract System.Collections.Generic.IList<Momentum.Level.Metadata.ILevelMetadata> Levels {get;}

		#endregion

		#region IHideable implementation

		public abstract bool Visible {get;set;}

		#endregion


	}
}

