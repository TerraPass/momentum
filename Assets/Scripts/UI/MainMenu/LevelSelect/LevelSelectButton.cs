using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

using Momentum.Level.Metadata;

namespace Momentum.UI.MainMenu.LevelSelect
{
	[RequireComponent(typeof(Button))]
	public class LevelSelectButton
		: AbstractLevelSelectButtonComponent, IPointerEnterHandler, IPointerExitHandler
	{
		[SerializeField]
		private Text levelNameLabel;
		// TODO: Level status icon (locked, unlocked, beaten, trophy).
		//[SerializeField]
		//private Image levelStatusImage;

		private Button button;

		private ILevelMetadata levelMetadata;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			DebugUtils.Assert(
				this.levelMetadata != null,
				string.Format(
					"LevelMetadata must not be null on call to Start() of {0} component",
					this.GetType()
				)
			);

			this.button = this.GetComponent<Button>();
			DebugUtils.Assert(
				this.button != null,
				string.Format(
					"{0} component requires a {1} component on the same game object",
					this.GetType(),
					typeof(Button)
				)
			);

			this.button.onClick.AddListener(
				() => this.LevelSelectButtonClicked(
					this,
					new LevelSelectButtonClickedEventArgs(this)
				)
			);
		}

		#region implemented abstract members of AbstractLevelSelectButtonComponent

		public override event EventHandler<LevelSelectButtonClickedEventArgs> LevelSelectButtonClicked = delegate {};

		public override event EventHandler<LevelSelectButtonMouseOverEventArgs> LevelSelectButtonMouseOver = delegate {};

		public override event EventHandler<LevelSelectButtonMouseOutEventArgs> LevelSelectButtonMouseOut = delegate {};

		public override ILevelMetadata LevelMetadata
		{
			get {
				return this.levelMetadata;
			}
			set {
				if(value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.levelMetadata = value;
				this.levelNameLabel.text = this.levelMetadata.Name;
			}
		}

		public override bool Enabled
		{
			get {
				return this.button.interactable;
			}
			set {
				this.button.interactable = value;
			}
		}

		#endregion

		#region IPointerEnterHandler implementation

		public void OnPointerEnter (PointerEventData eventData)
		{
			this.LevelSelectButtonMouseOver(
				this,
				new LevelSelectButtonMouseOverEventArgs(this)
			);
		}

		#endregion

		#region IPointerExitHandler implementation

		public void OnPointerExit (PointerEventData eventData)
		{
			this.LevelSelectButtonMouseOut(
				this,
				new LevelSelectButtonMouseOutEventArgs(this)
			);
		}

		#endregion
	}
}

