using UnityEngine;
using System;

using Momentum.UI.Common.Menu;

namespace Momentum.UI.MainMenu.Menu
{
	public abstract class AbstractMainMenuComponent : MonoBehaviour, IMenu<MainMenuOption>
	{
		#region IMenu implementation

		public abstract event EventHandler<MenuOptionSelectedEventArgs<MainMenuOption>> MenuOptionSelected;

		public abstract void SetOptionEnabled (MainMenuOption option, bool value);

		#endregion

		#region IHideable implementation

		public abstract bool Visible {get;set;}

		#endregion


	}
}

