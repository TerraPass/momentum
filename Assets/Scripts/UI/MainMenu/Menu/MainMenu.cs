using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

using Momentum.UI.Common.Menu;
using Momentum.UI.Common.Menu.UnityWorkaround;

namespace Momentum.UI.MainMenu.Menu
{
	// TODO: Think of a way to move common logic from PauseMenu and MainMenu
	// into a separate class.
	public class MainMenu : AbstractMainMenuComponent
	{
		[Serializable]
		private class MainMenuOptionToButtonMapping : OptionToButtonMapping<MainMenuOption>
		{
			
		}
		
		[SerializeField]
		private Canvas menuCanvas;
		[SerializeField]
		private MainMenuOptionToButtonMapping[] buttonMappings;
		
		private IMenu<MainMenuOption> menuImpl;
		
		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();
			
			this.menuImpl = new UnityButtonBasedMenu<MainMenuOption>(
				new MenuButtonsDictionaryBuilder<MainMenuOption>(buttonMappings).Build(),
				this
			);
		}
		
		public override event EventHandler<MenuOptionSelectedEventArgs<MainMenuOption>> MenuOptionSelected
		{
			add {
				this.menuImpl.MenuOptionSelected += value;
			}
			remove {
				this.menuImpl.MenuOptionSelected -= value;
			}
		}
		
		public override void SetOptionEnabled(MainMenuOption option, bool value)
		{
			this.menuImpl.SetOptionEnabled(option, value);
		}
		
		public override bool Visible
		{
			get {
				return this.menuCanvas.gameObject.activeSelf;
			}
			set {
				this.menuCanvas.gameObject.SetActive(value);
			}
		}
	}
}

