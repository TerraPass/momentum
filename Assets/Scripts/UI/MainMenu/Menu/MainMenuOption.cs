using System;

namespace Momentum.UI.MainMenu.Menu
{
	public enum MainMenuOption
	{
		PLAY 			= 0,
		LEVEL_EDITOR 	= 1,
		OPTIONS 		= 2,
		QUIT 			= 3
	}
}

