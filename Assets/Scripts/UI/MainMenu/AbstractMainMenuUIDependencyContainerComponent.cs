using UnityEngine;
using System;
using System.Collections.Generic;

using Momentum.Common.Commands;
using Momentum.Level.Metadata;
using Momentum.UI.Common.MouseCursor;

namespace Momentum.UI.MainMenu
{
	public abstract class AbstractMainMenuUIDependencyContainerComponent
		: MonoBehaviour, IMainMenuUIDependencyContainer
	{
		#region IDependencyContainer implementation

		public abstract void PerformInjection();

		#endregion

		#region IUIDependencyContainer implementation

		public abstract ICommand QuitCommand {get;set;}

		public abstract Action<ILevelId> LoadLevelAction {get;set;}

		public abstract IEnumerable<ILevelMetadata> BuiltinLevels {get;set;}

		public abstract IMouseCursor MouseCursor {get;set;}

		#endregion
	}
}

