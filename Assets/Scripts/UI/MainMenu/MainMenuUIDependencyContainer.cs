using UnityEngine;
using System;
using System.Collections.Generic;
using Terrapass.Extensions.Unity;

using Momentum.UI.Common.Menu;
using Momentum.Common.Commands;
using Momentum.Common.DependencyManagement;
using Momentum.Level.Metadata;
using Momentum.UI.Common.MouseCursor;

namespace Momentum.UI.MainMenu
{
	using LevelSelect;
	using Menu;

	public class MainMenuUIDependencyContainer : AbstractMainMenuUIDependencyContainerComponent
	{
		[SerializeField]
		AbstractMainMenuComponent mainMenu;
		[SerializeField]
		AbstractLevelSelectScreenComponent levelSelectScreen;

		private ICommand quitCommand;
		private Action<ILevelId> loadLevelAction;
		private IEnumerable<ILevelMetadata> builtinLevels;
		private IMouseCursor mouseCursor;

		private IMainMenuUI mainMenuUi;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();
		}

		#region implemented abstract members of AbstractMainMenuUIDependencyContainerComponent
		public override void PerformInjection ()
		{
			this.EnsureRequiredDependenciesAreProvided();

			this.mainMenuUi = new MainMenuUI(
				this.mainMenu,
				this.levelSelectScreen,
				this.quitCommand,
				this.loadLevelAction
			);

			// Setup level select screen to load selected level
			// and register built-in levels with it.
			new LevelLoadingLevelSelectScreen(
				this.levelSelectScreen,
				this.loadLevelAction
			).AddLevelRange(this.builtinLevels);

			// Display mouse cursor
			this.MouseCursor.Visible = true;
		}

		[Dependency]
		public override ICommand QuitCommand
		{
			get {
				return this.quitCommand;
			}
			set {
				this.quitCommand = value;
			}
		}

		[Dependency]
		public override Action<ILevelId> LoadLevelAction
		{
			get {
				return this.loadLevelAction;
			}
			set {
				this.loadLevelAction = value;
			}
		}

		[Dependency]
		public override IEnumerable<ILevelMetadata> BuiltinLevels
		{
			get {
				return this.builtinLevels;
			}
			set {
				this.builtinLevels = value;
			}
		}

		[Dependency]
		public override IMouseCursor MouseCursor {
			get {
				return this.mouseCursor;
			}
			set {
				this.mouseCursor = value;
			}
		}
		#endregion

		protected IMainMenuUI MainMenuUI
		{
			get {
				return this.mainMenuUi;
			}
		}
	}
}

