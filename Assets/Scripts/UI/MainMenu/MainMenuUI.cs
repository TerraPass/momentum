using System;
using System.Collections.Generic;

using Momentum.Common.Commands;
using Momentum.UI.Common;
using Momentum.UI.Common.Menu;
using Momentum.Level.Metadata;

namespace Momentum.UI.MainMenu
{
	using LevelSelect;
	using Menu;

	public class MainMenuUI : IMainMenuUI
	{
		private readonly IMenu<MainMenuOption> mainMenu;
		private readonly ILevelSelectScreen levelSelectScreen;

		public MainMenuUI(
			IMenu<MainMenuOption> mainMenu,
			ILevelSelectScreen levelSelectScreen,
			ICommand quitCommand,
			Action<ILevelId> loadLevelAction
		)
		{
			if(mainMenu == null)
			{
				throw new ArgumentNullException("mainMenu");
			}
			if(levelSelectScreen == null)
			{
				throw new ArgumentNullException("levelSelectScreen");
			}
			this.mainMenu = new CommandInvokingMenu<MainMenuOption>(
				mainMenu,
				this.BuildMainMenuCommandsDictionary(quitCommand, levelSelectScreen)
			);

			this.levelSelectScreen = levelSelectScreen;

			this.SetupLevelSelectScreenEventHandlers(this.levelSelectScreen);

			this.SwitchToMainMenu();
		}

		private void SwitchToMainMenu()
		{
			this.levelSelectScreen.Hide();
			this.mainMenu.Show();
		}

		private void SwitchToLevelSelectScreen()
		{
			this.mainMenu.Hide();
			this.levelSelectScreen.Show();
		}

		private void SetupLevelSelectScreenEventHandlers(ILevelSelectScreen levelSelectScreen)
		{
			levelSelectScreen.BackButtonPressed += (sender, args) => this.SwitchToMainMenu();
		}

		private IDictionary<MainMenuOption, ICommand> BuildMainMenuCommandsDictionary(
			ICommand quitCommand,
			ILevelSelectScreen levelSelectScreen
		)
		{
			var result = new Dictionary<MainMenuOption, ICommand>();
			result[MainMenuOption.PLAY] = new SimpleCommand(this.SwitchToLevelSelectScreen);
			result[MainMenuOption.QUIT] = quitCommand;
			return result;
		}
	}
}

