using System;
using System.Collections.Generic;

using Momentum.Common.DependencyManagement;
using Momentum.Common.Commands;
using Momentum.Level.Metadata;
using Momentum.UI.Common.MouseCursor;

namespace Momentum.UI.MainMenu
{
	public interface IMainMenuUIDependencyContainer : IDependencyContainer
	{
		ICommand QuitCommand {get;set;}
		Action<ILevelId> LoadLevelAction {get;set;}
		IEnumerable<ILevelMetadata> BuiltinLevels {get;set;}
		IMouseCursor MouseCursor {get;set;}
	}

	public static class IMainMenuUIDependencyContainerExtensions
	{
		public static void PerformInjection(
			this IMainMenuUIDependencyContainer container,
			ICommand quitCommand,
			Action<ILevelId> loadLevelAction,
			IEnumerable<ILevelMetadata> builtinLevels,
			IMouseCursor mouseCursor
		)
		{
			container.QuitCommand = quitCommand;
			container.LoadLevelAction = loadLevelAction;
			container.BuiltinLevels = builtinLevels;
			container.MouseCursor = mouseCursor;
			container.PerformInjection();
		}
	}
}

