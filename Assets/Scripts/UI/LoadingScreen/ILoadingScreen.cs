using System;

using Momentum.DependencyManagement.Loading;
using Momentum.UI.Common;

namespace Momentum.UI.LoadingScreen
{
	public interface ILoadingScreen : IHideable
	{
		ILoadingStatusMonitor LoadingStatusMonitor {get;set;}
	}
}

