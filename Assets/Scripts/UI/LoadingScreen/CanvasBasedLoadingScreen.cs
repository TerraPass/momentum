using UnityEngine;
using UnityEngine.UI;
using System;
using Terrapass.Extensions.Unity;

using Momentum.DependencyManagement.Loading;
using Momentum.UI.Common;
using Momentum.UI.Common.ProgressBar;

namespace Momentum.UI.LoadingScreen
{
	public class CanvasBasedLoadingScreen : AbstractLoadingScreenComponent
	{
		[SerializeField]
		Canvas loadingScreenCanvas;
		[SerializeField]
		AbstractProgressBarComponent progressBar;

		private ILoadingStatusMonitor loadingStatusMonitor;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			DontDestroyOnLoad(this.gameObject);
		}

		void Update()
		{
			if(this.loadingStatusMonitor != null)
			{
				this.progressBar.Progress = this.loadingStatusMonitor.LoadingProgress;
			}
			else
			{
				UnityEngine.Debug.LogWarningFormat(
					"{0} component on {1} has no loading status monitor set on Update()",
					this.GetType(),
					this.name
				);
				this.progressBar.Progress = 0.0f;
			}
		}

		#region implemented abstract members of AbstractLoadingScreenViewComponent

		public override ILoadingStatusMonitor LoadingStatusMonitor
		{
			get {
				return this.loadingStatusMonitor;
			}
			set {
				this.loadingStatusMonitor = value;
				// TODO: Maybe move to decorators
				this.loadingStatusMonitor.LoadingStarted += (sender, e) => this.Show();
				this.loadingStatusMonitor.LoadingCompleted += (sender, e) => this.Hide();
			}
		}

		public override bool Visible
		{
			get {
				return this.loadingScreenCanvas.enabled;
			}
			set {
				this.loadingScreenCanvas.enabled = value;
			}
		}

		#endregion
	}
}

