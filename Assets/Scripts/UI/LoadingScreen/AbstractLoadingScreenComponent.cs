using UnityEngine;
using System;

namespace Momentum.UI.LoadingScreen
{
	public abstract class AbstractLoadingScreenComponent : MonoBehaviour, ILoadingScreen
	{
		#region ILoadingScreen implementation
		public abstract Momentum.DependencyManagement.Loading.ILoadingStatusMonitor LoadingStatusMonitor {get;set;}
		#endregion

		#region IHideable implementation

		public abstract bool Visible {get;set;}

		#endregion


	}
}

