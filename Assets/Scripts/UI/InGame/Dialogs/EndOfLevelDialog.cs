using UnityEngine;
using UnityEngine.UI;
using System;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

using Momentum.Utils.Time;

namespace Momentum.UI.InGame.Dialogs
{
	public class EndOfLevelDialog : AbstractEndOfLevelDialogComponent
	{
		[SerializeField]
		private Canvas popupCanvas;
		[SerializeField]
		private Button quitButton;
		[SerializeField]
		private Button restartButton;
		[SerializeField]
		private Button nextLevelButton;
		[SerializeField]
		private Text timeToCompleteText;
		[SerializeField]
		private Text realTimeToCompleteText;

		private ITimeIntervalFormatter timeIntervalFormatter;

		private float secondsToComplete;
		private float realtimeSecondsToComplete;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();
			this.quitButton.onClick.AddListener(this.OnQuitButtonPressed);
			this.restartButton.onClick.AddListener(this.OnRestartButtonPressed);
			this.nextLevelButton.onClick.AddListener(this.OnNextLevelButtonPressed);

			this.timeIntervalFormatter = new MinutesSecondsFormatter();
		}

		void OnQuitButtonPressed()
		{
			this.QuitButtonPressed(
				this,
				new QuitButtonPressedEventArgs(
					this
				)
			);
		}

		void OnRestartButtonPressed()
		{
			this.RestartButtonPressed(
				this,
				new RestartButtonPressedEventArgs(
					this
				)
			);
		}

		void OnNextLevelButtonPressed()
		{
			DebugUtils.Assert(
				this.NextLevelButtonEnabled,
				"Attempting to handle click on nextLevelButton, which appears to be disabled"
			);

			this.NextLevelButtonPressed(
				this,
				new NextLevelButtonPressedEventArgs(
					this
				)
			);
		}

		#region implemented abstract members of AbstractEndOfLevelPopupComponent

		public override event EventHandler<QuitButtonPressedEventArgs> QuitButtonPressed = delegate{};
		public override event EventHandler<RestartButtonPressedEventArgs> RestartButtonPressed = delegate{};
		public override event EventHandler<NextLevelButtonPressedEventArgs> NextLevelButtonPressed = delegate{};

		public override float SecondsToComplete
		{
			get {
				return this.secondsToComplete;
			}
			set {
				this.secondsToComplete = value;
				this.timeToCompleteText.text = this.timeIntervalFormatter.Format(this.secondsToComplete);
			}
		}

		public override float RealTimeSecondsToComplete
		{
			get {
				return this.realtimeSecondsToComplete;
			}
			set {
				this.realtimeSecondsToComplete = value;
				this.realTimeToCompleteText.text = this.timeIntervalFormatter.Format(this.RealTimeSecondsToComplete);
			}
		}

		public override bool NextLevelButtonEnabled
		{
			get {
				return this.nextLevelButton.interactable;
			}
			set {
				this.nextLevelButton.interactable = value;
			}
		}

		public override bool Visible
		{
			get {
				return this.popupCanvas.gameObject.activeSelf;
			}
			set {
				this.popupCanvas.gameObject.SetActive(value);
			}
		}

		#endregion
	}
}

