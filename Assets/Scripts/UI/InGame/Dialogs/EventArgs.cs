using UnityEngine;
using System;

namespace Momentum.UI.InGame.Dialogs
{
	public abstract class EndOfLevelPopupEventArgs : EventArgs
	{
		private readonly IEndOfLevelDialog endOfLevelPopup;

		public EndOfLevelPopupEventArgs(IEndOfLevelDialog endOfLevelPopup)
		{
			this.endOfLevelPopup = endOfLevelPopup;
		}

		public IEndOfLevelDialog EndOfLevelPopup
		{
			get {
				return this.endOfLevelPopup;
			}
		}
	}

	public class QuitButtonPressedEventArgs : EndOfLevelPopupEventArgs
	{
		public QuitButtonPressedEventArgs(IEndOfLevelDialog endOfLevelPopup)
			: base(endOfLevelPopup)
		{

		}
	}

	public class RestartButtonPressedEventArgs : EndOfLevelPopupEventArgs
	{
		public RestartButtonPressedEventArgs(IEndOfLevelDialog endOfLevelPopup)
			: base(endOfLevelPopup)
		{
			
		}
	}

	public class NextLevelButtonPressedEventArgs : EndOfLevelPopupEventArgs
	{
		public NextLevelButtonPressedEventArgs(IEndOfLevelDialog endOfLevelPopup)
			: base(endOfLevelPopup)
		{

		}
	}
}

