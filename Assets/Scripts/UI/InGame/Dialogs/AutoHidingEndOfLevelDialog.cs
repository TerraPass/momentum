using UnityEngine;
using System;

using Momentum.Gameplay.Core;
using Momentum.DependencyManagement.Facades;

namespace Momentum.UI.InGame.Dialogs
{
	public class AutoHidingEndOfLevelDialog : AbstractEndOfLevelDialogDecorator
	{
		public AutoHidingEndOfLevelDialog(
			IEndOfLevelDialog dialog, 
			IGameLevelEventSystem gameLevelEventSystem
		) : base(dialog)
		{
			if(gameLevelEventSystem == null)
			{
				throw new ArgumentNullException("gameLevelEventSystem");
			}
			gameLevelEventSystem.LevelStarting += this.OnGameLevelStarting;
		}

		public AutoHidingEndOfLevelDialog(
			IEndOfLevelDialog dialog, 
			IGameplayEvents gameplayEvents
		) : base(dialog)
		{
			if(gameplayEvents == null)
			{
				throw new ArgumentNullException("gameplayEvents");
			}
			gameplayEvents.LevelStarting += this.OnGameLevelStarting;
		}

		private void OnGameLevelStarting(object sender, LevelStartingEventArgs args)
		{
			this.Visible = false;
		}
	}
}

