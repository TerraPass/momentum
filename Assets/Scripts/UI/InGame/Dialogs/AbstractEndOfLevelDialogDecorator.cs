using System;

namespace Momentum.UI.InGame.Dialogs
{
	public abstract class AbstractEndOfLevelDialogDecorator : IEndOfLevelDialog
	{
		private readonly IEndOfLevelDialog decoratedDialog;

		public AbstractEndOfLevelDialogDecorator(IEndOfLevelDialog decoratedDialog)
		{
			if(decoratedDialog == null)
			{
				throw new ArgumentNullException("decoratedDialog");
			}
			this.decoratedDialog = decoratedDialog;
		}

		protected IEndOfLevelDialog DecoratedDialog
		{
			get {
				return this.decoratedDialog;
			}
		}

		#region IEndOfLevelDialog implementation

		public virtual event EventHandler<QuitButtonPressedEventArgs> QuitButtonPressed
		{
			add {
				this.DecoratedDialog.QuitButtonPressed += value;
			}
			remove {
				this.DecoratedDialog.QuitButtonPressed -= value;
			}
		}

		public virtual event EventHandler<RestartButtonPressedEventArgs> RestartButtonPressed
		{
			add {
				this.DecoratedDialog.RestartButtonPressed += value;
			}
			remove {
				this.DecoratedDialog.RestartButtonPressed -= value;
			}
		}

		public virtual event EventHandler<NextLevelButtonPressedEventArgs> NextLevelButtonPressed
		{
			add {
				this.DecoratedDialog.NextLevelButtonPressed += value;
			}
			remove {
				this.DecoratedDialog.NextLevelButtonPressed -= value;
			}
		}

		public virtual float SecondsToComplete {
			get {
				return this.DecoratedDialog.SecondsToComplete;
			}
			set {
				this.DecoratedDialog.SecondsToComplete = value;
			}
		}

		public virtual float RealTimeSecondsToComplete {
			get {
				return this.DecoratedDialog.RealTimeSecondsToComplete;
			}
			set {
				this.DecoratedDialog.RealTimeSecondsToComplete = value;
			}
		}

		public virtual bool NextLevelButtonEnabled
		{
			get {
				return this.DecoratedDialog.NextLevelButtonEnabled;
			}
			set {
				this.DecoratedDialog.NextLevelButtonEnabled = value;
			}
		}

		#endregion

		#region IHideable implementation

		public virtual bool Visible {
			get {
				return this.DecoratedDialog.Visible;
			}
			set {
				this.DecoratedDialog.Visible = value;
			}
		}

		#endregion
	}
}

