using UnityEngine;
using System;

using Momentum.UI.Common;

namespace Momentum.UI.InGame.Dialogs
{
	public interface IEndOfLevelDialog : IHideable
	{
		event EventHandler<QuitButtonPressedEventArgs> QuitButtonPressed;
		event EventHandler<RestartButtonPressedEventArgs> RestartButtonPressed;
		event EventHandler<NextLevelButtonPressedEventArgs> NextLevelButtonPressed;

		float SecondsToComplete {get;set;}
		float RealTimeSecondsToComplete {get;set;}

		// TODO: Refactor using enum and make all buttons disableable.
		bool NextLevelButtonEnabled {get;set;}
	}

	public static class IEndOfLevelDialogExtensions
	{
		public static void Show(this IEndOfLevelDialog popup, float secondsToComplete, float realTimeSecondsToComplete)
		{
			popup.SecondsToComplete = secondsToComplete;
			popup.RealTimeSecondsToComplete = realTimeSecondsToComplete;
			popup.Show();
		}

		public static void EnableNextLevelButton(this IEndOfLevelDialog dialog)
		{
			dialog.NextLevelButtonEnabled = true;
		}

		public static void DisableNextLevelButton(this IEndOfLevelDialog dialog)
		{
			dialog.NextLevelButtonEnabled = false;
		}
	}
}
