using UnityEngine;
using System;

namespace Momentum.UI.InGame.Dialogs
{
	public abstract class AbstractEndOfLevelDialogComponent : MonoBehaviour, IEndOfLevelDialog
	{
		#region IEndOfLevelPopup implementation

		public abstract event EventHandler<QuitButtonPressedEventArgs> QuitButtonPressed;
		public abstract event EventHandler<RestartButtonPressedEventArgs> RestartButtonPressed;
		public abstract event EventHandler<NextLevelButtonPressedEventArgs> NextLevelButtonPressed;

		public abstract float SecondsToComplete {get;set;}
		public abstract float RealTimeSecondsToComplete {get;set;}

		public abstract bool NextLevelButtonEnabled {get;set;}

		#endregion

		#region IHideable implementation

		public abstract bool Visible {get;set;}

		#endregion


	}
}
