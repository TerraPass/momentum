using UnityEngine;
using System;

using Momentum.Gameplay.Core;
using Momentum.DependencyManagement.Facades;

namespace Momentum.UI.InGame.Dialogs
{
	public class AutoShowingEndOfLevelDialog : AbstractEndOfLevelDialogDecorator
	{
		public AutoShowingEndOfLevelDialog(
			IEndOfLevelDialog dialog, 
			IGameLevelEventSystem gameLevelEventSystem
		) : base(dialog)
		{
			if(gameLevelEventSystem == null)
			{
				throw new ArgumentNullException("gameLevelEventSystem");
			}
			gameLevelEventSystem.LevelCompleted += this.OnGameLevelCompleted;
		}

		public AutoShowingEndOfLevelDialog(
			IEndOfLevelDialog dialog, 
			IGameplayEvents gameplayEvents
		) : base(dialog)
		{
			if(gameplayEvents == null)
			{
				throw new ArgumentNullException("gameplayEvents");
			}
			gameplayEvents.LevelCompleted += this.OnGameLevelCompleted;
		}

		private void OnGameLevelCompleted(object sender, LevelCompletedEventArgs args)
		{
			this.SecondsToComplete = args.SecondsToComplete;
			this.RealTimeSecondsToComplete = args.RealTimeSecondsToComplete;
			this.Visible = true;
		}
	}
}

