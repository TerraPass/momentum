using System;
using Terrapass.Debug;

using Momentum.Common.Commands;
using Momentum.DependencyManagement.Facades;

namespace Momentum.UI.InGame.Dialogs
{
	public class CommandInvokingEndOfLevelDialog : AbstractEndOfLevelDialogDecorator
	{
		private readonly IEndOfLevelCommands endOfLevelCommands;

		public CommandInvokingEndOfLevelDialog(
			IEndOfLevelDialog decoratedDialog,
			IEndOfLevelCommands endOfLevelCommands
		) : base(decoratedDialog)
		{
			if(endOfLevelCommands == null)
			{
				throw new ArgumentNullException("endOfLevelCommands");
			}
			this.endOfLevelCommands = endOfLevelCommands;

			this.DecoratedDialog.QuitButtonPressed += this.OnQuitButtonPressed;
			this.DecoratedDialog.RestartButtonPressed += this.OnRestartButtonPressed;
			this.DecoratedDialog.NextLevelButtonPressed += this.OnNextLevelButtonPressed;

			// Disable next level button, if no command for it is provided
			this.DecoratedDialog.NextLevelButtonEnabled = (endOfLevelCommands.NextLevelCommand != null);
		}

		private void OnQuitButtonPressed(object sender, QuitButtonPressedEventArgs args)
		{
			this.endOfLevelCommands.QuitCommand.Execute();
		}

		private void OnRestartButtonPressed(object sender, RestartButtonPressedEventArgs args)
		{
			this.endOfLevelCommands.RestartCommand.Execute();
		}

		private void OnNextLevelButtonPressed(object sender, NextLevelButtonPressedEventArgs args)
		{
			DebugUtils.Assert(
				endOfLevelCommands.NextLevelCommand != null,
				string.Format(
					"Attempting to invoke a null next level command; next level button must have been disabled in constructor of {0}",
					this.GetType().ToString()
				)
			);

			this.endOfLevelCommands.NextLevelCommand.Execute();
		}
	}
}

