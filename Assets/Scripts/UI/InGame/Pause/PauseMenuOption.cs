using System;

namespace Momentum.UI.InGame.Pause
{
	public enum PauseMenuOption
	{
		RESUME 		= 0,
		RESTART 	= 1,
		OPTIONS 	= 2,
		MAIN_MENU 	= 3,
		QUIT 		= 4
	}
}

