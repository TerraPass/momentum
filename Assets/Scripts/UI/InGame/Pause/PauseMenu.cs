using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

using Momentum.UI.Common.Menu;
using Momentum.UI.Common.Menu.UnityWorkaround;

namespace Momentum.UI.InGame.Pause
{
	public class PauseMenu : AbstractPauseMenuComponent
	{
		[Serializable]
		private class PauseMenuOptionToButtonMapping : OptionToButtonMapping<PauseMenuOption>
		{

		}

		[SerializeField]
		private Canvas menuCanvas;
		[SerializeField]
		private PauseMenuOptionToButtonMapping[] buttonMappings;

		private IMenu<PauseMenuOption> menuImpl;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			this.menuImpl = new UnityButtonBasedMenu<PauseMenuOption>(
				new MenuButtonsDictionaryBuilder<PauseMenuOption>(buttonMappings).Build(),
				this
			);
		}

		public override event EventHandler<MenuOptionSelectedEventArgs<PauseMenuOption>> MenuOptionSelected
		{
			add {
				this.menuImpl.MenuOptionSelected += value;
			}
			remove {
				this.menuImpl.MenuOptionSelected -= value;
			}
		}

		public override void SetOptionEnabled(PauseMenuOption option, bool value)
		{
			this.menuImpl.SetOptionEnabled(option, value);
		}

		public override bool Visible
		{
			get {
				return this.menuCanvas.gameObject.activeSelf;
			}
			set {
				this.menuCanvas.gameObject.SetActive(value);
			}
		}
	}
}

