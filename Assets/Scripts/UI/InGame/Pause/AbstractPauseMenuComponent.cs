using UnityEngine;
using System;

using Momentum.UI.Common.Menu;

namespace Momentum.UI.InGame.Pause
{
	public abstract class AbstractPauseMenuComponent : MonoBehaviour, IMenu<PauseMenuOption>
	{
		public abstract event EventHandler<MenuOptionSelectedEventArgs<PauseMenuOption>> MenuOptionSelected;

		public abstract void SetOptionEnabled (PauseMenuOption option, bool value);

		public abstract bool Visible {get;set;}
	}
}

