using UnityEngine;
using System;
using System.Collections.Generic;

using Momentum.Common.DependencyManagement;
using Momentum.Common.Commands;
using Momentum.Gameplay.TimeMechanic;
using Momentum.Gameplay.Core;
using Momentum.DependencyManagement.Facades;
using Momentum.UI.Common.MouseCursor;
using Momentum.UI.InGame.Pause;

namespace Momentum.UI.InGame
{
	public interface IUIDependencyContainer : IDependencyContainer
	{
		ITimeMechanism TimeMechanism {get;set;}
		IGameplayEvents GameplayEvents {get;set;}
		IEndOfLevelCommands EndOfLevelCommands {get;set;}
		IDictionary<PauseMenuOption, ICommand> PauseMenuCommands {get;set;}
		IMouseCursor MouseCursor {get;set;}
	}

	public static class IUIDependencyContainerExtensions
	{
		public static void PerformInjection(
			this IUIDependencyContainer container,
			ITimeMechanism timeMechanism,
			IGameplayEvents gameplayEvents,
			IEndOfLevelCommands endOfLevelCommands,
			IDictionary<PauseMenuOption, ICommand> pauseMenuCommands,
			IMouseCursor mouseCursor
		)
		{

			container.TimeMechanism = timeMechanism;
			container.GameplayEvents = gameplayEvents;
			container.EndOfLevelCommands = endOfLevelCommands;
			container.PauseMenuCommands = pauseMenuCommands;
			container.MouseCursor = mouseCursor;
			container.PerformInjection();
		}
	}
}

