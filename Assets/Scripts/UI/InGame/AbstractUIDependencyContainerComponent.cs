using UnityEngine;
using System;

namespace Momentum.UI.InGame
{
	public abstract class AbstractUIDependencyContainerComponent : MonoBehaviour, IUIDependencyContainer
	{
		#region IDependencyContainer implementation

		public abstract void PerformInjection();

		#endregion
		#region IUIDependencyContainer implementation

		public abstract Momentum.Gameplay.TimeMechanic.ITimeMechanism TimeMechanism {get;set;}

		public abstract Momentum.DependencyManagement.Facades.IGameplayEvents GameplayEvents {get;set;}

		public abstract Momentum.DependencyManagement.Facades.IEndOfLevelCommands EndOfLevelCommands {get;set;}

		public abstract System.Collections.Generic.IDictionary<Momentum.UI.InGame.Pause.PauseMenuOption, Momentum.Common.Commands.ICommand> PauseMenuCommands {get;set;}

		public abstract Momentum.UI.Common.MouseCursor.IMouseCursor MouseCursor {get;set;}

		#endregion
	}
}

