using UnityEngine;
using System;

namespace Momentum.UI.InGame.HUD
{
	public abstract class AbstractHUDTimerComponent : MonoBehaviour, IHUDTimer
	{
		#region IHUDTimer implementation

		public abstract float Seconds {get;set;}

		#endregion

		#region IHideable implementation

		public abstract bool Visible {get;set;}

		#endregion


	}
}

