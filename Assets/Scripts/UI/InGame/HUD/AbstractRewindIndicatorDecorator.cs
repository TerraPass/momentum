using UnityEngine;
using System;

namespace Momentum.UI.InGame.HUD
{
	public abstract class AbstractRewindIndicatorDecorator : IRewindIndicator
	{
		private IRewindIndicator decoratedIndicator;

		public AbstractRewindIndicatorDecorator(IRewindIndicator decoratedIndicator)
		{
			if(decoratedIndicator == null)
			{
				throw new ArgumentNullException(
					"decoratedIndicator"
				);
			}
			this.decoratedIndicator = decoratedIndicator;
		}

		protected IRewindIndicator DecoratedIndicator
		{
			get {
				return this.decoratedIndicator;
			}
		}

		#region IRewindIndicator implementation

		public virtual RewindIndicatorState Indication {
			get {
				return this.decoratedIndicator.Indication;
			}
			set {
				this.decoratedIndicator.Indication = value;
			}
		}

		#endregion

		#region IHideable implementation

		public virtual bool Visible {
			get {
				return this.decoratedIndicator.Visible;
			}
			set {
				this.decoratedIndicator.Visible = value;
			}
		}

		#endregion
	}
}

