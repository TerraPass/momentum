using UnityEngine;
using UnityEngine.UI;
using System;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

using Momentum.UI.Common;

namespace Momentum.UI.InGame.HUD
{
	[RequireComponent(typeof(Image))]
	public class RewindIndicator : AbstractRewindIndicatorComponent
	{
		[SerializeField]
		private Sprite playbackSprite;
		[SerializeField]
		private Sprite rewindSprite;
		[SerializeField]
		private Sprite rewindStopSprite;
		[SerializeField]
		private Sprite pauseSprite;
		[SerializeField]
		private Sprite gameoverSprite;

		private Image image;
		private RewindIndicatorState indication;

		void Start()
		{
			this.image = this.GetComponent<Image>();
			DebugUtils.Assert(
				this.image != null,
				String.Format(
					"{0} requires Image component to be present on the same GameObject",
					this.GetType().ToString()
				)
			);
			this.EnsureRequiredFieldsAreSetInEditor();
			this.Indication = RewindIndicatorState.PLAYBACK;
		}

		#region implemented abstract members of AbstractRewindIndicatorComponent

		public override RewindIndicatorState Indication {
			get {
				return this.indication;
			}
			set {
				this.SetIndication(value);
			}
		}

		public override bool Visible {
			get {
				if(this.image.IsDestroyed())
				{
					throw new UIElementAlreadyDestroyedException();
				}
				return this.image.gameObject.activeSelf;
			}
			set {
				if(this.image.IsDestroyed())
				{
					throw new UIElementAlreadyDestroyedException();
				}
				this.image.gameObject.SetActive(value);
			}
		}

		#endregion

		protected void SetIndication(RewindIndicatorState indication)
		{
			if(this.image.IsDestroyed())
			{
				throw new UIElementAlreadyDestroyedException();
			}

			switch (indication) {
			default:
				DebugUtils.Assert(
					false,
					String.Format(
						"{0} does not expect indicator state {1} on call to SetIndication()",
						this.GetType().ToString(),
						indication.ToString()
					)
				);
				Debug.LogErrorFormat(
					"{0} does not expect indicator state {1} on call to SetIndication()",
					this.GetType().ToString(),
					indication.ToString()
				);
				break;
			case RewindIndicatorState.PLAYBACK:
				this.image.sprite = this.playbackSprite;
				break;
			case RewindIndicatorState.REWIND:
				this.image.sprite = this.rewindSprite;
				break;
			case RewindIndicatorState.REWIND_STOP:
				this.image.sprite = this.rewindStopSprite;
				break;
			case RewindIndicatorState.PAUSE:
				this.image.sprite = this.pauseSprite;
				break;
			case RewindIndicatorState.GAMEOVER:
				this.image.sprite = this.gameoverSprite;
				break;
			}
			this.indication = indication;
		}
	}
}

