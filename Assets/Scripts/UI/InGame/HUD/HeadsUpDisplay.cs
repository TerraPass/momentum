using UnityEngine;
using System;

using Momentum.Gameplay.TimeMechanic.Measurement;
using Momentum.Gameplay.TimeMechanic;
using Momentum.DependencyManagement.Facades;

namespace Momentum.UI.InGame.HUD
{
	public class HeadsUpDisplay : IHeadsUpDisplay
	{
		private IRewindIndicator rewindIndicator;
		private IHUDTimer hudTimer;

		private bool visible;

		public HeadsUpDisplay(
			IRewindIndicator rewindIndicator,
			IHUDTimer hudTimer,
			ITimeMechanism timeMechanism,
			IGameplayEvents gameplayEvents
		)
		{
			this.rewindIndicator = new SelfUpdatingRewindIndicator(
				new AutoHideOnPlaybackRewindIndicator(
					rewindIndicator
				),
				gameplayEvents
			);
			this.hudTimer = new SelfUpdatingHUDTimer(
				hudTimer,
				timeMechanism.ReadOnlyTimer
			);

			this.visible = true;
		}

		#region IHideable implementation
		public bool Visible {
			get {
				return this.visible;
			}
			set {
				this.visible = value;
				this.rewindIndicator.Visible = this.visible;
				this.hudTimer.Visible = this.visible;
			}
		}
		#endregion
	}
}

