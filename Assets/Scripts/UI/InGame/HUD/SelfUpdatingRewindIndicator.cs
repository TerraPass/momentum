using UnityEngine;
using System;

using Momentum.Gameplay.TimeMechanic;
using Momentum.Gameplay.Core;
using Momentum.DependencyManagement.Facades;

using Momentum.UI.Common;

namespace Momentum.UI.InGame.HUD
{
	/// <summary>
	/// This decorator automatically updates its underlying indicator's state
	/// whenever a relevant gameplay event occurs.
	/// </summary>
	public class SelfUpdatingRewindIndicator : AbstractRewindIndicatorDecorator
	{
		/// <summary>
		/// This value is used to restore indicator to its previous indication
		/// after the game gets unpaused.
		/// </summary>
		private RewindIndicatorState prePauseIndication;

		public SelfUpdatingRewindIndicator(
			IRewindIndicator rewindIndicator,
			IGameplayEvents gameplayEvents
		) : base(rewindIndicator)
		{
			if(gameplayEvents == null)
			{
				throw new ArgumentNullException(
					"gameplayEvents"
				);
			}
			gameplayEvents.RewindStarted += this.OnTimeMechanismRewindStarted;
			gameplayEvents.RewindEnded += this.OnTimeMechanismRewindEnded;
			gameplayEvents.RewindPaused += this.OnTimeMechanismRewindPaused;
			gameplayEvents.LevelStarted += this.OnGameLevelStarted;
			gameplayEvents.LevelPaused += this.OnGameLevelPaused;
			gameplayEvents.LevelResumed += this.OnGameLevelResumed;
			gameplayEvents.PseudoGameoverOccurred += this.OnPseudoGameoverOccurred;

			this.prePauseIndication = RewindIndicatorState.PLAYBACK;
		}

		private void OnTimeMechanismRewindStarted(object sender, RewindStartedEventArgs args)
		{
			this.DecoratedIndicator.Indication = RewindIndicatorState.REWIND;
		}

		private void OnTimeMechanismRewindEnded(object sender, RewindEndedEventArgs args)
		{
			this.DecoratedIndicator.Indication = RewindIndicatorState.PLAYBACK;
		}

		private void OnTimeMechanismRewindPaused(object sender, RewindPausedEventArgs args)
		{
			this.DecoratedIndicator.Indication = RewindIndicatorState.REWIND_STOP;
		}

		private void OnGameLevelStarted(object sender, LevelStartedEventArgs args)
		{
			this.DecoratedIndicator.Indication = RewindIndicatorState.PLAYBACK;
			this.prePauseIndication = RewindIndicatorState.PLAYBACK;
		}

		private void OnGameLevelPaused(object sender, LevelPausedEventArgs args)
		{
			this.prePauseIndication = this.DecoratedIndicator.Indication;
			this.DecoratedIndicator.Indication = RewindIndicatorState.PAUSE;
		}

		private void OnGameLevelResumed(object sender, LevelResumedEventArgs args)
		{
			// Return the indicator to the state it was in before the game was paused
			this.DecoratedIndicator.Indication = this.prePauseIndication;
		}

		private void OnPseudoGameoverOccurred(object sender, PseudoGameoverOccurredEventArgs args)
		{
			try
			{
				this.DecoratedIndicator.Indication = RewindIndicatorState.GAMEOVER;
			}
			catch(UIElementAlreadyDestroyedException)
			{
				// RewindIndicator's underlying image has already been destroyed by Unity.
				// This occurs on exiting the play mode in Unity editor.
			}
		}
	}
}
