using UnityEngine;
using System;

namespace Momentum.UI.InGame.HUD
{
	public enum RewindIndicatorState
	{
		PLAYBACK 	= 1,
		REWIND 		= 2,
		REWIND_STOP = 3,
		PAUSE		= 4,
		GAMEOVER	= 5
	}
}

