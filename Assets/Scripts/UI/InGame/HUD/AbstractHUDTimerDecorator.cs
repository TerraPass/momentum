using UnityEngine;
using System;

namespace Momentum.UI.InGame.HUD
{
	public abstract class AbstractHUDTimerDecorator : IHUDTimer
	{
		private IHUDTimer decoratedTimer;

		public AbstractHUDTimerDecorator(IHUDTimer decoratedTimer)
		{
			if(decoratedTimer == null)
			{
				throw new ArgumentNullException("decoratedTimer");
			}
			this.decoratedTimer = decoratedTimer;
		}

		protected IHUDTimer DecoratedTimer
		{
			get {
				return this.decoratedTimer;
			}
		}

		#region IHUDTimer implementation

		public virtual float Seconds {
			get {
				return this.DecoratedTimer.Seconds;
			}
			set {
				this.DecoratedTimer.Seconds = value;
			}
		}

		#endregion

		#region IHideable implementation

		public virtual bool Visible {
			get {
				return this.DecoratedTimer.Visible;
			}
			set {
				this.DecoratedTimer.Visible = value;
			}
		}

		#endregion
	}
}

