using UnityEngine;
using System;

namespace Momentum.UI.InGame.HUD
{
	public interface IRewindIndicator : Common.IHideable
	{
		RewindIndicatorState Indication {get;set;}
	}
}

