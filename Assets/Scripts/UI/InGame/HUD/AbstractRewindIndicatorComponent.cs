using UnityEngine;
using System;

namespace Momentum.UI.InGame.HUD
{
	public abstract class AbstractRewindIndicatorComponent : MonoBehaviour, IRewindIndicator
	{
		#region IRewindIndicator implementation
		public abstract RewindIndicatorState Indication {get;set;}
		public abstract bool Visible {get;set;}
		#endregion
	}
}

