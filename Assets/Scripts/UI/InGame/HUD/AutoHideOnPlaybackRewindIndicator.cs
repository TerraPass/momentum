using UnityEngine;
using System;
using Momentum.Utils.Time;

using Momentum.UI.Common;

namespace Momentum.UI.InGame.HUD
{
	/// <summary>
	/// This decorator automatically hides its underlying indicator
	/// after a certain number of seconds have passed since it was put into playback state.
	/// </summary>
	public class AutoHideOnPlaybackRewindIndicator : AbstractRewindIndicatorDecorator
	{
		/// <summary>
		/// Default delay for hiding in seconds.
		/// </summary>
		public const float DEFAULT_HIDE_DELAY = 3.0f;

		private ICallbackAlarmClock alarmClock;

		/// <summary>
		/// Decorates rewind indicator for it to automatically be hidden on playback after a delay.
		/// </summary>
		/// <param name="rewindIndicator">Rewind indicator.</param>
		/// <param name="delay">Delay after state has been changed to playback in seconds.</param>
		public AutoHideOnPlaybackRewindIndicator(
			IRewindIndicator rewindIndicator,
			float delay = DEFAULT_HIDE_DELAY
		) : base(rewindIndicator)
		{
			this.alarmClock = new CallbackAlarmClock(
				this.HideIfPlayback,
				delay,
				false
			);
		}

		public override RewindIndicatorState Indication {
			get {
				return base.Indication;
			}
			set {
				base.Indication = value;
				if(base.Indication.Equals(RewindIndicatorState.PLAYBACK))
				{
					this.alarmClock.Reset(false);
				}
				else
				{
					this.Show();
				}
			}
		}

		private void HideIfPlayback(AlarmFiredEventArgs args)
		{
			try
			{
				if(this.Indication.Equals(RewindIndicatorState.PLAYBACK))
				{
					this.Hide();
				}
			}
			catch(UIElementAlreadyDestroyedException)
			{
				// If Unity has already destroyed DecoratedRewindIndicator's underlying GUI element,
				// stop the alarm clock, as we are not going to need it anymore.
				this.alarmClock.Reset(true);
			}
		}
	}
}

