using UnityEngine;
using System;

using Momentum.UI.Common;
using Momentum.Gameplay.TimeMechanic.Measurement;
using Momentum.Utils.Time;

namespace Momentum.UI.InGame.HUD
{
	/// <summary>
	/// This decorator automatically periodically updates its underlying timer's displayed time.
	/// </summary>
	public class SelfUpdatingHUDTimer : AbstractHUDTimerDecorator
	{
		/// <summary>
		/// Default time in seconds between updates.
		/// </summary>
		public const float DEFAULT_UPDATE_INTERVAL = 0.5f;

		private ILevelTimer levelTimer;

		private ICallbackAlarmClock alarmClock;

		public SelfUpdatingHUDTimer(
			IHUDTimer hudTimer, 
			ILevelTimer levelTimer, 
			float interval = DEFAULT_UPDATE_INTERVAL
		) : base(hudTimer)
		{
			if(levelTimer == null)
			{
				throw new ArgumentNullException("levelTimer");
			}
			this.levelTimer = levelTimer;
			this.alarmClock = new CallbackAlarmClock(
				this.UpdateDisplayedTime,
				interval,
				false
			);
			this.UpdateDisplayedTime(null);
		}

		private void UpdateDisplayedTime(AlarmFiredEventArgs args)
		{
			try 
			{
				this.DecoratedTimer.Seconds = this.levelTimer.ElapsedSeconds;
				this.alarmClock.Reset(false);
			}
			catch(UIElementAlreadyDestroyedException)
			{
				// If Unity has already destroyed DecoratedTimer's underlying GUI element,
				// stop the alarm clock, as we are not going to need it anymore.
				this.alarmClock.Reset(true);
			}
		}
	}
}
