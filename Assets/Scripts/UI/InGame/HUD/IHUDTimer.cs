using UnityEngine;
using System;

namespace Momentum.UI.InGame.HUD
{
	public interface IHUDTimer : Common.IHideable
	{
		float Seconds {get;set;}
	}
}

