using UnityEngine;
using UnityEngine.UI;
using System;
using Terrapass.Debug;
using Terrapass.Extensions.Unity;

using Momentum.UI.Common;
using Momentum.Utils.Time;

namespace Momentum.UI.InGame.HUD
{
	[RequireComponent(typeof(Text))]
	public class HUDTimer : AbstractHUDTimerComponent
	{
		[SerializeField]
		private float seconds;

		private Text text;
		private ITimeIntervalFormatter formatter;

		void Start()
		{
			this.text = this.GetComponent<Text>();
			DebugUtils.Assert(
				this.text != null,
				String.Format(
					"{0} requires Text component to be present on the same GameObject",
					this.GetType().ToString()
				)
			);
			this.EnsureRequiredFieldsAreSetInEditor();
			this.seconds = 0;
			this.formatter = new MinutesSecondsFormatter();
		}

		#region implemented abstract members of AbstractHUDTimerComponent
		public override float Seconds {
			get {
				return this.seconds;
			}
			set {
				if(this.text.IsDestroyed())
				{
					throw new UIElementAlreadyDestroyedException();
				}
				this.seconds = value;
				this.text.text = this.formatter.Format(this.seconds);
			}
		}
		public override bool Visible {
			get {
				return this.text.gameObject.activeSelf;
			}
			set {
				this.text.gameObject.SetActive(value);
			}
		}
		#endregion
	}
}

