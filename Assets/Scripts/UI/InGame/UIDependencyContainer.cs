using UnityEngine;
using System;
using System.Collections.Generic;
using Terrapass.Extensions.Unity;

using Momentum.Common.Commands;
using Momentum.Common.DependencyManagement;
using Momentum.Gameplay.TimeMechanic;
using Momentum.Gameplay.TimeMechanic.Measurement;
using Momentum.Gameplay.Core;
using Momentum.DependencyManagement.Facades;
using Momentum.UI.Common;
using Momentum.UI.Common.Menu;
using Momentum.UI.Common.MouseCursor;

namespace Momentum.UI.InGame
{
	using HUD;
	using Dialogs;
	using Pause;

	public class UIDependencyContainer : AbstractUIDependencyContainerComponent
	{
		[SerializeField]
		private AbstractRewindIndicatorComponent rewindIndicator;
		[SerializeField]
		private AbstractHUDTimerComponent hudTimer;
		[SerializeField]
		private AbstractEndOfLevelDialogComponent endOfLevelDialog;
		[SerializeField]
		private AbstractPauseMenuComponent pauseMenu;

		private ITimeMechanism timeMechanism;
		private IGameplayEvents gameplayEvents;
		private IEndOfLevelCommands endOfLevelCommands;
		private IDictionary<PauseMenuOption, ICommand> pauseMenuCommands;
		private IMouseCursor mouseCursor;

		private IHeadsUpDisplay hud;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();
		}

		#region implemented abstract members of AbstractUIDependencyContainer

		public override void PerformInjection ()
		{
			this.EnsureRequiredDependenciesAreProvided();

			this.hud = new HeadsUpDisplay(
				this.rewindIndicator,
				this.hudTimer,
				this.TimeMechanism,
				this.gameplayEvents
			);

			// Setup end of level dialog to invoke commands
			// and be auto-shown on level completion
			// and auto-hidden on level (re)start.
			new AutoHidingEndOfLevelDialog(
				new AutoShowingEndOfLevelDialog(
					new CommandInvokingEndOfLevelDialog(
						this.endOfLevelDialog,
						this.endOfLevelCommands
					),
					this.gameplayEvents
				),
				this.gameplayEvents
			);

			// TODO: See the decorator hell below?
			// It should be refactored into a single HideableDecoratorBuilder
			// to be used like this:
			// new HideableDecoratorBuilder(hideable, gameplayEvents)
			//	.HideOn(LEVEL_COMPLETED | LEVEL_PAUSED)
			//	.ShowOn(LEVEL_STARTED | LEVEL_RESUMED)
			//  .Build();

			// Setup pause menu to invoke commands
			// and be auto-shown on game being paused
			// and auto-hidden on game being resumed,
			// then hide it for now.
			new AutoHidingOnLevelStartedHideable(
				new AutoHidingOnLevelResumedHideable(
					new AutoShowingOnLevelPausedHideable(
						new CommandInvokingMenu<PauseMenuOption>(
							this.pauseMenu,
							this.PauseMenuCommands
						),
						this.gameplayEvents
					),
					this.gameplayEvents
				),
				this.gameplayEvents
			);

			// Setup mouse cursor to only be shown 
			// whenever pause menu or end of level dialog are displayed.
			new AutoShowingOnLevelCompletedHideable(
				new AutoHidingOnLevelStartedHideable(
					new AutoShowingOnLevelPausedHideable(
						new AutoHidingOnLevelResumedHideable(
							this.mouseCursor,
							this.gameplayEvents
						),
						this.gameplayEvents
					),
					this.gameplayEvents
				),
				this.gameplayEvents
			);
		}

		[Dependency]
		public override ITimeMechanism TimeMechanism 
		{
			get {
				return this.timeMechanism;
			}
			set {
				this.timeMechanism = value;
			}
		}

		[Dependency]
		public override IGameplayEvents GameplayEvents
		{
			get {
				return this.gameplayEvents;
			}
			set {
				this.gameplayEvents = value;
			}
		}

		[Dependency]
		public override IEndOfLevelCommands EndOfLevelCommands
		{
			get {
				return this.endOfLevelCommands;
			}
			set {
				this.endOfLevelCommands = value;
			}
		}

		[Dependency]
		public override IDictionary<PauseMenuOption, ICommand> PauseMenuCommands
		{
			get {
				return this.pauseMenuCommands;
			}
			set {
				this.pauseMenuCommands = value;
			}
		}

		[Dependency]
		public override IMouseCursor MouseCursor
		{
			get {
				return this.mouseCursor;
			}
			set {
				this.mouseCursor = value;
			}
		}

		#endregion

		protected IHeadsUpDisplay HUD
		{
			get {
				return this.hud;
			}
		}
	}
}

