using System;

namespace Momentum.UI.Common
{
	public abstract class AbstractHideableDecorator : IHideable
	{
		private readonly IHideable decoratedHideable;

		public AbstractHideableDecorator(IHideable decoratedHideable)
		{
			if(decoratedHideable == null)
			{
				throw new ArgumentNullException("decoratedHideable");
			}
			this.decoratedHideable = decoratedHideable;
		}

		protected IHideable DecoratedHideable
		{
			get {
				return this.decoratedHideable;
			}
		}

		#region IHideable implementation
		public virtual bool Visible
		{
			get {
				return this.DecoratedHideable.Visible;
			}
			set {
				this.DecoratedHideable.Visible = value;
			}
		}
		#endregion
	}
}

