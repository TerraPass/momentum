using System;

using Momentum.DependencyManagement.Facades;
using Momentum.Gameplay.Core;

namespace Momentum.UI.Common
{
	public class AutoShowingOnLevelPausedHideable : AbstractHideableDecorator
	{
		public AutoShowingOnLevelPausedHideable(
			IHideable hideable, 
			IGameLevelEventSystem gameLevelEventSystem
		) : base(hideable)
		{
			if(gameLevelEventSystem == null)
			{
				throw new ArgumentNullException("gameLevelEventSystem");
			}
			gameLevelEventSystem.LevelPaused += (sender, e) => this.Show();
		}

		public AutoShowingOnLevelPausedHideable(
			IHideable hideable,
			IGameplayEvents gameplayEvents
		) : base(hideable)
		{
			if(gameplayEvents == null)
			{
				throw new ArgumentNullException("gameplayEvents");
			}
			gameplayEvents.LevelPaused += (sender, e) => this.Show();
		}
	}
}

