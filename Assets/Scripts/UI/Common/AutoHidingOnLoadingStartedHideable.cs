using System;

using Momentum.DependencyManagement.Loading;

namespace Momentum.UI.Common
{
	public class AutoHidingOnLoadingStartedHideable : AbstractHideableDecorator
	{
		public AutoHidingOnLoadingStartedHideable(
			IHideable hideable,
			ILoadingStatusMonitor loadingStatusMonitor
		) : base(hideable)
		{
			if(loadingStatusMonitor == null)
			{
				throw new ArgumentNullException("loadingStatusMonitor");
			}
			loadingStatusMonitor.LoadingStarted += (sender, e) => this.Hide();
		}
	}
}

