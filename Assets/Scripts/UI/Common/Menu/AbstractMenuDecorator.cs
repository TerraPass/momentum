using System;

namespace Momentum.UI.Common.Menu
{
	public abstract class AbstractMenuDecorator<TOptionEnum> : IMenu<TOptionEnum>
		where TOptionEnum : struct
	{
		private readonly IMenu<TOptionEnum> decoratedMenu;

		public AbstractMenuDecorator(IMenu<TOptionEnum> menu)
		{
			if(menu == null)
			{
				throw new ArgumentNullException("menu");
			}
			this.decoratedMenu = menu;
		}

		protected IMenu<TOptionEnum> DecoratedMenu
		{
			get {
				return this.decoratedMenu;
			}
		}

		#region IMenu implementation

		public virtual event EventHandler<MenuOptionSelectedEventArgs<TOptionEnum>> MenuOptionSelected;

		public virtual void SetOptionEnabled (TOptionEnum option, bool value)
		{
			this.DecoratedMenu.SetOptionEnabled(option, value);
		}

		#endregion

		#region IHideable implementation

		public virtual bool Visible {
			get {
				return this.DecoratedMenu.Visible;
			}
			set {
				this.DecoratedMenu.Visible = value;
			}
		}

		#endregion
	}
}

