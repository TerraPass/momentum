using UnityEngine;
using UnityEngine.UI;
using System;

namespace Momentum.UI.Common.Menu.UnityWorkaround
{
	[Serializable]
	public class OptionToButtonMapping<TOptionEnum> : IOptionToButtonMapping<TOptionEnum>
		where TOptionEnum : struct
	{
		[SerializeField]
		private TOptionEnum option;
		[SerializeField]
		private Button button;
		
		public TOptionEnum Option
		{
			get {
				return this.option;
			}
		}
		
		public Button Button
		{
			get {
				return this.button;
			}
		}
	}
}

