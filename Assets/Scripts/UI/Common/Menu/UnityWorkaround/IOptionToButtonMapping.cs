using UnityEngine.UI;
using System;

namespace Momentum.UI.Common.Menu.UnityWorkaround
{
	public interface IOptionToButtonMapping<TOptionEnum>
		where TOptionEnum : struct
	{
		TOptionEnum Option {get;}
		Button Button {get;}
	}
}

