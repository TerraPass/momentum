using UnityEngine.UI;
using System;
using System.Collections.Generic;

namespace Momentum.UI.Common.Menu.UnityWorkaround
{
	public class MenuButtonsDictionaryBuilder<TOptionEnum> : IMenuButtonsDictionaryBuilder<TOptionEnum>
		where TOptionEnum : struct
	{
		private readonly IDictionary<TOptionEnum, Button> buttons;

		public MenuButtonsDictionaryBuilder()
		{
			this.buttons = new Dictionary<TOptionEnum, Button>();
		}

		public MenuButtonsDictionaryBuilder(IEnumerable<IOptionToButtonMapping<TOptionEnum>> mappings)
		{
			this.buttons = new Dictionary<TOptionEnum, Button>();

			foreach(var mapping in mappings)
			{
				if(this.buttons.ContainsKey(mapping.Option))
				{
					throw new ArgumentException(
						string.Format(
							"Got mappings array with more than one mapping for {1} menu option in constructor of {0}",
							this.GetType(),
							mapping.Option
						),
						"mappings"
					);
				}
				if(this.buttons.Values.Contains(mapping.Button))
				{
					throw new ArgumentException(
						string.Format(
							"Got mappings array with {1} button mapped to more than one menu option in constructor of {0}",
							this.GetType(),
							mapping.Button.name
						),
						"mappings"
					);
				}

				this.buttons[mapping.Option] = mapping.Button;
			}
		}

		#region IMenuButtonsDictionaryBuilder implementation
		public void SetButton(TOptionEnum option, Button button)
		{
			if(button == null)
			{
				throw new ArgumentNullException("button");
			}
			this.buttons[option] = button;
		}

		public IDictionary<TOptionEnum, Button> Build()
		{
			return new Dictionary<TOptionEnum, Button>(this.buttons);
		}
		#endregion
	}
}

