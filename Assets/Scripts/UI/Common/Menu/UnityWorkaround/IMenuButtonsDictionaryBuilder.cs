using UnityEngine.UI;
using System;
using System.Collections.Generic;

namespace Momentum.UI.Common.Menu.UnityWorkaround
{
	public interface IMenuButtonsDictionaryBuilder<TOptionEnum>
		where TOptionEnum : struct
	{
		void SetButton(TOptionEnum option, Button button);

		IDictionary<TOptionEnum, Button> Build();
	}
}

