using System;

using Momentum.UI.Common;

namespace Momentum.UI.Common.Menu
{
	public interface IMenu<TOptionEnum> : IHideable
		where TOptionEnum : struct
	{
		event EventHandler<MenuOptionSelectedEventArgs<TOptionEnum>> MenuOptionSelected;

		void SetOptionEnabled(TOptionEnum option, bool value);
	}

	public static class IMenuExtensions
	{
		public static void EnableOption<TOptionEnum>(this IMenu<TOptionEnum> menu, TOptionEnum option)
			where TOptionEnum : struct
		{
			menu.SetOptionEnabled(option, true);
		}

		public static void DisableOption<TOptionEnum>(this IMenu<TOptionEnum> menu, TOptionEnum option)
			where TOptionEnum : struct
		{
			menu.SetOptionEnabled(option, false);
		}
	}
}

