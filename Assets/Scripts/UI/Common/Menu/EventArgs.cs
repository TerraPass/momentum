using System;

namespace Momentum.UI.Common.Menu
{
	public abstract class MenuEventArgs<TOptionEnum> : EventArgs
		where TOptionEnum : struct
	{
		private readonly IMenu<TOptionEnum> menu;

		public MenuEventArgs(IMenu<TOptionEnum> menu)
		{
			if(menu == null)
			{
				throw new ArgumentNullException("menu");
			}
			this.menu = menu;
		}

		public IMenu<TOptionEnum> Menu
		{
			get {
				return this.menu;
			}
		}
	}

	public class MenuOptionSelectedEventArgs<TOptionEnum> : MenuEventArgs<TOptionEnum>
		where TOptionEnum : struct
	{
		private readonly TOptionEnum option;

		public MenuOptionSelectedEventArgs(IMenu<TOptionEnum> menu, TOptionEnum option)
			: base(menu)
		{
			this.option = option;
		}

		public TOptionEnum Option
		{
			get {
				return this.option;
			}
		}
	}
}

