using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using Terrapass.Extensions.NET;
using Terrapass.Debug;

using Momentum.Common.Commands;

namespace Momentum.UI.Common.Menu
{
	public class CommandInvokingMenu<TOptionEnum> : AbstractMenuDecorator<TOptionEnum>
		where TOptionEnum : struct
	{
		private readonly IDictionary<TOptionEnum, ICommand> commands;

		/// <summary>
		/// Creates an instance of CommandInvokingMenu decorator 
		/// around an IMenu passed as the first parameter.
		/// This decorator will execute commands, specified in the dictionary, 
		/// copied from the one passed as this constructor's second parameter, 
		/// when corresponding options get selected in the decorated pause menu.
		/// </summary>
		/// <param name="menu">Decorated menu.</param>
		/// <param name="commands">Commands for supported options as an option-to-command dictionary.</param>
		/// <param name="disableUnsupportedOptions">
		/// If set to <c>true</c>, options with no associated commands,
		/// provided in the dictionary passed as the previous parameter,
		/// will be disabled. Defaults to <c>true</c>.
		/// </param>
		public CommandInvokingMenu(
			IMenu<TOptionEnum> menu,
			IDictionary<TOptionEnum, ICommand> commands,
			bool disableUnsupportedOptions = true
		) : base(menu)
		{
			if(commands == null)
			{
				throw new ArgumentNullException("commands");
			}
			this.commands = new Dictionary<TOptionEnum, ICommand>(commands);
			this.commands.DeleteNullEntries();
			if(disableUnsupportedOptions)
			{
				this.DisableUnsupportedOptions();
			}

			// Listen for MenuOptionSelected events from decorated pause menu
			this.DecoratedMenu.MenuOptionSelected += this.OnMenuOptionSelected;
		}

		/// <summary>
		/// Disables all the options of the underlying IMenu,
		/// for which no value is present in commands dictionry.
		/// </summary>
		private void DisableUnsupportedOptions()
		{
			var allOptions = (IEnumerable<TOptionEnum>)Enum.GetValues(typeof(TOptionEnum));
			var unsupportedOptions = from option in allOptions
				where !this.commands.ContainsKey(option)
				select option;
			foreach(TOptionEnum option in unsupportedOptions)
			{
				this.DisableOption(option);
			}
		}

		private void OnMenuOptionSelected(object sender, MenuOptionSelectedEventArgs<TOptionEnum> args)
		{
			try
			{
				this.commands[args.Option].Execute();
			}
			catch(KeyNotFoundException)
			{
				// This means that a client has manually re-enabled a menu option, 
				// which has no command bound to it.

				// TODO: Use a logging strategy, instead of directly accessing Unity's log from here.
				Debug.LogWarningFormat(
					"{0} has no command bound to {1} option, which was selected in the menu it decorates",
					this.GetType(),
					args.Option
				);
			}
		}
	}
}

