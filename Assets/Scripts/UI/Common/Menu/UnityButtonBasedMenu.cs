using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;


namespace Momentum.UI.Common.Menu
{
	/// <summary>
	/// Implements a menu based on a dictionary, which maps enum values to Unity buttons.
	/// </summary>
	public class UnityButtonBasedMenu<TOptionEnum> : IMenu<TOptionEnum>
		where TOptionEnum : struct
	{
		private readonly IDictionary<TOptionEnum, Button> buttons;

		private readonly Action<bool> setVisibilityDelegate;
		private readonly Predicate<object> getVisibilityDelegate;

		/// <summary>
		/// Creates a UnityButtonBasedMenu from a dictionary of buttons.
		/// </summary>
		/// <param name="buttons">Dictionary, mapping enum values to Unity buttons.</param>
		/// <param name="setVisibilityDelegate">
		/// This delegate will be invoked to set Visible property of this menu.
		/// </param>
		/// <param name="getVisibilityDelegate">
		/// This delegate will be invoked to get Visible property of this menu.
		/// </param>
		public UnityButtonBasedMenu(
			IDictionary<TOptionEnum, Button> buttons,
			Action<bool> setVisibilityDelegate = null,
			Predicate<object> getVisibilityDelegate = null
		)
		{
			if(buttons == null)
			{
				throw new ArgumentNullException("buttons");
			}
			this.buttons = new Dictionary<TOptionEnum, Button>(buttons);
			this.setVisibilityDelegate = setVisibilityDelegate;
			this.getVisibilityDelegate = getVisibilityDelegate;

			this.BindEventToButtons();
		}

		/// <summary>
		/// Creates a UnityButtonBasedMenu from a dictionary of buttons.
		/// </summary>
		/// <param name="buttons">Dictionary, mapping enum values to Unity buttons.</param>
		/// <param name="delegateHideable">
		/// Accesses to this menu's Visible property will be delegated to this IHideable.
		/// </param>
		public UnityButtonBasedMenu(
			IDictionary<TOptionEnum, Button> buttons,
			IHideable delegateHideable
		) : this(
			buttons,
			(value) => delegateHideable.Visible = value,
			(_) => delegateHideable.Visible
		)
		{

		}

		/// <summary>
		/// Binds MenuOptionSelected event to each button.
		/// </summary>
		private void BindEventToButtons()
		{
			foreach(var entry in this.buttons)
			{
				// Copy enum value for it to be captured correctly in lambda
				TOptionEnum option = entry.Key;

				entry.Value.onClick.AddListener(
					() => this.MenuOptionSelected(
						this, 
						new MenuOptionSelectedEventArgs<TOptionEnum>(
							this,
							option
						)
					)
				);
			}
		}

		#region IMenu implementation

		public event EventHandler<MenuOptionSelectedEventArgs<TOptionEnum>> MenuOptionSelected = delegate {};

		public void SetOptionEnabled(TOptionEnum option, bool value)
		{
			try
			{
				this.buttons[option].interactable = value;
			}
			catch(KeyNotFoundException e)
			{
				throw new ArgumentException(
					string.Format(
						"{0} has no mapping set for {1} menu option",
						this.GetType(),
						option
					),
					"option",
					e
				);
			}
		}

		#endregion

		#region IHideable implementation

		public bool Visible
		{
			get {
				if(this.getVisibilityDelegate == null)
				{
					throw new InvalidOperationException(
						string.Format(
							"No delegate for getting visibility has been provided for this {0}",
							this.GetType()
						)
					);
				}
				return this.getVisibilityDelegate(null);
			}
			set {
				if(this.setVisibilityDelegate == null)
				{
					throw new InvalidOperationException(
						string.Format(
							"No delegate for setting visibility has been provided for this {0}",
							this.GetType()
						)
					);
				}
				this.setVisibilityDelegate(value);
			}
		}

		#endregion
	}
}

