using UnityEngine;
using System;

namespace Momentum.UI.Common.MouseCursor
{
	public class UnityMouseCursor : IMouseCursor
	{
		private static UnityMouseCursor instance;

		private UnityMouseCursor()
		{

		}

		public static UnityMouseCursor Instance
		{
			get {
				if(instance == null)
				{
					instance = new UnityMouseCursor();
				}
				return instance;
			}
		}

		#region IHideable implementation

		public bool Visible
		{
			get {
				return Cursor.visible;
			}
			set {
				Cursor.visible = value;
				// Lock cursor to the center of the screen,
				// when it is invisible to prevent it from
				// leaving the game window and to make sure
				// that it reappears at the center of the game window
				// when Visible is set to true.
				Cursor.lockState = Cursor.visible
					? CursorLockMode.None
					: CursorLockMode.Locked;
			}
		}

		#endregion
	}
}

