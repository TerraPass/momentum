using System;

using Momentum.DependencyManagement.Facades;
using Momentum.Gameplay.Core;

namespace Momentum.UI.Common
{
	public class AutoShowingOnLevelCompletedHideable : AbstractHideableDecorator
	{
		public AutoShowingOnLevelCompletedHideable(
			IHideable hideable, 
			IGameLevelEventSystem gameLevelEventSystem
		) : base(hideable)
		{
			if(gameLevelEventSystem == null)
			{
				throw new ArgumentNullException("gameLevelEventSystem");
			}
			gameLevelEventSystem.LevelCompleted += (sender, e) => this.Show();
		}
		
		public AutoShowingOnLevelCompletedHideable(
			IHideable hideable,
			IGameplayEvents gameplayEvents
		) : base(hideable)
		{
			if(gameplayEvents == null)
			{
				throw new ArgumentNullException("gameplayEvents");
			}
			gameplayEvents.LevelCompleted += (sender, e) => this.Show();
		}
	}
}

