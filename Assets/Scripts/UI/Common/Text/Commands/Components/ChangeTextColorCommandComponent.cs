using UnityEngine;
using UnityEngine.UI;
using System;
using Terrapass.Extensions.Unity;

using Momentum.Common.Commands;

namespace Momentum.UI.Common.Text.Commands.Components
{
	using Text = UnityEngine.UI.Text;

	public class ChangeTextColorCommandComponent : AbstractCommandComponent<ChangeTextColorCommand>
	{
		[SerializeField]
		private Text text;
		[SerializeField]
		private Color color;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();
			this.Command = new ChangeTextColorCommand(
				this.text,
				this.color
			);
		}
	}
}

