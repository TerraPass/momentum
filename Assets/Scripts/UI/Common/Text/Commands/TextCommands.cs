using UnityEngine;
using UnityEngine.UI;
using System;

using Momentum.Common.Commands;

namespace Momentum.UI.Common.Text.Commands
{
	using Text = UnityEngine.UI.Text;

	public class ChangeTextColorCommand : ICommand
	{
		private readonly Text text;
		private readonly Color color;

		public ChangeTextColorCommand(Text text, Color color)
		{
			if(text == null)
			{
				throw new ArgumentNullException("text");
			}
			this.text = text;
			this.color = color;
		}

		#region ICommand implementation

		public void Execute()
		{
			this.text.color = this.color;
		}

		#endregion
	}
}

