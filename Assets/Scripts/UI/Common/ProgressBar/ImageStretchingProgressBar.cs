using UnityEngine;
using UnityEngine.UI;
using System;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

using Momentum.Utils.Math;

namespace Momentum.UI.Common.ProgressBar
{
	public class ImageStretchingProgressBar : AbstractProgressBarComponent
	{
		private enum Anchor
		{
			LEFT 	= 0,
			CENTER 	= 1,
			RIGHT 	= 2
		}

		[SerializeField]
		private Image progressBar;
		[SerializeField]
		private RectTransform maxProgressBarTransform;
		[SerializeField]
		private Anchor anchor = Anchor.LEFT;

		private float progress = 0.0f;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			DebugUtils.Assert(
				this.BarRectTransform != null,
				string.Format(
					"{0} component expects RectTransform to be attached to its gameobject",
					this.GetType()
				)
			);

			this.ResetBarTransform(this.anchor);
			this.UpdateBarTransform(this.progress);
		}

		#region implemented abstract members of AbstractProgressBarComponent

		public override float Progress
		{
			get {
				return this.progress;
			}
			set {
				this.progress = MathUtils.TrimToRange(value, 0.0f, 1.0f);
				this.UpdateBarTransform(this.progress);
			}
		}

		public override bool Visible
		{
			get {
				return this.gameObject.activeSelf;
			}
			set {
				this.gameObject.SetActive(value);
			}
		}

		#endregion

		private RectTransform BarRectTransform
		{
			get {
				return this.progressBar.GetComponent<RectTransform>();
			}
		}

		private void ResetBarTransform(Anchor anchor)
		{
			this.BarRectTransform.localPosition = this.maxProgressBarTransform.localPosition;
			this.BarRectTransform.localRotation = this.maxProgressBarTransform.localRotation;
			this.BarRectTransform.localScale = this.maxProgressBarTransform.localScale;

			this.BarRectTransform.pivot = new Vector2(GetPivotXByAnchor(anchor), this.BarRectTransform.pivot.y);
		}

		private static float GetPivotXByAnchor(Anchor anchor)
		{
			switch(anchor)
			{
			default:
				DebugUtils.Assert(
					false,
					string.Format(
						"Unexpected anchor enum value {0}",
						anchor
					)
				);
				throw new InvalidOperationException();
			case Anchor.LEFT: return 0.0f;
			case Anchor.CENTER: return 0.5f;
			case Anchor.RIGHT: return 1.0f;
			}
		}

		private void UpdateBarTransform(float value)
		{
			this.BarRectTransform.localScale = new Vector3(
				this.maxProgressBarTransform.localScale.x * value,
				this.BarRectTransform.localScale.y,
				this.BarRectTransform.localScale.z
			);
		}
	}
}

