using UnityEngine;
using System;

namespace Momentum.UI.Common.ProgressBar
{
	public abstract class AbstractProgressBarComponent : MonoBehaviour, IProgressBar
	{
		#region IProgressBar implementation

		public abstract float Progress {get;set;}

		#endregion

		#region IHideable implementation

		public abstract bool Visible {get;set;}

		#endregion


	}
}

