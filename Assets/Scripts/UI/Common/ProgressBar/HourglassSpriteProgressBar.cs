using UnityEngine;
using UnityEngine.UI;
using System;
using System.Diagnostics;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

using Momentum.Utils.Math;

namespace Momentum.UI.Common.ProgressBar
{
	// TODO: The fill rates are currently linear.
	// Maybe it would make for a better visualization
	// if they were to change logarithmically?
	// (Sprite filling strategy may be introduced.)
	public class HourglassSpriteProgressBar : AbstractProgressBarComponent
	{
		[SerializeField]
		private Image background;
		[SerializeField]
		private Image upperSand;
		[SerializeField]
		private Image lowerSand;

		[SerializeField]
		private bool useLerp = true;
		[SerializeField]
		private float lerpAmount = 0.25f;

		private float desiredProgress = 0.0f;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();
			this.ValidateImageComponents();

			this.UpdateSandFill(false);
		}

		void Update()
		{
			// If useLerp is false, the update has been performed immediately
			// when the Progress property was changed
			if(this.useLerp)
			{
				this.UpdateSandFill(true);
			}
		}

		private void UpdateSandFill(bool useLerp)
		{
			float lowerFillAmount = this.desiredProgress;
			float upperFillAmount = 1 - this.desiredProgress;
			
			if(useLerp)
			{
				this.LowerSandImageFillAmount = Mathf.Lerp(
					this.LowerSandImageFillAmount,
					lowerFillAmount,
					this.lerpAmount
					);
				this.UpperSandImageFillAmount = Mathf.Lerp(
					this.UpperSandImageFillAmount,
					upperFillAmount,
					this.lerpAmount
					);
			}
			else
			{
				this.LowerSandImageFillAmount = lowerFillAmount;
				this.UpperSandImageFillAmount = upperFillAmount;
			}
		}

		#region implemented abstract members of AbstractProgressBarComponent

		public override float Progress
		{
			get {
				return this.desiredProgress;
			}
			set {
				var progressDecreased = value < this.desiredProgress;
				this.desiredProgress = MathUtils.TrimToRange(value, 0.0f, 1.0f);
				// If not using lerp or if the progress bar is being reset
				// to a lower value,
				// perform immediate update.
				if(!this.useLerp || progressDecreased)
				{
					this.UpdateSandFill(false);
				}
			}
		}

		public override bool Visible
		{
			get {
				return this.gameObject.activeSelf;
			}
			set {
				this.gameObject.SetActive(value);
			}
		}

		#endregion

		private float UpperSandImageFillAmount
		{
			get {
				return this.upperSand.fillAmount;
			}
			set {
				this.upperSand.fillAmount = value;
			}
		}

		private float LowerSandImageFillAmount
		{
			get {
				return this.lowerSand.fillAmount;
			}
			set {
				this.lowerSand.fillAmount = value;
			}
		}

		[Conditional("UNITY_EDITOR")]
		void ValidateImageComponents()
		{
			var errorMessageTemplate = "{0} component expects {1} image to have {2} image type set in editor";

			DebugUtils.Assert(
				upperSand.type == Image.Type.Filled,
				errorMessageTemplate,
				this.GetType(),
				"upperSand",
				Image.Type.Filled
			);

			DebugUtils.Assert(
				lowerSand.type == Image.Type.Filled,
				errorMessageTemplate,
				this.GetType(),
				"lowerSand",
				Image.Type.Filled
			);
		}
	}
}

