using System;

namespace Momentum.UI.Common.ProgressBar
{
	public interface IProgressBar : IHideable
	{
		float Progress {get;set;}
	}
}

