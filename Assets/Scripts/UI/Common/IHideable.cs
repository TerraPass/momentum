using UnityEngine;
using System;

namespace Momentum.UI.Common
{
	public interface IHideable
	{
		bool Visible {get;set;}
	}

	public static class IHideableExtensions
	{
		public static void Hide(this IHideable indicator)
		{
			indicator.Visible = false;
		}
		
		public static void Show(this IHideable indicator)
		{
			indicator.Visible = true;
		}
	}
}

