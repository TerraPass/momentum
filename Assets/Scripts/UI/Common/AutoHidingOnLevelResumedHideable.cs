using System;

using Momentum.DependencyManagement.Facades;
using Momentum.Gameplay.Core;

namespace Momentum.UI.Common
{
	public class AutoHidingOnLevelResumedHideable : AbstractHideableDecorator
	{
		public AutoHidingOnLevelResumedHideable(
			IHideable hideable, 
			IGameLevelEventSystem gameLevelEventSystem
		) : base(hideable)
		{
			if(gameLevelEventSystem == null)
			{
				throw new ArgumentNullException("gameLevelEventSystem");
			}
			gameLevelEventSystem.LevelResumed += (sender, e) => this.Hide();
		}

		public AutoHidingOnLevelResumedHideable(
			IHideable hideable,
			IGameplayEvents gameplayEvents
		) : base(hideable)
		{
			if(gameplayEvents == null)
			{
				throw new ArgumentNullException("gameplayEvents");
			}
			gameplayEvents.LevelResumed += (sender, e) => this.Hide();
		}
	}
}

