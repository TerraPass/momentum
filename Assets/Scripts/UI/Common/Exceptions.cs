using System;

namespace Momentum.UI.Common
{
	// TODO: Create a proper, serializable exception hierarchy, as guidelines dictate.
	// See Momentum.Gameplay.TimeMechanic.Recording namespace.
	public abstract class UIException : Exception
	{
		public UIException(string message, Exception cause)
			: base(message, cause)
		{

		}
	}

	public class UIElementAlreadyDestroyedException : UIException
	{
		private const string DEFAULT_MESSAGE = "Underlying GUI element has already been destroyed";

		public UIElementAlreadyDestroyedException(Exception cause = null)
			: this(DEFAULT_MESSAGE, cause)
		{

		}

		public UIElementAlreadyDestroyedException(string message, Exception cause = null)
			: base(message, cause)
		{

		}
	}
}

