using System;

using Momentum.DependencyManagement.Facades;
using Momentum.Gameplay.Core;

namespace Momentum.UI.Common
{
	public class AutoHidingOnLevelStartedHideable : AbstractHideableDecorator
	{
		public AutoHidingOnLevelStartedHideable(
			IHideable hideable, 
			IGameLevelEventSystem gameLevelEventSystem
		) : base(hideable)
		{
			if(gameLevelEventSystem == null)
			{
				throw new ArgumentNullException("gameLevelEventSystem");
			}
			gameLevelEventSystem.LevelStarted += (sender, e) => this.Hide();
		}

		public AutoHidingOnLevelStartedHideable(
			IHideable hideable,
			IGameplayEvents gameplayEvents
		) : base(hideable)
		{
			if(gameplayEvents == null)
			{
				throw new ArgumentNullException("gameplayEvents");
			}
			gameplayEvents.LevelStarted += (sender, e) => this.Hide();
		}
	}
}

