using UnityEngine;
using System;
using System.Collections.Generic;
using Terrapass.Extensions.Unity;

using Momentum.Common.Commands;

namespace Momentum.UI.Common.Buttons
{
	public class ButtonBinder : MonoBehaviour
	{
		[SerializeField]
		private AbstractButtonComponent button;

		[SerializeField]
		private List<AbstractCommandComponent> onButtonClickedCommands;
		[SerializeField]
		private List<AbstractCommandComponent> onButtonMouseOverCommands;
		[SerializeField]
		private List<AbstractCommandComponent> onButtonMouseOutCommands;
		[SerializeField]
		private List<AbstractCommandComponent> onButtonEnabledCommands;
		[SerializeField]
		private List<AbstractCommandComponent> onButtonDisabledCommmands;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();

			this.button.ButtonClicked += this.OnButtonClicked;
			this.button.ButtonMouseOver += this.OnButtonMouseOver;
			this.button.ButtonMouseOut += this.OnButtonMouseOut;
			this.button.ButtonEnabledStatusChanged += this.OnButtonEnabledStatusChanged;
		}

		private void OnButtonClicked(object sender, ButtonClickedEventArgs args)
		{
			foreach(var command in this.onButtonClickedCommands)
			{
				command.Execute();
			}
		}

		private void OnButtonMouseOver(object sender, ButtonMouseOverEventArgs args)
		{
			foreach(var command in this.onButtonMouseOverCommands)
			{
				command.Execute();
			}
		}

		private void OnButtonMouseOut(object sender, ButtonMouseOutEventArgs args)
		{
			foreach(var command in this.onButtonMouseOutCommands)
			{
				command.Execute();
			}
		}

		private void OnButtonEnabledStatusChanged(object sender, ButtonEnabledStatusChangedEventArgs args)
		{
			foreach(
				var command in (args.Enabled
			                		? this.onButtonEnabledCommands 
			                		: this.onButtonDisabledCommmands)
			)
			{
				command.Execute();
			}
		}
	}
}

