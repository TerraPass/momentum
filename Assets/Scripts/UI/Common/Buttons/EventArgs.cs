using System;

namespace Momentum.UI.Common.Buttons
{
	public abstract class ButtonEventArgs : EventArgs
	{
		private readonly IButton button;

		public ButtonEventArgs(IButton button)
		{
			if(button == null)
			{
				throw new ArgumentNullException("button");
			}
			this.button = button;
		}

		public IButton Button
		{
			get {
				return this.button;
			}
		}
	}

	public class ButtonClickedEventArgs : ButtonEventArgs
	{
		public ButtonClickedEventArgs(IButton button)
			: base(button)
		{}
	}

	public class ButtonMouseOverEventArgs : ButtonEventArgs
	{
		public ButtonMouseOverEventArgs(IButton button)
			: base(button)
		{}
	}

	public class ButtonMouseOutEventArgs : ButtonEventArgs
	{
		public ButtonMouseOutEventArgs(IButton button)
			: base(button)
		{}
	}

	public class ButtonEnabledStatusChangedEventArgs : ButtonEventArgs
	{
		private readonly bool enabled;

		public ButtonEnabledStatusChangedEventArgs(
			IButton button,
			bool enabled
		) : base(button)
		{
			this.enabled = enabled;
		}

		public bool Enabled
		{
			get {
				return this.enabled;
			}
		}
	}
}

