using System;

namespace Momentum.UI.Common.Buttons
{
	public interface IButton
	{
		event EventHandler<ButtonClickedEventArgs> ButtonClicked;
		event EventHandler<ButtonMouseOverEventArgs> ButtonMouseOver;
		event EventHandler<ButtonMouseOutEventArgs> ButtonMouseOut;
		event EventHandler<ButtonEnabledStatusChangedEventArgs> ButtonEnabledStatusChanged;

		bool Enabled {get;set;}

		// TODO: Make a wrapper for Unity's UI.Button, implementing interface.
		// Then create a binder for IButtons (AbstractButtonComponents), which would
		// invoke custom ICommands (AbstractCommandComponents), analagous to SwitchBinder.
	}
}

