using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using Terrapass.Extensions.Unity;
using Terrapass.Debug;

namespace Momentum.UI.Common.Buttons
{
	using Button = UnityEngine.UI.Button;

	[RequireComponent(typeof(Button))]
	public class UnityButtonWrapper
		: AbstractButtonComponent, IPointerEnterHandler, IPointerExitHandler
	{
		private Button button;

		private bool lastEnabled;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();
			
			this.button = this.GetComponent<Button>();
			DebugUtils.Assert(
				this.button != null,
				"{0} component requires a {1} component on the same game object",
				this.GetType(),
				typeof(Button)
			);
			
			this.button.onClick.AddListener(
				() => this.ButtonClicked(
					this,
					new ButtonClickedEventArgs(this)
				)
			);

			// Raise a ButtonEnabledStatusChanged event immediately
			// to notify subscribers of the button's initial state.
			// TODO: Reconsider this part.
			// If a sound needs to be played when the button gets disabled, for example,
			// it would be played immediately when the disabled button gets loaded.
			this.ButtonEnabledStatusChanged(
				this,
				new ButtonEnabledStatusChangedEventArgs(this, this.Enabled)
			);

			this.lastEnabled = this.Enabled;
		}

		void Update()
		{
			if(this.lastEnabled != this.Enabled)
			{
				this.ButtonEnabledStatusChanged(
					this,
					new ButtonEnabledStatusChangedEventArgs(this, this.Enabled)
				);
				this.lastEnabled = this.Enabled;
			}
		}

		public override event EventHandler<ButtonClickedEventArgs> ButtonClicked 		= delegate{};
		public override event EventHandler<ButtonMouseOverEventArgs> ButtonMouseOver 	= delegate{};
		public override event EventHandler<ButtonMouseOutEventArgs> ButtonMouseOut 		= delegate{};
		public override event EventHandler<ButtonEnabledStatusChangedEventArgs> ButtonEnabledStatusChanged = delegate{};
		
		public override bool Enabled
		{
			get {
				return this.button.interactable;
			}
			set {
				this.button.interactable = value;
			}
		}
		
		#region IPointerEnterHandler implementation
		
		public void OnPointerEnter (PointerEventData eventData)
		{
			this.ButtonMouseOver(
				this,
				new ButtonMouseOverEventArgs(this)
			);
		}
		
		#endregion
		
		#region IPointerExitHandler implementation
		
		public void OnPointerExit (PointerEventData eventData)
		{
			this.ButtonMouseOut(
				this,
				new ButtonMouseOutEventArgs(this)
			);
		}
		
		#endregion
	}
}

