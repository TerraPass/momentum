using UnityEngine;
using System;

namespace Momentum.UI.Common.Buttons
{
	public abstract class AbstractButtonComponent : MonoBehaviour, IButton
	{
		#region IButton implementation

		public abstract event EventHandler<ButtonClickedEventArgs> ButtonClicked;
		public abstract event EventHandler<ButtonMouseOverEventArgs> ButtonMouseOver;
		public abstract event EventHandler<ButtonMouseOutEventArgs> ButtonMouseOut;
		public abstract event EventHandler<ButtonEnabledStatusChangedEventArgs> ButtonEnabledStatusChanged;

		public abstract bool Enabled {get;set;}

		#endregion


	}
}

