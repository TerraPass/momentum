using System;

namespace Momentum.Level.Metadata
{
	public class ImmutableLevelId : ILevelId
	{
		private readonly string sceneName;

		public ImmutableLevelId(string sceneName)
		{
			if(sceneName == null)
			{
				throw new ArgumentNullException("sceneName");
			}
			this.sceneName = sceneName;
		}

		#region ILevelId implementation

		public string SceneName {
			get {
				return this.sceneName;
			}
		}

		#endregion
	}
}

