using System;

namespace Momentum.Level.Metadata
{
	/// <summary>
	/// Describes objects, capable of identifying a game level.
	/// </summary>
	public interface ILevelId
	{
		string SceneName {get;}
	}
}
