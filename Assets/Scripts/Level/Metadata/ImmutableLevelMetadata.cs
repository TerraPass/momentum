using UnityEngine;
using System;

namespace Momentum.Level.Metadata
{
	public struct ImmutableLevelMetadata : ILevelMetadata
	{
		private readonly ILevelId id;
		private readonly ILevelId nextLevelId;
		private readonly string name;

		public ImmutableLevelMetadata(ILevelId id, string name, ILevelId nextLevelId = null)
		{
			this.id = id;
			this.name = name;
			this.nextLevelId = nextLevelId;
		}

		#region ILevelMetadata implementation

		public ILevelId Id
		{
			get {
				return this.id;
			}
		}

		public ILevelId NextLevelId
		{
			get {
				return  this.nextLevelId;
			}
		}

		public string Name
		{
			get {
				return this.name;
			}
		}

		#endregion


	}
}

