using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

namespace Momentum.Level.Metadata
{
	public class GameLevelMetadataComponent : AbstractGameLevelMetadataComponent
	{
		[SerializeField]
		private string levelName;
		[SerializeField]
		private string nextLevelSceneName;

		void Start()
		{
			this.EnsureRequiredFieldsAreSetInEditor();
		}

		#region ILevelMetadata implementation

		public override ILevelId Id
		{
			get {
				// FIXME: Application class SHOULD NOT be accessed directly from here.
				// The name of the current scene should be injected.
				// However, as using scene names as level IDs is a temporary solution anyway,
				// this might become moot.
				return new ImmutableLevelId(Application.loadedLevelName);
			}
		}

		public override ILevelId NextLevelId
		{
			get {
				return this.nextLevelSceneName.Length != 0
					? new ImmutableLevelId(this.nextLevelSceneName)
					: null;
			}
		}

		public override string Name
		{
			get {
				return this.levelName;
			}
		}

		#endregion
	}
}

