using UnityEngine;
using System;
using Terrapass.Extensions.Unity;

namespace Momentum.Level.Metadata
{
	public abstract class AbstractGameLevelMetadataComponent : MonoBehaviour, ILevelMetadata
	{
		#region ILevelMetadata implementation

		public abstract ILevelId Id {get;}

		public abstract string Name {get;}

		public abstract ILevelId NextLevelId {get;}

		#endregion
	}
}

