using UnityEngine;
using System;

namespace Momentum.Level.Metadata
{
	public interface ILevelMetadata
	{
		ILevelId Id {get;}
		string Name {get;}
		ILevelId NextLevelId {get;}
	}
}

