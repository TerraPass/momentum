using System;
using System.Collections.Generic;
using Terrapass.Debug;

using Momentum.Main.SceneTransition;
using Momentum.Level.Metadata;

namespace Momentum.Level.Loaders
{
	public class ProofOfConceptLevelLoader : ILevelLoader
	{
		private const string PROOF_SCENE_NAME_PREFIX = "proof_";

		private readonly ISceneLoader sceneLoader;
		private readonly IEnumerable<ILevelMetadata> availableLevels;

		public ProofOfConceptLevelLoader(
			ISceneLoader sceneLoader,
			uint totalNumberOfScenes,
			uint nonLevelScenesNumber
		)
		{
			if(sceneLoader == null)
			{
				throw new ArgumentNullException("sceneLoader");
			}

			if(totalNumberOfScenes < nonLevelScenesNumber)
			{
				throw new ArgumentException(
					"nonLevelScenesNumber cannot be less than totalNumberOfScenes",
					"nonLevelScenesNumber"
				);
			}

			this.sceneLoader = sceneLoader;

			this.availableLevels = this.BuildLevelMetadataCollection(
				totalNumberOfScenes,
				nonLevelScenesNumber
			);
		}

		#region ILevelLoader implementation

		public void LoadLevel(ILevelId levelId)
		{
			this.sceneLoader.LoadScene(levelId.SceneName);
		}

		public IEnumerable<ILevelMetadata> AvailableLevels
		{
			get {
				return this.availableLevels;
			}
		}

		#endregion

		private IEnumerable<ILevelMetadata> BuildLevelMetadataCollection(
			uint totalNumberOfScenes,
			uint nonLevelScenesNumber
		)
		{
			DebugUtils.Assert(
				totalNumberOfScenes >= nonLevelScenesNumber,
				"totalNumberOfScenes must be greater than or equal to nonLevelScenesNumber"
			);

			var result = new List<ILevelMetadata>();
			uint maxi = (totalNumberOfScenes - nonLevelScenesNumber);
			for(uint i = 1; i <= maxi; i++)
			{
				result.Add(
					new ImmutableLevelMetadata(
						this.LevelIdByNumber(i),
						i.ToString(),
						i == maxi ? null : this.LevelIdByNumber(i + 1)
					)
				);
			}
			return result;
		}

		private ImmutableLevelId LevelIdByNumber(uint number)
		{
			return new ImmutableLevelId(PROOF_SCENE_NAME_PREFIX + number.ToString());
		}
	}
}

