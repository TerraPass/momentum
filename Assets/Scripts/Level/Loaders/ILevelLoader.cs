using System;
using System.Collections.Generic;

using Momentum.Level.Metadata;

namespace Momentum.Level.Loaders
{
	// TODO:???
	// SceneBasedLevelLoader
	// DataBasedLevelLoader (in the future)
	// NetworkBasedLevelLoader (in  the future)
	// ChainLevelLoader (chain of responsibility/composite/stack based level loader)
	public interface ILevelLoader
	{
		void LoadLevel(ILevelId levelId);

		IEnumerable<ILevelMetadata> AvailableLevels {get;}
	}
}

