using System;

namespace Momentum.Configuration
{
	using Internal;

	public class HardcodedConfiguration : IConfiguration
	{
		private static HardcodedConfiguration instance = null;

		private HardcodedConfiguration()
		{

		}

		public static HardcodedConfiguration Instance
		{
			get {
				if(instance == null)
				{
					instance = new HardcodedConfiguration();
				}
				return instance;
			}
		}

		#region IConfiguration implementation

		public IInternalSettings InternalSettings
		{
			get {
				return new ImmutableInternalSettings("main", 2);
			}
		}

		#endregion
	}
}

