using System;

namespace Momentum.Configuration.Internal
{
	// TODO: If the constructor becomes too cluttered,
	// refactor using builder pattern.
	public class ImmutableInternalSettings : IInternalSettings
	{
		private readonly string mainMenuSceneName;
		private readonly uint nonLevelScenesNumber;

		public ImmutableInternalSettings(
			string mainMenuSceneName,
			uint nonLevelScenesNumber
		)
		{
			if(mainMenuSceneName == null)
			{
				throw new ArgumentNullException("mainMenuSceneName");
			}
			if(mainMenuSceneName.Length == 0)
			{
				throw new ArgumentException(
					"mainMenuSceneName string cannot be empty",
					"mainMenuSceneName"
				);
			}
			this.mainMenuSceneName = mainMenuSceneName;
			this.nonLevelScenesNumber = nonLevelScenesNumber;
		}

		#region IInternalSettings implementation

		public string MainMenuSceneName
		{
			get {
				return this.mainMenuSceneName;
			}
		}

		public uint NonLevelScenesNumber
		{
			get {
				return this.nonLevelScenesNumber;
			}
		}

		#endregion
	}
}

