using System;

namespace Momentum.Configuration.Internal
{
	public interface IInternalSettings
	{
		/// <summary>
		/// Gets the name of the main menu scene.
		/// </summary>
		/// <value>The name of the main menu scene.</value>
		string MainMenuSceneName {get;}

		/// <summary>
		/// The number of scenes, which are not game levels.
		/// (This number includes init scene, main menu scene, loading scene etc.)
		/// </summary>
		/// <value>Number.</value>
		uint NonLevelScenesNumber {get;}
	}
}

